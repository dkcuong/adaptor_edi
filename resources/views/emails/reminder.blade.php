<h3>Seldat's {{$re_process_title}}Inbound EDI has just updated your file: </h3>
<p>{{ $file }}</p>


@if (isset($data['message']))

    @if($data['status_code'] == 200)
        {{--*/  $color = '#4cae4c' /*--}}
    @else
        {{--*/ $color = 'red' /*--}}
    @endif

    <div>
        <div style="color: {{$color}}">
            <b>
                @if (is_array($data['message']))
                    <?php show_array_message($data['message']) ?>
                @else
                    {{ $data['message'] }}
                @endif
            </b>
        </div>
        <br>
        @if($data['data'])
            <?php show_json($data['data']) ?>
        @endif
    </div>
    <hr>
@else
    @foreach ($data as $index => $item)

        @if($item['status_code'] == 200)
            {{--*/  $color = '#4cae4c' /*--}}
        @else
            {{--*/ $color = 'red' /*--}}
        @endif
        <div>
            <div style="color: {{$color}}">
                <b>
                    @if (is_array($item['message']))
                        <?php show_array_message($item['message']) ?>
                    @else
                        {{ $item['message'] }}
                    @endif
                </b>
            </div>
            <br>
            @if($item['data'])
                <u>Your Data Return:</u>
                <?php show_json($item['data']) ?>
            @endif
        </div>
        <hr>
    @endforeach
@endif


