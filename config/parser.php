<?php

return [

    'aliases' => [
        'text' => App\Parsers\TextParser\TextParser::class,
        'excel' => App\Parsers\ExcelParser\ExcelParser::class,
        'x12' => App\Parsers\X12Parser\X12Parser::class,
        'pdf' => App\Parsers\PdfParser\PdfParser::class,
        'xml' => App\Parsers\XmlParser\XmlParser::class,
    ]
];
