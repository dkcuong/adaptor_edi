<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbounds extends Model
{
    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ftpAccount()
    {
        return $this->belongsTo(FtpAccounts::class, 'ftp_account_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function inboundMapping()
    {
        return $this->hasOne(InboundMapping::class, 'inbound_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inboundGroup()
    {
        return $this->belongsTo(X12InboundGroups::class, 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function api()
    {
        return $this->belongsTo(Api::class, 'api_id');
    }

    public function dataFormat()
    {
        return $this->hasOne(DataFormat::class, 'id', 'data_format_id');
    }
}
