<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertDataAdminUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert([
            'id' => 1,
            'username' => 'admin',
            'email' => 'vuong.nguyen@seldatinc.com',
            'password' => bcrypt('admin'),
            'status' => 1,
        ]);

        DB::table('roles')->insert([
           [
               'name' => 'Administrator',
               'short_name' => 'admin',
               'description' => 'Client admin',
               'status' => 1,
           ],
           [
               'name' => 'User',
               'short_name' => 'user',
               'description' => 'Client User',
               'status' => 1,
           ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
