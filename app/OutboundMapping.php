<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutboundMapping extends Model
{
    /**
     * @var string
     */
    protected $table = 'outbound_mapping';
}
