<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class X12InboundTransactionLogs
 *
 * @package App\Modules\As2\Api\V1\Models
 *
 * @property  As2s   $as2
 * @property  Ftps   $ftpInbound
 * @property  string $short_name
 *
 * @method static where($arg1)
 */
class X12InboundTransactionLogs extends Model
{

    /**
     * @var array
     */
    protected $table = 'x12_inbound_transaction_logs';
    protected $fillable = ['process_key', 'tran_id', 'log_message', 'raw_data', 'header', 'payload', 'type', 'status'];
}
