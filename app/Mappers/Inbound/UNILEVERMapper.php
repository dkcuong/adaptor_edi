<?php

namespace App\Mappers\Inbound;

use App\Mappers\MapperBase;

class UNILEVERMapper extends MapperBase
{
    private $meta_fields = [
        'vendor' => 'Vendor #',
        'ship_to_address_2'    => 'Ship To Address 2',
        'inventory_site'    => 'Inventory Site',
        'routing_guide'     => 'Routing Guide',
        'ship_service'      => 'Ship Service',
        'shipping_account'  => 'Shipping Account #',
        'billing_type'      => 'Billing Type',
        'carrier_account_zip_code' => 'Carrier Account Zip Code'

    ];

    // pack_size : UnitVolumes.UnitVolume
    // quantity : Quantity.Value
    // whs_id : BasicDetails.BookingNo
    // vendor_id : Clients.Client

    private $item_detail = [
        'ordered_upc' => [
            'type' => 'only'
        ],
        'quantity' => [
            'type' => 'only'
        ],
        'pack_size' => [
            'type' => 'multi'
        ],
        'vendor_id' => [
            'type' => 'multi'
        ]
    ];

    private $element_has_child_loop_940 = [
        'Product','Client','UnitVolume','Booking'
    ];
    /**
     * @return $this
     */
    protected function process()
    {
        $data = $this->getData();

        // right column map : column in excel
        // data : text xml
        // parse array with name of left column and group with item send to wms2Api
        $data = $data['xml'];

        $result = [];
        switch ($this->getEdi()->name) {
            case '888' :
                $result = $this->map888($data);
                break;
            case '940' :
                $result = $this->map940($data);
                break;
            default :
                $result = [];
        }

        //$result = array_map([$this, 'map'], $data);
dd($result);
        $this->setResult($result);

        return $this;
    }

    protected function map940($data){

        //$data = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $data);
        $data = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
        $data = $this->xml2array($data, ['Product','Client','UnitVolume','Booking']);
        $data = $data['Bookings']['Booking'];

        $this->config['mapping'] = $this->restructureMappingConfig($this->config['mapping'], ['Bookings','Booking']);
        $conditions = $this->getMapCondition();

        $result = [];

        $tag_item = 'Products';
        $tag_item_child = 'Product';

        $item_key_output = 'items';

        $results = [];

        foreach ($data as $item) {
            // Set Info Meta
            $item_header = $item;
            unset($item_header[$tag_item]);
            $result['order_meta_full'] = $item_header;

            if (isset($item[$tag_item][$tag_item_child])) {
                // if there are loop tag xml
                $item_loop = $item[$tag_item][$tag_item_child];

                foreach ($item_loop as $k => $v) {
                    $result[$item_key_output][$k]['full_info'] = $v;
                }
            }

            // Set data mapper
            foreach ($conditions as $k => $v) {
                $temp = explode(".", $v);

                if ($temp[0] == $tag_item && $temp[1] == $tag_item_child) {
                    // in tag loop

                    unset($temp[0]);
                    unset($temp[1]);

                    $arrTemp = [];

                    foreach ($item[$tag_item][$tag_item_child] as $m => $n) {
                        $arrTemp[$m] = $this->getElementXml($n, $temp);
                    }

                    foreach ($arrTemp as $i => $j) {
                        $result[$item_key_output][$i][$k] = $j;
                    }
                } else {
                    // not in tag loop
                    $result[$k] = $this->getElementXml($item, $temp);
                }
            }

            $json = '
{
    "client_order_id": "CS162708264",
    "order_meta_full": {"ShipperReferenceNo":"0000001186734402"},
    "customer_order_id": "CS162708271",
    "vendor_id": 11298,
    "shipping_to": "Rosemarie Gallo",
    "shipping_address_street": "20 Juniper Lane",
    "shipping_city": "Blackwood",
    "shipping_state": "NJ",
    "shipping_postal_code": "8012",
    "additional_shipper_information": "TBD",
    "start_ship_date": "4/26/2019",
    "cancel_date": "5/3/2019",
    "carrier": "",
    "ordernotes": "No Comments",
    "ordered_upc": "612409785831",
    "case_pack": "1",
    "mstr": "",
    "inner": "1",
    "quantity": "1",
    "items": [
        {
            "quantity": "1",
            "case_pack": "8",
            "ordered_upc": "612409785831",
            "mstr": "",
            "inner": "1",
            "full_info":{"a":"b"}
        }
    ]
}
';
            //$result = json_decode($json, true);
            $results[] = $result;
        }
        return $results;
    }

    protected function map888($data) {
        $data = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
        $data = $this->xml2array($data, ['Product']);

        $data = $data['Products']['Product'];

        $this->config['mapping'] = $this->restructureMappingConfig($this->config['mapping'], ['Products','Product']);
        $conditions = $this->getMapCondition();

        $results = [];

        foreach ($data as $item) {
            // Set data mapper
            $result = [];
            foreach ($conditions as $k => $v) {
                $temp = explode(".", $v);
                $result[$k] = $this->getElementXml($item, $temp);
            }

            $results[] = $result;
        }

        return $results;
    }

    private function setDataLoop($conditions, $loop_items){
        $result = [];

        foreach ($loop_items as $loop_item){
            $tmp = [];
            foreach ($conditions as $target => $from) {
                if (!array_key_exists($target, $this->item_detail)) continue;

                $explode_from = explode('.', $from);
                if($this->item_detail[$target]['type'] == 'only'){
                    $tmp[$target] = $this->getElementXml($loop_item, $explode_from);
                }

                if($this->item_detail[$target]['type'] == 'multi'){
                    $child_loop_item = $loop_item[array_shift($explode_from)];

                    $tmp[$target] = [];

                    foreach ($child_loop_item[array_shift($explode_from)] as $key=>$child) {
                        $tmp[$target][] = $this->getElementXml($child, $explode_from);
                    }
                }
            }
            $result[] = $tmp;
        }
        return $result;
    }

    private function getElementXml ($data,$keys){
        if (empty($keys)) {
            return $data;
        }

        foreach($keys as $key){
            if(!isset($data[$key])){
                return null;
            }
            $data = $data[$key];
        }
        return $data;
    }

    public function restructureMappingConfig($condition, $unsetArray = []) {

        $condition = json_decode($condition, true);

        foreach ($condition as $k=>$v) {
            $temp = explode(".", $v);

            if (is_array($temp)) {
                foreach ($temp as $i=>$j) {
                    if (in_array($j, $unsetArray) && in_array($i, ["0","1"]))
                        unset($temp[$i]);
                }
                $condition[$k] = implode('.',$temp);
            }
        }
        $condition = json_encode($condition);
        return $condition;
    }

    public function xml2array($xml, $loop_array)
    {
        $arr = array();

        foreach ($xml->children() as $r)
        {
            if(count($r->children()) == 0)
            {
                if (strval($r) == '') {
                    $arr[$r->getName()] = [];
                } else {
                    $arr[$r->getName()] = strval($r);
                }
            }
            else
            {
                if (in_array($r->getName(),$loop_array)) {
                    $arr[$r->getName()][] = $this->xml2array($r, $loop_array);
                } else {
                    $arr[$r->getName()] = $this->xml2array($r, $loop_array);
                }
            }
        }
        return $arr;
    }
}
