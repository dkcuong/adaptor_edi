@include('emails.template')
<table border="1" cellspacing="0" style="border-collapse: collapse">
    <thead>
    <tr>
        <th>Ref#</th>
        <th>Receiving Number</th>
        {{--<th>LOT#</th>--}}
        <th>Status</th>
        <th>Status Type</th>
        <th>Message</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $item)
        <?php
        $item['status_code'] = isset($item['status_code']) ? $item['status_code'] : '';
        if ($item['status_code'] == '200') {
            $status = 'Completed';
            $statusType = 'CC';
            $message = 'Your Receiving has been created successfully';
            $color = '#4CAE4C';
        } elseif (isset($item['data']['re_process'])) {
            $status = 'Reprocess';
            $statusType = 'RP';
            $message = is_array($item['message']) ? implode('<br/>', $item['message']) :
                    $item['message'];
            $color = '#FF9900';
        } else {
            $status = 'Error';
            $statusType = 'ER';
            $message = is_array($item['message']) ? implode('<br/>', $item['message']) :
                    $item['message'];
            $color = 'red';
        }
        ?>
        <tr>
            <th style="padding:0 15px">{{$item['reference']}}</th>
            <th style="padding:0 15px">{{isset($item['data']['asn_hdr_num'])? $item['data']['asn_hdr_num']: ""}}</th>
{{--            <th style="padding:0 15px">{{$item['lot']}}</th>--}}
            <th style="padding:0 15px">{{$status}}</th>
            <th style="padding:0 15px">{{$statusType}}</th>
            <th style="color: {{$color}};padding:0 15px">{{$message}}</th>
            <th></th>
        </tr>
    @endforeach
    </tbody>
</table>