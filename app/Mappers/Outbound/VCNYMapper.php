<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/03/2016
 * Time: 09:22
 */

namespace App\Mappers\Outbound;


use App\Mappers\MapperBase;

use App\Mappers\SimpleXMLElementExtend;
use DOMDocument;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class VCNYMapper extends MapperBase
{
    const AI_CODE = '00';
    const CUSTOMER_CODE = '73573280';

    /**
     * Mapper data for Concept One customer
     * Edi945
     *
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map945($items, $conditions)
    {
        $results[] = $this->parseTree($items);

        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parseTree($tranData)
    {
        $tran1 = ['transaction_code' => '945'];
        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];
        $additional = isset($order_meta['additional']) ? $order_meta['additional'] : [];
        $ship_to = isset($order_meta['ship_to']) ? $order_meta['ship_to'] : '';
        $ship_from = isset($order_meta['ship_from']) ? $order_meta['ship_from'] : '';
        $bill_to = isset($order_meta['bill_to']) ? $order_meta['bill_to'] : [];
        $bol_to = isset($order_meta['bol_to']) ? $order_meta['bol_to'] : [];
        $third_party = isset($order_meta['3rd_party']) ? $order_meta['3rd_party'] : [];

        //W06*F*DN17022700999*20170308*07357320000059025**BA4K8KR*SN17022700511*21061~
        $tran1[] = [
            'segment'        => 'W06',
            'report_code'    => 'F',
            'client_order'   => $tranData['client_pick_ticket'],
            'date'           => (isset($tranData['order_date']) && !empty($tranData['order_date'])) ? date('Ymd', strtotime($tranData['order_date'])) : '',
            'shipment_id'    => $tranData['bolnumber'],
            'agent_id'       => '',
            'purchase_order' => $tranData['customerordernumber'],
            'clientordernumber' => substr($tranData['pickid'], 0, 6),
            'scanordernumber' => $tranData['clientordernumber'],
        ];

        if (! empty($bill_to['name'])) {
            //N1*ST*BED BATH & BEYOND #0655*92*0655~
            $tran1[] = [
                'segment'      => 'N1',
                'entity_code'  => 'BT',
                'name'         => $bill_to['name'],
                'id_qualifier' => $bill_to['qualifier'],
                'id'           => $bill_to['id'],
            ];
            //N3*3717 BAY LAKE TRAIL~
            $tran1[] = [
                'segment'      => 'N3',
                'name '        => $bill_to['address'],
                'address'      => '',
            ];
            //N4*NORTH LAS VEGAS*NV*89030*US~
            $tran1[] = [
                'segment'           => 'N4',
                'name'              => substr($bill_to['city'], 0, 30),
                'province_code'     => substr($bill_to['state'], 0, 2),
                'postal_code'       => substr($bill_to['code'], 0, 15),
                'country_code'       => substr($bill_to['country'], 0, 3),
            ];
        }

        //Ship To

        //N1*ST*BED BATH & BEYOND #0655*92*0655~
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'ST',
            'name'         => isset($ship_to['name']) ? $ship_to['name'] : $tranData['shiptoname'],
            'id_qualifier' => isset($ship_to['qualifier']) ? $ship_to['qualifier'] : 'US',
            'id'           => isset($ship_to['id']) ? $ship_to['id'] : '0000',
        ];
        //N3*3717 BAY LAKE TRAIL~
        $tran1[] = [
            'segment'      => 'N3',
            'name '        => isset($ship_to['address']) ? $ship_to['address'] : $tranData['shipping_address_street'],
        ];
        //N4*NORTH LAS VEGAS*NV*89030*US~
        $ship_to_city = isset($ship_to['city']) ? $ship_to['city'] : $tranData['shiptocity'];
        $ship_to_state = isset($ship_to['state']) ? $ship_to['state'] : $tranData['shiptocity'];
        $ship_to_code = isset($ship_to['code']) ? $ship_to['code'] : $tranData['shiptozip'];
        $ship_to_country = isset($ship_to['country']) ? $ship_to['country'] : $tranData['shiptocountry'];
        $tran1[] = [
            'segment'           => 'N4',
            'name'              => substr($ship_to_city, 0, 30),
            'province_code'     => substr($ship_to_state, 0, 2),
            'postal_code'       => substr($ship_to_code, 0, 15),
            'country_code'       => substr($ship_to_country, 0, 3),
        ];

        //$third_party
        if (! empty($third_party['name'])) {
            //N1*ST*BED BATH & BEYOND #0655*92*0655~
            //N1*ST*BED BATH & BEYOND #0655*92*0655~
            $tran1[] = [
                'segment'      => 'N1',
                'entity_code'  => 'TP',
                'name'         => $third_party['name'],
                'id_qualifier' => $third_party['qualifier'],
                'id'           => $third_party['id'],
            ];
            //N3*3717 BAY LAKE TRAIL~
            $tran1[] = [
                'segment'      => 'N3',
                'name '        => $third_party['address'],
                'address'      => '',
            ];
            //N4*NORTH LAS VEGAS*NV*89030*US~
            $tran1[] = [
                'segment'           => 'N4',
                'name'              => substr($third_party['city'], 0, 30),
                'province_code'     => substr($third_party['state'], 0, 2),
                'postal_code'       => substr($third_party['code'], 0, 15),
                'country_code'       => substr($third_party['country'], 0, 3),
            ];
        }

        if (! empty($bol_to['name'])) {
            //N1*ST*BED BATH & BEYOND #0655*92*0655~
            $tran1[] = [
                'segment'      => 'N1',
                'entity_code'  => 'OB',
                'name'         => $bol_to['name'],
                'id_qualifier' => $bol_to['qualifier'],
                'id'           => $bol_to['id'],
            ];
        }

        //Ship From
        //N1*SF*TEXTILES FROM EUROPE~
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'SF',
            'name'         => isset($ship_from['name']) ? $ship_from['name'] : 'VCNY Home',
        ];

        //N3*73 STATION ROAD~
        $tran1[] = [
            'segment'      => 'N3',
            'address'      => isset($ship_from['address']) ? $ship_from['address'] : '7676 Kimbel Street, Unit# 16',
        ];
        $ship_from_city = isset($ship_from['city']) ? $ship_from['city'] : 'Mississauga';
        $ship_from_state = isset($ship_from['state']) ? $ship_from['state'] : 'ON';
        $ship_from_code = isset($ship_from['code']) ? $ship_from['code'] : 'L5S 1J8';
        $ship_from_country = isset($ship_from['country']) ? $ship_from['country'] : 'CANADA';

        //N4*CRANBURY*NJ*08512~
        $tran1[] = [
            'segment'           => 'N4',
            'name'              => substr($ship_from_city, 0, 30),
            'province_code'     => substr($ship_from_state, 0, 2),
            'postal_code'       => substr($ship_from_code, 0, 15),
            'country_code'       => substr($ship_from_country, 0, 3),
        ];


        //

        //N9*WA*Fedex Home Delivery CO~
        if ( ! empty($additional['clear_text_clause'])) {
            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'CU',
                'ref_qualifier'      => $additional['clear_text_clause'],
            ];
        }

        $order_type = ! empty($additional['sales_allowance_number']) ? 'PP' : 'BK';
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'OT',
            'ref_qualifier'     => $order_type,
        ];

        if (! empty($additional['release_number'])) {
            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'RE',
                'ref_qualifier'      => $additional['release_number'],
            ];
        }
        if (! empty($tranData['pronumber'])) {
            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'CN',
                'ref_qualifier'      => $tranData['pronumber'],
            ];
        }
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'LD',
            'ref_qualifier'      => $tranData['scanordernumber'],
        ];

        $tran1[] = [
            'segment'        => 'G62',
            'date_qualifier' => '10',
            'date'           => date('Ymd', strtotime($tranData['start_ship_date'])),
        ];

        $tran1[] = [
            'segment'        => 'G62',
            'date_qualifier' => '01',
            'date'           => date('Ymd', strtotime($tranData['canceldate'])),
        ];

        $tran1[] = [
            'segment'               => 'W27',
            'transportation_method' => getArr($additional, 'transportation_method', 'U'),
            'carrier_code'          => getArr($additional, 'carrier_code', 'UPGN'),
            'routing'               => getArr($additional, 'routing', 'UPS GROUND UG'),
            'payment_method'        => getArr($additional, 'payment_method', 'TP'),
            'equipment_code'        => '',
            'equipment_init'        => '',
            'equipment_number'      => $tranData['pronumber']
        ];

        $total_carton = 1;
        foreach ($tranData['items'] as $item_id => $item) {
            $cartonData = $item['cartons'];
            $firstCarton = array_shift($cartonData);
            foreach ($item['cartons'] as $cart_id => $carton) {
                $product_meta = isset($carton['product_meta']) ? json_decode($carton['product_meta'], true) : [];
                //LX*7~
                $tran1[] = [
                    'segment' => 'Lx',
                ];
                //MAN*GM*00007357328004124800**CP*832150070055699~
                $tran1[] = [
                    'segment'     => 'MAN',
                    'qualifier1'   => 'GM',
                    'ref11'         => self::AI_CODE . generateUCC128('00'. self::CUSTOMER_CODE, $carton['cartonID']),
                    'ref12' => '',
                    'qualifier2'   => 'CP',
                    'ref21'         => $firstCarton['tracking_number'] ? $firstCarton['tracking_number'] : '',
                ];
                //W12*SH**1*1*EA*735732870705*VN*MMK-3CS-KING-CS-GV*************ZZ*MMK-3CS-KING-CS~

                $tran1[] = [
                    'segment'          => 'W12',
                    '01' => 'SH',
                    '02' => $item['piecesCount'],
                    '03' => $item['piecesCount'],
                    '04' => '',
                    '05' => isset($product_meta['qualifier']) ? $product_meta['qualifier'] : 'EA',
                    '06' => '',
                    '07' => 'UP',
                    '08' => isset($product_meta['cus_upc'])?$product_meta['cus_upc']:'',
                    '09' => '',
                    '10' => $carton['weight'],
                    '11' => 'FR',
                    '12' => 'L',
                    '13' => '',
                    '14' => '',
                    '15' => '',
                    '16' => '',
                    '17' => isset($product_meta['qualifier6']) ? $product_meta['qualifier6'] : '',
                    '18' => isset($product_meta['product_name']) ? $product_meta['product_name'] : '',
                    '19' => '',
                    '20' => '',
                    '21' => 'ZZ',
                    '22' => isset($product_meta['sku'])?$product_meta['sku']:''
                ];

                //G69*08*Boho Embellished 11-Piece Comforter Set~
                if (! empty($carton['product_description'])) {
                    $tran1[] = [
                        'segment'           => 'G69',
                        'entity_code'       => '08',
                        'description'       => $carton['product_description'],
                    ];
                }
                //G69*74*GREEN~
                $tran1[] = [
                    'segment'           => 'G69',
                    'entity_code'       => '74',
                    'description'       => $carton['color'],
                ];
                //G69*73*X25~
                $tran1[] = [
                    'segment'           => 'G69',
                    'entity_code'       => '73',
                    'description'       => isset($product_meta['g6973']) ? $product_meta['g6973'] : 1,
                ];
                //N9*LI*01~
                if (! empty($carton['product_line_num'])) {
                    $tran1[] = [
                        'segment'           => 'N9',
                        'entity_code'       => 'LI',
                        'ref_qualifier'     => (int) $carton['product_line_num'] == 1 ? 1: str_pad($carton['product_line_num'], 5,0,STR_PAD_LEFT),
                    ];
                }
                //N9*CL*GRY21~
                if (! empty($carton['color_ref'])) {
                    $tran1[] = [
                        'segment'           => 'N9',
                        'entity_code'       => 'CL',
                        'ref_qualifier'     => $carton['color_ref'],
                        'description'       => $carton['color_desc'],
                    ];
                }

                $tran1[] = [
                    'segment'           => 'N9',
                    'entity_code'       => 'OL',
                    'ref_qualifier'      => (int) ! empty($carton['product_load_id']) ? $carton['product_load_id'] : 1,
                ];

                if (! empty($carton['size_ref'])) {
                    //N9*SZ*XL~
                    $tran1[] = [
                        'segment'           => 'N9',
                        'entity_code'       => 'SZ',
                        'ref_qualifier'      => $carton['size_ref'],
                        'description'      => $carton['size_desc'],
                    ];
                }

            }
            $total_carton += $item['cartonCount'];
        }

        $tran1[] = [
            'segment'     => 'W03',
            'unit_number' => $tranData['total_pieces'],
            'weight'      => $tranData['total_weight'],
            'unit_code'   => 'LB',
            'volume'      => $tranData['total_volume'],
            'label_code' => 'CF',
            'label' => $total_carton,
            'label_type' => 'CT'

        ];

        return $tran1;
    }



}
