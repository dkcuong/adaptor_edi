<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateX12InboundGroupRevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('x12_inbound_group_revisions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender_code', 15);
            $table->string('receiver_code', 15);
            $table->bigInteger('date');
            $table->bigInteger('time');
            $table->bigInteger('control_number');
            $table->char('control_standard', 1);
            $table->smallInteger('control_version');
            $table->tinyInteger('status');
            $table->bigInteger('interchange_id');
            $table->bigInteger('inbound_interchange_id');
            $table->bigInteger('group_id');      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('x12_inbound_group_revisions');
    }
}
