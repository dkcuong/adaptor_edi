<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSuccessNumberInInboundAndOutboundTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `x12_inbound_transactions` 
            ADD `success_number` VARCHAR(50) NULL AFTER `message`;');
        DB::statement('ALTER TABLE `x12_outbound_transactions` 
            CHANGE `data` `success_number` VARCHAR(50) NULL DEFAULT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
