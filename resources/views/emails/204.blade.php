@include('emails.template')
<table border="1" cellspacing="0" style="border-collapse: collapse">
    <thead>
    <tr>
        <th>Job Order Number</th>
        <th>Status</th>
        <th>Message</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($data as $index => $item)
        <tr>
            <?php
            $item['data']['status_type'] = isset($item['data']['status_type']) ? $item['data']['status_type'] : '';
            if ($item['data']['status_type']) {
                logger('Missing Status Response');
            }
            $result = format_data_204($item['data'], $item['message'], $data);
            $color = format_color_204($item['data']['status_type']);
            ?>
            <th style="color: {{$color['job_ord_hdr_num']}}; font-weight:normal; padding:0 15px">
                {{$result['job_ord_hdr_num']}}</th>
            <th style="color: {{$color['status_type'] }}; font-weight:normal; padding:0 15px">{{$result['status']}}</th>
            <th style="color: {{$color['message']}}; font-weight:normal; padding:0 15px">{{$result['message']}}</th>
        </tr>
    @endforeach
    </tbody>
</table>
