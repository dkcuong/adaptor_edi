<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetForeignKeyForClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_addresses', function (Blueprint $table) {
            $table->unsignedBigInteger('client_id')->change();
            $table->unsignedBigInteger('address_id')->change();
            $table->unsignedBigInteger('location_id')->change();
            $table->unsignedBigInteger('type_id')->change();
        });
        
        Schema::table('client_addresses', function (Blueprint $table) {
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('type_id')->references('id')->on('address_types');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->unsignedBigInteger('client_id')->change();
            $table->unsignedBigInteger('type_id')->change();
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('type_id')->references('id')->on('contact_types');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
