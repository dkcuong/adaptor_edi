<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsOnOutboundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outbounds', function (Blueprint $table) {
            $table->char('transactional_code',4);
            $table->unsignedBigInteger('api_id');
            
            $table->dropColumn('decription');
            $table->dropColumn('fpt_account_id');
            $table->dropColumn('email');
            
            $table->text('description');
            $table->unsignedBigInteger('ftp_account_id');
            $table->string('emails', 255);
            
        });
        
        Schema::table('outbound_mapping', function (Blueprint $table) {
            $table->unsignedBigInteger('outbound_id');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outbounds', function (Blueprint $table) {
            //
        });
    }
}
