<?php
/**
 * Created by PhpStorm.
 * User: viet.le
 * Date: 8/7/2018
 * Time: 4:05 PM
 */

namespace App\Apis\Clients;

use App\Apis\ApiBase;
use App\Apis\ApiInterface;
use App\Modules\Edi\V1\Services\ServiceHelpers\ChildrenService;

class Edi204Api extends ApiBase implements ApiInterface
{

    /**
     * @var string
     */
    protected $api = 'items/v1/204';

    /**
     * Call Api items/v1/204
     * With data from client
     *
     * @return array
     */
    public function callApi()
    {
        $result = [];
        $vendor_id = $warehouse_code = $vendor_name = '';
        $data = $this->getConfig('data');

        $reProcess = $this->getConfig('re_process');
        $isX12 = $this->getConfig('isX12');

        if (! $isX12 && ! $reProcess) {
            $data = $this->groupBy($data, 'customer_order_id', 'items', [
                'sku', 'color', 'size', 'quantity', 'description', 'carton', 'qty', 'weight', 'desc', 'lot',
                'is_print_ucc_edi', 'product_load_id', 'segment_code',
                'case_pack', 'ordered_upc', 'mstr', 'inner'
            ]);
        }

        foreach ($data as $index => $input) {
            $response = $this->post($input);
            $result[$index] = json_decode($response->getBody(), true);
        }
        $results = [
            'data' => $data,
            'output' => $result,
        ];

        return $results;
    }


    /**
     * @param array $data
     *
     */
    private function fixDuplicateSKU(array &$data)
    {
        foreach ($data as $index => $transaction) {
            if (!isset($transaction['items'])) {
                return;
            }

            $fixItems = $this->fixItems($transaction['items']);
            $data[ $index ]['items'] = $fixItems;
        }
    }

    /**
     * @param array $items
     *
     * @return array
     */
    private function fixItems(array $items)
    {
        $fixItems = [];

        $children = new ChildrenService();

        foreach ($items as $index => $item) {
            $condition = ['sku' => $item['sku']];
            $config = ['getIndex' => true];

            $existItem = $children->subArrayGet($fixItems, $condition, $config);

            if ($existItem) {
                $fixItems[ $existItem[0] ]['quantity'] += $item['quantity'];
            } else {
                $fixItems[] = $item;
            }
        }

        return $fixItems;
    }
}
