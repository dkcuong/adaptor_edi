<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inbound()
    {
        return $this->hasMany(Inbounds::class, 'api_id');
    }
}
