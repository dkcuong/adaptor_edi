<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataFormatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_formats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('extension', 20);
            $table->string('description');
            $table->tinyInteger('status');
            $table->string('component_separator', 20);
            $table->string('element_separator', 20);
            $table->string('segment_terminator', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_formats');
    }
}
