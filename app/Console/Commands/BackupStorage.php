<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Mockery\CountValidator\Exception;

class BackupStorage extends Command
{
    /**
     * @var \App\Utils\BackupStorage
     */
    public $service;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:storage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'backup Storage';

    /**
     * backupStorage constructor.
     * @param \App\Utils\BackupStorage $backupStorage
     */
    public function __construct(\App\Utils\BackupStorage $backupStorage)
    {
        $this->service = $backupStorage;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            logger('Backup Storage is running');
            $this->service->process();
        } catch (\Exception $e) {
            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            Log::error($e->getMessage(), ['info' => $errorInfo]);
        }
    }
}
