<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddX12OutboundTransactionFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('x12_outbound_transaction_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('outbound_group_id');
            $table->string('file_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('x12_outbound_transaction_files');
    }
}
