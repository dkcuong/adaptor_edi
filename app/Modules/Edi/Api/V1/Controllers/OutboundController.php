<?php
/**
 * Created by PhpStorm.
 * User: TuLe
 * Date: 20/07/2016
 * Time: 11:22
 */
namespace App\Modules\Edi\Api\V1\Controllers;

use App\Clients;
use App\Http\Requests\Request;
use app\Modules\Edi\Api\V1\Requests\OutboundRequests;
use App\Modules\Edi\V1\Models\ClientModel;
use App\Modules\Edi\V1\Services\OutboundService;
use App\Http\Controllers\Controller;
use App\Modules\Edi\V1\Models\OutboundModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class OutboundController extends Controller
{

    /**
     *
     */
    const API_APP = 'edi';

    /**
     * @var OutboundModel
     */
    protected $outboundModel;

    /**
     * @var ClientModel
     */
    protected $clientModel;

    /**
     * @var OutboundService
     */
    protected $outboundSer;

    function __construct(
        OutboundModel $outboundModel,
        ClientModel $clientModel,
        OutboundService $outboundService
    )
    {
        $this->outboundModel = $outboundModel;
        $this->clientModel = $clientModel;
        $this->outboundSer = $outboundService;
    }

    public function updateOutput(\Illuminate\Http\Request $request)
    {
        $api_app = $request->get('api_app');
        if ($api_app != self::API_APP)
        {
            return false;
        }

        $cus_id = $request->get('cus_id');
        $whs_id = $request->get('whs_id');
        $transactional = $request->get('transactional_code');
        $data = $request->get('data');
        $successKey = $request->get('success_key');

        $response = $this->outboundSer->updateOutboundData($whs_id, $cus_id, $transactional, $data, $successKey);
        return $response;
    }
    public function getFileStorage($filePath, $fileName)
    {
        return Response::download(storage_path('app/dms-transactions/'). $filePath, $fileName);
    }

}