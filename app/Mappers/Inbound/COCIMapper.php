<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 22-Jan-16
 * Time: 9:33 AM
 */

namespace App\Mappers\Inbound;

use App\Mappers\MapperBase;

class COCIMapper extends MapperBase
{
    /**
     * Mapper data for Concept One customer
     * Edi888
     *
     * @param $item
     * @param $conditions
     * @return array
     */
    protected function map888($item, $conditions)
    {
        $result = [];

        foreach ($conditions as $target => $from) {
            if (isset($item[$from])) {
                $result[$target] = $item[$from];

                $item['Pack-Qty'] = empty($item['Pack-Qty']) ? null : intval($item['Pack-Qty']);
                $result['cube'] = $item['Cubic-Feet'] * $item['Pack-Qty'];
                $result['weight'] = $item['Weight'] * $item['Pack-Qty'];
            }
        }
        return $result;
    }

    /**
     * Mapper data for Concept One customer
     * Edi940
     * @param $item
     * @param $conditions
     * @return array
     */
    protected function map940($item, $conditions)
    {
        $result = [];

        foreach ($conditions as $target => $from) {

            if (isset($item[$from])) {
                $item['Qty Ordered'] = intval($item['Qty Ordered']);
                $result[$target] = $item[$from];
            }
        }

        return $result;
    }
}
