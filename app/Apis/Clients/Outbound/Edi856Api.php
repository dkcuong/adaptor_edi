<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 24-Jan-16
 * Time: 9:37 PM
 */

namespace App\Apis\Clients\Outbound;

use App\Apis\ApiBase;
use App\Apis\ApiInterface;

class Edi856Api extends ApiBase implements ApiInterface
{
    /**
     * @var string
     */
    protected $api = 'items/v1/outbound-856';

    /**
     * Call Api items/v1/945
     * With data from client
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function callApi()
    {
        $data = $this->get(['data' => $this->getConfig('data')]);

        return $data;

        // TODO: Implement callApi() method.
    }
}
