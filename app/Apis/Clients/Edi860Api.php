<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 24-Jan-16
 * Time: 9:37 PM
 */

namespace App\Apis\Clients;

use App\Apis\ApiBase;
use App\Apis\ApiInterface;

class Edi860Api extends ApiBase implements ApiInterface
{

    /**
     * @var string
     */
    protected $api = 'items/v1/860';

    /**
     * Call Api items/v1/940
     * With data from client
     *
     * @return array
     */
    public function callApi()
    {
        $result = [];
        $vendor_id = $warehouse_code = $vendor_name = '';
        $data = $this->getConfig('data');
        foreach ($data as $key => $val) {
            if (! isset($val['customer_order_id']) || ! $val['customer_order_id']) {
                unset($data[$key]);
            }
            if (isset($val['vendor_id'])) {
                if ($vendor_id && $vendor_id != $val['vendor_id']) {
                    logger('Too many vendorID in an imported file. The row: ' . $val['vendor_id']);
                    dd('Too many vendorID in an imported file.' . $val['vendor_id'], __FILE__ . ' ' . __CLASS__ . ' '. __LINE__);
                } else {
                    $vendor_id = $val['vendor_id'];
                }
            }

            if (isset($val['warehouse_code'])) {
                if ($warehouse_code && $warehouse_code != $val['warehouse_code']) {
                    logger('Too many WarehouseShortName in an imported file. The row: ' . $val['vendor_id']);
                    dd('Too many WarehouseShortName in an imported file.' . $val['warehouse_code'], __FILE__ . ' ' . __CLASS__ . ' '. __LINE__);
                } else {
                    $warehouse_code = $val['warehouse_code'];
                }
            }

            if (isset($val['vendor_name'])) {
                if ($vendor_name && $vendor_name != $val['vendor_name']) {
                    logger('Too many VendorName in an imported file. The row: ' . $val['vendor_id']);
                    dd('Too many VendorName in an imported file.' . $val['vendor_name'], __FILE__ . ' ' . __CLASS__ . ' '. __LINE__);
                } else {
                    $vendor_name = $val['vendor_name'];
                }
            }
        }
        $reProcess = $this->getConfig('re_process');
        $isX12 = $this->getConfig('isX12');

        if (! $isX12 && ! $reProcess) {
            $data = $this->groupBy($data, 'customer_order_id', 'items', [
                'sku', 'color', 'size', 'quantity', 'description', 'carton', 'qty', 'weight', 'desc', 'lot','is_print_ucc_edi', 'product_load_id'
            ]);
        }

        //$this->fixDuplicateSKU($data);
        //dd($data, __CLASS__ .'-'. __FUNCTION__ .'-'. __LINE__);
        foreach ($data as $index => $input) {
            $response = $this->post($input);
            $result[$index] = json_decode($response->getBody(), true);
        }

        $results = [
            'data' => $data,
            'output' => $result,
        ];

        return $results;
    }
}
