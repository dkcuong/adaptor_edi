<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 21-Jan-16
 * Time: 9:25 PM
 */

namespace App\Parsers\X12Parser;

use App\Modules\Edi\R4010\EDI;
use App\Outbounds;
use App\Parsers\ParserBase;
use App\Parsers\ParserInterface;
use App\Utils\X12ParserAdapter;
use GuzzleHttp\Client;

class X12Parser extends ParserBase implements ParserInterface
{
    /**
     * Default Text Parser Element Separator
     *
     * @var string
     */
    protected $elementSeparator = '*';

    /**
     * Default component separator for text parser
     *
     * @var string
     */
    protected $componentSeparator = "~";



    /**
     * Default component separator for text parser
     *
     * @var string
     */
    protected $segmentTerminator = "\r\n";

    /**
     * @var
     */
    protected $edi;

    /**
     * @var
     */
    protected $inbound;

    /**
     * @var
     */
    protected $clients;

    /**
     * @var
     */
    public $originalParser;

    /**
     * Get return data after parsed
     *
     * @return array
     */
    public function get()
    {
        return $this->getResult();
    }

    /**
     * @return array|void
     */
    public function process()
    {
        $input = [
            'content' => $this->getContent(),
            'client' => $this->getClient(),
            'element_separator' => $this->getElementSeparator(),
            'component_separator' => $this->getComponentSeparator(),
            'apiEndPoint' => $this->getApiEndPoint(),
            'inbound'   => $this->getConfig('inbound')
        ];

        $edi = new X12ParserAdapter();

        $result = $edi->parse($input)->get();

        logger($result, ['context' => 'X12 Parser']);

        $this->originalParser = $result['data_full'];

        $this->setResult($result['data']);

        $this->setResultError($result['error']);
    }

    /**
     * @return array
     */
    public function afterProcess()
    {
        return parent::afterProcess(); // TODO: Change the autogenerated stub
    }

    /**
     * processOutbound function
     */
    public function processOutbound()
    {
        $input = [
            'data' => $this->getContent(),
            'client' => $this->getClient(),
            'element_separator' => $this->getElementSeparator(),
            'component_separator' => $this->getComponentSeparator(),
            'segment_terminator' => $this->getSegmentTerminator(),
            'apiEndPoint' => $this->getApiEndPoint(),
        ];

        $edi = new X12ParserAdapter();

        $result = $edi->create($input)->get();

        logger($result, ['context' => 'X12 Parser']);

        $this->setResult($result['data']['x12string']);

    }

    /**
     * @return bool
     */
    public function writerOutboundKnowledge()
    {
        return true;
    }
}
