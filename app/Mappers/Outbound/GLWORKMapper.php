<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/03/2016
 * Time: 09:22
 */

namespace App\Mappers\Outbound;


use App\Mappers\MapperBase;

use App\Mappers\SimpleXMLElementExtend;
use DOMDocument;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class GLWORKMapper extends MapperBase
{
    /**
     * Mapper data for Concept One customer
     * Edi945
     *
     * @param $items
     * @param $conditions
     * @return array
     */
    const CUSTOMER_CODE = '81235011';
    const AI_CODE = '00';

    protected function map856($items, $conditions)
    {
        $results = $this->parseTree856($items);


        // initializing or creating array
        $data = array('total_stud' => 500);

        // creating object of SimpleXMLElement
        $xml_data = new SimpleXMLElementExtend('<?xml version="1.0"?><File></File>');

        // function call to convert array to xml
        $this->arrayToXML($results, $xml_data);

        //saving generated xml file;
        return $this->prepareCharset($xml_data->asXML());
    }

    function prepareCharset($str) {

        // set default encode
        mb_internal_encoding('UTF-8');

        // pre filter
        if (empty($str)) {
            return $str;
        }

        // get charset
        $charset = mb_detect_encoding($str, array('ISO-8859-1', 'UTF-8', 'ASCII'));

        if (stristr($charset, 'utf') || stristr($charset, 'iso')) {
            $str = iconv('ISO-8859-1', 'UTF-8//TRANSLIT', utf8_decode($str));
        } else {
            $str = mb_convert_encoding($str, 'UTF-8', 'UTF-8');
        }

        // remove BOM
        $str = urldecode(str_replace("%C2%81", '', urlencode($str)));

        // prepare string
        return $str;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parseTree856($tranData)
    {
        $header = [
            'Header' => [
                'CompanyCode' => '10123',
                'CustomerNumber' => $tranData['first_name'],
                'Direction' => 'Outbound',
                'DocumentType' => '856',
                'Version' => '3.5',
                'Footprint' => 'ASN',
                'ShipmentID' => getDefault($tranData['pronumber']),
                'InternalDocumentNumber' => getDefault($tranData['clientordernumber']),

            ],
        ];

        $freightChargeTerm = [
            'freightchargetermbycollect' => 'CC',
            'freightchargetermbyprepaid' => 'PP',
            'freightchargetermby3rdparty' => 'TP',
        ];

        $shipmentLevel = [
            'ShipmentLevel' => [
                'TransactionSetPurposeCode' => '00',
                'PRONumber' => getDefault($tranData['pronumber']),
                'DateLoop' => [
                    'DateQualifier_ShipDate' => [
                        'keyName' => 'DateQualifier',
                        'value' => '011',
                        'attributes' => [
                            'Desc' => 'ShipDate'
                        ]
                    ],
                    'Date_ShipDate' => [
                        'keyName' => 'Date',
                        'value' => (isset($tranData['start_ship_date']) && !empty($tranData['start_ship_date'])) ? date('Y-m-d', strtotime($tranData['start_ship_date'])) : date('Y-m-d'),
                    ],
                    'DateQualifier_DeliveryDate' => [
                        'keyName' => 'DateQualifier',
                        'value' => '002',
                        'attributes' => [
                            'Desc' => 'DeliveryDate'
                        ]
                    ],
                    'Date_DeliveryDate' => [
                        'keyName' => 'Date',
                        'value' => (isset($tranData['carrier']) && !empty($tranData['carrier'])) ? date('Y-m-d', strtotime($tranData['carrier'])) : date('Y-m-d'),
                    ],
                ],
                'ManifestCreateTime' => date('H:i') . ':00',
                'HierarchyStructureCode' => '0001',
                'ShipmentTotals' => [
                    'ShipmentTotalCube' => $tranData['total_volume'],
                    'ShipmentTotalCases' => $tranData['total_pieces'],
                    'ShipmentTotalWeight' => $tranData['total_weight'],
                ],
                'Carrier' => [
                    'CarrierCode' => getDefault($tranData['scac']),
                ],
                'BillOfLadingNumber' => [
                    'type' => 'CDATA',
                    'value' => getDefault($tranData['bolnumber'])
                ],
                'MethodOfPayment' => getDefault($tranData['shipType']),
                'Name_SF' => [
                    'keyName' => 'Name',
                    'value' => [
                        'BillAndShipToCode' => 'SF',
                        'DUNSOrLocationNumber' => '828766738',
                        'NameComponentQualifier' => 'Ship From',
                        'NameComponent' =>[
                            'type' => 'CDATA',
                            'value' => 'LIFEWORKS'
                        ],
                        'CompanyName' => [
                            'type' => 'CDATA',
                            'value' => 'LIFEWORKS'
                        ],
                        'Address' => [
                            'type' => 'CDATA',
                            'value' => '809 E. 236TH STREET'
                        ],
                        'City' => [
                            'type' => 'CDATA',
                            'value' => 'CARSON'
                        ],
                        'State' => 'CA',
                        'Zip' => '90745',
                        'Country' => 'USA'
                    ],
                ],
                'Name_ST' => [
                    'keyName' => 'Name',
                    'value' => [
                        'BillAndShipToCode' => 'ST',
                        'DUNSOrLocationNumber' => str_pad($tranData['special_instruction'], 4, 0, STR_PAD_LEFT),
                        'NameComponentQualifier' => 'Ship To',
                        'NameComponent' => [
                            'type' => 'CDATA',
                            'value' => getDefault($tranData['shiptoname'])
                        ],
                        'CompanyName' => [
                            'type' => 'CDATA',
                            'value' => getDefault($tranData['shiptoname'])
                        ],
                        'Address' => [
                            'type' => 'CDATA',
                            'value' => getDefault($tranData['online_orders']['shipping_address_street']) . getDefault($tranData['online_orders']['shipping_address_street_cont'])
                        ],
                        'City' => [
                            'type' => 'CDATA',
                            'value' => getDefault($tranData['online_orders']['shipping_city'])
                        ],
                        'State' => getDefault($tranData['online_orders']['shipping_state']),
                        'Zip' => getDefault($tranData['online_orders']['shipping_postal_code']),
                        'Country' => getDefault($tranData['online_orders']['shipping_country'])
                    ]
                ],
                'VendorCode' => str_pad($tranData['additional_shipper_information'], 10, 0, STR_PAD_LEFT),
                'ContactType' => [
                    'FunctionCode' => [
                        'type' => 'CDATA',
                        'value' => 'CUST'
                    ],
                    'ContactName' => [
                        'type' => 'CDATA',
                        'value' => 'CARLOS RODRIGUEZ'
                    ],
                    'ContactQualifier_FX' => [
                        'keyName' => 'ContactQualifier',
                        'value' => 'FX',
                    ],
                    'PhoneEmail_PX' => [
                        'keyName' => 'PhoneEmail',
                        'value' => [
                            'type' => 'CDATA',
                            'value' => '3102417654'
                        ],
                    ],
                    'ContactQualifier_PH' => [
                        'keyName' => 'PhoneEmail',
                        'value' => 'PH',
                    ],
                    'PhoneEmail_PH' => [
                        'keyName' => 'PhoneEmail',
                        'value' => [
                            'type' => 'CDATA',
                            'value' => '3102417655'
                        ],
                    ],
                ]
            ]
        ];

        $bill_to = getDefault($tranData['bill_to']);
        $bill_to_Arr = explode('%%%', $bill_to);

        $orderLevels = [
            'OrderLevel' => [
                'IDs' => [
                    'PurchaseOrderNumber' => [
                        'type' => 'CDATA',
                        'value' => str_pad($tranData['customerordernumber'],10,0,STR_PAD_LEFT),
                    ],
                    'PurchaseOrderSourceID' => $tranData['last_name'],
                    'PurchaseOrderDate' => (isset($tranData['order_date']) && !empty($tranData['order_date'])) ? date('Y-m-d', strtotime($tranData['order_date'])) : '',
                    'StoreNumber' => $tranData['client_pick_ticket'],
                    'DepartmentNumber' => $tranData['client_department'],
                    'DivisionNumber' => '',
                ],
                'OrderTotals' => [
                    'OrderTotalCases' => $tranData['total_pieces'],
                    'OrderTotalWeight' => $tranData['total_weight'],
                    'OrderTotalCube' => $tranData['total_volume'],
                ],
                'DateLoop' => [
                    'DateQualifier_PurchaseOrderDate' => [
                        'keyName' => 'DateQualifier',
                        'value' => '004',
                        'attributes' => [
                            'Desc' => 'PurchaseOrderDate'
                        ]
                    ],
                    'Date' => (isset($tranData['order_date']) && !empty($tranData['order_date'])) ? date('Y-m-d', strtotime($tranData['order_date'])) : '',

                ],
                'Name_BT' => [
                    'keyName' => 'Name',
                    'value' => [
                        'BillAndShipToCode' => 'BT',
                        'DUNSOrLocationNumber' => str_pad($tranData['client_pick_ticket'], 4, 0, STR_PAD_LEFT),
                        'NameComponentQualifier' => 'Bill To',
                        'NameComponent' =>[
                            'type' => 'CDATA',
                            'value' => isset($bill_to_Arr[0]) ? strtoupper($bill_to_Arr[0]) : '',
                        ],
                        'CompanyName' => [
                            'type' => 'CDATA',
                            'value' => isset($bill_to_Arr[0]) ? strtoupper($bill_to_Arr[0]) : '',
                        ],
                        'Address' => [
                            'type' => 'CDATA',
                            'value' => isset($bill_to_Arr[1]) ? strtoupper($bill_to_Arr[1]) : '',
                        ],
                        'City' => [
                            'type' => 'CDATA',
                            'value' => isset($bill_to_Arr[2]) ? strtoupper($bill_to_Arr[2]) : '',
                        ],
                        'State' => isset($bill_to_Arr[3]) ? strtoupper($bill_to_Arr[3]) : '',
                        'Zip' => isset($bill_to_Arr[4]) ? strtoupper($bill_to_Arr[4]) : '',
                        'Country' => isset($bill_to_Arr[5]) ? strtoupper($bill_to_Arr[5]) : '',
                    ]
                ],
                'Name_RE' => [
                    'keyName' => 'Name',
                    'value' => [
                        'BillAndShipToCode' => 'RE',
                        'DUNSQualifier' => '1',
                        'DUNSOrLocationNumber' => '828766738',
                        'NameComponentQualifier' => 'Remit To',
                        'NameComponent' =>[
                            'type' => 'CDATA',
                            'value' => 'LIFEWORKS'
                        ],
                        'CompanyName' => [
                            'type' => 'CDATA',
                            'value' => 'LIFEWORKS'
                        ],
                        'Address' => [
                            'type' => 'CDATA',
                            'value' => '1412 BROADWAY, 7TH FLOOR'
                        ],
                        'City' => [
                            'type' => 'CDATA',
                            'value' => 'NEW YORK'
                        ],
                        'State' => 'NY',
                        'Zip' => '10018',
                        'Country' => 'USA'
                    ]
                ],
            ],
        ];
        foreach ($tranData['items'] as $item_id => $item) {
            foreach ($item['cartons'] as $cart_id => $carton) {
                $pickPackStructure = [
                    'keyName' => 'PickPackStructure',
                    'value' => [
                        'Carton' => [
                            'Marks' => [
                                'UCC128' => self::AI_CODE . generateUCC128('00'. self::CUSTOMER_CODE, $carton['cartonID']),
                            ],
                            'Quantities' => [
                                'QtyQualifier' => 'ZZ',
                                'QtyUOM' => 'ZZ',
                                'Qty' => $carton['uom'],
                            ]
                        ],
                        'Item' => [
                            'ItemIDs_CP' => [
                                'keyName' => 'ItemIDs',
                                'value' => [
                                    'IdQualifier' => 'CP',
                                    'Id' => [
                                        'type' => 'CDATA',
                                        'value' => $carton['shipment_tracking_id']
                                    ],
                                ],
                            ],
                            'ItemIDs_UP' => [
                                'keyName' => 'ItemIDs',
                                'value' => [
                                    'IdQualifier' => 'UP',
                                    'Id' => [
                                        'type' => 'CDATA',
                                        'value' => $carton['internalID']
                                    ],
                                ],
                            ],
                            'ItemIDs_VN' => [
                                'keyName' => 'ItemIDs',
                                'value' => [
                                    'IdQualifier' => 'VN',
                                    'Id' => [
                                        'type' => 'CDATA',
                                        'value' => $carton['sku']
                                    ],
                                ],
                            ],
                            'Quantities' => [
                                'QtyQualifier' => '39',
                                'QtyUOM' => 'EA',
                                'Qty' => $carton['uom'],
                            ],
                            'PackSize' => $carton['uom'],
                            'Inners' => '1',
                            'EachesPerInner' => '1',
                            'InnersPerPacks' => $carton['uom'],
                            'Size' => [
                                'type' => 'CDATA',
                                'value' => $carton['size'] != 'NA' ? $carton['size'] : '',
                            ],
                            'Measurement' => [
                                'MeasureQual' => 'WT',
                                'MeasureValue' => '0.00',
                            ],
                        ]
                    ],
                ];

                $orderLevels['OrderLevel'][$item_id.$cart_id] = $pickPackStructure;
            }

        }
        //$orderLevels = array_merge($orderLevels['OrderLevel'], $pickPackStructures);
        $data = [
            'Document' => array_merge($header, $shipmentLevel, $orderLevels)
        ];

        return $data;
    }

    // function defination to convert array to xml
    function arrayToXML( $data, &$xml_data) {
        foreach( $data as $key => $value ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            if( is_array($value) ) {
                // Add any attributes
                if ( ! empty( $value['type'] ) && ! is_array( $value['type'] ) ) {
                    $value = $value['value'];
                    $xml_data->addChildCData("$key", htmlspecialchars("$value"));
                } else if ( ! empty( $value['attributes'] ) && is_array( $value['attributes'] ) ) {
                    $ccValue = $value['value'];
                    if ( ! empty( $value['keyName'] ) && $value['keyName'] ) {
                        $ccValue = $value['value'];
                        $subnode = $xml_data->addChild($value['keyName'], htmlspecialchars("$ccValue"));
                    } else {
                        $subnode = $xml_data->addChild($key, htmlspecialchars("$ccValue"));
                    }

                    foreach ( $value['attributes'] as $attribute_key => $attribute_value ) {
                        $subnode->addAttribute( $attribute_key, $attribute_value );
                    }
                } else if ( ! empty( $value['keyName'] ) && $value['keyName'] ) {
                    $ccValue = $value['value'];
                    $keyName = $value['keyName'];
                    if (is_array($ccValue)) {
                        if ( ! empty( $ccValue['type'] ) && ! is_array( $ccValue['type'] ) ) {
                            $value = $ccValue['value'];
                            $xml_data->addChildCData("$keyName", htmlspecialchars("$value"));
                        } else {
                            $subnode = $xml_data->addChild($keyName);
                            $this->arrayToXML($ccValue, $subnode);
                        }

                    } else {
                        $xml_data->addChild($keyName, htmlspecialchars("$ccValue"));
                    }

                } else {
                    $subnode = $xml_data->addChild($key);
                    $this->arrayToXML($value, $subnode);
                }
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

}
