<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnOnX12InboundGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            ALTER TABLE  x12_inbound_groups 
            DROP FOREIGN KEY FK_x12_inbound_groups_0; 
        ');
        DB::statement('
            ALTER TABLE x12_inbound_groups 
            CHANGE inbound_interchange_id  interchange_id BIGINT NOT NULL;
        ');
        DB::statement('
            ALTER TABLE  x12_inbound_groups  
            ADD CONSTRAINT  FK_x12_inbound_groups_0 
            FOREIGN KEY (interchange_id) REFERENCES x12_interchanges(id);
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('x12_inbound_groups', function (Blueprint $table) {
            //
        });
    }
}
