<?php
/**
 * Created by PhpStorm.
 * User: tranp
 * Date: 3/19/2018
 * Time: 16:57
 */

namespace App\Modules\Edi\V1\Models;


use Illuminate\Support\Facades\DB;

class DBModel
{
    public function getNextID($table)
    {
        $statement = DB::select("show table status like '".$table."'");

        return $statement[0]->Auto_increment;
    }
}