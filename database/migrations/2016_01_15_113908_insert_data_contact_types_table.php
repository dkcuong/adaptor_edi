<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataContactTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('contact_types')->insert(
            [
                'name' => 'Business Contact',
                'short_name' => 'BC',
                'description' => 'Business Contact'
            ]
        );
        
        DB::table('contact_types')->insert(
            [
                'name' => 'Technical Contact',
                'short_name' => 'TC',
                'description' => 'Technical Contact'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
