<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClientApiEndPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_api_endpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('client_id', false, true);
            $table->string('api_name', 50);
            $table->string('api_endpoint', 150);
            $table->text('certificate_type')->nullable();
            $table->text('certificate_key')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
