<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 22-Mar-16
 * Time: 2:13 PM
 */

namespace App\Modules\Edi\V1\Services;

use App\Setting;

class SettingService
{

    /**
     * @return $this
     */
    public function emailSetting()
    {
        $setting = (new Setting())->email_smtp;

        if (! $setting) {
            return $this;
        }

        $defaultEncryption = config('mail.encryption');
        $encryption = $setting->encryption == 'no' ? $defaultEncryption : $setting->encryption;

        config([
            'mail.host' => $setting->smtp_host,
            'mail.port' => $setting->smtp_port,
            'mail.encryption' => $encryption,
            'mail.username' => $setting->username,
            'mail.password' => $setting->password,
        ]);

        return $this;
    }
}
