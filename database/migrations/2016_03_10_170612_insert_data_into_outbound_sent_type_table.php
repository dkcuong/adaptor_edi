<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataIntoOutboundSentTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('outbound_sent_types')->insert(
            array(
                'name' => 'Sent Mail',
                'short_name' => 'SM',
                'description' => 'Only sent mail',
                'status' => 1
            )
        );
        
        DB::table('outbound_sent_types')->insert(
            array(
                'name' => 'Create File',
                'short_name' => 'CF',
                'description' => 'Only create file',
                'status' => 1
            )
        );
         
        DB::table('outbound_sent_types')->insert(
            array(
                'name' => 'Sent mail and Create file',
                'short_name' => 'SC',
                'description' => 'Sent mail and Create file',
                'status' => 1
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outbound_sent_types', function (Blueprint $table) {
            //
        });
    }
}
