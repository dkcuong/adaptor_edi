<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 3/17/2017
 * Time: 17:07
 */

namespace app\Modules\Edi\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Responder;
use App\Modules\Edi\V1\Services\ServiceHelpers\DMSClientService;
use Illuminate\Http\Request;

class JWTController extends Controller
{
    function dmsIntegration(Request $request)
    {
        $rs = [];
        $requestContent = $request->all();
        if(!empty($requestContent['jwt'])) {
            $data = DMSClientService::validJWT($requestContent['jwt']);
            if(!empty($data)) {
                header('Content-Type: application/json');
                $rs = $data->data;
            }
        }

        return response()->json($rs);
    }
}