<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseTypes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_key',
        'type_field',
        'type_value',
    ];

    /**
     * @var string
     */
    protected $table = 'warehouse_types';
}
