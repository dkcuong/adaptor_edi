<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 15-Jan-16
 * Time: 3:32 PM
 */

namespace App\Utils;

use App\Exceptions\ParserHeaderMappingException;
use Codeception\Extension\Logger;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

/**
 * Class FtpStorage
 *
 * @package App\Utils
 */
class FtpStorage
{

    /**
     * @var string
     */
    public $name = 'FtpStorage';

    /**
     * Store Ftp configuration info
     *
     * @var array
     */
    protected $clientInfo;

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $disk;

    /**
     * @return mixed
     */
    public function getClientInfo()
    {
        return $this->clientInfo;
    }

    /**
     * @param mixed $clientInfo
     */
    public function setClientInfo($clientInfo)
    {
        $this->clientInfo = $clientInfo;
    }

    /**
     * FtpStorage constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->setClientInfo($config);

        $this->disk = $this->connect();
    }

    /**
     *  Connect To Ftp Server by Customer information
     *
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    public function connect()
    {
        $ftpID = $this->getClientInfo();
        $passive = $ftpID['transfer_mode'] ? true : false;

        $connectInfo = [
            'host'     => $ftpID['host'],
            'username' => $ftpID['username'],
            'password' => $ftpID['password'],
            'passive'  => $passive,
        ];
        try {
            //logger($connectInfo, ['info' => 'FTP Connect']);
            return Storage::createFtpDriver($connectInfo);
        } catch (\Exception $e) {
            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            logger($e->getMessage(), ['info' => $errorInfo]);
        }

    }

    /**
     * Remove Ftp file
     *
     * @param $file
     */
    public function removeFile($file)
    {
        try {

            $this->disk()->delete($file);
        } catch (\Exception $e) {
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            logger($e->getMessage(), ['info' => $errorInfo]);
        }
    }

    /**
     * Move Ftp file to new Path
     *
     * @param $old
     * @param $new
     *
     * @internal param $file
     */
    public function moveFile($old, $new)
    {
        $path = pathinfo($new, PATHINFO_DIRNAME);
        try {
            $this->disk()->makeDirectory($path);

            $this->disk()->move($old, $new);
        } catch (\Exception $e) {
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            logger($e->getMessage(), ['info' => $errorInfo]);
        }
    }

    /**
     * @param $path
     * @return array
     */
    public function readFiles($path)
    {
        try {
            $files = $this->disk()->files($path);

            return $files;
        } catch (\Exception $e) {
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            logger($e->getMessage(), ['info' => $errorInfo]);
        }
    }

    /**
     * Get content of the file
     *
     * @param $file
     *
     * @return mixed
     */
    public function parseFile($file)
    {
        try {
            return $this->disk()->get($file);
        } catch (\Exception $e) {
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            logger($e->getMessage(), ['info' => $errorInfo]);
        }
    }

    /**
     * @param $file
     * @param $content
     */
    public function makeFile($file, $content)
    {
        try {
            $this->disk()->put($file, $content);
        } catch (\Exception $e) {
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            logger($e->getMessage(), ['info' => $errorInfo]);
        }
    }

    /**
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    public function disk()
    {
        return $this->disk;
    }
}
