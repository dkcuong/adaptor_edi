DROP TABLE IF EXISTS `noti_ftp_uploads`;
CREATE TABLE `noti_ftp_uploads` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;