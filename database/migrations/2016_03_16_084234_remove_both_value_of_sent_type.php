<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveBothValueOfSentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::statement('
           DELETE FROM outbound_sent_types 
           WHERE outbound_sent_types.short_name = "SC"'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outbound_sent_types', function (Blueprint $table) {
            //
        });
    }
}
