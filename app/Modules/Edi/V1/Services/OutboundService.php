<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 23/02/2016
 * Time: 15:13
 */

namespace App\Modules\Edi\V1\Services;

use App\Modules\Edi\V1\Models\ClientModel;
use App\Modules\Edi\V1\Models\OutboundModel;
use App\Outbounds;
use App\X12Interchanges;
use App\X12OutboundGroupRevisions;
use App\X12OutboundGroups;
use App\X12OutboundInterchanges;
use App\X12OutboundTransactionFiles;
use App\X12OutboundTransactions;
use Illuminate\Support\Facades\DB;

class OutboundService
{
    /**
     * @var
     */
    protected $obModel;
    /**
     * @var
     */
    public $x12String;

    /**
     * @var
     */
    public $data;

    /**
     * @var
     */
    protected $orderData;

    /**
     * @var
     */
    protected $tranData;

    /**
     * @var
     */
    protected $transactionCodes;

    /**
     * @string
     */
    const SHIPPED_ORDER = 'SHCO';

    /**
     * @string
     */
    const CANCEL_ORDER = 'CNCL';

    /**
     * @var EmailHandlingService
     */
    public $emailHandling;

    /**
     * @var
     */
    public $warehouseID = '';

    /**
     * @var
     */
    protected $clientModel;

    /**
     * @var
     */
    public $customerId;

    /**
     * @var
     */
    public $clientWarehouses;

    /**
     * OutboundService constructor.
     * @param OutboundModel $outboundModel
     * @param EmailHandlingService $emailHandlingService
     */
    public function __construct(
        OutboundModel $outboundModel,
        EmailHandlingService $emailHandlingService,
        ClientModel $clientModel
    )
    {
        $this->obModel = $outboundModel;
        $this->obInter = new X12OutboundInterchanges;
        $this->obGroup = new X12OutboundGroups;
        $this->obTran = new X12OutboundTransactions;
        $this->emailHandling = $emailHandlingService;
        $this->clientModel = $clientModel;

    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function getOutboundInterchanges($clientId)
    {
        return X12Interchanges::where('client_id', $clientId)->first();
    }

    /**
     * @param $interchangeId
     * @param $groupCode
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getOutboundGroupByGroupCode($interchangeId, $groupCode)
    {
        return X12OutboundGroups::with('groupRevisions')
            ->with('outboundTransaction')
            ->where('interchange_id', $interchangeId)
            ->where('group_code', $groupCode)
            ->first();
    }

    /**
     * @param $interchangeId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOutboundGroups($interchangeId)
    {
        return X12OutboundGroups::with('groupRevisions')
            ->where('interchange_id', $interchangeId)->get();
    }

    /**
     * @param $groupId
     * @param $controlNumber
     * @return mixed
     */
    public function getOutboundTrans($groupId, $controlNumber)
    {
        return X12OutboundTransactions::where('group_id', $groupId)
            ->where('group_control_number', $controlNumber)
            ->where('status', 'open')
            ->get();
    }

    /**
     * @param $client
     * @param $outboundGroup
     * @param array $parseHeaders
     * @return array
     */
    public function insertRsa($client, $outboundGroup, $parseHeaders = [])
    {
        $interRevId = $this->obModel->insertOutboundInterchange($client->rsa, $parseHeaders);

        $this->obModel->updateInterChange($client->rsa);

        $obGroup = $outboundGroup->toArray();

        $groupRev = $this->obModel->updateGroup($obGroup, $interRevId, $parseHeaders);

        return $groupRev;
    }

    /**
     * @param $edi
     * @param $client
     * @return array
     */
    public function getDataCreator($edi, $client)
    {
        $tranData = [];
        $this->data = $inter = X12Interchanges::where('client_id', $client->id)->first();
        $this->data->interchange = $inter->toArray();
        $this->data->groups = $groupRevs = $this->getGroups($client->id, $edi->group_id);

        foreach ($groupRevs as $gKey => $groupRev) {
            $this->data->groups[$gKey]->trans = $tranData =
                $this->getOutboundTransactions($groupRev->id, $groupRev->control_number);
        }

        $contents = $this->reStructureContents();

        return $contents;

    }

    /**
     * @param $groupId
     * @param $controlNumber
     * @return mixed
     */
    public function getOutboundTransactions($groupId, $controlNumber)
    {
        return X12OutboundTransactions::where('group_id', $groupId)
            ->where('group_control_number', $controlNumber)
            ->where('status', 'open')
            ->first();
    }

    /**
     * @param $clientId
     * @param $groupId
     * @return mixed
     */
    public function getGroups($clientId, $groupId)
    {
        return $this->obModel->getOutboundGroups($clientId, $groupId);
    }

    /**
     * @return array
     */
    public function reStructureContents()
    {
        $results = [];
        foreach ($this->data->groups as $index => $group) {
            $interchange = $this->obModel->getOutboundInterChange($group->id);
            $results[$index]['interchange'] = $this->refactorArray($interchange, $this->obInter->obFields);
            $results[$index]['transactionId'] = $group->trans->id;
            $results[$index]['success_number'] = $group->trans->success_number;
            $results[$index]['statusWms'] = $group->trans->status_wms;
            $results[$index]['interId'] = $interchange->id;
            $results[$index]['interNum'] = $interchange->control_number;
            $results[$index]['groupNum'] = $group->control_number;
            $results[$index]['groups'][0] = $this->refactorArray($group, $this->obGroup->obFields);
            $results[$index]['groups'][0]['transactions'][] =
                $this->refactorArray($group->trans, $this->obTran->obFields);
        }

        return $results;
    }

    /**
     * @param $dataInput
     * @param array $outboundFields
     * @return array
     */
    public function refactorArray($dataInput, $outboundFields = [])
    {
        $dataOutput = [];
        if ($outboundFields) {
            foreach ($outboundFields as $field) {
                $dataOutput[$field] = $dataInput->$field;

            }
        }

        return $dataOutput;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function updateGroups($data)
    {
        return X12OutboundGroupRevisions::update($data['update'])
            ->where('id', $data['where']['id'])
            ->where('control_number', $data['where']['control_number']);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function updateTransactions($data)
    {
        return X12OutboundTransactions::where('id', $data['where']['id'])
            ->update($data['update']);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function updateTrans($data)
    {
        return X12OutboundTransactions::update($data['update'])
            ->where('id', $data['where']['id'])
            ->where('control_number', $data['where']['control_number']);
    }


    /**
     * @param $dataInput
     * @param $key
     * @param $dataOutput
     * @param $client
     * @param $outboundGroup
     * @param array $parseHeaders
     */
    public function createOutbound($dataInput, $key, $dataOutput, $client, $outboundGroup, $parseHeaders = [])
    {
        if($outboundGroup){
            foreach ($outboundGroup as $k => $oG) {
                $dataOutput['status'] = $dataOutput['status_code'] == '200' ? 'Open' : 'Error';
                $groupData = $this->insertRsa($client, $oG, $parseHeaders);

                $this->obModel->insertOutboundTransactions(
                    $groupData,
                    $key + 1,
                    $dataInput,
                    $dataOutput,
                    $oG
                );
            }
        }
    }

    /**
     * @param $content
     * @param $results
     */
    public function updateStatusTransaction($content, $results)
    {
        $dataTran = [
            'update' => [
                'status' => 'Sent',
                'message' => 'Your Transaction has been created successfully',
                'data_output' => $results,
            ],
            'where' => [
                'id' => $content['transactionId'],
            ]
        ];
        $this->updateTransactions($dataTran);

    }

    /**
     * @param $transactionId
     * @param $statusWms
     */
    public function updateStatusWmsTransaction($transactionId, $statusWms)
    {
        $dataTran = [
            'update' => [
                'status_wms' => $statusWms,
            ],
            'where' => [
                'id' => $transactionId,
            ]
        ];
        $this->updateTransactions($dataTran);

    }

    /**
     * @param $content
     * @param $fileName
     */
    public function insertTransactionFiles($content, $fileName, $outputContent = '')
    {
        $transaction = X12OutboundTransactions::where('id', $content['transactionId'])->first();
        $groupId = $transaction->group_id;
        if (!$this->isExistTransactionFile($groupId)) {
            X12OutboundTransactionFiles::create([
                'outbound_group_id' => $groupId,
                'file_name' => $fileName,
                'content' => $outputContent
            ]);
        }

    }

    /**
     * @param $client
     * @param $parseResults
     * @param $originalParse
     * @param $content
     * @return array
     */
    public function createData($client, $parseResults, $originalParse, $content, $response)
    {
        $statusCode = null;
        //$dataTransaction['transaction_code'] = '997';

        $control_number = $originalParse['groups'][0]['control_number'];
        $acceptedData = $orderCount = $parseResults['output'];

        $countReceived = $countAccepted = $countRejected = 0;
        $countTransaction = count($content);

        $dataTransaction[] = [
            'segment' => "Ak1",
            "group_code" => $client->rsa->inboundGroup[0]->group_code,
            "control_number" => $control_number,
        ];

        $dataTransaction[] = [
            'segment' => "Ak2",
            "trans_code" => $content[0]['transaction_code'],
            "control_number" => str_pad($control_number,4, 0, STR_PAD_LEFT),
        ];

        if (isset($parseResults['output']['error'])) {
            $statusCode = 'R';
            $countReceived = $countTransaction;
        } else {
            foreach ($acceptedData as $tran) {
                if (isset($tran['status_code']) && $tran['status_code'] != '200') {
                    $countRejected++;
                }
            }

            if ($countRejected == 0) {
                $statusCode = 'A';
                $countReceived = $countTransaction;
            } elseif ($countTransaction == $countRejected) {
                $statusCode = 'R';
                $countReceived = $countRejected;
            } elseif ($countTransaction > $countRejected) {
                $statusCode = 'E';
                $countReceived = $countRejected;
                $countAccepted = $countTransaction - $countRejected;
            }
        }

        if($statusCode == 'R') {
            if (isset($response['error']) && count($response['error']['errors']) == 1) {
                $dataErr = $response['error']['errors'];
                foreach ($dataErr as $err) {
                    if (!empty($err['T997'])) {
                        $dataTransaction[] = [
                            "segment" => 'Ak3',
                            "err_seg" => $err['T997']['segment_code'],
                            "err_line" => $err['T997']['segment_position'],
                            "seg_err_code" => ''
                        ];

                        $dataTransaction[] = [
                            "segment" => 'Ak4',
                            "position_err" => $err['T997']['element_error']['element_position'],
                            "id_err" => '',
                            "err_code" => $err['T997']['element_error']['element_error_code'],
                            "bad_data" => $err['T997']['element_error']['bad_data'],
                        ];
                    }
                }
            }
        }

        $dataTransaction[] = [
            "segment"   => 'Ak5',
            "ack_code"  => $statusCode,
        ];

        $dataTransaction[] = [
            'segment' => 'Ak9',
            "ack_code" => $statusCode,
            "transaction_count" => $countTransaction,
            "received_count" => $countReceived,
            "accepted_count" => $countAccepted,
        ];

        $dataTransaction['isSuccess'] = 1; // default for outbound 997

        //$dataTransactions[] = $dataTransaction;

        // Add Customer id for next step: IMPORTANT!!
        $this->customerId = !empty($client->clientWarehouse[0]) ? $client->clientWarehouse[0]->customer_id  : 'NA';

        return $dataTransaction;
    }

    /**
     * @param $client
     * @param $dataOutput
     * @param $groupData
     */
    public function createX12Knowledge($client, $dataOutput, $groupData)
    {
        $params = [
            'outboundGroup' => $groupData,
            'transactionCode' => '997',
            'dataTransaction' => $dataOutput,
            'control_number' => 1
        ];

        try {
            DB::beginTransaction();
            $interId = $this->obModel->insertOutboundInterchange($client->rsa);

            $this->obModel->updateInterChange($client->rsa);

            $this->insertTransaction($params, $interId);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $messageHandle = [
                'Message: ' . $e->getMessage(),
                'File:' . __FILE__,
                'Class:' . __CLASS__,
                'Function:' . __FUNCTION__,
                'Line Number:' . __LINE__
            ];

            logger($messageHandle);
            $this->emailHandling->sendEmail([$messageHandle]);
        }
    }

    /**
     * @param $params
     * @param $interId
     * @return mixed
     */
    public function insertTransaction($params, $interId)
    {
        $outboundGroup = $params['outboundGroup'];
        $dataTransaction = $params['dataTransaction'];
        $transactionCode = $params['transactionCode'];
        $control_number = $params['control_number'];

        if (isset($params['isOutbound']) && $params['isOutbound']) {
            $tranIds = [];
            foreach ($this->clientWarehouses as $clientWarehouse) {

                $tran = $this->createOutboundTransaction($outboundGroup, $dataTransaction, $transactionCode,
                    $control_number, $interId, $clientWarehouse->customer_id);

                if ($tran && isset($tran->id)) {
                    $tranIds[] = $tran->id;
                }
                $control_number++;
            }
            return $tranIds;
        } else {

            $tran = $this->createOutboundTransaction($outboundGroup, $dataTransaction, $transactionCode,
                $control_number, $interId, $this->customerId);
            if ($tran && isset($tran->id)) {
                return $tran->id;
            }
        }

    }

    /**
     * function create Outbound Tramsaction
     */
    private function createOutboundTransaction($outboundGroup, $dataTransaction, $transactionCode, $control_number, $interId, $vendorID) {

        $tran = null;
        $results = X12OutboundGroupRevisions::create([
            'group_code' => $outboundGroup->group_code,
            'sender_code' => $outboundGroup->sender_code,
            'receiver_code' => $outboundGroup->receiver_code,
            'date' => date('Ymd'),
            'time' => date('Hi'),
            'control_number' => $outboundGroup->control_number,
            'control_standard' => $outboundGroup->control_standard,
            'control_version' => $outboundGroup->control_version,
            'status' => 'open',
            'interchange_id' => $interId,
            'group_id' => $outboundGroup->id
        ]);

        if ($results->id) {
            $this->obGroup->updateControlNumber(
                $outboundGroup->id,
                $outboundGroup->control_number + 1
            );

            $tran = X12OutboundTransactions::create([
                'transaction_code' => $transactionCode,
                'control_number' => $control_number,
                'group_id' => $results->id,
                'group_control_number' => $outboundGroup->control_number,
                'success_number' => $vendorID,
                'status' => 'Open',
                'message' => '',
                'data_output' => json_encode($dataTransaction)
            ]);
        }
        return $tran;
    }

    /**
     * @param $contents
     * @param $edi
     * @param $client
     * @return array|mixed|null
     */
    public function getWMSData($contents, $edi, $client)
    {
        $successNumber = $dataOutput = null;
        foreach ($contents as $index => $content) {
            foreach ($content['transactions'] as $transaction) {
                if (empty($transaction['success_number'])) {
                    continue;
                }
                $successNumber = $transaction['success_number'];
                if ($successNumber) {
                    $dataInput = isset($transaction['data_input']) ? $transaction['data_input'] : [];
                    $dataOutput[] = $this->getWMSApiData($successNumber, $edi, $client);
                    return $this->reStructureResults($edi, $dataInput, $dataOutput);
                } else {
                    $dataOutput['data'][] = $transaction['data_output'];
                }
            }
        }
        return $dataOutput;
    }

    /**
     * @param $contents
     * @param $orderData
     * @return array|mixed
     */
    public function reStructureX12Data($contents, $orderData)
    {
        $data = json_decode($contents[0]['transactions'][0]['data'], true);
        if (!is_array($data)) {
            $orderNumber = $contents[0]['transactions'][0]['data'];
            if (!isset($orderData[$orderNumber])) {
                return [];
            }
            switch ($orderData[$orderNumber]['orderStatus']) {
                case self::SHIPPED_ORDER:
                    $results[] = $this->parseTree($orderData[$orderNumber]);
                    break;
                case self::CANCEL_ORDER:
                    $data = json_decode($contents[0]['transactions'][0]['data_input'], true);
                    $results[] = $this->parseTreeError($data);
                    break;
                default:
                    $results = [];
                    break;
            }
        } else {
            $results = $data;
        }
        return $results;

    }

    /**
     * @param $successNumber
     * @param $edi
     * @param $client
     * @return mixed
     */
    public function getWMSApiData($successNumber, $edi, $client)
    {
        //Call Api
        $api = app(
            'Api',
            [
                'client' => $client->short_name,
                'edi' => $edi->name,
                'data' => [$successNumber],
                'isX12' => false,
            ]
        );
        $response = $api->callApi();
        $results = json_decode($response->getBody(), true);

        logger($results, ['context' => 'outbound api result']);
        return $results;
    }

    /**
     * @param $dataInput
     * @return array
     */
    public function parseTreeError($dataInput)
    {
        $tran1 = ['transaction_code' => '945'];
        $tran1[] = [
            'segment' => 'W06',
            'report_code' => 'F',
            'client_order' => $dataInput['client_order_id'],
            'date' => date('Ymd'),
            'shipment_id' => '',
            'agent_id' => '',
            'purchase_order' => $dataInput['customer_order_id'],
        ];

        $tran1[] = [
            'segment' => 'N1',
            'entity_code' => 'ST',
            'name' => '',
            'id_qualifier' => '92',
            'id' => $dataInput['shipping_to'],
        ];

        $tran1[] = [
            'segment' => 'N1',
            'entity_code' => 'WH',
            'name' => 'SELDAT FULFILLMENT SERVICES',
            'id_qualifier' => '92',
            'id' => '20',
        ];

        $tran1[] = [
            'segment' => 'G62',
            'date_qualifier' => '11',
            'date' => date('Ymd'),
        ];

        $orderStatus = 'BO';
        $totalQuantity = 0;
        foreach ($dataInput['items'] as $tran) {

            $shippedQuantity = 0;
            $orderQuantity = $tran['quantity'];
            $totalQuantity += $orderQuantity;

            $tran1[] = [
                'segment' => 'Lx',
            ];
            $tran1[] = [
                'segment' => 'W12',
                'ordered_quantity' => $orderStatus,
                'quantity' => $orderQuantity,
                'unit_number' => $shippedQuantity,
                'quantity_def' => $orderQuantity,
                'uom' => 'EA',
                'upc' => '',
                'id_qualifier' => 'VC',
                'product_id' => $tran['sku'],
            ];

        }
        return $tran1;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parseTree($tranData)
    {
        $tran1 = ['transaction_code' => '945'];
        $tran1[] = [
            'segment' => 'W06',
            'report_code' => 'F',
            'client_order' => $tranData['clientordernumber'],
            'date' => date('Ymd'),
            'shipment_id' => '',
            'agent_id' => '',
            'purchase_order' => $tranData['customerordernumber'],
        ];

        $tran1[] = [
            'segment' => 'N1',
            'entity_code' => 'ST',
            'name' => '',
            'id_qualifier' => '92',
            'id' => $tranData['shiptoname'],
        ];

        $tran1[] = [
            'segment' => 'N1',
            'entity_code' => 'WH',
            'name' => 'SELDAT FULFILLMENT SERVICES',
            'id_qualifier' => '92',
            'id' => '20',
        ];

        $tran1[] = [
            'segment' => 'G62',
            'date_qualifier' => '11',
            'date' => date('Ymd'),
        ];

        $tran1[] = [
            'segment' => 'NTE',
            'ref_code' => 'BOL',
            'description' => $tranData['bolnumber'],
        ];

        $tran1[] = [
            'segment' => 'W27',
            'transportation_method' => 'T',
            'carrier_code' => $tranData['scac'],
            'routing' => $tranData['pronumber'],
            'payment_method' => $tranData['shipType'],
            'equipment_code' => 'TL',
            'equipment_init' => '',
            'equipment_number' => $tranData['trailerNumber'],
        ];

        $tran1[] = [
            'segment' => 'W10',
            'load_code' => 'PL',
            'pallet_quantity' => '1',
        ];

        $orderStatus = $tranData['orderStatus'];
        foreach ($tranData['items'] as $tran) {

            $shippedQuantity = $tran['product_order']['shipped_quantity'];
            $orderQuantity = $tran['product_order']['order_quantity'];

            if ($shippedQuantity == 0 || $orderStatus == 'CNCL') {
                $tran['status'] = 'BO';
            } else {
                if ($orderQuantity == $shippedQuantity) {
                    $tran['status'] = 'CC';
                } else {
                    if ($orderQuantity > $shippedQuantity) {
                        $tran['status'] = 'BP';
                    }
                }
            }

            $tran1[] = [
                'segment' => 'Lx',
            ];
            $tran1[] = [
                'segment' => 'W12',
                'ordered_quantity' => $tran['status'],
                'quantity' => $tran['product_order']['order_quantity'],
                'unit_number' => $tran['product_order']['shipped_quantity'] ?: 0,
                'quantity_def' => $tran['product_order']['diff_quantity'],
                'uom' => 'EA',
                'upc' => $tran['product_order']['upc'],
                'id_qualifier' => 'VC',
                'product_id' => $tran['product_order']['sku'],
            ];
            if (isset($tran['product_order']['ucc128']) && $tran['product_order']['ucc128']) {
                foreach ($tran['product_order']['ucc128'] as $ucc128) {
                    $tran1[] = [
                        'segment' => 'N9',
                        'qualifier' => 'GM',
                        'ref' => $ucc128,
                        'description' => '',
                    ];
                }
            }

        }

        $tran1[] = [
            'segment' => 'W03',
            'unit_number' => $tranData['piecesCount'],
            'weight' => $tranData['weight'],
            'unit_code' => 'LB',
        ];

        return $tran1;
    }

    private function reStructureResults($edi, $dataInput, $dataOutput)
    {
        $dataOutput = array_shift($dataOutput);
        $content = json_decode($dataInput, true);

        if (!$content || $edi->transactional_code != 'T861' || count($content['containers']) == count($dataOutput['data'])) {
            return $dataOutput;
        }
        $itemData = [];

        foreach ($content['containers'] as $item) {
            $container = array_get($item, 'container', 'NA');
            $sku = array_get($item, 'sku', 'NA');
            $pack_size = array_get($item, 'pack_size', 'NA');
            $primKey = $container . $sku . $pack_size;
            $primKey = strtolower(str_replace(" ", "", $primKey));
            $itemData[$primKey] = $item;
        }
        foreach ($dataOutput['data'] as $index => &$data) {
            $container = array_get($data, 'container_name', 'NA');
            $sku = array_get($data, 'sku', 'NA');
            $pack_size = array_get($data, 'pack_size', 'NA');
            $concatId = $container . $sku . $pack_size;
            $concatId = strtolower(str_replace(" ", "", $concatId));
            if (!isset($itemData[$concatId])) {
                $data['ctns_descrepancy'] = $data['ctns_received'];
                $data['pcs_descrepancy'] = $data['pcs_received'];
                $data['pcs_expected'] = 0;
                $data['ctns_expected'] = 0;
            }
        }
        return $dataOutput;
    }

    /**
     * @param $successKeys
     * @param $edi
     * @param $client
     * @return array
     */
    public function getMultiWMSData($successKeys, $edi, $client, $apiEndPoints, $customRequired = '', $inboundMappingCustom = '')
    {
        $data = [];
        //Call Api
        $ediName = ($client->short_name == 'GLWORK' && $edi->transactional_code == 'T856') ? '945' : $edi->name;
        $api = app(
            'Api',
            [
                'apiEndPoints' => $apiEndPoints,
                'client' => $client->id,
                'edi' => $ediName,
                'data' => $successKeys,
                'isX12' => false,
                'custom_required' => $customRequired,
                'inbound_custom_required' => $inboundMappingCustom,
                'inbound' => $edi,
                'outbound_process' => true

            ]
        );
        $response = $api->callApi();
        $results = json_decode($response->getBody(), true);

        logger($results, ['context' => 'outbound api result']);

        return $results;
    }

    public function getSuccessKeys($data)
    {
        $result = null;
        $content = array_shift($data);
        $transactions = isset($content['transactions']) ? $content['transactions'] : [];
        if (!$transactions) {
            return false;
        }
        $transaction = array_shift($transactions);

        if (!empty($transaction['success_number'])) {
            $result['success_number'] = $transaction['success_number'];
            if (!empty($transaction['data_output'])) {
                $result['data_output'] = json_decode($transaction['data_output'], true);
            }
        }
        return $result;
    }

    public function isExistTransactionFile($groupId)
    {
        $result = X12OutboundTransactionFiles::where('outbound_group_id', $groupId)->first();

        return $result ? true : false;
    }

    public function getOutboundGroupByTransactionCode($client_id, $transactional_code)
    {
        $tranCode = '';
        switch ($transactional_code) {
            case 'REC':
                $tranCode = ['T861', 'T944'];
                break;
            case 'ORD':
                $tranCode = ['T945','T856'];
                break;
            case 'BOL':
                $tranCode = ['T858'];
                break;
            case 'INV':
                $tranCode = ['T810'];
                break;
            case 'ORD_UPDATED':
                $tranCode = ['T855'];
                break;
            default:
                $tranCode = '';
                break;
        }

        return Outbounds::with('outboundGroup')
            ->where('client_id', $client_id)
            ->whereIn('transactional_code', $tranCode)
            ->active()
            ->first();
    }

    /**
     * @param $clientId
     * @param $groupId
     * @param $transaction_code
     * @param $transaction_num
     * @return mixed
     */
    public function validateOutboundTransactions($clientId, $groupId, $transaction_code, $transaction_num)
    {
        $results = X12Interchanges::select('t.*')
            ->join('x12_outbound_interchanges AS oi', 'oi.interchange_id', '=', 'x12_interchanges.id')
            ->join('x12_outbound_groups AS g', 'g.interchange_id', '=', 'x12_interchanges.id')
            ->join('x12_outbound_group_revisions AS gr', 'gr.interchange_id', '=', 'oi.id')
            ->join('x12_outbound_transactions AS t', 't.group_id', '=', 'gr.id')
            ->join('outbounds AS ob', 'ob.group_id', '=', 'gr.group_id')
            ->whereRaw('gr.interchange_id = oi.id')
            ->whereRaw('g.id = gr.group_id')
            ->whereRaw('t.group_control_number = gr.control_number')
            ->whereRaw('ob.client_id = x12_interchanges.client_id')
            ->whereRaw('ob.transactional_code = CONCAT("T", t.transaction_code)')
            ->where('x12_interchanges.client_id', $clientId)
            ->where('oi.client_id', $clientId)
            ->where('g.id', $groupId)
            ->where('gr.status', '=', 'Open')
            ->where('t.transaction_code', '=', $transaction_code)
            ->where('t.success_number', '=', $transaction_num)
            ->groupBy('gr.id')
            ->first();

        return $results;
    }

    public function addNewTransactions($client, $data, $successKey, $outbounds)
    {
        $transaction_code = '858';
        $data[$successKey]['data_output'] = $data[$successKey];
        $data[$successKey]['status_wms'] = 'NEW';
        $data[$successKey]['status_code'] = '200';
        $data[$successKey]['message'] = 'Successfully';
        $data[$successKey]['data']['success_number'] = $successKey;

        if (! isset($outbounds->outboundGroup->id)) {
            logger('Outbound Group not found');
            return false;
        }

        $isTrans = $this->validateOutboundTransactions($client->id, $outbounds->outboundGroup->id, $transaction_code, $successKey);

        if (!$isTrans) {
            try {
                DB::beginTransaction();
                $this->createOutbound([], 1, $data[$successKey], $client, $outbounds->outboundGroup);

                DB::commit();
            } catch (\Exception $e) {

                DB::rollback();
                $messageHandle = [
                    'Message: ' . $e->getMessage(),
                    'File:' . __FILE__,
                    'Class:' . __CLASS__,
                    'Function:' . __FUNCTION__,
                    'Line Number:' . __LINE__
                ];

                logger($messageHandle);
            }
        }
    }

    /**
     * @param $whs_id
     * @param $cus_id
     * @param $transactional
     * @param $data
     * @param $successKey
     * @return bool
     */
    public function updateOutboundData($whs_id, $cus_id, $transactional, $data, $successKey)
    {
        $response = [];
        $client = $this->clientModel->getClient($whs_id, $cus_id);

        if (! $client) {
            logger('Warehouse: '. $whs_id . ' - Client' .  $cus_id . ' not found');

            return false;
        }

        $outbounds = $this->getOutboundGroupByTransactionCode($client->id, $transactional);

        if ($transactional == 'BOL') {
            $this->addNewTransactions($client, $data, $successKey, $outbounds);
        }
        logger('data,successKey'. json_encode($data).$successKey,['type' => ' debug'] );
        if (isset($data[$successKey])) {
            logger('$data[$successKey]'. json_encode($data[$successKey]),['type' => ' debug']);
            $response = $this->obModel->updateDataOutputBySuccessNumber($successKey, json_encode($data[$successKey]), true, $transactional);
            return $response;

        }
        return true;
    }

    //add new function for update outbound data form tms2
    /**
     * @param $transactional
     * @param $data
     * @param $successKey
     * @return bool
     */
    public function updateTMSOutboundData($data, $successKey)
    {
        $response = $this->obModel->updateTMSDataOutputBySuccessNumber($successKey, json_encode($data), true);

        return $response;
    }

    public function downloadFile($inboundTranID)
    {
        $object = $this->getOutboundActivityLogModel($inboundTranID);
        $files = $this->getFileByGroupID($object->group_id);
        if (!$files || empty($files->content)) {
            return response()->json('File Download not found!', 404);
        }

        return [
            'file_name' => $files->file_name,
            'file_content' => $files->content
        ];
    }

    /**
     * @param $inboundTransactionID
     * @return mixed
     */
    public function getOutboundActivityLogModel($inboundTransactionID)
    {
        $result = X12OutboundTrans::where('id', '=', $inboundTransactionID)->first();
        return $result;
    }

    /**
     * @param $groupID
     * @return mixed
     */
    public function getFileByGroupID($groupID)
    {
        $data = X12OutboundTransactionFiles::select('file_name', 'content')->where('inbound_group_id', '=', $groupID)->first();
        return $data;
    }
}
