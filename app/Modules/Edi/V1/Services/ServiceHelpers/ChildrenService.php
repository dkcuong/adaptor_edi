<?php
/**
 * Created by PhpStorm.
 * User: Simon Sai (Duc)
 * Date: 4/22/2016
 * Time: 9:57 AM
 */

namespace App\Modules\Edi\V1\Services\ServiceHelpers;

/**
 * Class ChildrenService
 *
 * @package App\Modules\Edi\V1\Services\ServiceHelpers
 */
class ChildrenService
{

    /**
     * @param array $array
     * @param array $conditions
     * @param array $config
     *
     * @return array
     *
     */
    public function subArrayGet(array $array, array $conditions, array $config = [])
    {
        $child = [];

        $getAll = isset($config['getAll']) ? $config['getAll'] : false;
        $getIndex = isset($config['getIndex']) ? $config['getIndex'] : false;

        foreach ($array as $index => $item) {
            if ($this->conditionCheck($item, $conditions)) {

                if ($getIndex) {
                    $child[] = $index;
                } else {
                    $child[] = $item;
                }

                if ($getAll === false) {
                    break;
                }
            }
        }

        return $child;
    }

    /**
     * @param array $array
     * @param array $conditions
     *
     * @return bool
     */
    public function conditionCheck(array $array, array $conditions)
    {
        $passed = true;
        foreach ($conditions as $index => $condition) {

            if (!isset($array[ $index ]) || $array[ $index ] !== $condition) {
                $passed = false;
                break;
            }
        }

        return $passed;
    }
}