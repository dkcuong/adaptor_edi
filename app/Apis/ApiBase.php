<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 24-Jan-16
 * Time: 9:37 PM
 */

namespace App\Apis;

use App\Clients;
use App\ClientWarehouse;
use App\Modules\Edi\V1\Services\EmailHandlingService;
use App\Utils\WmsApiClient;
use Illuminate\Support\Facades\Log;

class ApiBase
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var WmsApiClient
     */
    protected $client;

    /**
     * @var string
     */
    protected $api;

    /**
     * @var
     */
    protected $cus_id;

    /**
     * @var
     */
    protected $whs_id;

    /**
     * @var
     */
    protected $vendor_id;

    /**
     * @var array
     */
    protected $extra_option = [];
    /**
     * @var array
     */
    protected $clientWarehouses = [];
    public $emailHandler;
    private $setting;

    /**
     * ApiBase constructor.
     * @param WmsApiClient $client
     * @param $config
     */
    public function __construct(WmsApiClient $client, $config)
    {
        $this->setConfig($config);

        $this->client = $client;

        $this->getWarehouseId($this->getClient());

        $this->emailHandler =  new EmailHandlingService();
    }

    /**
     * @param $key
     * @param string $default
     * @return array
     */
    public function getConfig($key, $default = '')
    {
        return isset($this->config[$key]) ? $this->config[$key] : $default;
    }

    /**
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function post($data)
    {
//        $d1 = $data;
        $this->makeClientWarehouse($data);

        $api = $this->getApiEndPoint();

        $clientShortName = $this->getConfig('client_short_name');

        if ((! $this->whs_id || ! $this->cus_id) &&
            ! in_array($clientShortName,  config('client.move_file_client')))  // will be remove this lines after done migrate
        {
            $clientID = $this->getClient();

            //$this->emailHandler->sendEmail(['Warehouse_id and Customer ID are not found']);
            $errLogMess = "Warehouse_id and Customer ID are not found. Client ID: $clientID . Client Short Name: $clientShortName";
            logger(['api' => $api,'data' => $data, 'error' => $errLogMess], ['API End Point']);
            return [
                'missing_wh' => true,
                'message' => $errLogMess
            ];
        }

        if (! empty($data['vendor_id']) && (int) $data['vendor_id']  == (int) EmailHandlingService::$TLI_CA_VENDOR_ID && env('APP_ENV') == 'production') {
            EmailHandlingService::$TLI_CA_Email_Flag = true;
        }

        logger(['api' => $api,'data' => $data], ['API End Point']);

        try {
            if(empty($this->getConfig('custom_required'))){
                $data['custom_required'] = [];
            } else{
                $data['custom_required'] = json_decode($this->getConfig('custom_required'));
            }
            if(empty($this->getConfig('inbound_custom_required'))) {
                $data['inbound_custom_required'] = [];
            } else {
                $data['inbound_custom_required'] = json_decode($this->getConfig('inbound_custom_required'));
            }
//            dd($data);
//$append = '{
//    "cus_id": 11298,
//    "vendor_ids": [
//                11298
//            ],
//    "whs_id": 4,
//    "extra_option": {
//                "create_sku": 0,
//        "compare_upc": 0
//    },
//    "custom_required": [
//                "ordered_upc"
//            ],
//    "inbound_custom_required": []
//}';
//$append = json_decode($append, true);
//$data = array_merge($data, $append);

//            $data =
//'{'.$d1.','.
//    '"cus_id": 11298,
//    "vendor_ids": [
//        11298
//    ],
//    "whs_id": 4,
//    "extra_option": {
//        "create_sku": 0,
//        "compare_upc": 0
//    },
//    "custom_required": [
//        "ordered_upc"
//    ],
//    "inbound_custom_required": []
//}';

            //logger(json_encode($data), ['POST API Data Request']);
            return $this->client->post($api, ['json' => $data]);

        } catch (\Exception $e) {
            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            logger($e->getMessage(), ['info' => $errorInfo]);
        }
    }


    /**
     * @param $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get($data)
    {
        $this->makeClientWarehouse($data);

        $api = $this->getApiEndPoint();
        if (! $this->whs_id || ! $this->cus_id) {
            logger($data, ['type' => 'Warehouse_id and Customer ID are not found']);
        }

        $data['cus_id'] = $data['vendor_id'] = $this->cus_id;
        $data['whs_id'] = $this->whs_id;

        logger(['api' => $api,'data' => json_encode($data)], ['API End Point']);

        try {
            //logger(json_encode($data), ['Get API Data Request']);
            return $this->client->get($api, ['json' => $data]);

        } catch (\Exception $e) {
            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            logger($e->getMessage(), ['info' => $errorInfo]);
        }
    }

    /**
     * Group by $Key - Group in field name by $groupInField
     * Group with all fields in $fieldInLoop
     * 
     * @param $data
     * @param $key
     * @param $groupInField
     * @param array $fieldInLoop
     * @return array
     */
    protected function groupBy($data, $key, $groupInField, $fieldInLoop = [])
    {
        $result = $storage = [];
        $autoIndex = 0;

        foreach ($data as $index => $item) {
            $storage[$item[$key]][] = $index;
        }

        foreach ($storage as $index => $items) {
            $autoIndex++;
            $result[$autoIndex] = $data[$items[0]];

            $result[$autoIndex][$groupInField] = array_map(function ($item) use ($data, $groupInField, $fieldInLoop) {
                foreach ($fieldInLoop as $fieldKey => $field) {
                    if (isset($data[$item][$field])) {

                        $child[$field] = $data[$item][$field];
                    }
                }
                return $child;
            }, $items);
        }

        return $result;
    }

    /**
     * @param $client
     * @return mixed
     */
    public function getWarehouseId($client)
    {
        $client = Clients::where('id', $client)->first();

        $clientWarehouses =  ClientWarehouse::with('client')
                        ->with('warehouseTypes')
                        ->where('client_id', $client->id)
                        ->active()
                        ->get();
        $this->clientWarehouses = $clientWarehouses->toArray();
        /*foreach ($result as $clientData) {
            $clientArr = $clientData->toArray();

            $clientArr['cus_id'] = $clientArr['customer_id'];
            $clientArr['whs_id'] = $clientArr['warehouse_id'];
            unset($clientArr['client']);
            if ($clientArr['default']) {
                $this->clientWarehouses['default'] = $clientArr;

                $this->cus_id = $this->vendor_id = $clientArr['customer_id'];
                $this->whs_id = $clientArr['warehouse_id'];

            } else {
                $this->clientWarehouses[]= $clientArr;
            }

            if(empty($this->cus_id)){
                $this->cus_id = $clientArr['cus_id'];
                $this->whs_id = $clientArr['whs_id'];
            }
        }

        // Only TLI remove default wh
        if(in_array($client->short_name, config('client.disabled_wh_default'))) {
            //remove default:
            $this->cus_id = null;
            $this->whs_id = null;
        }*/
    }

    /**
     * @return array
     */
    public function getClient()
    {
        return $this->getConfig('client');
    }

    /**
     * @return string
     */
    protected function getApiEndPoint()
    {
        $listApi = $this->getConfig('apiEndPoints');
        if (! isset($listApi['wms_api'])) {
            logger('API END POINT NOT FOUND');
            return;
        }
        $domain = $listApi['wms_api']['api_endpoint'];

        if (! isset($this->getConfig('inbound')->api->url)) {
            return $domain . $this->api;
        }

        $endpoint = $domain . $this->getConfig('inbound')->api->url;

        return $endpoint;
    }

    private function validateStructure($dataReturns)
    {
        return true;
    }

    private function makeClientWarehouse(&$data)
    {
        $vendor_ids = [];
        $errors = $validClient = false;

        //Only X12 file has data_full
        $data_full = $this->getConfig('data_full');
        //Add extra option
        //Anh.Cao 20180222
        $edi_inbound = $this->getConfig('inbound');
        $this->extra_option['create_sku'] = $edi_inbound->is_createSKU;
        $this->extra_option['compare_upc'] = $edi_inbound->is_compare;

        if(count($this->clientWarehouses) > 1) {
            foreach ($this->clientWarehouses as $clientWarehouse) {
                if (! empty($data_full)) {
                    $interchange  = !empty($data_full['interchange']) ? $data_full['interchange'] : [];

                    if(isset($interchange[$clientWarehouse['cond']])){
                        $data[$clientWarehouse['cond']] = $interchange[$clientWarehouse['cond']];
                    }
                }
                $vendor_ids[] = $clientWarehouse['customer_id'];
                if(isset($data[$clientWarehouse['cond']]) &&
                    trim($data[$clientWarehouse['cond']]) == $clientWarehouse['cond_value']){
                    $this->cus_id = $clientWarehouse['customer_id'];
                    $this->whs_id = $clientWarehouse['warehouse_id'];
                }
            }
        }else if(count($this->clientWarehouses) == 1) {
            $clientWarehouse = array_shift($this->clientWarehouses);
            $vendor_ids[] = $clientWarehouse['customer_id'];
            $this->cus_id = $clientWarehouse['customer_id'];
            $this->whs_id = $clientWarehouse['warehouse_id'];
        }

        $data['cus_id'] = $data['vendor_id'] = $this->cus_id;
        $data['vendor_ids'] = $vendor_ids;
        $data['whs_id'] = $this->whs_id;
        $data['extra_option'] = $this->extra_option;
    }
}
