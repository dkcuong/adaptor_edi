<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStatusFieldsX12GroupsAndTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `x12_inbound_groups`
            CHANGE `status` `status` VARCHAR(20) NOT NULL');
        ;
        DB::statement('ALTER TABLE `x12_inbound_group_revisions`
        CHANGE `status` `status` VARCHAR(20) NOT NULL');
        DB::statement('ALTER TABLE `x12_inbound_transactions`
            CHANGE `status` `status` VARCHAR(20) NOT NULL');
        DB::statement('ALTER TABLE `x12_outbound_groups`
            CHANGE `status` `status` VARCHAR(20) NOT NULL');
        DB::statement('ALTER TABLE `x12_outbound_group_revisions`
            CHANGE `status` `status` VARCHAR(20) NOT NULL');
        DB::statement('ALTER TABLE `x12_outbound_transactions`
            CHANGE `status` `status` VARCHAR(20) NOT NULL');
        ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
