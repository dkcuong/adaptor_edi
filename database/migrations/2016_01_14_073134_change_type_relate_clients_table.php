<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeRelateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->string('name', 100)->change();
            $table->string('short_name', 10)->change();
        });
        
        Schema::table('client_types', function(Blueprint $table) {
            $table->string('name', 50)->change();
            $table->string('short_name', 10)->change();
        });
        
        Schema::table('addresses', function(Blueprint $table) {
            $table->string('street', 55)->change();
            $table->string('department', 55)->change();
        });
        
        Schema::table('address_types', function(Blueprint $table) {
            $table->string('name', 50)->change();
            $table->string('short_name', 10)->change();
        });
        
        Schema::table('locations', function(Blueprint $table) {
            $table->string('city_name', 30)->change();
            $table->string('state_province_code', 3)->change();
            $table->string('state_province_name', 30)->change();
            $table->string('location_identifier', 30)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            //
        });
    }
}
