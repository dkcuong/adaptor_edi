<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/02/2016
 * Time: 16:41
 */

namespace App\Modules\Edi\V1\Services;

use App\Modules\Edi\V1\Headers\X12Transaction;
use App\Modules\Edi\V1\Models\InboundModel;
use App\Modules\Edi\V1\Models\OutboundModel;
use App\Modules\Edi\V1\Services\ServiceHelpers\LogService;
use App\X12InboundGroupRevisions;
use App\X12InboundGroups;
use App\X12InboundTransactions;
use App\X12Interchanges;
use App\X12InboundInterchange;
use App\X12InboundTransactionFiles;
use Illuminate\Support\Facades\DB;

class InboundService
{
    /**
     * @var
     */
    protected $inbound;
    /**
     * @var InboundModel
     */
    protected $ibModel;
    /**
     * @var
     */
    protected $inboundData;
    /**
     * @var
     */
    protected $inter;
    /**
     * @var
     */
    protected $ibGroup;

    /**
     * @var EmailHandlingService
     */
    public $emailHandling;

    public $obSer;

    public $obModel;

    /**
     * InboundService constructor.
     * @param InboundModel $inboundModel
     * @param OutboundModel $outboundModel
     * @param OutboundService $outboundService
     */
    public function __construct(
        InboundModel $inboundModel,
        OutboundModel $outboundModel,
        OutboundService $outboundService,
        EmailHandlingService $emailHandlingService
    ) {
        $this->ibModel = $inboundModel;
        $this->obModel = $outboundModel;
        $this->emailHandling = $emailHandlingService;
        $this->obSer = $outboundService;
    }

    /**
     * @param $interchange
     * @param $groupCode
     * @return null
     */
    public function getInboundGroupByGroupCode($interchange, $groupCode)
    {
        $result = X12InboundGroups::where('interchange_id', $interchange['id'])
            ->where('group_code', $groupCode)
            ->first();

        return $result ? $result->toArray() : null;
    }

    /**
     * @param $groupId
     * @param $interchangeId
     * @return mixed
     */
    public function getLastGroupRevision($groupId, $interchangeId)
    {
        return X12InboundGroupRevisions::where('group_id', $groupId)
            ->where('interchange_id', $interchangeId)
            ->max('control_number');
    }

    /**
     * @param $inboundInterchangeId
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getInboundInterchange($inboundInterchangeId)
    {
        return X12InboundInterchange::find($inboundInterchangeId);
    }

    /**
     * @param $groupId
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getInboundTransactionFile($groupId)
    {
        return X12InboundTransactionFiles::where('inbound_group_id', $groupId)->first();
    }

    /**
     * @param $clientId
     * @param $groupId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getInboundGroups($clientId, $groupId)
    {
        return $this->ibModel->getInboundGroups($clientId, $groupId);
    }

    /**
     * @param $groupId
     * @param $controlNumber
     * @return mixed
     */
    public function getReProcessInboundTrans($groupId, $controlNumber)
    {
        return X12InboundTransactions::where('group_id', $groupId)
            ->where('group_control_number', $controlNumber)
            ->where('status', 'Re-process')
            ->get();
    }

    /**
     * @param $groupId
     * @param $interchangeId
     * @return mixed
     */
    public function getInboundGroupRevisions($groupId, $interchangeId)
    {
        return X12InboundGroupRevisions::where('group_id', $groupId)
            ->where('inbound_interchange_id', $interchangeId)
            ->get();
    }

    /**
     * @param $groupId
     * @param $controlNumber
     * @return mixed
     */
    public function getInboundTrans($groupId, $controlNumber)
    {
        return X12InboundTransactions::where('group_id', $groupId)
            ->where('group_control_number', $controlNumber)
            ->first();
    }

    /**
     * @param $parseResults
     * @param $edi
     * @param $client
     * @param $file
     * @param $outboundGroup
     * @param string $processKey
     * @return array
     */
    public function create($parseResults, $edi, $client, $file, $outboundGroup, $processKey = '')
    {
        $logService = new LogService;
        $originContent = $edi->ftp->parseFile($file);
        $file = str_replace('#pKey#' . $processKey . '#', '', $file);
        $parseHeaders = isset($parseResults['originalParse']) ?
        $parseResults['originalParse'] : [];

        if (!isset($parseResults['input']) || !isset($parseResults['output'])) {
            return [];
        }
        $dataInputs = $parseResults['input'];
        $dataOutputs = $parseResults['output'];
        $conNum = 0;

        $rsaData = $this->setRsaInsert($parseHeaders, $dataOutputs, $edi, $client, $file);
        $interchange = $rsaData['interchange'];
        $inboundGroup = $rsaData['inboundGroup'];
        $status = $rsaData['status'] ?: '';
        $results['status'] = $statusOutput = false;

        foreach ($dataInputs as $index => $tran) {
            if (isset($dataOutputs[$index]['data']['success_number'])) {
                $successNumbers[$index] = $dataOutputs[$index]['data']['success_number'];
            }
        }
        try {
            DB::beginTransaction();
            $groupInsert = $this->insertRsa($interchange, $inboundGroup, $file, $originContent);
            $results['fileName'] = $groupInsert['fileName'];
            $results['tran_number'] = $groupInsert['tran_number'];
            $results['control_number'] = $groupInsert['control_number'];
            foreach ($dataInputs as $key => $dataInput) {
                $logService->setProcessKey($processKey);
                $logService->addLogMessage('Processing inbound transactions...');
                $conNum++;
                $message = $rsaData['message'];
                $dataOutput = isset($dataOutputs[$key]) ? $dataOutputs[$key] : null;
                if (($dataInput && $dataOutput['status_code'] != 200) || !$dataInput) {
                    if (isset($dataOutput['data']['re_process'])) {
                        $status = $dataOutput['data']['re_process'] == true ? 'Re-process' : 'Error';
                        unset($dataOutput['data']['re_process']);
                    } elseif (isset($dataOutput['data']['status_type']) &&
                        ($dataOutput['data']['status_type'] == SHIPMENT_INVALID_SKU_CODE ||
                            $dataOutput['data']['status_type'] == SHIPMENT_LACK_INVENTORY_CODE)) {
                        $status = 'Re-process';
                    }
                    else {
                        $status = 'Error';
                    }
                    $logService->setStatus(0);
                } else {
                    $status = 'Received';
                }

                $dataOutput['status'] = $status;
                $logService->addLogMessage($dataOutput['status']);
                if (isset($dataOutput['message']) && is_array($dataOutput['message']) && ! $message) {
                    $logService->addLogMessages($dataOutput['message']);
                    $message = array_shift($dataOutput['message']);
                } else {
                    $message = $message ?: (isset($dataOutput['message']) ? $dataOutput['message']: '');
                    $logService->addLogMessage($message);
                }

                $dataOutput['message'] = $message;

                $results['inbound_tran_id'] = $this->ibModel->insertInboundTransactions($edi, $groupInsert, $dataInput, $dataOutput, $conNum);
                if ($results['inbound_tran_id']) {
                    $logService->setTranId($results['inbound_tran_id']);
                    $logService->writeLog('inbound', true);
                }
                if ($outboundGroup && $dataOutput['status'] == 'Received') {
                    $this->obSer->createOutbound($dataInput, $key, $dataOutput, $client, $outboundGroup, $parseHeaders);
                }
            }
            DB::commit();
        } catch (\Exception $e) {

            DB::rollback();
            $messageHandle = [
                'Message: ' . $e->getMessage(),
                'File:' . __FILE__,
                'Class:' . __CLASS__,
                'Function:' . __FUNCTION__,
                'Line Number:' . __LINE__
            ];

            logger($messageHandle);
            $this->emailHandling->sendEmail([$messageHandle]);
        }

        return $results;
    }

    /**
     * @param $interchange
     * @param $inboundGroup
     * @param $file
     * @return array|bool
     */
    public function insertRsa($interchange, $inboundGroup, $file, $content = '')
    {
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $ext = '.' . pathinfo($file, PATHINFO_EXTENSION);
        $interId = $this->ibModel->insertInboundInterchange($interchange);
        if (!$interId) {
            return false;
        }
        $this->ibModel->updateInboundGroup($interchange, $inboundGroup);
        $results = $this->ibModel->insertInboundGroupRevisions($interId, $inboundGroup, $interchange);

        $results['fileName'] = $fileName . '-' . sprintf('%09d', $interchange['control_number'])
            . '-' . $results['group_control_number'] . $interId . $ext;
        $results['control_number'] = sprintf('%09d', $interchange['control_number']);
        $results['tran_number'] = sprintf('%09d', $interchange['control_number'])
            . '-' . $results['group_control_number'] . $interId;

        $this->ibModel->insertTransactionFiles($results, $file, $content);

        return $results;
    }

    /**
     * @param $parseHeaders
     * @param $dataOutputs
     * @param $edi
     * @param $client
     * @return array
     */
    public function setRsaInsert($parseHeaders, $dataOutputs, $edi, $client)
    {
        $interchange = $inboundGroup = $dataErrors = [];
        $status = $message = null;

        if (isset($parseHeaders['interchange']) && isset($parseHeaders['groups'])) {
            $groups = $edi->inboundGroup->toArray();
            $interchangeIn = $parseHeaders['interchange'];
            $interchange = $client->rsa->toArray();
            $interchange['client_id'] = $client->id;
            $interchange['id'] = $client->rsa->id;
            $interchange['interchange_id'] = $client->rsa->id;
            $interchange['control_number'] = $interchangeIn['control_number'];

            $inboundGroup = array_shift($parseHeaders['groups']);
            $inboundGroup['id'] = $groups['id'];
            if (isset($dataOutputs['error']) && $dataOutputs['error']) {
                $dataErrors = $dataOutputs['error'];
            }
            if ($dataErrors) {
                $status = 'Error';
                $message = array_shift($dataErrors['messages']);
            }
            $interchange['status'] = $status ? $status : '';
        } else {
            $interchange = $client->rsa->toArray();

            $inboundGroup = $edi->inboundGroup->toArray();
            $nextConNum = $this->ibModel->getNextIsaConNum($interchange);

            $interchange['control_number'] = $nextConNum;
        }
        return [
            'interchange' => $interchange,
            'inboundGroup' => $inboundGroup,
            'status' => $status,
            'message' => $message,
        ];
    }

    public function downloadFile($inboundTranID)
    {
        $object = $this->getInboundActivityLogModel($inboundTranID);
        $files = $this->getFileByGroupID($object->group_id);
        if (!$files || empty($files->content)) {
            return response()->json('File Download not found!', 404);
        }

        return [
            'file_name' => $files->file_name,
            'file_content' => $files->content
        ];
    }

    /**
     * @param $inboundTransactionID
     * @return mixed
     */
    public function getInboundActivityLogModel($inboundTransactionID)
    {
        $result = X12InboundTransactions::where('id', '=', $inboundTransactionID)->first();
        return $result;
    }

    /**
     * @param $groupID
     * @return mixed
     */
    public function getFileByGroupID($groupID)
    {
        $data = X12InboundTransactionFiles::select('file_name', 'content')->where('inbound_group_id', '=', $groupID)->first();
        return $data;
    }
}
