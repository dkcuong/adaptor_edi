@include('emails.template')
@if(isset($data[0]['reference']))
    <table border="1" cellspacing="0" style="border-collapse: collapse">
        <thead>
        <tr>
            <th>Ref#</th>
            <th>ContainerNum</th>
            <th>LOT#</th>
            <th>Status</th>
            <th>Message</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <?php
            $status = '';
            $statusType = '';
            $message = '';
            $color = '';
            $item['status_code'] = isset($item['status_code']) ? $item['status_code'] : '';
            if ($item['status_code'] == '200') {
                $status = 'Completed';
                $statusType = 'CC';
                $message = 'Your Receiving has been created successfully';
                $color = '#4CAE4C';
            } elseif (isset($item['data']['re_process'])) {
                $status = 'Reprocess';
                $statusType = 'RP';
                $message = is_array($item['message']) ? implode('<br/>', $item['message']) :
                    $item['message'];
                $color = '#FF9900';
            } else {
                $status = 'Error';
                $statusType = 'ER';
                $message = is_array($item['message']) ? implode('<br/>', $item['message']) :
                    $item['message'];
                $color = 'red';
            }
            ?>
            <tr>
                <th style="padding:0 15px">{{$item['reference']}}</th>
                <th style="padding:0 15px">{{$item['container']}}</th>
                <th style="padding:0 15px">{{$item['lot']}}</th>
                <th style="padding:0 15px">{{$status}}</th>
                <th style="color: {{$color}};padding:0 15px">{{$message}}</th>
                <th></th>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <table border="1" cellspacing="0" style="border-collapse: collapse">
        <thead>
        <tr>
            <th>PO</th>
            <th>Client PO</th>
            <th>WMS Order Number</th>
            <th>Status</th>
            <th>Message</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $index => $item)
            <tr>
                <?php
                    $item['data']['status_type'] = isset($item['data']['status_type']) ? $item['data']['status_type'] : '';
                    if ($item['data']['status_type']) {

                        logger('Missing Status Response');
                    }
                    $result = format_data_940($item['data'], $item['message'], $data);
                    logger(json_encode($result), ['data' => 'Data re-process']);
                    $color = format_color_940($item['data']['status_type']);
                ?>
                <th style="color: {{$color['po']}}; font-weight:normal; padding:0 15px">{{$result['po']}}</th>
                <th style="color: {{$color['client_po']}}; font-weight:normal; padding:0 15px">{{$result['client_po']}}</th>
                <th style="color: {{$color['order_number']}}; font-weight:normal; padding:0 15px">
                    {{$result['order_number']}}</th>
                <th style="color: {{$color['status_type'] }}; font-weight:normal; padding:0 15px">{{$result['status']}}</th>
                <th style="color: {{$color['message']}}; font-weight:normal; padding:0 15px">{{$result['message']}}</th>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif