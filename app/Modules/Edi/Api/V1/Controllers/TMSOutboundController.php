<?php

namespace App\Modules\Edi\Api\V1\Controllers;

use App\Clients;
use App\Http\Requests\Request;
use app\Modules\Edi\Api\V1\Requests\OutboundRequests;
use App\Modules\Edi\V1\Models\ClientModel;
use App\Modules\Edi\V1\Services\OutboundService;
use App\Http\Controllers\Controller;
use App\Modules\Edi\V1\Models\OutboundModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class TMSOutboundController extends Controller
{

    /**
     *
     */
    const API_APP = 'edi';

    /**
     * @var OutboundModel
     */
    protected $outboundModel;

    /**
     * @var ClientModel
     */
    protected $clientModel;

    /**
     * @var OutboundService
     */
    protected $outboundSer;

    function __construct(
        OutboundModel $outboundModel,
        ClientModel $clientModel,
        OutboundService $outboundService
    )
    {
        $this->outboundModel = $outboundModel;
        $this->clientModel = $clientModel;
        $this->outboundSer = $outboundService;
    }

    public function updateOutput(\Illuminate\Http\Request $request)
    {
        $inputs = $request->all();
        logger('--------------- request input here -------------');
        logger($inputs);
        $api_app = $inputs['api_app'];

        $tms_api_key = env('TMS_API_KEY', '');

        if ($api_app != $tms_api_key)
        {
            logger('wrong api key');
            return;
        }

        $transactional = isset($inputs['transaction_code']) ? $inputs['transaction_code'] : '';
        $data = isset($inputs['data']) ? $inputs['data'] : '';

        if (!$transactional || !$data) {
            logger('empty transaction code or data');
            return;
        }

        foreach ($data as $dt) {
            if ($transactional == 210) {
                $successKey = isset($dt['job_ord_hdr_num']) ? $dt['job_ord_hdr_num'] : '';
            } else {
                $successKey = isset($dt['job_ord_dtl_id']) ? $dt['job_ord_dtl_id'] : '';

            }
            if (!$successKey) {
                logger('empty success key');
                return;
            }

            $dt['isSuccess'] = true;

            $response = $this->outboundSer->updateTMSOutboundData($dt, $successKey);
        }


        return $response;
    }
    public function getFileStorage($filePath, $fileName)
    {
        return Response::download(storage_path('app/dms-transactions/'). $filePath, $fileName);
    }

}
