<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/02/2016
 * Time: 16:53
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class X12InboundInterchange extends Model
{
    /**
     * @var string
     */
    protected $table = 'x12_inbound_interchanges';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'auth_qualifier',
        'auth_info',
        'security_qualifier',
        'security_info',
        'sender_qualifier',
        'sender_code',
        'receiver_qualifier',
        'receiver_code',
        'date',
        'time',
        'repetition_separator',
        'control_version',
        'control_number',
        'requested',
        'usage',
        'separator',
        'status',
        'client_id',
        'interchange_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inboundGroups()
    {
        return $this->hasMany(X12InboundGroups::class, 'interchange_id');
    }
}
