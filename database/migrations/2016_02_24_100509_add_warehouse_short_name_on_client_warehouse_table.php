<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWarehouseShortNameOnClientWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `client_warehouse` 
            ADD `warehouseShortName` VARCHAR(3) AFTER `warehouseName`');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_warehouse', function (Blueprint $table) {
            //
        });
    }
}
