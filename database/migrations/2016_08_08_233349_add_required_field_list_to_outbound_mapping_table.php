<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequiredFieldListToOutboundMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outbound_mapping', function (Blueprint $table) {
            $table->text('custom_required_field')->after('fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outbound_mapping', function (Blueprint $table) {
            $table->dropColumn('custom_required_field');
        });
    }
}
