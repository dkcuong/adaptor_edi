<?php

namespace App\Mappers\Outbound;

use App\Mappers\MapperBase;

class HEDAYMapper extends MapperBase
{
    const AI_CODE = '00';
    const CUSTOMER_CODE = '73573280';

    /**
     * Mapper data for Concept One customer
     * Edi945
     *
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map945($items, $conditions)
    {

        $results[] = $this->parseTree($items);

        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parseTree($tranData)
    {
        $tran1 = ['transaction_code' => '945'];
        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];
        $additional = isset($order_meta['additional']) ? $order_meta['additional'] : [];
        $ship_to = isset($order_meta['ship_to']) ? $order_meta['ship_to'] : '';
        $ship_from = isset($order_meta['ship_from']) ? $order_meta['ship_from'] : '';
        $bill_to = isset($order_meta['bill_to']) ? $order_meta['bill_to'] : [];
        $bol_to = isset($order_meta['bol_to']) ? $order_meta['bol_to'] : [];
        $third_party = isset($order_meta['3rd_party']) ? $order_meta['3rd_party'] : [];

        //W06
        $tran1[] = [
            'segment'        => 'W06',
            'report_code'    => 'J', // report code
            'client_order'   => $tranData['client_pick_ticket'], // pick ticket number
            'date'           => (isset($tranData['order_date']) && !empty($tranData['order_date'])) ? date('Ymd', strtotime($tranData['order_date'])) : '', // creation date
            'shipment_id'    => $tranData['bolnumber'],
            'agent_id'       => '',
            'purchase_order' => $tranData['customerordernumber']
        ];

        //Ship To
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'ST',
            'name'         => isset($ship_to['name']) ? $ship_to['name'] : $tranData['shiptoname'],
            'id_qualifier' => isset($ship_to['qualifier']) ? $ship_to['qualifier'] : 'US',
            'id'           => isset($ship_to['id']) ? $ship_to['id'] : '0000',
        ];

        $tran1[] = [
            'segment'      => 'N3',
            'name '        => isset($ship_to['address']) ? $ship_to['address'] : $tranData['shipping_address_street'],
        ];
        $ship_to_city = isset($ship_to['city']) ? $ship_to['city'] : $tranData['shiptocity'];
        $ship_to_state = isset($ship_to['state']) ? $ship_to['state'] : $tranData['shiptocity'];
        $ship_to_code = isset($ship_to['code']) ? $ship_to['code'] : $tranData['shiptozip'];
        $ship_to_country = isset($ship_to['country']) ? $ship_to['country'] : $tranData['shiptocountry'];
        $tran1[] = [
            'segment'           => 'N4',
            'name'              => substr($ship_to_city, 0, 30),
            'province_code'     => substr($ship_to_state, 0, 2),
            'postal_code'       => substr($ship_to_code, 0, 15),
            'country_code'       => substr($ship_to_country, 0, 3),
        ];

        // Bill to
        if (! empty($bill_to['name'])) {
            $tran1[] = [
                'segment'      => 'N1',
                'entity_code'  => 'BT',
                'name'         => $bill_to['name'],
                'id_qualifier' => $bill_to['qualifier'],
                'id'           => $bill_to['id'],
            ];
            $tran1[] = [
                'segment'      => 'N3',
                'name '        => isset($bill_to['address']) ? $bill_to['address'] : '',
                'address'      => '',
            ];
            $tran1[] = [
                'segment'           => 'N4',
                'name'              => substr($bill_to['city'], 0, 30),
                'province_code'     => substr($bill_to['state'], 0, 2),
                'postal_code'       => substr($bill_to['code'], 0, 15),
                'country_code'       => substr($bill_to['country'], 0, 3),
            ];
        }

        //$third_party
        if (! empty($third_party['name'])) {
            $tran1[] = [
                'segment'      => 'N1',
                'entity_code'  => 'TP',
                'name'         => $third_party['name'],
                'id_qualifier' => $third_party['qualifier'],
                'id'           => $third_party['id'],
            ];
            $tran1[] = [
                'segment'      => 'N3',
                'name '        => isset($third_party['address']) ? $third_party['address'] : '',
                'address'      => '',
            ];
            $tran1[] = [
                'segment'           => 'N4',
                'name'              => substr($third_party['city'], 0, 30),
                'province_code'     => substr($third_party['state'], 0, 2),
                'postal_code'       => substr($third_party['code'], 0, 15),
                'country_code'       => substr($third_party['country'], 0, 3),
            ];
        }

        if (! empty($bol_to['name'])) {
            $tran1[] = [
                'segment'      => 'N1',
                'entity_code'  => 'OB',
                'name'         => $bol_to['name'],
                'id_qualifier' => $bol_to['qualifier'],
                'id'           => $bol_to['id'],
            ];
        }

        //Ship From
        if (!empty($ship_from['name'])) {
            $tran1[] = [
                'segment' => 'N1',
                'entity_code' => 'SF',
                'name' => $ship_from['name'],
            ];
            $tran1[] = [
                'segment' => 'N3',
                'address' => isset($ship_from['address']) ? $ship_from['address'] : '',
            ];
            $ship_from_city = isset($ship_from['city']) ? $ship_from['city'] : '';
            $ship_from_state = isset($ship_from['state']) ? $ship_from['state'] : '';
            $ship_from_code = isset($ship_from['code']) ? $ship_from['code'] : '';
            $ship_from_country = isset($ship_from['country']) ? $ship_from['country'] : '';

            $tran1[] = [
                'segment' => 'N4',
                'name' => substr($ship_from_city, 0, 30),
                'province_code' => substr($ship_from_state, 0, 2),
                'postal_code' => substr($ship_from_code, 0, 15),
                'country_code' => substr($ship_from_country, 0, 3),
            ];
        }

        // N9 - BM - Bill of Lading
        if (!empty($tranData['bolnumber'])) {
            $tran1[] = [
                'segment' => 'N9',
                'entity_code' => 'BM',
                'ref_qualifier' =>$tranData['bolnumber']
            ];
        }

        // N9 - CN - Carrier Pro Number
        if (!empty($tranData['pronumber'])) {
            $tran1[] = [
                'segment' => 'N9',
                'entity_code' => 'CN',
                'ref_qualifier' => $tranData['pronumber']
            ];
        }

        // N9 - SN - Pickup reference
        if (!empty($tranData['sealnumber'])) {
            $tran1[] = [
                'segment' => 'N9',
                'entity_code' => 'SN',
                'ref_qualifier' => $tranData['sealnumber']
            ];
        }

        //FC - Freight charge
        if (!empty($tranData['freightchargeterminfo'])) {
            $tran1[] = [
                'segment' => 'N9',
                'entity_code' => 'FC',
                'ref_qualifier' => $tranData['freightchargeterminfo']
            ];
        }

        //TN - Small Package Carrier Tracking Number
        if (!empty($tranData['trackingNumber'])) {
            $tran1[] = [
                'segment' => 'N9',
                'entity_code' => 'TN',
                'ref_qualifier' => $tranData['trackingNumber']
            ];
        }

        //G62 - 10 - Shipped date
        $tran1[] = [
            'segment'        => 'G62',
            'date_qualifier' => '10',
            'date'           => date('Ymd', strtotime($tranData['start_ship_date'])),
        ];

        //G62 17 - Estimated Delivery Date
        $tran1[] = [
            'segment'        => 'G62',
            'date_qualifier' => '17',
            'date'           => date('Ymd', strtotime($tranData['canceldate'])),
        ];

        //W27 - Carrier Detail
        $tran1[] = [
            'segment'               => 'W27',
            'transportation_method' => getArr($additional, 'transportation_method', 'U'),
            'carrier_code'          => getArr($additional, 'carrier_code', 'UPGN'),
            'routing'               => getArr($additional, 'routing', 'UPS GROUND UG'),
        ];

        //LX*1~
        $tran1[] = [
            'segment' => 'Lx',
        ];

        //MAN*GM
        if (in_array(strtoupper($tranData['carrier']), ['UPS', 'FEDEX'])) {
            $tran1[] = [
                'segment'     => 'MAN',
                'qualifier1'  => 'GM',
                'number2'     => self::AI_CODE . generateUCC128('00'. self::CUSTOMER_CODE, $tranData['orderID']),
                'number3'     => $tranData['trackingNumber']
            ];
        } else {
            $tran1[] = [
                'segment'     => 'MAN',
                'qualifier1'     => 'GM',
                'number2'     => self::AI_CODE . generateUCC128('00'. self::CUSTOMER_CODE, $tranData['orderID']),
            ];
        }

        $total_carton = 1;
        foreach ($tranData['items'] as $item_id => $item) {

            foreach ($item['cartons'] as $cart_id => $carton) {
                $product_meta = isset($carton['product_meta']) ? json_decode($carton['product_meta'], true) : [];

                //W12*SH
                $tran1[] = [
                    'segment'          => 'W12',
                    '01' => 'SH',
                    '02' => '',
                    '03' => $carton['uom'],
                    '04' => '',
                    '05' => 'EA',
                    '06' => '',
                    '07' => 'VN',
                    '08' => $product_meta['sku'],
                    '09' => 'UP',
                    '10' => $product_meta['cus_upc'],
                    '11' => '',
                    '12' => '',
                    '13' => '',
                    '14' => '',
                    '15' => '',
                    '16' => '',
                    '17' => '',
                    '18' => '',
                    '19' => 'SZ',
                    '20' => $product_meta['size'],
                    '21' => 'CL',
                    '22' => $product_meta['color']
                ];

                //N9 - OL - AMT Order Line
                $tran1[] = [
                    'segment'           => 'N9',
                    'entity_code'       => 'OL',
                    'ref_qualifier'      => (int) ! empty($carton['product_load_id']) ? $carton['product_load_id'] : 1,
                ];

            }
            $total_carton += $item['cartonCount'];
        }

        $tran1[] = [
            'segment'     => 'W03',
            'unit_number' => $tranData['total_pieces'],
            'weight'      => $tranData['total_weight'],
            'unit_code'   => 'LB',
            'volume'      => $tranData['total_volume'],
            'label_code' => 'CF',

        ];
        return $tran1;
    }
}
