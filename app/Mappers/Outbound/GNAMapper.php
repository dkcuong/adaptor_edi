<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/03/2016
 * Time: 09:22
 */

namespace App\Mappers\Outbound;


use App\Mappers\MapperBase;

class GNAMapper extends MapperBase
{
    /**
     * Mapper data for Concept One customer
     * Edi945
     *
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map945($items, $conditions)
    {
        $results[] = $this->parseTree($items);

        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parseTree($tranData)
    {
        $tran1 = ['transaction_code' => '945'];
        $tran1[] = [
            'segment'        => 'W06',
            'report_code'    => 'F',
            'client_order'   => $tranData['pickid'],
            'date'           => $tranData['orderShipDate'],
            'shipment_id'    => $tranData['bolID'],
            'agent_id'       => '',
            'purchase_order' => $tranData['customerordernumber'],
            'clientordernumber' => $tranData['clientordernumber'],
            'scanordernumber' => $tranData['scanordernumber'],
        ];
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'BT',
            'name'         => 'SELDAT FULFILLMENT SERVICES',
            'id_qualifier' => '92',
            'id'           => '20',
        ];

        $tran1[] = [
            'segment'      => 'N3',
            'address'      => $tranData['shipfromaddress'],
        ];
        $tran1[] = [
            'segment'           => 'N4',
            'name'              => substr($tranData['shipfromaddress'], 0, 30),
            'province_code'     => substr($tranData['shipfromstate'], 0, 2),
            'postal_code'       => substr($tranData['shipfromzip'], 0, 15),
            'country_code'       => substr($tranData['shipfromcity'], 0, 3),
        ];
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'ST',
            'name'         => $tranData['shiptoname'],
            'id_qualifier' => '92',
            'id'           => $tranData['vendorID'],
        ];

        $tran1[] = [
            'segment'      => 'N3',
            'name '        => $tranData['shiptoaddress'],
            'address'      => '',
        ];
        $tran1[] = [
            'segment'           => 'N4',
            'name'              => substr($tranData['shiptoaddress'], 0, 30),
            'province_code'     => substr($tranData['shiptostate'], 0, 2),
            'postal_code'       => substr($tranData['shiptozip'], 0, 15),
            'country_code'       => substr($tranData['shiptocountry'], 0, 3),
        ];
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'MB',
            'ref_qualifier'      => $tranData['bolID'],
        ];
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'BM',
            'ref_qualifier'      => $tranData['bolnumber'],
        ];
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'DP',
            'ref_qualifier'      => $tranData['deptid'],
        ];
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'IA',
            'ref_qualifier'      => $tranData['vendorID'],
        ];
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'CN',
            'ref_qualifier'      => $tranData['pronumber'],
        ];
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'EQ',
            'ref_qualifier'      => $tranData['trailernumber'],
        ];

        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'LO',
            'ref_qualifier'      => '21486863',
        ];
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'LU',
            'ref_qualifier'      => '04303',
        ];

        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'MR',
            'ref_qualifier'      => '0003',
        ];
        $tran1[] = [
            'segment'           => 'N9',
            'entity_code'       => 'PK',
            'ref_qualifier'      => $tranData['clientpickticket'],
        ];

        $tran1[] = [
            'segment'        => 'G62',
            'date_qualifier' => '10',
            'date'           => $tranData['startshipdate'],
        ];

        $tran1[] = [
            'segment'        => 'G62',
            'date_qualifier' => '54',
            'date'           => $tranData['canceldate'],
        ];

        $tran1[] = [
            'segment'               => 'W27',
            'transportation_method' => 'M',
            'carrier_code'          => $tranData['scac'],
            'routing'               => $tranData['carrier'],
            'payment_method'        => $tranData['shipType'],
            'equipment_code'        => '',
            'equipment_init'        => 'SYST',
            'equipment_number'      => 'EM'
        ];

        $tran1[] = [
            'segment'        => 'G72',
            'entity_code' => '504',
            'charge_method' => 'ZZ',
            'charge_number'=> '',
            'exception_number'=> '',
            'charge_rate' => '',
            'charge_qty' => '',
            'measurement_code' => '',
            'charge_amount' => $tranData['freightchargeterm'],

        ];

        $tran1[] = [
            'segment'        => 'LM',
            'date_qualifier' => 'ZZ',
            'date'           => '1',
        ];
        $tran1[] = [
            'segment'        => 'LQ',
            'date_qualifier' => 'ZZ',
            'date'           => '002',
        ];

        $orderStatus = $tranData['orderStatus'];

        foreach ($tranData['items'] as $tran) {
            $tran['status'] = 'CC';
            $shippedQuantity = $tran['product_order']['shipped_quantity'];
            $orderQuantity = $tran['product_order']['order_quantity'];

            if ($shippedQuantity == 0 || $orderStatus == self::CANCEL_ORDER) {
                $tran['status'] = 'BO';
            } else {
                if ($orderQuantity == $shippedQuantity || $orderQuantity < $shippedQuantity) {
                    $tran['status'] = 'CC';
                } else {
                    if ($orderQuantity > $shippedQuantity) {
                        $tran['status'] = 'BP';
                    }
                }
            }

            $tran1[] = [
                'segment' => 'Lx',
            ];

            if (isset($tran['product_order']['ucc128']) && $tran['product_order']['ucc128']) {
                foreach ($tran['product_order']['ucc128'] as $ucc128) {
                    $tran1[] = [
                        'segment'     => 'MAN',
                        'qualifier'   => 'GM',
                        'ref'         => $ucc128,
                        'description' => '',
                    ];
                }
            }
            // W12*CC*1*1*0*EA**VN**********CB*FC153001-DKGR***UP*8502406177510~
            $tran1[] = [
                'segment'          => 'W12',
                'ordered_quantity' => $tran['status'],
                'quantity'         => $tran['product_order']['order_quantity'],
                'unit_number'      => $tran['product_order']['shipped_quantity'] ?: 0,
                'quantity_def'     => $tran['product_order']['diff_quantity'],
                'uom'              => 'EA',
                'upc_1'            => '',
                'qualifier_id1'    => 'VN',
                'product_id1'      => '',
                'warehouse_num'    => '',
                'weight'           => '',
                'weight_type'      => '',
                'weight_unit'      => '',
                'weight_2'         => '',
                'weight_type_2'    => '',
                'weight_unit_2'    => '',
                'upc_2'            => '',
                'qualifier_id2'    => 'CB',
                'product_id2'      => $tran['product_order']['sku'],
                'line_item'        => '',
                'identifier_code'  => '',
                'qualifier_id3'    => 'UP',
                'product_id3'      => $tran['product_order']['upc']
             ];

            $tran1[] = [
                'segment'        => 'G69',
                'item_description' => $tran['product_order']['description'],
            ];

            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'LI',
                'ref_qualifier'      => '1',
                'description'      => '',
            ];
            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'ZZ',
                'ref_qualifier'      => '',
                'description'      => '0.00',
            ];

            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'VD',
                'ref_qualifier'      => $tran['product_order']['volume'],
                'description'      => 'CF',
            ];

            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'I1',
                'ref_qualifier'      => '12',
                'description'      => 'IN',
            ];

            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'I2',
                'ref_qualifier'      => '7.2',
                'description'      => 'LB',
            ];

            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'I3',
                'ref_qualifier'      => '8',
                'description'      => 'IN',
            ];

            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'I',
                'ref_qualifier'      => '',
            ];
        }

        $tran1[] = [
            'segment'     => 'W03',
            'unit_number' => $tranData['totalPieces'],
            'weight'      => $tranData['totalWeight'],
            'unit_code'   => 'LB',
        ];
        return $tran1;
    }
}
