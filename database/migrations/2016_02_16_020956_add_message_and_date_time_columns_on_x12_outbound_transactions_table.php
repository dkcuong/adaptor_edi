<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessageAndDateTimeColumnsOnX12OutboundTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('x12_outbound_transactions', function (Blueprint $table) {
            
            $table->string('message');
            $table->timestamps();
            
        });
        
        DB::statement('ALTER TABLE `x12_outbound_transactions` 
            ADD `status` TINYINT(1) NOT NULL AFTER `group_control_number`');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('x12_outbound_transactions', function (Blueprint $table) {
            //
        });
    }
}
