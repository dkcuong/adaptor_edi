<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientSshTable extends Migration
{

    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('client_ssh', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('client_id', false, true);
            $table->bigInteger('ssh_id', false, true);
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('client_ssh')) {
            Schema::drop('client_ssh');
        }
    }
}
