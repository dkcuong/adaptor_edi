<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 24-Feb-16
 * Time: 9:30 AM
 */

namespace App\Exceptions;


class ParserHeaderMappingException extends \Exception
{
    protected $message = 'You content data has some special character that break our parser! Please contact Edi Admin';
}
