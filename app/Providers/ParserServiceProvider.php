<?php

namespace App\Providers;

use App\Parsers\ParserInterface;
use Illuminate\Support\ServiceProvider;

class ParserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Parser', function ($app, $config) {

            $aliases = $app['config']->get('parser.aliases');

            $typeSupport = $config['edi']->support;

            return new $aliases[$typeSupport]($config);
        });
    }
}
