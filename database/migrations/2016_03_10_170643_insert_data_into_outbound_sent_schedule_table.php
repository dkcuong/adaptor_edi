<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataIntoOutboundSentScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('outbound_sent_schedules')->insert(
            array(
                'name' => 'Immediate',
                'short_name' => 'IM',
                'description' => 'Immediate',
                'status' => 1
            )
        );
        
        DB::table('outbound_sent_schedules')->insert(
            array(
                'name' => 'Daily',
                'short_name' => 'DL',
                'description' => 'Daily',
                'status' => 1
            )
        );
        
        DB::table('outbound_sent_schedules')->insert(
            array(
                'name' => 'Weekly',
                'short_name' => 'WL',
                'description' => 'Weekly',
                'status' => 1
            )
        );
        
        DB::table('outbound_sent_schedules')->insert(
            array(
                'name' => 'Monthly',
                'short_name' => 'ML',
                'description' => 'Monthly',
                'status' => 1
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outbound_sent_schedules', function (Blueprint $table) {
            //
        });
    }
}
