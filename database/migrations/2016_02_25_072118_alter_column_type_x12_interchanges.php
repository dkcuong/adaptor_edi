<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnTypeX12Interchanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `x12_interchanges` 
            CHANGE `sender_code` `sender_code` VARCHAR(15) NOT NULL');
        DB::statement('ALTER TABLE `x12_interchanges` 
            CHANGE `receiver_code` `receiver_code` VARCHAR(15) NOT NULL');
        DB::statement('ALTER TABLE `x12_interchanges` 
            CHANGE `auth_info` `auth_info` VARCHAR(10) NULL');
        DB::statement('ALTER TABLE `x12_interchanges` 
            CHANGE `security_info` `security_info` VARCHAR(10) NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('x12_interchanges', function (Blueprint $table) {
            //
        });
    }
}
