<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterControlStandardToRepetitionSeparatorFieldInX12InboundAndOutboundInterchange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `x12_outbound_interchanges`
            CHANGE COLUMN `control_standard` `repetition_separator`  char(1)
            CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "X" AFTER `time`');

        DB::statement('ALTER TABLE `x12_inbound_interchanges`
            CHANGE COLUMN `control_standard` `repetition_separator`  char(1)
            CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "X" AFTER `time`');

        DB::statement('
            ALTER TABLE `x12_outbound_transactions`
                DROP FOREIGN KEY `FK_x12_outbound_transactions_0`;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
