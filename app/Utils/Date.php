<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 17-Mar-16
 * Time: 10:46 AM
 */

namespace App\Utils;


class Date
{
    /**
     * @return bool
     */
    public function todayIsBeginOfMonth()
    {
        return date('j') == config('setting.outbound_schedule.monthly');
    }

    /**
     * @return bool
     */
    public function todayIsBeginOfWeek()
    {
        return date('w') == config('setting.outbound_schedule.weekly');
    }

    /**
     * @return bool
     */
    public function nowIsBeginOfDay()
    {
        return date('G') == config('setting.outbound_schedule.daily');
    }
}
