<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/03/2016
 * Time: 09:19
 */

namespace App\Mappers\Outbound;


use App\Mappers\MapperBase;

class WAMapper extends MapperBase
{
    /**
     * Mapper data for Concept One customer
     * Edi945
     *
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map945($items, $conditions)
    {
        $results[] = $this->parseTree($items);

        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parseTree($tranData)
    {
        $tran1 = ['transaction_code' => '945'];
        $tran1[] = [
            'segment'        => 'W06',
            'report_code'    => 'F',
            'client_order'   => $tranData['clientordernumber'],
            'date'           => date('Ymd'),
            'shipment_id'    => '',
            'agent_id'       => '',
            'purchase_order' => $tranData['customerordernumber'],
        ];

        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'ST',
            'name'         => '',
            'id_qualifier' => '92',
            'id'           => $tranData['shiptoname'],
        ];

        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'WH',
            'name'         => 'SELDAT FULFILLMENT SERVICES',
            'id_qualifier' => '92',
            'id'           => '20',
        ];

        $tran1[] = [
            'segment'        => 'G62',
            'date_qualifier' => '11',
            'date'           => date('Ymd'),
        ];

        $tran1[] = [
            'segment'     => 'NTE',
            'ref_code'    => 'BOL',
            'description' => $tranData['bolnumber'],
        ];

        $tran1[] = [
            'segment'               => 'W27',
            'transportation_method' => 'T',
            'carrier_code'          => $tranData['scac'],
            'routing'               => $tranData['pronumber'],
            'payment_method'        => $tranData['shipType'],
            'equipment_code'        => 'TL',
            'equipment_init'        => '',
            'equipment_number'      => $tranData['trailernumber'],
        ];

        $tran1[] = [
            'segment'         => 'W10',
            'load_code'       => 'PL',
            'pallet_quantity' => '1',
        ];

        $orderStatus = $tranData['orderStatus'];

        foreach ($tranData['items'] as $tran) {

            $shippedQuantity = $tran['product_order']['shipped_quantity'];
            $orderQuantity = $tran['product_order']['order_quantity'];

            if ($shippedQuantity == 0 || $orderStatus == self::CANCEL_ORDER) {
                $tran['status'] = 'BO';
            } else {
                if ($orderQuantity == $shippedQuantity) {
                    $tran['status'] = 'CC';
                } else {
                    if ($orderQuantity > $shippedQuantity) {
                        $tran['status'] = 'BP';
                    }
                }
            }

            $tran1[] = [
                'segment' => 'Lx',
            ];
            $tran1[] = [
                'segment'          => 'W12',
                'ordered_quantity' => $tran['status'],
                'quantity'         => $tran['product_order']['order_quantity'],
                'unit_number'      => $tran['product_order']['shipped_quantity'] ?: 0,
                'quantity_def'     => $tran['product_order']['diff_quantity'],
                'uom'              => 'EA',
                'upc'              => $tran['product_order']['upc'],
                'id_qualifier'     => 'VC',
                'product_id'       => $tran['product_order']['sku'],
            ];
            if (isset($tran['product_order']['ucc128']) && $tran['product_order']['ucc128']) {
                foreach ($tran['product_order']['ucc128'] as $ucc128) {
                    $tran1[] = [
                        'segment'     => 'N9',
                        'qualifier'   => 'GM',
                        'ref'         => $ucc128,
                        'description' => '',
                    ];
                }
            }

        }

        $tran1[] = [
            'segment'     => 'W03',
            'unit_number' => $tranData['totalPieces'],
            'weight'      => $tranData['totalWeight'],
            'unit_code'   => 'LB',
        ];
        return $tran1;
    }
}
