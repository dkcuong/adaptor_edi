<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FtpSsh extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ftp_id',
        'ssh_id',
        'active',
    ];

    /**
     * @var string
     */
    protected $table = 'ftp_ssh';

    public function sshKey()
    {
        return $this->belongsTo(SshKeys::class,'ssh_id');
    }
}
