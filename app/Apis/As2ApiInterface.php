<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 17/06/2016
 * Time: 10:53
 */

namespace App\Apis\Clients;


interface As2ApiInterface
{
    /**
     * @return mixed
     */
    public function callAs2Api();
}