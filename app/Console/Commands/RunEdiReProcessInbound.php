<?php

namespace App\Console\Commands;

use App\Modules\Edi\V1\Services\ClientService;
use App\Modules\Edi\V1\Services\EmailHandlingService;
use App\Utils\FtpStorage;
use App\Utils\WmsApiClient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RunEdiReProcessInbound extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 're-process:inbound';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic running re-process inbound worker';


    /**
     * @var ClientService
     */
    protected $service;

    /**
     * @var EmailHandlingService
     */
    protected $errorHandle;

    /**
     * RunEdi constructor.
     * @param ClientService $service
     * @param EmailHandlingService $emailHandlingService
     */
    public function __construct(
        ClientService $service,
        EmailHandlingService $emailHandlingService
    )
    {
        parent::__construct();

        $this->service = $service;
        $this->errorHandle = $emailHandlingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->service->init();
            $this->service->reProcessInbound();
        } catch (\Exception $e) {

            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            Log::error($e->getMessage(), ['info' => $errorInfo]);

            $messageError = [
                'Message' => $e->getMessage(),
                'File' => $e->getFile(),
                'Line' => $e->getLine(),
            ];

            $this->errorHandle->sendEmail([$messageError]);

            //Show error when run CLI
            $this->error($e->getMessage());
            $this->error($errorInfo);
        }
    }
}
