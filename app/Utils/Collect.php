<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 23-Jan-16
 * Time: 2:56 AM
 */

namespace App\Utils;


class Collect
{
    /**
     * @param $collection
     * @return Collection
     */
    public static function removeEmpty($collection)
    {
        return $collection->reject(function ($item) {
            return empty($item);
        });
    }

    /**
     * @param $collection
     * @param $header
     * @return mixed
     */
    public static function combine($collection, $header)
    {
        return $collection->transform(function ($items) use ($header) {
            return array_combine($header->toArray(), $items->toArray());
        });
    }
}
