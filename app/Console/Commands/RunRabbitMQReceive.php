<?php

namespace App\Console\Commands;

use App\Modules\Edi\V1\Services\RabbitMQService;
use Illuminate\Console\Command;

class RunRabbitMQReceive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:receive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'RabbitMQ running Receive worker';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RabbitMQService $rabbitMQService)
    {
        $this->service = $rabbitMQService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        logger('RabbitMQ Is Running');
        try {
            $this->service->receive();

        } catch (\Exception $e) {

            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            $messageHandle = [
                'Message: ' . $e->getMessage(),
                'File:' . __FILE__,
                'Class:' . __CLASS__,
                'Function:' . __FUNCTION__,
                'Line Number:' . __LINE__
            ];

            logger($messageHandle);

            //Show error when run CLI
            $this->error($e->getMessage());
            $this->error($errorInfo);
        }
    }
}
