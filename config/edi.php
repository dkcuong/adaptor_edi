<?php

return [
    'validation_messages' => [
        'required'  => ':attribute is required',
        'min'       => ':attribute length must be equal or greater then :min',
        'max'       => ':attribute length must be equal or less then :max',
        'size'      => ':attribute length must be exactly :size.',
        'regex'     => ':attribute is in wrong format',
        'between'   => ':attribute must be between :min - :max.',
        'in'        => ':attribute must be one of the following types: :values',
        'numeric'   => ':attribute must be a numeric',
        'alpha_num' => ':attribute must be a alpha-numeric',
    ],

    'validation_segment' => [
        'segment.unrecognized' => "Can't recognized segment ID [%s] in Transaction has control number [%s]",
        'segment.missing'      => "Can't find segment ID [%s] in Transaction has control number [%s]",
        'loop.maximum'         => "Loop [%s] occurs over maximum times in Transaction has control number [%s]",
        'segment.maximum'      => "Segment ID [%s] occurs over maximum times in Transaction has control number [%s]",
    ],
];
