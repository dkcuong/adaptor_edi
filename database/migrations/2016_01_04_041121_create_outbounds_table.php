<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutboundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outbounds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('decription');
            $table->bigInteger('client_id');
            $table->bigInteger('fpt_account_id');
            $table->string('path');
            $table->string('filename_prefix');
            $table->string('email');
            $table->bigInteger('group_id');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outbounds');
    }
}
