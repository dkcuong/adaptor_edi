<?php

return [

    'wms_uri' => env('API_WMS_URI', ''),

    'wms_token' => '',

    'edi_parser_api' => env('EDI_PARSER_API', ''),

    'edi_parser_api_token' => env('EDI_PARSER_API_TOKEN', ''),

    'as2_outbound_api' => env('AS2_OUTBOUND_API', ''),

    'as2_outbound_token' => env('AS2_OUTBOUND_API_TOKEN', ''),
];
