<?php

namespace App\Mappers\Outbound;


use App\Mappers\MapperBase;

class OCEANMapper extends MapperBase
{
    const AI_CODE = '00';
    const CUSTOMER_CODE = '73573280';

    /**
     * Mapper data for Concept One customer
     * Edi210
     *
     * @param $items
     * @return array
     */
    protected function map210($items)
    {
        $results[] = $this->parseTree210($items);

        return $results;
    }

    protected function map214($items)
    {
        $results[] = $this->parseTree214($items);

        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parseTree210($tranData)
    {
        $tran1 = ['transaction_code' => '210'];

        $invoice = $tranData['invoice'];
        //invoice information
        $inv_num = isset($invoice['inv_num']) ? $invoice['inv_num'] : '';

        $inv_shipment_method = '';
        if (isset($invoice['inv_sts'])) {
            if ($invoice['inv_sts'] == 'Posted') {
                $inv_shipment_method = 'CC';
            } else if ($invoice['inv_sts'] == 'Posted') {
                $inv_shipment_method = 'PP';
            } else {
                $inv_shipment_method = 'TP';
            }
        }
        $inv_date = isset($invoice['inv_dt']) ? $invoice['inv_dt'] : '';
        $inv_amount = isset($invoice['inv_amt']) ? $invoice['inv_amt'] : '';

        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];

        // B3**INVCxxxx*TNAM14120685*PP**20141009*30000****xxxx
        $tran1[] = [
            'segment'        => 'B3',
            'shipment_qualifier' => '', //147 Shipment Qualifier
            'invoice_number' => $inv_num, //76 Invoice Number
            'shipment_identifier' => '', //145 Shipment Identification Number
            'shipment_method' => $inv_shipment_method, //146 Shipment Method of Payment Code
            'weight_unit_code' => '', //188 Weight Unit Code
            'invoice_date' => $inv_date, // 373 Date
            'net_amount' => $inv_amount, //193 Net Amount Due
            'correction_indicator_code' => '', //202 Correction Indicator Code
            'delivery_date' => '', //32 Delivery Date
            'date_qualifier' => '', //374 Date/Time Qualifier
            'carrier_code' => isset($tranData['ssl_code']) ? $tranData['ssl_code'] : '', // 140 Standard Carrier Alpha Code
        ];

        // C3*USD
        $tran1[] = [
            'segment' => 'C3',
            'currency_code' => isset($tranData['loc_code']) ? ($tranData['loc_code'] == 'CA' ? 'CAD' : 'USD') : ''
        ];

        // N9*BM*TNAM14120685
        if ($tranData['class_type'] == 'Import' || $tranData['class_type'] == 'Export') {
            $tran1[] = [
                'segment'      => 'N9',
                'entity_code'       => 'BM',
                'ref_qualifier'     => isset($tranData['edi_ref_order']) ? $tranData['edi_ref_order'] : '',
            ];
        } else {

            // N9*SI*TNAM14120685

            $tran1[] = [
                'segment' => 'N9',
                'entity_code' => 'SI',
                'ref_qualifier' => isset($tranData['edi_ref_order']) ? $tranData['edi_ref_order'] : '',
            ];
        }

        // K1
        $tran1[] = [
            'segment' => 'K1',
            'free_form_message' => '',
        ];

        //loop container
        foreach ($tranData['jobOrderDetail'] as $key => $jobDt) {
            //N7
            $tran1[] = [
                'segment' => 'N7',
                'equipment_initial' => isset($jobDt['cntr_num']) ? substr($jobDt['cntr_num'], 0, 4) : '',
                'equipment_number' => isset($jobDt['cntr_num']) ? substr($jobDt['cntr_num'], 4) : ''
            ];
        }

        //loop charge
        foreach ($tranData['chargeDetail'] as $key => $charge) {

                //LX*7~
                $tran1[] = [
                    'segment' => 'Lx',
                    'assign_number' => $key + 1
                ];

                $rate_qualifier = '';
                if (isset($charge['chg_code_name'])) {
                    if ($charge['chg_code_name'] == 'Per Flat Bed') {
                        $rate_qualifier = 'FL';
                    } else if ($charge['chg_code_name'] == 'Flat Rate' ) {
                        $rate_qualifier = 'FR';
                    } else {
                        $rate_qualifier = 'PR';
                    }
                }
                //L1
                $tran1[] = [
                    'segment'     => 'L1',
                    'landing_line_number' => $key + 1, // 213 Lading Line Item Number
                    'freight_rate' => isset($charge['chg_dtl_rate']) ? $charge['chg_dtl_rate'] : '', //60 Freight Rate
                    'rate_qualifier' => $rate_qualifier, //122 Rate/Value Qualifier
                    'amount_charged' => isset($charge['chg_dtl_amt']) ? $charge['chg_dtl_amt'] : '', // 58 Amount Charged
                    'advances' => '', //191 Advances
                    'prepaid_amount' => '', //117 Prepaid Amount
                    'rate_code' => '', //120 Rate Combination Point Code
                    'special_charge_code' => 'FUE', //150 Special Charge or Allowance Code - default
                ];

        }

        return $tran1;
    }

    public function parseTree214($tranData)
    {
        $tran1 = ['transaction_code' => '214'];

        $jobHeader = isset($tranData['job_order_header']) ? $tranData['job_order_header'] : [];

        //B10
        $tran1[] = [
            'segment' => 'B10',
            'id' => $jobHeader ? $jobHeader['job_ord_hdr_num'] : '',
            'shipment_id_number' => $jobHeader ? $jobHeader['edi_ref_order'] : '',
            'carrier_code' => $jobHeader ? $jobHeader['ssl_code'] : ''
        ];

        //LX*1~
        $tran1[] = [
            'segment' => 'Lx',
            'line_number' => '1',
        ];

        $shipment_status_code = '';
        if (in_array($tranData['cntr_sts'], ['CX', 'T']) && $jobHeader['class_type'] == 'Export') {
            $shipment_status_code = 'X3';
        }

        if (in_array($tranData['cntr_sts'], ['CX', 'T']) && $jobHeader['class_type'] == 'Import') {
            $shipment_status_code = 'X1';
        }

        $shipment_appointment_code = '';
        if (!in_array($tranData['cntr_sts'], ['CX', 'T']) && $jobHeader['class_type'] == 'Export') {
            $shipment_appointment_code = 'AA';
        }

        if (!in_array($tranData['cntr_sts'], ['CX', 'T']) && $jobHeader['class_type'] == 'Import') {
            $shipment_appointment_code = 'AB';
        }

        $date_ctn = $tranData['updated_at'];
        $at705 = date('Ymd', strtotime($date_ctn));
        $at706 = date('Hi', strtotime($date_ctn));

        //AT7
        $tran1[] = [
            'segment' => 'AT7',
            'shipment_status_code' => $shipment_status_code,
            'status_reason_code' => 'NS', // default
            'shipment_appointment_code' => $shipment_appointment_code,
            'shipment_status_code2' => '',
            'date' => $at705,
            'time' => $at706,
            'time_code' => 'PS',
        ];

        $ms201 = substr($tranData['cntr_num'], 0, 4);
        $ms202 = substr($tranData['cntr_num'], 4);
        //MS2
        $tran1[] = [
            'segment' => 'MS2',
            'carrier_code' => $ms201,
            'equipment_number' =>  $ms202,
            'equipment_des_code' => 'CN',
        ];

        return $tran1;
    }

}
