<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\RunEdi::class,
        Commands\RunEdiOutbound::class,
        Commands\BackupStorage::class,
        Commands\ReProcessFTP::class,
        Commands\RunEdiReProcessInbound::class,
//        Commands\RunRabbitMQReceive::class,
        Commands\RunNotiFtpInbound::class,
        Commands\RunReNotiFtpInbound::class,
        //Commands\RunRabbitMQReceive::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $env = env('CRON_TAB', 'production');
        $cronInbound = config('setting.cronTabs.inbounds.' . $env);
        $cronNotiFtpUploadInbound = config('setting.cronTabs.notiftpupload.' . $env);
        $cronReNotiFtpUpload = config('setting.cronTabs.renotiftpupload.' . $env);
        $cronOutbound = config('setting.cronTabs.outbounds.' . $env);

        $backup_storage = config('setting.cronTabs.inbounds.' . $env);
        $re_process_ftp = config('setting.cronTabs.re-process.ftp.' . $env);
        $re_process_inbound = config('setting.cronTabs.re-process.inbound.' . $env);
//        $rabbitmq_outbound = config('setting.cronTabs.rabbitmq-outbound.' . $env);

        /*$schedule->command('edi:inbound')
            ->cron($cronInbound)->withoutOverlapping();*/
        $schedule->command('edi:runnotiftpinbound')
            ->cron($cronNotiFtpUploadInbound)->withoutOverlapping();
        $schedule->command('edi:renotiftpinbound')
            ->cron($cronReNotiFtpUpload)->withoutOverlapping();
        $schedule->command('edi:outbound')
            ->cron($cronOutbound)->withoutOverlapping();
        $schedule->command('re-process:inbound')
            ->cron($re_process_inbound)->withoutOverlapping();
        /*$schedule->command('backup:storage')
            ->cron($backup_storage)->withoutOverlapping();*/
        $schedule->command('re-process:ftp')
            ->daily('00:05')->withoutOverlapping();
//        $schedule->command('rabbitmq:receive')
//            ->cron($rabbitmq_outbound)->withoutOverlapping();
    }
}
