<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 22-Jan-16
 * Time: 12:28 AM
 */

namespace App\Parsers;


interface ParserInterface
{

    /**
     * Get return data after parsed
     *
     * @return array
     */
    public function get();

    /**
     * Process Parse Content to array
     *
     * @return array
     */
    public function process();

    public function beforeProcess();

    public function afterProcess();

    public function writerOutboundKnowledge();
}
