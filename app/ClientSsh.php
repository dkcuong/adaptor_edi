<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientSsh
 *
 * @package App
 */
class ClientSsh extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'ssh_id',
        'active',
    ];

    /**
     * @var string
     */
    protected $table = 'client_ssh';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sshKey()
    {
        return $this->belongsTo(SshKeys::class,'ssh_id');
    }
}
