<?php

namespace App\Console\Commands;

use App\Modules\Edi\V1\Services\ClientService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Mockery\CountValidator\Exception;

class ReProcessFTP extends Command
{
    /**
     * @var ClientService
     */
    public $service;
    /**
     * @var \App\Utils\ReProcessFTP
     */
    public $reProcessFTP;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 're-process:ftp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Re-process FTP';

    /**
     * ReProcessFTP constructor.
     * @param ClientService $clientService
     * @param \App\Utils\ReProcessFTP $reProcessFTP
     */
    public function __construct(ClientService $clientService,
                                \App\Utils\ReProcessFTP $reProcessFTP)
    {
        $this->service = $clientService;
        $this->reProcessFTP = $reProcessFTP;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        logger('Backup Storage is running');
        try {

            $this->service->init();
            $this->service->reProcessFTP();
        } catch (\Exception $e) {
            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            Log::error($e->getMessage(), ['info' => $errorInfo]);
        }
    }
}
