<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/4/2019
 * Time: 10:43 AM
 */

namespace App\Mappers\Outbound;

use App\Mappers\MapperBase;

use App\Mappers\SimpleXMLElementExtend;
use DOMDocument;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;
use SoapBox\Formatter\Formatter;



class UNILEVERMapper extends MapperBase
{
    const AI_CODE = '00';
    const CUSTOMER_CODE = '95011010';

    protected function map945($items, $conditions)
    {
        $results = [];

        // items : data_output column
        // convert to array => output xml
//        $results = ArrayToXml::convert(['a'=>'b'], [
//            'rootElementName' => 'Message',
//            '_attributes' => [
//                'xmlns:sl' => 'urn:gs1:ecom:transport_instruction:xsd:3',
//            ],
//        ], true, 'UTF-8');

        $conditions = [
            "uniqueCreatorIdentification" => "ns2:transaction.entityIdentification.uniqueCreatorIdentification"
        ];

        $arr = [
            "ns2:transaction  xmlns:ns2=\"urn:ean.ucc:2\" xmlns:ns3=\"urn:ean.ucc:deliver:2\" xmlns:ns4=\"urn:ean.ucc:align:2\"" => [
                "entityIdentification" => [
                    "uniqueCreatorIdentification" => "2019-04-11T17:48:05"
                ]
            ]
        ];

        $results = $this->parseInventoryCorrectionOut();
dd($results);

//        $str = "<ns2:transaction>
//			<entityIdentification>
//				<uniqueCreatorIdentification>2019-04-11T17:48:05</uniqueCreatorIdentification>
//				</entityIdentification>
//				</ns2:transaction>";

//        $result = Formatter::make($arr, Formatter::XML);
//        $results  = $result->toXml();
//        dd($results);


        // mapping
//        $conditions = [
//              "clientordernumber" => "Bookings.Booking.BasicDetails.BookingNo",
//              "sku" => "Bookings.Booking.Products.Product.ProductCode",
//              "quantity" => "Bookings.Booking.Products.Product.Value"
//        ];
//
//        $items = [
//            'clientordernumber' => 'ORD-1',
//            'items' => [
//                [
//                    'sku' => 'A',
//                    'quantity' => 2
//                ],
//                [
//                    'sku' => 'B',
//                    'quantity' => 3
//                ]
//            ]
//        ];
//
//        $tag_order = 'Bookings';
//        $tag_order_child = 'Booking';
//        $tag_item = 'Products';
//        $tag_item_child = 'Product';
//
//        $item_key_output = 'items';
//
//        $result = [];
//        $items_result = [];
//
//        foreach ($conditions as $k=>$v) {
//
//            $temp = explode(".", $v);
//
//            $tag_temp = $temp[count($temp)-1];
//            if (in_array($tag_item_child, $temp)) {
//                $arrTemp = array_column($items[$item_key_output], $k);
//
//                foreach ($arrTemp as $i=>$j) {
//                    $result[$tag_order][$tag_order_child][$tag_item][$i][$tag_temp] = $j;
//                }
//
//            } else {
//
//                $result[$tag_order][$tag_order_child][$tag_temp] = $items[$k];
//            }
//        }
//
//
//        $result = Formatter::make($result, Formatter::XML);
//        $results   = $result->toXml();
//dd($results);
        return $results;
    }
    /**
     * Mapper data for Concept One customer
     * Edi945
     *
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map855($items, $conditions)
    {
        $results[] = $this->parse855($items);

        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parse855($tranData)
    {
        $tran1 = ['transaction_code' => '855'];
        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];

        //BAK*00*AD*ZUTSRQ6543217*20110920
        $tran1[] = [
            'segment' => 'BAK',
            'BAK01' => '00',
            'BAK02' => 'AC',
            'BAK03' => $tranData['cus_po'],
            'BAK04' => !empty($tranData['odr_req_dt'])?date('Ymd', $tranData['odr_req_dt']):'',
        ];

        if (getArr($order_meta, 'product_type', '')) {
            $tran1[] = [
                'segment' => 'REF',
                'REF01' => 'PRT',
                'REF02' => getArr($order_meta, 'product_type'),
            ];
        }

        if (getArr($order_meta, 'vendor_id_ref')) {
            $tran1[] = [
                'segment' => 'REF',
                'REF01' => 'VR',
                'REF02' => $order_meta['vendor_id_ref'],
            ];
        }

        if (!empty($tranData['sac_data'])) {
            $sac_data = $tranData['sac_data'];
            foreach ($sac_data as $sac) {
                /*$tran1[] = [
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC02' => '',
                    'SAC03' => '',
                    'SAC04' => '',
                    'SAC05' => '',
                    'SAC06' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                ];*/
            }
        }
        //        ITD*14*3*****60*****NET~
        $tran1[] = [
            'segment' => 'ITD',
            'ITD01' => getArr($order_meta, 'term_type_code'),
            'ITD02' => getArr($order_meta, 'date_code'),
            'ITD03' => getArr($order_meta, 'discount_percent'),
            'ITD04' => '',
            'ITD05' => '',
            'ITD06' => getArr($order_meta, 'net_due_date'),
            'ITD07' => getArr($order_meta, 'net_days'),
            'ITD08' => '',
            'ITD09' => '',
            'ITD10' => '',
            'ITD11' => '',
            'ITD12' => getArr($order_meta, 'term_sale_desc'),
        ];
        //        DTM*010*20110916~
        $tran1[] = [
            'segment' => 'DTM',
            'REF01' => '010',
            'REF02' => date('Ymd', $tranData['cancel_by_dt']),
        ];
//        ACK*IA~

//        N1*ST*ZAPPOS MERCHANDISING INC.*92*0001~
        $tran1[] = [
            'segment' => 'N1',
            'entity_code' => 'ST',
            'name' => getArr($tranData, 'ship_to_name'),
            'id_qualifier' => getArr($order_meta, 'shipping_code', '92'),
            'id' => getArr($order_meta, 'ship_to_id', "0001"),
        ];

//        N3*376 ZAPPOS.COM BLVD~
        $tran1[] = [
            'segment' => 'N3',
            'address' => $tranData['ship_to_add_1']
        ];

//        N3*FTZ#029, SITE 6~

//        if (!getArr($additional_order, 'shipping_address_2')) {
//            $tran1[] = [
//                'segment' => 'N3',
//                'address' => $tranData['shipping_address_street']
//            ];
//        }

//        N4*SHEPHERDSVILLE*KY*40165*US~
        $tran1[] = [
            'segment' => 'N4',
            'name' => $tranData['ship_to_city'],
            'province_code' => $tranData['ship_to_state'],
            'postal_code' => $tranData['ship_to_zip'],
            'country_code' => $tranData['ship_to_country'],
        ];

//        PO1*1*6*EA*18.90*WE*UP*801234567891*PI*ZZZ-49876543*VA*7607~

        $num = 0;
        foreach ($tranData['items'] as $item_id => $item) {
            $num++;
            $product_order  = $item['product_order'];
            $product_meta   = isset($product_order['product_meta']) ? json_decode($product_order['product_meta'], true) : [];

            //        PO1*1*6*EA*18.90*WE*UP*801234567891*PI*ZZZ-49876543*VA*7607~
            $tran1[] = [
                'segment'          => 'PO1',
                'PO101'     => $num,
                'PO102'     => $product_order['qty'],
                'PO103'     => 'EA',
                'PO104'     => getArr($product_meta, 'price', ''),
                'PO105'     => getArr($product_meta, 'price_unit', 'WE'),
                'PO106'     => getArr($product_meta, 'upc_qualifier', 'UP'),
                'PO107'     => getArr($product_meta, 'upc', ''),
                'PO108'     => getArr($product_meta, 'item_qualifier', 'VA'),
                'PO109'     => $product_order['sku'],
                'PO110'     => getArr($product_meta, 'style_qualifier',''),
                'PO111'     => getArr($product_meta, 'product_load_id', ''),
            ];

            //PID*~
            if(!empty($product_meta['product_item_description'])){
                $tran1[] = [
                    'segment'       => 'PID',
                    'entity_code'   => !empty($product_meta['product_item_description']['entity_code'])?$product_meta['product_item_description']['entity_code']:'F',
                    'process_code'  => !empty($product_meta['product_item_description']['process_code'])?$product_meta['product_item_description']['process_code']:'91',
                    "agency_code"      => !empty($product_meta['product_item_description']['agency_code'])?$product_meta['product_item_description']['agency_code']:'',
                    "product_des_code" => !empty($product_meta['product_item_description']['product_des_code'])?$product_meta['product_item_description']['product_des_code']:'',
                    'description'   => !empty($product_meta['product_item_description']['description'])?$product_meta['product_item_description']['description']:'',
                ];
            }
            $tran1[]=[
                'segment' => 'ACK',
                'ACK01' => 'IA'
            ];
        }

        return $tran1;
    }


    /**
     * @param $items
     * @param $conditions
     * @return array
     */
    public function map856($items, $conditions){
        $results[] = $this->parse856($items);
        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    private function parse856($tranData)
    {
        $tran1 = ['transaction_code' => '856'];
        //BSN*00**20171212**~

        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];
        $ship_from = isset($order_meta['ship_from']) ? $order_meta['ship_from'] : '';

        $tran1[] = [
            'segment'       => 'BSN',
            'entity_code'   => '00',
            'BSN02' => $tranData['cus_po'],
            'BSN03' => date('Ymd', strtotime($tranData['ship_by_dt'])),
            'BSN04' => date('His', strtotime($tranData['ship_by_dt'])),
        ];
        //HL*1**S~
        $tran1[] = [
            'segment'       => 'HL',
            'entity_code'   => '1',
            'parent_number' => '',
            'level_code'    => 'S'
        ];
        //TD1*CTN*40*G*52*LB~
        $tran1[] = [
            'segment'     => 'TD1',
            'entity_code' => 'CTN',
            'landing_quantity' => $tranData['total_piece'],
            'weight_code' => 'G',
            'weight'      => $tranData['total_weight'],
            'measurement_code' => 'LB',

        ];
        //TD5*O*2***~
        $tran1[] = [
            'segment'     => 'TD5',
            'entity_code' => 'O',
            'qualifier_code' => '2',
            'routing'     => $tranData['carrier_code']??'',

        ];
        //REF*2I*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => '2I',
            'REF02'           => $tranData['track_num']??'',
        ];

        //REF*BX*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'BX',
            'REF02'           => $tranData['odr_num'],
        ];

        //DTM
        $tran1[] = [
            'segment'       => 'DTM',
            'entity_code'   => '011',
            'date'          => date('Ymd', strtotime($tranData['ship_by_dt'])),
            'time'          => '',
            'time_code'     => 'UT',
        ];

        //FOB
        $tran1[] = [
            'segment'     => 'FOB',
            'entity_code' => 'CC',
        ];

        //N1*SF*TEXTILES FROM EUROPE~
        if(!empty($ship_from)){
            $tran1[] = [
                'segment'      => 'N1',
                'entity_code'  => 'SF',
                'name'         => $ship_from['name'],
            ];
        }

        //N1*ST*BED BATH & BEYOND #0655*92*0655~
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'ST',
            'name'         => $tranData['ship_to_name'],
            'id_qualifier' => '92',
            'id'           => $tranData['ship_to_zip'],
        ];
        //N2*C/O AMAZON.COM KYDC LLC
        $tran1[] = [
            'segment'     => 'N2',
            'name' => $tranData['ship_to_name'],
        ];
        //N3
        $tran1[] = [
            'segment'      => 'N3',
            'name '        => $tranData['ship_to_add_1'],
        ];
        //N4*SHEPHERDSVILLE*KY*40165*US
        $tran1[] = [
            'segment'           => 'N4',
            'name'              => substr($tranData['ship_to_city'], 0, 30),
            'province_code'     => substr($tranData['ship_to_state'], 0, 2),
            'postal_code'       => substr($tranData['ship_to_zip'], 0, 15),
            'country_code'      => substr($tranData['ship_to_country'], 0, 3),
        ];


        //HL*2*1*T
        $tran1[] = [
            'segment'       => 'HL',
            'entity_code'   => '2',
            'parent_number' => '1',
            'level_code'    => 'O'
        ];

        $tran1[] = [
            'segment'       => 'PRF',
            'order_number'  => $tranData['cus_po'],
            'date'          => date('Ymd',strtotime($tranData['odr_req_dt'])),
        ];

        //REF*VR*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'VR',
            'REF02'           => getArr($order_meta, 'vendor_id_ref'),
        ];

        //REF*PRT*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'PRT',
            'REF02'           => getArr($order_meta, 'product_type'),
        ];

        $HL = 3;
        $LIN = 1;
        foreach ($tranData['items'] as $item_id => $item) {
            $product_order  = $item['product_order'];
            $product_meta   = isset($product_order['product_meta']) ? json_decode($product_order['product_meta'], true) : [];
            foreach ($product_order['cartons'] as $cart_id => $carton) {
                //HL*3*2*P~
                $tran1[] = [
                    'segment'       => 'HL',
                    'entity_code'   => $HL,
                    'parent_number' => 2,
                    'level_code'    => 'P'
                ];
                //MAN*GM*00007357328004124800**CP*832150070055699~
                $tran1[] = [
                    'segment' => 'MAN',
                    'entity_code' => 'GM',
                    'ref11' => $carton['tracking_number']??'',
                    'ref12' => '',
                    'qualifier2' => !isset($order_meta['order_type']) || $order_meta['order_type'] != 1 ? 'CP' : '',
                    'ref21' => $carton['pack_hdr_num']
                ];

                $tran1[] = [
                    'segment'       => 'HL',
                    'entity_code'   => $HL+= 1,
                    'parent_number' => $HL - 1,
                    'level_code'    => 'I'
                ];
                //LIN*~
                $tran1[] = [
                    'segment'       => 'LIN',
                    'entity_code'   => str_pad($LIN,4,0,STR_PAD_LEFT),
                    'product_code'  => $product_meta['upc_qualifier'],
                    'upc'           => $product_meta['upc'],
                ];

                //SN1*~
                $tran1[] = [
                    'segment'           => 'SN1',
                    'entity_code'       => '',
                    'units'             => $carton['piece_qty'],
                    'measure_code'      => 'EA',
                    'units_ship_date'   => '',
                    'quantity_ordered'  => $product_order['picked_qty'],
                ];

                //PID*~
                if(!empty($product_meta['product_item_description'])){
                    $tran1[] = [
                        'segment'       => 'PID',
                        'entity_code'   => !empty($product_meta['product_item_description']['entity_code'])?$product_meta['product_item_description']['entity_code']:'F',
                        'process_code'  => !empty($product_meta['product_item_description']['process_code'])?$product_meta['product_item_description']['process_code']:'91',
                        "agency_code"      => !empty($product_meta['product_item_description']['agency_code'])?$product_meta['product_item_description']['agency_code']:'',
                        "product_des_code" => !empty($product_meta['product_item_description']['product_des_code'])?$product_meta['product_item_description']['product_des_code']:'',
                        'description'   => !empty($product_meta['product_item_description']['description'])?$product_meta['product_item_description']['description']:'',
                    ];
                }

                $HL ++ ;
            }

        }

        $countHL = 0;
        foreach ($tran1 as $index => $value){
            if(isset($value['segment']) && $value['segment'] == 'HL'){
                $countHL +=1;
            }
        }
        $tran1[] = [
            'segment'       => 'CTT',
            'number_item'   => $countHL,
        ] ;

        return $tran1;
    }

    private function parse856_bk($tranData)
    {

        $tran1 = ['transaction_code' => '856'];
        //BSN*00**20171212**~

        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];
        $additional_order = isset($order_meta['additional']) ? $order_meta['additional'] : [];
        $ship_to_order = isset($order_meta['ship_to']) ? $order_meta['ship_to'] : [];

        $tran1[] = [
            'segment'       => 'BSN',
            'entity_code'   => '00',
            'BSN02' => $tranData['customerordernumber'],
            'BSN03' => $tranData['start_ship_date'],
            'BSN04' => '',
        ];
        //HL*1**S~
        $tran1[] = [
            'segment'       => 'HL',
            'entity_code'   => '1',
            'parent_number' => '',
            'level_code'    => 'S'
        ];
        //TD1*CTN*40*G*52*LB~
        $tran1[] = [
            'segment'     => 'TD1',
            'entity_code' => 'CTN',
            'landing_quantity' => $tranData['total_pieces'],
            'weight_code' => 'G',
            'weight'      => $tranData['total_weight'],
            'measurement_code' => 'LB',

        ];
        //TD5*O*2***~
        $tran1[] = [
            'segment'     => 'TD5',
            'entity_code' => 'O',
            'qualifier_code' => '2',
            'routing'     => $tranData['shipType'],

        ];
        //REF*2I*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => '2I',
            'REF02'           => $tranData['trackingNumber'],
        ];

        //REF*BX*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'BX',
            'REF02'           => $tranData['odr_num'],
        ];

        //DTM
        $tran1[] = [
            'segment'       => 'DTM',
            'entity_code'   => '011',
            'date'          => $tranData['start_ship_date'],
            'time'          => '',
            'time_code'     => 'UT',
        ];

        //FOB
        $tran1[] = [
            'segment'     => 'FOB',
            'entity_code' => 'CC',
        ];

        //N1*SF*TEXTILES FROM EUROPE~
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'SF',
            'name'         => $tranData['shipfromname'],
        ];

        //N1*ST*BED BATH & BEYOND #0655*92*0655~
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'ST',
            'name'         => $tranData['shiptoname'],
            'id_qualifier' => '92',
            'id'           => $tranData['shiptozip'],
        ];
        //N2*C/O AMAZON.COM KYDC LLC
        $tran1[] = [
            'segment'     => 'N2',
            'name' => $tranData['shiptoname'],
        ];
        //N3
        $tran1[] = [
            'segment'      => 'N3',
            'name '        => $tranData['shiptoaddress'],
        ];
        //N4*SHEPHERDSVILLE*KY*40165*US
        $tran1[] = [
            'segment'           => 'N4',
            'name'              => substr($tranData['shiptocity'], 0, 30),
            'province_code'     => substr($tranData['shiptostate'], 0, 2),
            'postal_code'       => substr($tranData['shiptozip'], 0, 15),
            'country_code'      => substr($tranData['shiptocountry'], 0, 3),
        ];


        //HL*2*1*T
        $tran1[] = [
            'segment'       => 'HL',
            'entity_code'   => '2',
            'parent_number' => '1',
            'level_code'    => 'O'
        ];

        $tran1[] = [
            'segment'       => 'PRF',
            'order_number'  => $tranData['customerordernumber'],
            'date'          => $tranData['order_date'],
        ];

        //REF*VR*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'VR',
            'REF02'           => getArr($order_meta, 'vendor_id_ref'),
        ];

        //REF*PRT*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'PRT',
            'REF02'           => getArr($order_meta, 'product_type'),
        ];

        $HL = 3;
        $LIN = 1;
        foreach ($tranData['items'] as $item_id => $item) {
            $cartonData = $item['cartons'];
            $firstCarton = array_shift($cartonData);

            foreach ($item['cartons'] as $carton) {
                $product_meta = isset($carton['product_meta'])? json_decode($carton['product_meta'],true):[];
                //HL*3*2*P~
                $tran1[] = [
                    'segment'       => 'HL',
                    'entity_code'   => $HL,
                    'parent_number' => $HL - 1,
                    'level_code'    => 'P'
                ];
                //MAN*GM*00007357328004124800**CP*832150070055699~
                $tran1[] = [
                    'segment' => 'MAN',
                    'entity_code' => 'GM',
                    'ref11' => self::AI_CODE . generateUCC128('00' . self::CUSTOMER_CODE, $carton['cartonID']),
                    'ref12' => '',
                    'qualifier2' => $tranData['order_type'] != 1 ? 'CP' : '',
                    'ref21' => $tranData['order_type'] != 1 ? $firstCarton['tracking_number'] : '',
                ];

                $tran1[] = [
                    'segment'       => 'HL',
                    'entity_code'   => $HL + 1,
                    'parent_number' => $HL,
                    'level_code'    => 'I'
                ];
                //LIN*~
                $tran1[] = [
                    'segment'       => 'LIN',
                    'entity_code'   => str_pad($LIN,4,0,STR_PAD_LEFT),
                    'product_code'  => $product_meta['upc_qualifier'],
                    'upc'           => $carton['upc'],
                ];

                //SN1*~
                $tran1[] = [
                    'segment'           => 'SN1',
                    'entity_code'       => '',
                    'units'             => $item['piecesCount'],
                    'measure_code'      => $product_meta['uom_code'],
                    'units_ship_date'   => '',
                    'quantity_ordered'  => $item['shippedQuantity'],
                ];
                //PID*~
                $tran1[] = [
                    'segment'     => 'PID',
                    'entity_code' => 'F',
                    'code_name'   => '91',
                    'description' => '',
                ];
                $HL ++ ;
            }

        }

        $countHL = 0;
        foreach ($tran1 as $index => $value){
            if(isset($value['segment']) && $value['segment'] == 'HL'){
                $countHL +=1;
            }
        }
        $tran1[] = [
            'segment'       => 'CTT',
            'number_item'   => $countHL,
        ] ;

        return $tran1;
    }


    protected function map810($items, $conditions){
        $results[] = $this->parse810($items);
        //dd($results);
        return $results;
    }

    private function parse810($items)
    {
        $data = $items;
        $tran1 = ['transaction_code' => '810'];
        if(empty($data)){
            return $tran1;
        }
        $order = array_shift($data['orders']);
        
        //BIG*20110706*0714449999**ABCDEF5432107

        $tran1[] = [
            'segment'           => 'BIG',
            'inv_date'          => date('Ymd', strtotime($data['created_at'])),
            'inv_num'           => $data['inv_num'],
            'inv_date_assigned' => date('Ymd', strtotime($data['created_at'])),
            'po_num'            => $order['po'],
        ];

        //REF*VR*4321

        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'VR',
            'REF02'           => $order['order_meta']['vendor_id_ref'],
        ];
        //ITD*******60

        $tran1[] = [
            'segment' => 'ITD',
            'ITD01' => getArr($order['order_meta'], 'term_type_code'),
            'ITD02' => getArr($order['order_meta'], 'date_code'),
            'ITD03' => getArr($order['order_meta'], 'discount_percent'),
            'ITD04' => '',
            'ITD05' => '',
            'ITD06' => getArr($order['order_meta'], 'net_due_date'),
            'ITD07' => getArr($order['order_meta'], 'net_days'),
            'ITD08' => '',
            'ITD09' => '',
            'ITD10' => '',
            'ITD11' => '',
            'ITD12' => getArr($order['order_meta'], 'term_sale_desc'),
        ];


        //IT1*000010*6*EA*34.2**UP*765432123456
        //IT1*000010*7*EA*57.7**EN*7766554433217

        foreach ($order['items'] as $item){
            $tran1[] = [
                'segment'                   => 'IT1',
                'purchase_order_line_item'  => getArr($item['product_meta'], 'item_qualifier'),
                'number_of_unit'            => getArr($item, 'qty'),
                'unit_code'                 => 'EA',
                'unit_price'                => $item['product_meta']['price'],
                'undefined'                 => '',
                'product_qualify_code'      => $item['product_meta']['upc_qualifier'],
                'product_id'                => $item['product_meta']['upc'],
            ];
        }

        //TDS*44425

        $tran1[] = [
            'segment'           => 'TDS',
            'monetary_amount'   => $data['total'],
        ];

        //PLEASE NOTE THAT AT LEAST 2 DIGITS ARE REQUIRED
        //Example a value of $0.09 should be TDS*09

        //SAC*A*C000***2.78**********Defective Allowance

        //foreach ($items['sac'] as $sac){
        foreach ($data['sacs'] as $sac){ //test
            $tran1[] = [
                'segment'                   => 'SAC',
                'entity_code'               => 'A',
                'service'                   => 'C000',
                'qualifier_code'            => '',
                'agency_service'            => '',
                'amount'                    => $sac['sac_amount'],
                'charge_percent_qualifier'  => '',
                'format'                    => '',
                'rate'                      => '',
                'measurement_code'          => '',
                'quantity'                  => '',
                'quantity2'                 => '',
                'handling_code'             => '',
                'ref_identification'        => '',
                'option_number'             => '',
                'description'               => $sac['sac_description'],
                'language_code'             => '',
            ];
        }

        //CTT*37
        $tran1[] = [
            'segment'                   => 'CTT',
            'total_item_number'         => $data['item_ttl'],
        ];

        return $tran1;
    }


    /**
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map997($items, $conditions)
    {
        $results[] = $this->parse997($items);

        return $results;
    }

    private function parse997($items)
    {
        if(isset($items['isSuccess']))
            unset($items['isSuccess']);
        $tran1 = $items;
        $tran1['transaction_code'] = '997';
        return $tran1;
    }

    private function parsePickOut() {
        $doc = new DomDocument('1.0', 'utf-8');

        $StandardBusinessDocument = $doc->createElementNS("http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader","ns1:StandardBusinessDocument");
        $doc->appendChild($StandardBusinessDocument);

        // ns1:StandardBusinessDocumentHeader
        $StandardBusinessDocumentHeader = $doc->createElement('ns1:StandardBusinessDocumentHeader');
        $StandardBusinessDocument->appendChild($StandardBusinessDocumentHeader);

        $HeaderVersion = $doc->createElement("ns1:HeaderVersion", "2.2");
        $StandardBusinessDocumentHeader->appendChild($HeaderVersion);

        $Sender = $doc->createElement("ns1:Sender");
        $Identifier = $doc->createElement("ns1:Identifier", "8997204507003");
        $Sender->appendChild($Identifier);
        $StandardBusinessDocumentHeader->appendChild($Sender);

        $Receiver = $doc->createElement("ns1:Receiver");
        $Identifier = $doc->createElement("ns1:Identifier", "8712423000339");
        $Receiver->appendChild($Identifier);
        $StandardBusinessDocumentHeader->appendChild($Receiver);

        $DocumentIdentification = $doc->createElement("ns1:DocumentIdentification");
        $StandardBusinessDocumentHeader->appendChild($DocumentIdentification);

        $Standard = $doc->createElement("ns1:Standard", "EAN.UCC");
        $DocumentIdentification->appendChild($Standard);

        $TypeVersion = $doc->createElement("ns1:TypeVersion", "2.1.1");
        $DocumentIdentification->appendChild($TypeVersion);

        $InstanceIdentifier = $doc->createElement("ns1:InstanceIdentifier", "2019-04-11T17:48:05");
        $DocumentIdentification->appendChild($InstanceIdentifier);

        $Type = $doc->createElement("ns1:Type", "DespatchAdvice");
        $DocumentIdentification->appendChild($Type);

        $CreationDateAndTime = $doc->createElement("ns1:CreationDateAndTime", "2019-04-11T17:48:05.674");
        $DocumentIdentification->appendChild($CreationDateAndTime);

        $BusinessScope = $doc->createElement("ns1:BusinessScope");
        $StandardBusinessDocumentHeader->appendChild($BusinessScope);

        $Scope = $doc->createElement("ns1:Scope");
        $BusinessScope->appendChild($Scope);

        $Type = $doc->createElement("ns1:Type", "DELIVERY");
        $Scope->appendChild($Type);

        $InstanceIdentifier = $doc->createElement("ns1:InstanceIdentifier", "AAR_WCI");
        $Scope->appendChild($InstanceIdentifier);

        $Identifier = $doc->createElement("ns1:Identifier", "Picking_Only");
        $Scope->appendChild($Identifier);

        //ns2:message
        $message = $doc->createElementNS('urn:ean.ucc:2', 'ns2:message');
        $message->setAttribute("xmlns:ns3", "urn:ean.ucc:deliver:2");
        $message->setAttribute("xmlns:ns4", "urn:ean.ucc:align:2");

        $StandardBusinessDocument->appendChild($message);

        //entityIdentification
        $entityIdentification = $doc->createElement('entityIdentification');
        $entityIdentification = $message->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification", '4002683002');
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement("gln", "0000000000000");
        $contentOwner->appendChild($gln);

        //ns2:transaction
        $transaction = $doc->createElement('ns2:transaction');
        $message->appendChild($transaction);

        $entityIdentification = $doc->createElement('entityIdentification');
        $transaction->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification", '2019-04-11T17:48:05');
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $contentOwner = $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement("gln", "0000000000000");
        $contentOwner->appendChild($gln);

        // command
        $command = $doc->createElement('command');
        $transaction->appendChild($command);

        //documentCommand
        $documentCommand = $doc->createElement('ns2:documentCommand');
        $command->appendChild($documentCommand);

        //documentCommandHeader
        $documentCommandHeader = $doc->createElement('documentCommandHeader');
        $documentCommandHeader->setAttribute("type", "ADD");
        $documentCommand->appendChild($documentCommandHeader);

        $entityIdentification = $doc->createElement('entityIdentification');
        $documentCommandHeader->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement('uniqueCreatorIdentification', "2019-04-11T17:48:05");
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement('contentOwner');
        $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement('gln', "0000000000000");
        $contentOwner->appendChild($gln);

        //documentCommandOperand
        $documentCommandOperand = $doc->createElement('documentCommandOperand');
        $documentCommand->appendChild($documentCommandOperand);

        //despatchAdvice
        $despatchAdvice = $doc->createElement('ns3:despatchAdvice');
        $despatchAdvice->setAttribute("documentStatus", "ORIGINAL");
        $despatchAdvice->setAttribute("creationDateTime", "2019-04-11T17:48:05");
        $documentCommandOperand->appendChild($despatchAdvice);

        $despatchAdviceIdentification = $doc->createElement('despatchAdviceIdentification');
        $despatchAdvice->appendChild($despatchAdviceIdentification);

        $uniqueCreatorIdentification = $doc->createElement('uniqueCreatorIdentification',"2029635201");
        $despatchAdviceIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement('contentOwner');
        $despatchAdviceIdentification->appendChild($contentOwner);

        $gln = $doc->createElement('gln',"0000000000000");
        $contentOwner->appendChild($gln);

        $shipTo = $doc->createElement('shipTo');
        $despatchAdvice->appendChild($shipTo);

        $gln = $doc->createElement('gln', "0000000000000");
        $shipTo->appendChild($gln);

        $receiver = $doc->createElement('receiver');
        $despatchAdvice->appendChild($receiver);

        $gln = $doc->createElement('gln', "0000000000000");
        $receiver->appendChild($gln);

        $shipper = $doc->createElement('shipper');
        $despatchAdvice->appendChild($shipper);

        $gln = $doc->createElement('gln', "0000000000000");
        $shipper->appendChild($gln);

        $deliveryNote = $doc->createElement('deliveryNote');
        $despatchAdvice->appendChild($deliveryNote);

        $referenceDateTime = $doc->createElement('referenceDateTime', "2019-04-11T17:48:05");
        $deliveryNote->appendChild($referenceDateTime);

        $referenceIdentification = $doc->createElement('referenceIdentification', "2029635201");
        $deliveryNote->appendChild($referenceIdentification);

        $despatchInformation = $doc->createElement('despatchInformation');
        $despatchAdvice->appendChild($despatchInformation);

        $despatchDateTime = $doc->createElement('despatchDateTime', "2019-04-11T17:48:05");
        $despatchInformation->appendChild($despatchDateTime);

        $LotNo = $doc->createElement('LotNo',"9153");
        $despatchAdvice->appendChild($LotNo);

        $total = 2;
        for ($i=1; $i<=$total; $i++) {
            $despatchAdviceItemContainmentLineItem = $doc->createElement('despatchAdviceItemContainmentLineItem');
            $despatchAdviceItemContainmentLineItem->setAttribute("number","000060");
            $despatchAdvice->appendChild($despatchAdviceItemContainmentLineItem);

            $containedItemIdentification = $doc->createElement("containedItemIdentification");
            $despatchAdviceItemContainmentLineItem->appendChild($containedItemIdentification);

            $gtin = $doc->createElement("gtin", "00000000000000");
            $containedItemIdentification->appendChild($gtin);

            $additionalTradeItemIdentification = $doc->createElement("additionalTradeItemIdentification");
            $containedItemIdentification->appendChild($additionalTradeItemIdentification);

            $additionalTradeItemIdentificationValue = $doc->createElement("additionalTradeItemIdentificationValue", "67044825");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationValue);

            $additionalTradeItemIdentificationType = $doc->createElement("additionalTradeItemIdentificationType", "SUPPLIER_ASSIGNED");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationType);

            $quantityContained = $doc->createElement("quantityContained");
            $despatchAdviceItemContainmentLineItem->appendChild($quantityContained);

            $value = $doc->createElement("value", "64");
            $quantityContained->appendChild($value);

            $extendedAttributes = $doc->createElement("extendedAttributes");
            $despatchAdviceItemContainmentLineItem->appendChild($extendedAttributes);

            $batchNumber = $doc->createElement("batchNumber", "191162");
            $extendedAttributes->appendChild($batchNumber);
        }

        $partyDocument = $doc->createElement("ns4:partyDocument");
        $partyDocument->setAttribute("documentStatus", "ORIGINAL");
        $partyDocument->setAttribute("creationDateTime", "2019-04-11T17:48:05");
        $documentCommandOperand->appendChild($partyDocument);

        $partyDocumentNumber = $doc->createElement("partyDocumentNumber");
        $partyDocument->appendChild($partyDocumentNumber);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification");
        $partyDocumentNumber->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $partyDocumentNumber->appendChild($contentOwner);

        $gln = $doc->createElement("gln", "0000000000000");
        $contentOwner->appendChild($gln);

        $party = $doc->createElement("party");
        $partyDocument->appendChild($party);

        $partyIdentification = $doc->createElement("partyIdentification");
        $party->appendChild($partyIdentification);

        $gln = $doc->createElement("gln");
        $partyIdentification->appendChild($gln);

        $partyInformation = $doc->createElement("partyInformation");
        $party->appendChild($partyInformation);

        $partyDates = $doc->createElement("partyDates");
        $partyInformation->appendChild($partyDates);

        $partyStartDate = $doc->createElement("partyStartDate");
        $partyDates->appendChild($partyStartDate);

        $partyRole = $doc->createElement("partyRole");
        $partyInformation->appendChild($partyRole);

        $nameAndAddress = $doc->createElement("nameAndAddress");
        $partyInformation->appendChild($nameAndAddress);

        $city = $doc->createElement("city");
        $nameAndAddress->appendChild($city);

        $countryCode = $doc->createElement("countryCode");
        $nameAndAddress->appendChild($countryCode);

        $countryISOCode = $doc->createElement("countryISOCode");
        $countryCode->appendChild($countryISOCode);

        $languageOfTheParty = $doc->createElement("languageOfTheParty");
        $nameAndAddress->appendChild($languageOfTheParty);

        $languageISOCode = $doc->createElement("languageISOCode");
        $languageOfTheParty->appendChild($languageISOCode);

        $name = $doc->createElement("name");
        $nameAndAddress->appendChild($name);

        $results = $doc->saveXML();

        return $results;
    }

    private function parseGRNOut() {
        $doc = new DomDocument('1.0', 'UTF-8');

        $StandardBusinessDocument = $doc->createElement("ns1:StandardBusinessDocument");
        $StandardBusinessDocument->setAttribute("xmlns:ns1","http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader");
        $doc->appendChild($StandardBusinessDocument);

        $StandardBusinessDocumentHeader = $doc->createElement("ns1:StandardBusinessDocumentHeader");
        $StandardBusinessDocument->appendChild($StandardBusinessDocumentHeader);

        $HeaderVersion = $doc->createElement("ns1:HeaderVersion", "2.2");
        $StandardBusinessDocumentHeader->appendChild($HeaderVersion);

        $Sender = $doc->createElement("ns1:Sender");
        $StandardBusinessDocumentHeader->appendChild($Sender);

        $Identifier = $doc->createElement("ns1:Identifier", "8997204507003");
        $Identifier->setAttribute("Authority","EAN.UCC");
        $Sender->appendChild($Identifier);

        $Receiver = $doc->createElement("ns1:Receiver");
        $StandardBusinessDocumentHeader->appendChild($Receiver);

        $Identifier = $doc->createElement("ns1:Identifier", "8712423000339");
        $Identifier->setAttribute("Authority","EAN.UCC");
        $Receiver->appendChild($Identifier);

        $DocumentIdentification = $doc->createElement("ns1:DocumentIdentification");
        $StandardBusinessDocumentHeader->appendChild($DocumentIdentification);

        $Standard = $doc->createElement("ns1:Standard","EAN.UCC");
        $DocumentIdentification->appendChild($Standard);

        $TypeVersion = $doc->createElement("ns1:TypeVersion","2.1.1");
        $DocumentIdentification->appendChild($TypeVersion);

        $InstanceIdentifier = $doc->createElement("ns1:InstanceIdentifier","2019-04-11T02:51:11.0150");
        $DocumentIdentification->appendChild($InstanceIdentifier);

        $Type = $doc->createElement("ns1:Type","ReceivingAdvice");
        $DocumentIdentification->appendChild($Type);

        $CreationDateAndTime = $doc->createElement("ns1:CreationDateAndTime","2019-04-11T02:51:11.015");
        $DocumentIdentification->appendChild($CreationDateAndTime);

        // BusinessScope
        $BusinessScope = $doc->createElement("ns1:BusinessScope");
        $StandardBusinessDocumentHeader->appendChild($BusinessScope);

        $Scope = $doc->createElement("ns1:Scope");
        $BusinessScope->appendChild($Scope);

        $Type = $doc->createElement("ns1:Type", "DELIVERY");
        $Scope->appendChild($Type);

        $InstanceIdentifier = $doc->createElement("ns1:InstanceIdentifier", "AAR_WCI");
        $Scope->appendChild($InstanceIdentifier);

        $Identifier = $doc->createElement("ns1:Identifier", "GoodsReceipt_Delivery");
        $Scope->appendChild($Identifier);


        // ns2:message
        $message = $doc->createElement("ns2:message");
        $message->setAttribute("xmlns:ns2","urn:ean.ucc:2");
        $message->setAttribute("xmlns:ns3","urn:ean.ucc:deliver:2");
        $StandardBusinessDocument->appendChild($message);

        //entityIdentification
        $entityIdentification = $doc->createElement('entityIdentification');
        $message->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification", '2019-04-11T02:51:11.0150');
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement("gln", "0000000000000");
        $contentOwner->appendChild($gln);

        //ns2:transaction
        $transaction = $doc->createElement('ns2:transaction');
        $message->appendChild($transaction);

        $entityIdentification = $doc->createElement('entityIdentification');
        $transaction->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification", '2019-04-11T02:51:11.0150');
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $contentOwner = $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement("gln", "0000000000000");
        $contentOwner->appendChild($gln);

        // command
        $command = $doc->createElement('command');
        $transaction->appendChild($command);

        //documentCommand
        $documentCommand = $doc->createElement('ns2:documentCommand');
        $command->appendChild($documentCommand);

        //documentCommandHeader
        $documentCommandHeader = $doc->createElement('documentCommandHeader');
        $documentCommandHeader->setAttribute("type", "ADD");
        $documentCommand->appendChild($documentCommandHeader);

        $entityIdentification = $doc->createElement('entityIdentification');
        $documentCommandHeader->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement('uniqueCreatorIdentification', "2019-04-11T02:51:11.0150");
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement('contentOwner');
        $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement('gln', "0000000000000");
        $contentOwner->appendChild($gln);

        //documentCommandOperand
        $documentCommandOperand = $doc->createElement('documentCommandOperand');
        $documentCommand->appendChild($documentCommandOperand);

        //receivingAdvice
        $receivingAdvice = $doc->createElement('ns3:receivingAdvice');
        $receivingAdvice->setAttribute("creationDateTime", "2019-04-11T02:51:11.02");
        $receivingAdvice->setAttribute("documentStatus", "ORIGINAL");
        $documentCommandOperand->appendChild($receivingAdvice);

        $reportingCode = $doc->createElement('reportingCode', "CONFIRMATION");
        $receivingAdvice->appendChild($reportingCode);

        $receivingAdviceIdentification = $doc->createElement('receivingAdviceIdentification');
        $receivingAdvice->appendChild($receivingAdviceIdentification);

        $uniqueCreatorIdentification = $doc->createElement('uniqueCreatorIdentification',"110419");
        $receivingAdviceIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement('contentOwner');
        $receivingAdviceIdentification->appendChild($contentOwner);

        $gln = $doc->createElement('gln', "0000000000000");
        $contentOwner->appendChild($gln);

        $additionalPartyIdentification = $doc->createElement('additionalPartyIdentification');
        $contentOwner->appendChild($additionalPartyIdentification);

        $additionalPartyIdentificationValue = $doc->createElement('additionalPartyIdentificationValue', "2029653320");
        $additionalPartyIdentification->appendChild($additionalPartyIdentificationValue);

        $additionalPartyIdentificationType = $doc->createElement('additionalPartyIdentificationType', "FOR_INTERNAL_USE_6");
        $additionalPartyIdentification->appendChild($additionalPartyIdentificationType);

        $shipTo = $doc->createElement('shipTo');
        $receivingAdvice->appendChild($shipTo);

        $gln = $doc->createElement('gln', "0000000000000");
        $shipTo->appendChild($gln);

        $shipper = $doc->createElement('shipper');
        $receivingAdvice->appendChild($shipper);

        $gln = $doc->createElement('gln', "0000000000000");
        $shipper->appendChild($gln);

        $receiver = $doc->createElement('receiver');
        $receivingAdvice->appendChild($receiver);

        $gln = $doc->createElement('gln',"0000000000000");
        $receiver->appendChild($gln);

        $additionalPartyIdentification = $doc->createElement('additionalPartyIdentification');
        $receiver->appendChild($additionalPartyIdentification);

        $additionalPartyIdentificationValue = $doc->createElement('additionalPartyIdentificationValue',"9153");
        $additionalPartyIdentification->appendChild($additionalPartyIdentificationValue);

        $additionalPartyIdentificationType = $doc->createElement('additionalPartyIdentificationType',"SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY");
        $additionalPartyIdentification->appendChild($additionalPartyIdentificationType);

        // receivingAdviceLogisticUnitLineItem
        $receivingAdviceLogisticUnitLineItem = $doc->createElement('receivingAdviceLogisticUnitLineItem');
        $receivingAdvice->appendChild($receivingAdviceLogisticUnitLineItem);

        $logisticUnitIdentification = $doc->createElement('logisticUnitIdentification');
        $receivingAdviceLogisticUnitLineItem->appendChild($logisticUnitIdentification);

        $serialShipmentContainerCode = $doc->createElement('serialShipmentContainerCode');
        $logisticUnitIdentification->appendChild($serialShipmentContainerCode);

        $serialShippingContainerCode = $doc->createElement('serialShippingContainerCode');
        $serialShipmentContainerCode->appendChild($serialShippingContainerCode);

        $n = 1;
        for ($i=1; $i<=2; $i++) {
            $receivingAdviceItemContainmentLineItem = $doc->createElement('receivingAdviceItemContainmentLineItem');
            $receivingAdviceItemContainmentLineItem->setAttribute("number","900001");
            $receivingAdviceLogisticUnitLineItem->appendChild($receivingAdviceItemContainmentLineItem);

            $containedItemIdentification = $doc->createElement('containedItemIdentification');
            $receivingAdviceItemContainmentLineItem->appendChild($containedItemIdentification);

            $gtin = $doc->createElement('gtin', "00000000000000");
            $containedItemIdentification->appendChild($gtin);

            $additionalTradeItemIdentification = $doc->createElement('additionalTradeItemIdentification');
            $containedItemIdentification->appendChild($additionalTradeItemIdentification);

            $additionalTradeItemIdentificationValue = $doc->createElement('additionalTradeItemIdentificationValue',"9911");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationValue);

            $additionalTradeItemIdentificationType = $doc->createElement('additionalTradeItemIdentificationType',"FOR_INTERNAL_USE_2");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationType);

            //
            $additionalTradeItemIdentification = $doc->createElement('additionalTradeItemIdentification');
            $containedItemIdentification->appendChild($additionalTradeItemIdentification);

            $additionalTradeItemIdentificationValue = $doc->createElement('additionalTradeItemIdentificationValue',"67005545");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationValue);

            $additionalTradeItemIdentificationType = $doc->createElement('additionalTradeItemIdentificationType',"SUPPLIER_ASSIGNED");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationType);

            //
            $additionalTradeItemIdentification = $doc->createElement('additionalTradeItemIdentification');
            $containedItemIdentification->appendChild($additionalTradeItemIdentification);

            $additionalTradeItemIdentificationValue = $doc->createElement('additionalTradeItemIdentificationValue',"AVAILABLE_FOR_SALE");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationValue);

            $additionalTradeItemIdentificationType = $doc->createElement('additionalTradeItemIdentificationType',"FOR_INTERNAL_USE_8");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationType);

            //
            $additionalTradeItemIdentification = $doc->createElement('additionalTradeItemIdentification');
            $containedItemIdentification->appendChild($additionalTradeItemIdentification);

            $additionalTradeItemIdentificationValue = $doc->createElement('additionalTradeItemIdentificationValue');
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationValue);

            $additionalTradeItemIdentificationType = $doc->createElement('additionalTradeItemIdentificationType',"StockType");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationType);

            //
            $extendedAttributes = $doc->createElement('extendedAttributes');
            $receivingAdviceItemContainmentLineItem->appendChild($extendedAttributes);

            $itemExpirationDate = $doc->createElement('itemExpirationDate', "2021-09-13");
            $extendedAttributes->appendChild($itemExpirationDate);

            $batchNumber = $doc->createElement('batchNumber', "191133");
            $extendedAttributes->appendChild($batchNumber);

            $productionDate = $doc->createElement('productionDate', "2019-03-13");
            $extendedAttributes->appendChild($productionDate);

            // quantityAccepted
            $quantityAccepted = $doc->createElement('quantityAccepted');
            $receivingAdviceItemContainmentLineItem->appendChild($quantityAccepted);

            $value = $doc->createElement('value', "0");
            $quantityAccepted->appendChild($value);

            //quantityReceived
            $quantityReceived = $doc->createElement('quantityReceived');
            $receivingAdviceItemContainmentLineItem->appendChild($quantityReceived);

            $value = $doc->createElement('value', "44");
            $quantityReceived->appendChild($value);

            $unitOfMeasure = $doc->createElement('unitOfMeasure');
            $quantityReceived->appendChild($unitOfMeasure);

            $measurementUnitCodeValue = $doc->createElement('measurementUnitCodeValue', "CS");
            $unitOfMeasure->appendChild($measurementUnitCodeValue);

        }

        $n_receiptInformation = 2;
        for ($i = 1;$i<=$n_receiptInformation; $i++) {
            $receiptInformation = $doc->createElement('receiptInformation');
            $receivingAdvice->appendChild($receiptInformation);

            $receivingDateTime = $doc->createElement('receivingDateTime', "2019-04-11T00:00:00.000");
            $receiptInformation->appendChild($receivingDateTime);
        }


        $despatchAdvice = $doc->createElement('despatchAdvice');
        $receivingAdvice->appendChild($despatchAdvice);

        $uniqueCreatorIdentification = $doc->createElement('uniqueCreatorIdentification', "2029653320");
        $despatchAdvice->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement('contentOwner');
        $despatchAdvice->appendChild($contentOwner);

        $gln = $doc->createElement('gln', "0000000000000");
        $contentOwner->appendChild($gln);

        $results = $doc->saveXML();
        return $results;
    }

    private function parseDispatchOut() {
        $doc = new DomDocument('1.0', 'utf-8');

        $transportStatusNotificationMessage = $doc->createElement("trsn:transportStatusNotificationMessage");
        $transportStatusNotificationMessage->setAttribute("xmlns:trsn","urn:gs1:ecom:transport_status_notification:xsd:3");
        $transportStatusNotificationMessage->setAttribute("xmlns:userCSharp","http://schemas.microsoft.com/BizTalk/2003/userCSharp");
        $transportStatusNotificationMessage->setAttribute("xmlns:ns1","http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader");
        $doc->appendChild($transportStatusNotificationMessage);


        //StandardBusinessDocumentHeader
        $StandardBusinessDocumentHeader  = $doc->createElement('ns1:StandardBusinessDocumentHeader');
        $transportStatusNotificationMessage->appendChild($StandardBusinessDocumentHeader);

        $HeaderVersion  = $doc->createElement('ns1:HeaderVersion',"1.0");
        $StandardBusinessDocumentHeader->appendChild($HeaderVersion);

        // Sender
        $Sender  = $doc->createElement('ns1:Sender');
        $StandardBusinessDocumentHeader->appendChild($Sender);

        $Identifier  = $doc->createElement('ns1:Identifier',"8997204507003");
        $Identifier->setAttribute("Authority","GS1");
        $Sender->appendChild($Identifier);

        // Receiver
        $Receiver  = $doc->createElement('ns1:Receiver');
        $StandardBusinessDocumentHeader->appendChild($Receiver);

        $Identifier  = $doc->createElement('ns1:Identifier',"8712423000339");
        $Identifier->setAttribute("Authority","GS1");
        $Receiver->appendChild($Identifier);

        // DocumentIdentification
        $DocumentIdentification  = $doc->createElement('ns1:DocumentIdentification');
        $StandardBusinessDocumentHeader->appendChild($DocumentIdentification);

        $Standard  = $doc->createElement('ns1:Standard', "GS1");
        $DocumentIdentification->appendChild($Standard);

        $TypeVersion  = $doc->createElement('ns1:TypeVersion', "3.2");
        $DocumentIdentification->appendChild($TypeVersion);

        $InstanceIdentifier  = $doc->createElement('ns1:InstanceIdentifier', "20190411120000");
        $DocumentIdentification->appendChild($InstanceIdentifier);

        $Type  = $doc->createElement('ns1:Type', "TransportStatusNotification");
        $DocumentIdentification->appendChild($Type);

        $MultipleType  = $doc->createElement('ns1:MultipleType', "False");
        $DocumentIdentification->appendChild($MultipleType);

        $CreationDateAndTime  = $doc->createElement('ns1:CreationDateAndTime', "2019-04-11T21:12:07.999+00:00");
        $DocumentIdentification->appendChild($CreationDateAndTime);

        // BusinessScope
        $BusinessScope  = $doc->createElement('ns1:BusinessScope');
        $StandardBusinessDocumentHeader->appendChild($BusinessScope);

        $Scope  = $doc->createElement('ns1:Scope');
        $BusinessScope->appendChild($Scope);

        $Type  = $doc->createElement('ns1:Type', "SHIPMENTEXECUTION");
        $Scope->appendChild($Type);

        $InstanceIdentifier  = $doc->createElement('ns1:InstanceIdentifier', "U2K2");
        $Scope->appendChild($InstanceIdentifier);

        $Identifier  = $doc->createElement('ns1:Identifier', "ShipmentUpdate");
        $Scope->appendChild($Identifier);

        // transportStatusNotification
        $transportStatusNotification  = $doc->createElement('transportStatusNotification');
        $transportStatusNotificationMessage->appendChild($transportStatusNotification);

        $creationDateTime  = $doc->createElement('creationDateTime', "2019-04-11T21:12:07.999");
        $transportStatusNotification->appendChild($creationDateTime);

        $documentStatusCode  = $doc->createElement('documentStatusCode', "ORIGINAL");
        $transportStatusNotification->appendChild($documentStatusCode);

        $transportStatusNotificationIdentification  = $doc->createElement('transportStatusNotificationIdentification');
        $transportStatusNotification->appendChild($transportStatusNotificationIdentification);

        $entityIdentification  = $doc->createElement('entityIdentification', "4002683002");
        $transportStatusNotificationIdentification->appendChild($entityIdentification);

        $transportStatusInformationCode  = $doc->createElement('transportStatusInformationCode', "INFORMATION_ON_DELIVERY");
        $transportStatusNotification->appendChild($transportStatusInformationCode);

        $transportStatusObjectCode  = $doc->createElement('transportStatusObjectCode', "SHIPMENT");
        $transportStatusNotification->appendChild($transportStatusObjectCode);

        $transportStatusRequestor  = $doc->createElement('transportStatusRequestor');
        $transportStatusNotification->appendChild($transportStatusRequestor);

        $gln  = $doc->createElement('gln', "8712423000339");
        $transportStatusRequestor->appendChild($gln);

        $transportStatusProvider  = $doc->createElement('transportStatusProvider');
        $transportStatusNotification->appendChild($transportStatusProvider);

        $gln  = $doc->createElement('gln', "KBKSCB");
        $transportStatusProvider->appendChild($gln);

        $transportStatusNotificationShipment  = $doc->createElement('transportStatusNotificationShipment');
        $transportStatusNotification->appendChild($transportStatusNotificationShipment);

        $gsin  = $doc->createElement('gsin', "00000000000000000");
        $transportStatusNotificationShipment->appendChild($gsin);

        $transportStatus  = $doc->createElement('transportStatus');
        $transportStatusNotificationShipment->appendChild($transportStatus);

        $transportStatusConditionCode  = $doc->createElement('transportStatusConditionCode', "00");
        $transportStatus->appendChild($transportStatusConditionCode);

        $results = $doc->saveXML();
        return $results;
    }

    private function parseInventoryCorrectionOut() {
        $doc = new DomDocument('1.0', 'utf-8');

        $StandardBusinessDocument = $doc->createElement("ns1:StandardBusinessDocument");
        $StandardBusinessDocument->setAttribute("xmlns:ns1","http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader");
        $StandardBusinessDocument->setAttribute("xmlns:ns2","urn:ean.ucc:2");
        $StandardBusinessDocument->setAttribute("xmlns:ns3","urn:ean.ucc:deliver:2");
        $doc->appendChild($StandardBusinessDocument);

        // StandardBusinessDocumentHeader
        $StandardBusinessDocumentHeader = $doc->createElement("ns1:StandardBusinessDocumentHeader");
        $StandardBusinessDocument->appendChild($StandardBusinessDocumentHeader);

        // HeaderVersion
        $HeaderVersion = $doc->createElement("ns1:HeaderVersion", "2.2");
        $StandardBusinessDocumentHeader->appendChild($HeaderVersion);

        // HeaderVersion
        $Sender = $doc->createElement("ns1:Sender");
        $StandardBusinessDocumentHeader->appendChild($Sender);

            $Identifier = $doc->createElement("ns1:Identifier", "8997204507003");
            $Identifier->setAttribute("Authority", "EAN.UCC");
            $Sender->appendChild($Identifier);

        // HeaderVersion
        $Receiver = $doc->createElement("ns1:Receiver");
        $StandardBusinessDocumentHeader->appendChild($Receiver);

            $Identifier = $doc->createElement("ns1:Identifier", "8712423000339");
            $Identifier->setAttribute("Authority", "EAN.UCC");
            $Receiver->appendChild($Identifier);

        // DocumentIdentification
        $DocumentIdentification = $doc->createElement("ns1:DocumentIdentification");
        $StandardBusinessDocumentHeader->appendChild($DocumentIdentification);

            $Standard = $doc->createElement("ns1:Standard", "EAN.UCC");
            $DocumentIdentification->appendChild($Standard);

            $TypeVersion = $doc->createElement("ns1:TypeVersion", "2.1");
            $DocumentIdentification->appendChild($TypeVersion);

            $InstanceIdentifier = $doc->createElement("ns1:InstanceIdentifier", "20180127114140");
            $DocumentIdentification->appendChild($InstanceIdentifier);

            $Type = $doc->createElement("ns1:Type", "InventoryActivityOrInventoryStatus");
            $DocumentIdentification->appendChild($Type);

            $CreationDateAndTime = $doc->createElement("ns1:CreationDateAndTime", "2019-04-05T07:39:59.595");
            $DocumentIdentification->appendChild($CreationDateAndTime);

        $BusinessScope = $doc->createElement("ns1:BusinessScope");
        $StandardBusinessDocumentHeader->appendChild($BusinessScope);

        $Scope = $doc->createElement("ns1:Scope");
        $BusinessScope->appendChild($Scope);

        $Type = $doc->createElement("ns1:Type", "DELIVER");
        $Scope->appendChild($Type);

        $InstanceIdentifier = $doc->createElement("ns1:InstanceIdentifier", "WCI");
        $Scope->appendChild($InstanceIdentifier);

        $Identifier = $doc->createElement("ns1:Identifier", "StockStatusChange");
        $Scope->appendChild($Identifier);

        // message
        $message = $doc->createElement("ns2:message");
        $StandardBusinessDocument->appendChild($message);

        // entityIdentification
        $entityIdentification = $doc->createElement("entityIdentification");
        $message->appendChild($entityIdentification);

        // uniqueCreatorIdentification
            $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification", "KBKSCB-IM-190400064");
            $entityIdentification->appendChild($uniqueCreatorIdentification);

            $contentOwner = $doc->createElement("contentOwner");
            $entityIdentification->appendChild($contentOwner);

            $gln = $doc->createElement("gln", "0000000000000");
            $contentOwner->appendChild($gln);

        // transaction
        $transaction = $doc->createElement("ns2:transaction");
        $message->appendChild($transaction);

        $entityIdentification = $doc->createElement("entityIdentification");
        $transaction->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification");
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement("gln");
        $contentOwner->appendChild($gln);

        // command
        $command = $doc->createElement("command");
        $transaction->appendChild($command);

        // documentCommand
        $documentCommand = $doc->createElement("ns2:documentCommand");
        $command->appendChild($documentCommand);

        // documentCommandHeader
        $documentCommandHeader = $doc->createElement("documentCommandHeader");
        $documentCommandHeader->setAttribute("type","ADD");
        $documentCommand->appendChild($documentCommandHeader);

        $entityIdentification = $doc->createElement("entityIdentification");
        $documentCommandHeader->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification", "KBKSCB-IM-190400064");
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement("gln", "0000000000000");
        $contentOwner->appendChild($gln);

        // documentCommandOperand
        $documentCommandOperand = $doc->createElement("documentCommandOperand");
        $documentCommand->appendChild($documentCommandOperand);

        // inventoryActivityOrInventoryStatus
        $inventoryActivityOrInventoryStatus = $doc->createElement("ns3:inventoryActivityOrInventoryStatus");
        $inventoryActivityOrInventoryStatus->setAttribute("creationDateTime","2019-04-05T07:39:59");
        $inventoryActivityOrInventoryStatus->setAttribute("documentStatus","ORIGINAL");
        $documentCommandOperand->appendChild($inventoryActivityOrInventoryStatus);

        $contentVersion = $doc->createElement("contentVersion");
        $inventoryActivityOrInventoryStatus->appendChild($contentVersion);

        $versionIdentification = $doc->createElement("versionIdentification", "2.1");
        $contentVersion->appendChild($versionIdentification);

        $documentStructureVersion = $doc->createElement("documentStructureVersion");
        $inventoryActivityOrInventoryStatus->appendChild($documentStructureVersion);

        $versionIdentification = $doc->createElement("versionIdentification", "2.1" );
        $documentStructureVersion->appendChild($versionIdentification);

        $inventoryActivityOrInventoryStatusIdentification = $doc->createElement("inventoryActivityOrInventoryStatusIdentification");
        $inventoryActivityOrInventoryStatus->appendChild($inventoryActivityOrInventoryStatusIdentification);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification", "DMG-MFG/9914/2029266662");
        $inventoryActivityOrInventoryStatusIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $inventoryActivityOrInventoryStatusIdentification->appendChild($contentOwner);

        $gln = $doc->createElement("gln", "8994274017003");
        $contentOwner->appendChild($gln);

        $beginDateTime = $doc->createElement("beginDateTime", "2019-04-05T07:39:59");
        $inventoryActivityOrInventoryStatus->appendChild($beginDateTime);

        $inventoryDocumentType = $doc->createElement("inventoryDocumentType", "INVENTORY_ACTIVITY");
        $inventoryActivityOrInventoryStatus->appendChild($inventoryDocumentType);

        $structureType = $doc->createElement("structureType", "LOCATION_BY_ITEM");
        $inventoryActivityOrInventoryStatus->appendChild($structureType);

        $inventoryReportingParty = $doc->createElement("inventoryReportingParty");
        $inventoryActivityOrInventoryStatus->appendChild($inventoryReportingParty);

        $gln = $doc->createElement("gln", "0000000000000");
        $inventoryReportingParty->appendChild($gln);

        $additionalPartyIdentification = $doc->createElement("additionalPartyIdentification");
        $inventoryReportingParty->appendChild($additionalPartyIdentification);

        $additionalPartyIdentificationValue = $doc->createElement("additionalPartyIdentificationValue");
        $additionalPartyIdentification->appendChild($additionalPartyIdentificationValue);

        $additionalPartyIdentificationType = $doc->createElement("additionalPartyIdentificationType", "FOR_INTERNAL_USE_2");
        $additionalPartyIdentification->appendChild($additionalPartyIdentificationType);

        $inventoryReportToParty = $doc->createElement("inventoryReportToParty");
        $inventoryActivityOrInventoryStatus->appendChild($inventoryReportToParty);

        $gln = $doc->createElement("gln", "00000000000000");
        $inventoryReportToParty->appendChild($gln);

        $inventoryItemLocationInformation = $doc->createElement("inventoryItemLocationInformation");
        $inventoryActivityOrInventoryStatus->appendChild($inventoryItemLocationInformation);

        $tradeItemIdentification = $doc->createElement("tradeItemIdentification");
        $inventoryItemLocationInformation->appendChild($tradeItemIdentification);

        $gtin = $doc->createElement("gtin", "00000000000000");
        $tradeItemIdentification->appendChild($gtin);

        $additionalTradeItemIdentification = $doc->createElement("additionalTradeItemIdentification");
        $tradeItemIdentification->appendChild($additionalTradeItemIdentification);

        $additionalTradeItemIdentificationValue = $doc->createElement("additionalTradeItemIdentificationValue", "20066269");
        $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationValue);

        $additionalTradeItemIdentificationType = $doc->createElement("additionalTradeItemIdentificationType", "SUPPLIER_ASSIGNED");
        $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationType);

        $additionalTradeItemIdentificationValue = $doc->createElement("additionalTradeItemIdentificationValue");
        $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationValue);

        $additionalTradeItemIdentificationType = $doc->createElement("additionalTradeItemIdentificationType", "FOR_INTERNAL_USE_3");
        $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationType);

        $additionalTradeItemIdentificationValue = $doc->createElement("additionalTradeItemIdentificationValue");
        $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationValue);

        $additionalTradeItemIdentificationType = $doc->createElement("additionalTradeItemIdentificationType", "FOR_INTERNAL_USE_4");
        $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationType);

        $inventoryLocation = $doc->createElement("inventoryLocation");
        $inventoryItemLocationInformation->appendChild($inventoryLocation);

        $gln = $doc->createElement("gln", "8997204507249");
        $inventoryLocation->appendChild($gln);

        $inventoryActivityLineItem = $doc->createElement("inventoryActivityLineItem");
        $inventoryActivityLineItem->setAttribute("number","1");
        $inventoryItemLocationInformation->appendChild($inventoryActivityLineItem);

        $inventorySublocation = $doc->createElement("inventorySublocation");
        $inventoryActivityLineItem->appendChild($inventorySublocation);

        $gln = $doc->createElement("gln", "8997204507249");
        $inventorySublocation->appendChild($gln);

        $additionalPartyIdentification = $doc->createElement("additionalPartyIdentification");
        $inventorySublocation->appendChild($additionalPartyIdentification);

        $additionalPartyIdentificationValue = $doc->createElement("additionalPartyIdentificationValue", "9911");
        $additionalPartyIdentification->appendChild($additionalPartyIdentificationValue);

        $additionalPartyIdentificationType = $doc->createElement("additionalPartyIdentificationType", "SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY");
        $additionalPartyIdentification->appendChild($additionalPartyIdentificationType);

        $inventoryActivityQuantitySpecification = $doc->createElement("inventoryActivityQuantitySpecification");
        $inventoryActivityQuantitySpecification->setAttribute("inventoryActivityType", "BLOCKED_FROM_QI");
        $inventoryActivityLineItem->appendChild($inventoryActivityQuantitySpecification);

        $quantityOfUnits = $doc->createElement("quantityOfUnits");
        $inventoryActivityQuantitySpecification->appendChild($quantityOfUnits);

        $value = $doc->createElement("value", "10");
        $quantityOfUnits->appendChild($value);

        $unitOfMeasure = $doc->createElement("unitOfMeasure");
        $quantityOfUnits->appendChild($unitOfMeasure);

        $measurementUnitCodeValue = $doc->createElement("measurementUnitCodeValue", "CS");
        $unitOfMeasure->appendChild($measurementUnitCodeValue);

        $transactionalItemData = $doc->createElement("transactionalItemData");
        $inventoryActivityQuantitySpecification->appendChild($transactionalItemData);

        $batchNumber = $doc->createElement("batchNumber", "190872");
        $transactionalItemData->appendChild($batchNumber);

        $results = $doc->saveXML();
        return $results;
    }

    private function parseStockOut() {
        $doc = new DomDocument('1.0', 'utf-8');

        $StandardBusinessDocument = $doc->createElement("ns1:StandardBusinessDocument");
        $StandardBusinessDocument->setAttribute("xmlns:ns1", "http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader");
        $StandardBusinessDocument->setAttribute("xmlns:ns2", "urn:ean.ucc:2");
        $StandardBusinessDocument->setAttribute("xmlns:ns3", "urn:ean.ucc:deliver:2");
        $doc->appendChild($StandardBusinessDocument);

        // ns1:StandardBusinessDocumentHeader
        $StandardBusinessDocumentHeader = $doc->createElement('ns1:StandardBusinessDocumentHeader');
        $StandardBusinessDocument->appendChild($StandardBusinessDocumentHeader);

        $HeaderVersion = $doc->createElement("ns1:HeaderVersion", "2.2");
        $StandardBusinessDocumentHeader->appendChild($HeaderVersion);

        $Sender = $doc->createElement("ns1:Sender");
        $Identifier = $doc->createElement("ns1:Identifier", "8997204507003");
        $Sender->appendChild($Identifier);
        $StandardBusinessDocumentHeader->appendChild($Sender);

        $Receiver = $doc->createElement("ns1:Receiver");
        $Identifier = $doc->createElement("ns1:Identifier", "8712423000339");
        $Receiver->appendChild($Identifier);
        $StandardBusinessDocumentHeader->appendChild($Receiver);

        $DocumentIdentification = $doc->createElement("ns1:DocumentIdentification");
        $StandardBusinessDocumentHeader->appendChild($DocumentIdentification);

        $Standard = $doc->createElement("ns1:Standard", "EAN.UCC");
        $DocumentIdentification->appendChild($Standard);

        $TypeVersion = $doc->createElement("ns1:TypeVersion", "2.1");
        $DocumentIdentification->appendChild($TypeVersion);

        $InstanceIdentifier = $doc->createElement("ns1:InstanceIdentifier", "2019-04-11T07:05:32");
        $DocumentIdentification->appendChild($InstanceIdentifier);

        $Type = $doc->createElement("ns1:Type", "InventoryActivityOrInventoryStatus");
        $DocumentIdentification->appendChild($Type);

        $CreationDateAndTime = $doc->createElement("ns1:CreationDateAndTime", "2019-04-11T07:05:32.362");
        $DocumentIdentification->appendChild($CreationDateAndTime);

        $BusinessScope = $doc->createElement("ns1:BusinessScope");
        $StandardBusinessDocumentHeader->appendChild($BusinessScope);

        $Scope = $doc->createElement("ns1:Scope");
        $BusinessScope->appendChild($Scope);

        $Type = $doc->createElement("ns1:Type", "DELIVER");
        $Scope->appendChild($Type);

        $InstanceIdentifier = $doc->createElement("ns1:InstanceIdentifier", "WCI");
        $Scope->appendChild($InstanceIdentifier);

        $Identifier = $doc->createElement("ns1:Identifier", "Reconciliation_Activity");
        $Scope->appendChild($Identifier);

        //ns2:message
        $message = $doc->createElement( 'ns2:message');
        $StandardBusinessDocument->appendChild($message);

        //entityIdentification
        $entityIdentification = $doc->createElement('entityIdentification');
        $entityIdentification = $message->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification", 'KBKSCB-9911_20190411');
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement("gln", "8997204507249");
        $contentOwner->appendChild($gln);

        //ns2:transaction
        $transaction = $doc->createElement('ns2:transaction');
        $message->appendChild($transaction);

        $entityIdentification = $doc->createElement('entityIdentification');
        $transaction->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement("uniqueCreatorIdentification", '2019-04-11T07:05:32');
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement("contentOwner");
        $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement("gln", "8997204507249");
        $contentOwner->appendChild($gln);

        // command
        $command = $doc->createElement('command');
        $transaction->appendChild($command);

        //documentCommand
        $documentCommand = $doc->createElement('ns2:documentCommand');
        $command->appendChild($documentCommand);

        //documentCommandHeader
        $documentCommandHeader = $doc->createElement('documentCommandHeader');
        $documentCommandHeader->setAttribute("type", "ADD");
        $documentCommand->appendChild($documentCommandHeader);

        $entityIdentification = $doc->createElement('entityIdentification');
        $documentCommandHeader->appendChild($entityIdentification);

        $uniqueCreatorIdentification = $doc->createElement('uniqueCreatorIdentification', "KBKSCB-9911");
        $entityIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement('contentOwner');
        $entityIdentification->appendChild($contentOwner);

        $gln = $doc->createElement('gln', "8997204507249");
        $contentOwner->appendChild($gln);

        //documentCommandOperand
        $documentCommandOperand = $doc->createElement('documentCommandOperand');
        $documentCommand->appendChild($documentCommandOperand);

        //inventoryActivityOrInventoryStatus
        $inventoryActivityOrInventoryStatus = $doc->createElement('ns3:inventoryActivityOrInventoryStatus');
        $inventoryActivityOrInventoryStatus->setAttribute("creationDateTime", "2019-04-11T07:05:32");
        $inventoryActivityOrInventoryStatus->setAttribute("documentStatus", "ORIGINAL");
        $documentCommandOperand->appendChild($inventoryActivityOrInventoryStatus);

        $contentVersion = $doc->createElement('contentVersion');
        $inventoryActivityOrInventoryStatus->appendChild($contentVersion);

        $versionIdentification = $doc->createElement('versionIdentification', "2.1");
        $contentVersion->appendChild($versionIdentification);

        $documentStructureVersion = $doc->createElement('documentStructureVersion');
        $inventoryActivityOrInventoryStatus->appendChild($documentStructureVersion);

        $versionIdentification = $doc->createElement('versionIdentification', "2.1");
        $documentStructureVersion->appendChild($versionIdentification);

        $inventoryActivityOrInventoryStatusIdentification = $doc->createElement('inventoryActivityOrInventoryStatusIdentification');
        $inventoryActivityOrInventoryStatus->appendChild($inventoryActivityOrInventoryStatusIdentification);

        $uniqueCreatorIdentification = $doc->createElement('uniqueCreatorIdentification', "KBKSCB-9911");
        $inventoryActivityOrInventoryStatusIdentification->appendChild($uniqueCreatorIdentification);

        $contentOwner = $doc->createElement('contentOwner');
        $inventoryActivityOrInventoryStatusIdentification->appendChild($contentOwner);

        $gln = $doc->createElement('gln', "8997204507249");
        $contentOwner->appendChild($gln);

        $beginDateTime = $doc->createElement('beginDateTime', "2019-04-11T07:05:32");
        $inventoryActivityOrInventoryStatus->appendChild($beginDateTime);

        $inventoryDocumentType = $doc->createElement('inventoryDocumentType', "INVENTORY_STATUS");
        $inventoryActivityOrInventoryStatus->appendChild($inventoryDocumentType);

        $structureType = $doc->createElement('structureType', "LOCATION_BY_ITEM");
        $inventoryActivityOrInventoryStatus->appendChild($structureType);

        $inventoryReportingParty = $doc->createElement('inventoryReportingParty');
        $inventoryActivityOrInventoryStatus->appendChild($inventoryReportingParty);

        $gln = $doc->createElement('gln', "8997204507249");
        $inventoryReportingParty->appendChild($gln);

        $inventoryReportToParty = $doc->createElement('inventoryReportToParty');
        $inventoryActivityOrInventoryStatus->appendChild($inventoryReportToParty);

        $gln = $doc->createElement('gln', "8712423000339");
        $inventoryReportToParty->appendChild($gln);

        $total = 1;
        for ($i=0; $i<$total; $i++) {
            $inventoryItemLocationInformation = $doc->createElement('inventoryItemLocationInformation');
            $inventoryActivityOrInventoryStatus->appendChild($inventoryItemLocationInformation);


            $tradeItemIdentification = $doc->createElement('tradeItemIdentification');
            $inventoryItemLocationInformation->appendChild($tradeItemIdentification);

            $gtin = $doc->createElement('gtin', "0000000000000");
            $tradeItemIdentification->appendChild($gtin);

            $additionalTradeItemIdentification = $doc->createElement('additionalTradeItemIdentification');
            $tradeItemIdentification->appendChild($additionalTradeItemIdentification);

            $additionalTradeItemIdentificationValue = $doc->createElement('additionalTradeItemIdentificationValue', "20066269");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationValue);

            $additionalTradeItemIdentificationType = $doc->createElement('additionalTradeItemIdentificationType', "SUPPLIER_ASSIGNED");
            $additionalTradeItemIdentification->appendChild($additionalTradeItemIdentificationType);

            $inventoryLocation = $doc->createElement('inventoryLocation');
            $inventoryItemLocationInformation->appendChild($inventoryLocation);

            $gln = $doc->createElement('gln', "8997204507249");
            $inventoryLocation->appendChild($gln);

            $inventoryStatusLineItem = $doc->createElement('inventoryStatusLineItem');
            $inventoryStatusLineItem->setAttribute("number","1");
            $inventoryItemLocationInformation->appendChild($inventoryStatusLineItem);

            $inventorySublocation = $doc->createElement('inventorySublocation');
            $inventoryStatusLineItem->appendChild($inventorySublocation);

            $gln = $doc->createElement('gln', "0000000000000");
            $inventorySublocation->appendChild($gln);

            $additionalPartyIdentification = $doc->createElement('additionalPartyIdentification');
            $inventorySublocation->appendChild($additionalPartyIdentification);

            $additionalPartyIdentificationValue = $doc->createElement('additionalPartyIdentificationValue', "9911");
            $additionalPartyIdentification->appendChild($additionalPartyIdentificationValue);

            $additionalPartyIdentificationType = $doc->createElement('additionalPartyIdentificationType', "SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY");
            $additionalPartyIdentification->appendChild($additionalPartyIdentificationType);

            $inventoryStatusQuantitySpecification = $doc->createElement('inventoryStatusQuantitySpecification');
            $inventoryStatusQuantitySpecification->setAttribute("inventoryStatusType","AVAILABLE_FOR_SALE");
            $inventoryStatusLineItem->appendChild($inventoryStatusQuantitySpecification);

            $quantityOfUnits = $doc->createElement('quantityOfUnits');
            $inventoryStatusQuantitySpecification->appendChild($quantityOfUnits);

            $value = $doc->createElement('value', "3");
            $quantityOfUnits->appendChild($value);

            $unitOfMeasure = $doc->createElement('unitOfMeasure');
            $quantityOfUnits->appendChild($unitOfMeasure);

            $measurementUnitCodeValue = $doc->createElement('measurementUnitCodeValue', "CS");
            $unitOfMeasure->appendChild($measurementUnitCodeValue);

            $transactionalItemData = $doc->createElement('transactionalItemData');
            $inventoryStatusQuantitySpecification->appendChild($transactionalItemData);

            $batchNumber = $doc->createElement('batchNumber', "183463");
            $transactionalItemData->appendChild($batchNumber);

        }

        $results = $doc->saveXML();

        return $results;
    }
}
