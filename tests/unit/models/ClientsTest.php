<?php

namespace tests\unit\models;

use App\Clients;
use App\SshKeys;
use TestCase;

/**
 * Class SshKeysTest
 *
 * @package tests\unit\models
 */
class ClientsTest extends TestCase
{

    /**
     * @var Clients
     */
    protected $client;

    /**
     * @var array
     */
    protected $raw_data = [];

    /**
     * @var array
     */
    protected $ssh_keys_raw_data = [];

    /**
     * ClientsTest constructor.
     */
    public function __construct()
    {
        $this->raw_data = [
            'name'        => sprintf('name_%s', str_random(50)),
            'short_name'  => sprintf('sn_%s', str_random(5)),
            'description' => sprintf('data_%s', str_random(200)),
            'status'      => rand(0, 1),
            'type_id'     => 1,
        ];

        $this->client = new Clients();
    }

    /**
     *
     */
    public function testSshKeys()
    {
        $key = new SshKeys();
        $client = Clients::first();
        $this->ssh_keys_raw_data['client_id'] = $client->id;

        //Create key belong to client
        $key->setRawAttributes($this->ssh_keys_raw_data);
        $key->save();

        $clientTest = Clients::find($client->id);
        $sshKeys = $clientTest->sshKeys;
        $keyCount = count($sshKeys);
        $this->assertGreaterThanOrEqual(1, $keyCount);

        //clear keys created
        $keys = SshKeys::where('title', 'LIKE', '%title_test_%')->get();
        foreach ($keys as $index => $key) {
            $key->delete();
        }
    }
}
