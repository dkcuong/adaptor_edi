<?php

return [
    'sequentially' => env('SEQUENTIALLY', false),

    'outbound_schedule' => [
        'monthly' => '1',
        'weekly' => '1',
        'daily' => '0',
    ],
    'emailHandleException' => [
        'phong.tran@seldatinc.com',
    ],
    'cronTabs' => [
        'inbounds' => [
            'develop' => '* * * * * *',
            'staging' => '* * * * * *',
            'production' => '* * * * * *',
        ],
        'notiftpupload' => [
            'develop' => '*/30 * * * * *',
            'staging' => '* * * * * *',
            'production' => '* * * * * *',
        ],
        'renotiftpupload' => [
            'develop' => '*/30 * * * * *',
            'staging' => '0 13 * * * *',
            'production' => '*/30 * * * * *',
        ],
        'outbounds' => [
            'develop' => '* * * * * *',
            'staging' => '* * * * * *',
            'production' => '*/15 * * * * *',
        ],
        'storage' => [
            'develop' => '* * * * * *',
            'staging' => '* * * * * *',
            'production' => '0 */6 * * * *',
        ],
        're-process' => [
            'ftp' => [
                'develop' => '* * * * * *',
                'staging' => '* * * * * *',
                'production' => '0 0 * * * *',
            ],
            'inbound' => [
                'develop' => '* * * * * *',
                'staging' => '* * * * * *',
                'production' => '*/15 * * * * *',
            ]
        ],
        'rabbitmq-outbound' => [
            'develop' => '* * * * * *',
            'staging' => '* * * * * *',
            'production' => '*/15 * * * * *',
        ]
    ],
    'inOutMapping' => [
        'OW' => ['SW', 'SH'],
        'SH' => ['RC'],
        'AR' => ['RE'],
        'SM' => ['IM', 'QM'],
        'PO' => ['IN', 'PR', 'SH'],
        'PC' => ['PR'],
    ],
    'transCodeOutbound' => [
        "T945", "T210", "T214", 'T810', 'T997'
    ]
];
