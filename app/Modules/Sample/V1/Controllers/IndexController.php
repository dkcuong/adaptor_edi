<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 06-Jan-16
 * Time: 10:32 AM
 */
namespace App\Modules\Sample\V1\Controllers;

use App\Http\Controllers\Controller;

/**
 *
 * @author Simon Sai (Duc)
 *
 */
class IndexController extends Controller
{

    /**
     *
     * @return string
     */
    public function index()
    {
        return 'IndexController index action';
    }
}
