<?php
/**
 * Created by PhpStorm.
 * User: Dung.Dang
 */

namespace App\Modules\Edi\V1\Services;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQService
{
    /**
     * @var AMQPStreamConnection
     */
    protected $connection;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    /**
     * @param array $config
     */
    protected $config = [];

    /**
     * @var array
     */
    protected $inputs = [];

    /**
     * @param $inputs
     */
    public function init($inputs)
    {
        $this->inputs = $inputs;
    }

    public function __construct(OutboundService $outboundService)
    {

        $this->config = config('queue.connections.rabbitmq_edi');
        $this->connection = new AMQPStreamConnection(
            $this->config['host'],
            $this->config['port'],
            $this->config['login'],
            $this->config['password'],
            $this->config['vhost']
        );

        $this->channel = $this->connection->channel();

        $this->channel->queue_declare( $this->config['queue'], false, true, false, false);

        $this->queue =  $this->config['queue'];

        $this->obSer = $outboundService;
    }

    /**
     * @return mixed
     */
    public function receive()
    {
       echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
        $callback = function($msg) {
            $jsonData = $msg->body;
            if(!empty($jsonData)){

                logger($jsonData, ['type' => 'MQ Request']);

                $response = \GuzzleHttp\json_decode($jsonData, true);

                if (! is_array($response)) {
                    return false;
                }

                $whs_id = $response['whs_id'] ?: null;
                $cus_id = $response['cus_id'] ?: null;
                $transactional = $response['transactional_code'] ?: null;
                $data = $response['data'] ?: null;
                $successKey = $response['success_key'] ?: null;

                //$this->obSer->updateOutboundData($whs_id, $cus_id, $transactional, $data, $successKey);

                $res = $this->updateOutboundData($whs_id, $cus_id, $transactional, $data, $successKey);

                if(!$res){
                    logger($res, ['type' => 'MQ updateOutboundData fail']);
                }

                //$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

                $response['message'] = ' [*] Receive success full : success_key '.$successKey." \n";

                logger($response, ['type' => 'MQ response']);

                echo $response['message'];
            }
        };
        $this->channel->basic_consume($this->queue, '', false, true, false, false, $callback);

        while ( count($this->channel->callbacks) ) {
            $this->channel->wait();
        }

        $this->channel->close();
        $this->connection->close();

    }

    public function updateOutboundData($whs_id, $cus_id, $transactional, $data, $successKey,$times=1){
        logger('func updateOutboundData', ['type' => 'MQ updateOutboundData','times' => $times]);
        if($times > 1){
            logger('sleeping', ['type' => 'MQ updateOutboundData']);
            sleep(60*2);//sleep 2 minute
        }

        $res = $this->obSer->updateOutboundData($whs_id, $cus_id, $transactional, $data, $successKey);
        if($res){
            return true;
        }
        if($times == 3){
            return false;
        }
        $times++;
        return $this->updateOutboundData($whs_id, $cus_id, $transactional, $data, $successKey,$times);
    }

}