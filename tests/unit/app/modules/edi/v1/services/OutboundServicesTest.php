<?php

class OutboundServicesTest extends TestCase
{
    /**
     * @var \App\Modules\Edi\V1\Services\OutboundService
     */
    public $service;

    /**
     * OutboundServicesTest constructor.
     */

    public function __construct()
    {
        dd('123rt', __CLASS__ .' - '. __FUNCTION__ .' - '. __LINE__);
        $this->service = Mockery::mock('\App\Modules\Edi\V1\Services\OutboundService');
    }

    /**
     *
     */
    public function isExistObTranFile()
    {
        $ob_group_id = \App\X12OutboundTransactions::where('status', 'Sent')->first();

        $this->service->isExistTransactionFile($ob_group_id);
        dd('12345', __CLASS__ . ' : ' . __FUNCTION__ . ' - ' . __LINE__);
        $this->assertTrue(true);
    }

    /**
     *
     */
    public function updateOutboundData()
    {
        $testParams =  [
            'cus_id' => '11249',
            'whs_id' => '1',
            'transactional_code' => 'T861',
            'success_key' => '1479',
            'data' => [
                '1479' => [
                    'a20160922002111529852852052985-royalstripe-tcs12n' => [
                        'reference' => 'A20160922002' ,
                        'purchase_order' => '11152985' ,
                        'container_name' => 'A20160922002',
                        'suffix_name' => 'A20160922002',
                        'batchID' => '10019622' ,
                        'sku' => 'PT160512001' ,
                        'upcID' => 285 ,
                        'color' => 'BLUE',
                        'size' => '7"X12"/8"X8"' ,
                        'pack_size' => 20 ,
                        'pcs_expected' => 200 ,
                        'ctns_expected' => 10 ,
                        'description' => '52985-ROYAL STRIPE-TCS12N' ,
                        'product_type' => 'TBL KIT_C' ,
                        'pcs_received' => 0 ,
                        'ctns_received' => 0 ,
                        'pcs_descrepancy' => -200 ,
                        'ctns_descrepancy' => -10 ,
                    ],
                    'a20160922002111529852862052985-royalstripe-tcs12n' => [
                        'reference' => 'A20160922002' ,
                        'purchase_order' => '11152985' ,
                        'container_name' => 'A20160922002',
                        'suffix_name' => 'A20160922002' ,
                        'batchID' => '10019623' ,
                        'sku' => 'PT160512002' ,
                        'upcID' => 286 ,
                        'color' => 'RED' ,
                        'size' => '7"X12"/8"X11"' ,
                        'pack_size' => 20 ,
                        'pcs_expected' => 200 ,
                        'ctns_expected' => 10 ,
                        'description' => '52985-ROYAL STRIPE-TCS12N' ,
                        'product_type' => 'TBL KIT_C' ,
                        'pcs_received' => 0 ,
                        'ctns_received' => 0 ,
                        'pcs_descrepancy' => -200 ,
                        'ctns_descrepancy' => -10,
                    ],

                    'a20160922002111529852872052985-royalstripe-tcs12n' => [
                        'reference' => 'A20160922002' ,
                        'purchase_order' => '11152985' ,
                        'container_name' => 'A20160922002' ,
                        'suffix_name' => 'A20160922002',
                        'batchID' => '10019624' ,
                        'sku' => 'PT160512003' ,
                        'upcID' => 287 ,
                        'color' => 'GREEN' ,
                        'size' => '7"X12"/8"X10"' ,
                        'pack_size' => 20 ,
                        'pcs_expected' => 200 ,
                        'ctns_expected' => 10 ,
                        'description' => '52985-ROYAL STRIPE-TCS12N' ,
                        'product_type' => 'TBL KIT_C' ,
                        'pcs_received' => 0 ,
                        'ctns_received' => 0 ,
                        'pcs_descrepancy' => -200 ,
                        'ctns_descrepancy' => -10,
                    ],
                    'isSuccess' => 1 ,
                    'wms_status_code' => 'RCT' ,
                    'wms_status_name' => 'Receipted'

                ]
            ]
        ];
        dd($testParams, __CLASS__ .' - '. __FUNCTION__ .' - '. __LINE__);
        $cus_id = $testParams['cus_id'];
        $whs_id = $testParams['whs_id'];
        $transactional = $testParams['transactional_code'];
        $data = $testParams['data'];
        $successKey = $testParams['success_key'];

        $this->service->updateOutboundData($whs_id, $cus_id, $transactional, $data, $successKey);

        dd('12345', __CLASS__ . ' : ' . __FUNCTION__ . ' - ' . __LINE__);
        $this->assertTrue(true);
    }
}
