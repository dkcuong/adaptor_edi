<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 20-Jan-16
 * Time: 10:57 AM
 */

namespace App\Modules\Edi\V1\Services;

use App\Clients;
use App\FtpAccounts;
use App\Modules\Edi\V1\Models\ClientModel;
use App\Modules\Edi\V1\Models\InboundModel;
use App\Modules\Edi\V1\Models\OutboundModel;
use App\Modules\Edi\V1\Services\ServiceHelpers\DMSClientService;
use App\Modules\Edi\V1\Services\ServiceHelpers\LogService;
use App\Outbounds;
use App\Parsers\ParserInterface;
use App\Parsers\X12Parser\X12Parser;
use App\Setting;
use App\Utils\Date;
use App\Utils\FtpStorage;
use App\Utils\SftpStorage;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

/**
 * Class ClientService
 *
 * @package App\Modules\Edi\V1\Services
 */
class ClientService
{
    public $insertedTransaction;

    public $tran_number;
    public $inboundTransaction;
    /**
     * ClientModel Instance
     *
     * @var ClientModel
     */
    protected $client;

    /**
     * @var InboundService
     */
    protected $ibSer;

    /**
     * @var OutboundService
     */
    protected $obSer;

    /**
     * @var array
     */
    protected $clientInfo;

    /**
     * @var ParserInterface
     */
    protected $parser;

    /**
     * @var ParserInterface
     */
    protected $outbounds;

    /**
     * @var
     */
    protected $inOutMapping;

    /**
     * @var
     */
    protected $funcKnowledge;

    /**
     * @var
     */
    protected $date;

    /**
     * @var
     */
    protected $outboundModel;

    /**
     * @var
     */
    protected $settingService;

    /**
     * @var EmailHandlingService
     */
    protected $emailHandling;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var
     */
    public $reProcessDir;

    /**
     * @var
     */
    public $logService;

    /**
     * @var
     */
    public $listApiEndPoint;
    private $data_full;

    /**
     * ClientService constructor.
     *
     * @param ClientModel          $client
     * @param InboundService       $inboundService
     * @param Outbounds            $outbounds
     * @param OutboundService      $outboundService
     * @param Date                 $date
     * @param SettingService       $settingService
     * @param OutboundModel        $outboundModel
     * @param EmailHandlingService $emailHandlingService
     */
    public function __construct(
        ClientModel $client,
        InboundService $inboundService,
        Outbounds $outbounds,
        OutboundService $outboundService,
        Date $date,
        SettingService $settingService,
        OutboundModel $outboundModel,
        EmailHandlingService $emailHandlingService,
        LogService $logService
    ) {
        $this->client = $client;
        $this->ibSer = $inboundService;
        $this->obSer = $outboundService;
        $this->outbounds = $outbounds;
        $this->date = $date;
        $this->outboundModel = $outboundModel;
        $this->settingService = $settingService;
        $this->emailHandling = $emailHandlingService;
        $this->logService = $logService;
    }

    /**
     * @return array
     */
    public function getClientInfo()
    {
        return $this->clientInfo;
    }

    /**
     * @param array $clientInfo
     */
    public function setClientInfo($clientInfo)
    {
        $this->clientInfo = $clientInfo;
    }

    /**
     * Init Master data
     */
    public function init()
    {
        $this->settingService->emailSetting();

        $this->setClientInfo($this->client->allClientInformation());
    }

    /**
     * Process Inbound Action
     */
    public function processInbound()
    {
        $clientsInfo = $this->getClientInfo();

        foreach ($clientsInfo as $index => $client) {
            if (!empty($client->apiEndPoint)) {
                $this->getApiEndPointClients($client->apiEndPoint);
            } else {
                logger('API END POINT NOT FOUND');
                break;
            }

            $this->processInboundClients($client);
        }
    }

    /**
     * @param $client
     */
    public function processInboundClients($client)
    {
        $inboundEdi = $client->edi['inbound'];

        foreach ($inboundEdi as $index => $edi) {
            if (! empty($edi->dataFormat)) {
                $client->data_support = $this->client->getEDIDataFormat($edi);
            }
            $this->processInboundClientEdi($client, $edi);
        }
    }

    /**
     * @param $client
     * @param $edi
     */
    public function processInboundClientEdi($client, $edi)
    {
        $edi->ftp = $this->getStorageMethod($edi->ftpAccount, $client);
        $content = $this->readFromFtp($client, $edi);
        //parser data
        $this->emailHandling->setConfig($edi, $client);
        $this->processInboundFlow($content, $edi, $client);
    }

    /**
     * Total flow of Edi Inbound processing
     *
     * @param $content
     * @param $edi
     * @param $client
     */
    protected function processInboundFlow($content, $edi, $client)
    {
        if (empty($content)) {
            return;
        }
        $this->logService->addLogMessage('Inbound is running...');
        $api_end_point = isset($edi->api->url) && isset($this->listApiEndPoint['wms_api']) ? $this->listApiEndPoint['wms_api']['api_endpoint'] . $edi->api->url : null;
        foreach ($content as $file => $data) {
            if (!$this->logService->detectProcessKey($file)) {
                $this->logService->setProcessKey(generateKeyX12Log());
            }
            $this->logService->addLogMessage('Received file');
            if (!$data) {
                $this->isEmptyContent($edi, $file);
                break;
            }
            $this->logService->addLogMessage('Parser data successfully');
            $this->setInOutMapping($client, $edi);

            $responses = [];
            $mapData = $data;
            $isX12 = true;

            //Mapping data if type data is not x12
            if (!$this->parser instanceof X12Parser) {
                $mapData = $this->mapApiData($data, $edi, $client);
                $isX12 = false;
            }

            $getParseError = $this->getParseError($file);
            if ((!$getParseError && $isX12) || !$isX12) {
                $this->logService->addLogMessage('Call WMS API: ' . $api_end_point);
                $responses = $this->ApiEndPoint($client, $edi, $mapData, $isX12);

            } else {
                $responses['data'] = $data;
                $responses['output'] = $getParseError;
                $this->logService->addLogMessage('Parser data failure: ' . json_encode($getParseError));
            }

            $response = isset($responses['output']) ? $responses['output'] : $responses;

            //process pass to other files
            //missing WH id
            if(isset($response['missing_wh']) && $response['missing_wh']){
                continue;
            }
            $this->parser->setResult($responses);

            $this->parser->addConfig([
                'output' => $response,
            ]);
            $parseResults = $this->parser->afterProcess();
            $this->logService->writeLog('inbound');
            //Process result of Api
            $this->processInboundApiResult($parseResults, $response, $edi, $client, $file, $mapData);

            $this->isAcknowledgment($client, $parseResults, $data, $response);

            //sleep(60);
        }
    }

    /**
     * Store api result into database
     *
     * @param $parseResults
     * @param $response
     * @param $edi
     * @param $client
     * @param $file
     */
    protected function processInboundApiResult($parseResults, $response, $edi, $client, $file, $mapData = array())
    {
        $this->insertedTransaction = $results = $this->insertData($parseResults, $edi, $client, $file);

        $file_packing_list = false;
        if($client->short_name == config('client.make_it_real')){
            //$file_packing_list = $this->getFilePackingList($results['inbound_tran_id'], $client->short_name);
        }

        //Log Api response for easy tracking
        logger($response, ['client' => $client->name]);

        //Init attachment file
        $backupFile = $this->makeLocalFilePath($results['fileName'], $client, $edi, 'inbounds');
        $this->parser->attachment = storage_path('app') . DIRECTORY_SEPARATOR . $backupFile;

        //Missing vendor_id move file to WMS360
        $this->processMoveFileToWMS360($response, $file, $edi, $parseResults, $client);

        //Remove file in FTP after process
        $this->moveSuccessFtpFile($client, $file, $backupFile, $edi);

        //Send Email to customer data response
        if($file_packing_list){
            $attachment = [
                $file_packing_list,
                $this->parser->attachment
            ];
        }else{
            $attachment = $this->parser->attachment;
        }

        if (is_array($response) && !isset($response['error'])) {
            $responseError = [];
            $responseSuccess = [];
            foreach ($response as $value) {
                if (isset($value['status_code']) && $value['status_code'] != 200) {
                    $responseError[] = $value;
                } else {
                    $responseSuccess[] = $value;
                }
            }
            if (count($responseError) > 0) {
                $response = array_merge(array_values($responseError), array_values($responseSuccess));
                $results['error_number'] = count($responseError);
                $results['total_number'] = count($response);
            }
        }
        $errorHandleStatus = $this->emailHandling->sendEmail(
            $response,
            $results,
            $results['fileName'],
            'inbound',
            $attachment,
            $edi->name
        );
        $this->makeFileReProcess($file, $backupFile, $errorHandleStatus);
        $this->logService->addLogMessage('Process inbound transactions successfully');
    }

    /**
     * @param $response
     *
     * @return bool
     */
    protected function isApiSuccess($response)
    {
        return $response->getStatusCode() == 200;
    }

    /**
     * @param $file
     * @param $edi
     */
    protected function moveErrorFtpFile($file, $edi)
    {
        $errorDir = 'errors';
        $edi->ftp->moveFile($file, $errorDir . '/' . make_temp_name($file));
    }

    /**
     * @param        $edi
     * @param        $client
     * @param        $data
     * @param        $file
     * @param string $type
     *
     * @return mixed
     */
    protected function sendEmail($edi, $client, $data, $file, $type = 'inbound')
    {
        return Mail::send(
            'emails.reminder',
            ['data' => $data, 'file' => $file],
            function ($mailer) use ($edi, $client, $type) {

                //Get list email in inbound table first, otherwise get in contact
                $emails = !(empty($edi->emails)) ? explode(',', $edi->emails) : $client->contact->toArray();

                $emails = array_merge($emails, $this->getNotificationEmail($type));

                //Message Title for email
                $message = 'EDI ' . $edi->name . ' Notification For ' . $client->name;

                //has attachment file
                $hasAttachment = $type == 'inbound' ? true : $this->hasAttachment($edi);

                $this->sendTo($mailer, $emails, $message, $hasAttachment);
            }
        );
    }

    /**
     * @param $type
     *
     * @return array
     */
    protected function getNotificationEmail($type)
    {
        $emails = (new Setting())->email_notification;

        if (empty($emails->{$type . '_emails'})) {
            return [];
        }

        $result = explode(',', $emails->{$type . '_emails'});

        return $result;
    }

    /**
     * @param      $mailer
     * @param      $mails
     * @param      $message
     * @param bool $hasAttachment
     */
    protected function sendTo($mailer, $mails, $message, $hasAttachment = false)
    {
        logger($this->parser->attachment, ['attachment file path']);

        if ($this->parser->attachment && $hasAttachment) {
            $mailer->attach($this->parser->attachment);
        }
        foreach ($mails as $mail) {
            $toEmail = is_array($mail) ? $mail['email'] : $mail;

            $mailer->to($toEmail)->subject($message);
        }
    }

    /**
     * @param $from
     * @param $to
     * @param $edi
     * @return mixed
     */
    protected function moveSuccessFtpFile($client, $from, $to, $edi)
    {
        // Read content FTP file
        $content = $edi->ftp->parseFile($from);

        // Move content file to local Storage
        Storage::put($to, $content);

        $dmsFile = str_replace('/', '_', str_replace('backup/', '', $to));
        Storage::put('dms-transactions/' . $dmsFile, $content);

        if (file_exists(storage_path('app') . DIRECTORY_SEPARATOR . 'dms-transactions/' . $dmsFile)) {
            //put in DMS

            $dmsIntegration = new DMSClientService($this);

            $data = [
                'file_name' => $this->insertedTransaction['fileName'],
                'file_size' => Storage::size('dms-transactions/' . $dmsFile),
                'urlDownload' => env('APP_DOMAIN') . '/edi/api/v1/storage/' . $dmsFile . '/' . $this->insertedTransaction['fileName'],
                'size' => Storage::size($to),
                'insertedTranID' => $this->insertedTransaction['inbound_tran_id'],
                'status' => $this->insertedTransaction['status'],
                'tran_number' => getDefault($this->insertedTransaction['tran_number'])
            ];

            $dmsIntegration->define($client, $edi, $data);

            $statusCode = $dmsIntegration->putAPI();
        }

        // Delete FTP File
//        $edi->ftp->removeFile($from);
    }

    /**
     * @param $file
     * @param $client
     * @param $edi
     * @param $path
     * @return string
     */
    protected function makeLocalFilePath($file, $client, $edi, $path)
    {
        $this->reProcessDir = DIRECTORY_SEPARATOR . $client->short_name .
        DIRECTORY_SEPARATOR . $edi->transactional_code;

        $addDir = 'backup' .
        DIRECTORY_SEPARATOR . date('Y-m-d') .
        DIRECTORY_SEPARATOR . $client->short_name .
        DIRECTORY_SEPARATOR . $path .
        DIRECTORY_SEPARATOR . $edi->transactional_code;

        return $addDir . DIRECTORY_SEPARATOR . $file;
    }

    /**
     * @param        $data
     * @param        $edi
     * @param        $client
     * @param string $type
     *
     * @return mixed
     */
    public function mapApiData($data, $edi, $client, $type = 'Inbound')
    {
        //UNILEVERMapper
        return app(
            'Mapper',
            [
                'data' => $data,
                'mapping' => $edi->inboundMapping->fields,
                'edi' => $edi,
                'client' => $client->short_name,
                'type' => $type,
            ]
        )->get();
    }

    /**
     * @param        $data
     * @param        $edi
     * @param        $client
     * @param string $type
     *
     * @return mixed
     */
    public function mapApiOutboundData($data, $edi, $client, $type = 'Outbound')
    {
        $mapping = isset($edi->outboundMapping->fields) ? $edi->outboundMapping->fields : null;

        $transCodeOutbound = config('setting.transCodeOutbound');

        $data = (
            in_array($edi->transactional_code, $transCodeOutbound)
            || ($client->short_name == 'GLWORK' && $edi->transactional_code == 'T856')
            || ($client->short_name == 'ZAPPOSKY' && $edi->transactional_code == 'T856')
            || ($client->short_name == 'ZAPPOSKY' && $edi->transactional_code == 'T855'))? [$data] : $data;

        if ((! empty($data['vendorID']) && (int) $data['vendorID']  == (int) EmailHandlingService::$TLI_CA_VENDOR_ID && env('APP_ENV') == 'production') ||
            (! empty($data[0]['vendorID']) && (int) $data[0]['vendorID']  == (int) EmailHandlingService::$TLI_CA_VENDOR_ID && env('APP_ENV') == 'production')
        ) {
            EmailHandlingService::$TLI_CA_Email_Flag = true;
        } else {
            EmailHandlingService::$TLI_CA_Email_Flag = false;
        }
        unset($data['vendorID']);
        unset($data['isSuccess']);

        //app/Mappers/Outbound/UNILEVERMapper.php
        return app('Mapper', [
            'data' => $data,
            'mapping' => $mapping,
            'edi' => $edi,
            'client' => $client->short_name,
            'type' => $type,
        ])->get();
    }

    /**
     * @param $client
     * @param $edi
     *
     * @return array
     */
    public function readFromFtp($client, $edi)
    {
        return $this->parseFile($client, $edi);
    }

    /**
     * @param $client
     * @param $edi
     *
     * @return array
     */
    public function parseFile($client, $edi)
    {
        $result = [];
        $files = $edi->ftp->readFiles($edi->path);
        $data = $client->data_support;
        if (!is_array($files)) {
            return false;
        }
        //logger($files, ['parseFile' => $client->name]);

        foreach ($files as $file) {

            if (!$this->fileValidate($data, $edi, $file)) {
                continue;
            }
            $this->client->touchInbound($edi->id);
            $parameters = [
                'content' => $edi->ftp->parseFile($file),
                'edi' => $data,
                'inbound' => $edi,
                'client' => $client,
                'apiEndPoints' => $this->listApiEndPoint,
            ];
            //check title is null
            //add custom title
            if(! $edi->is_title){
                $customTitle = json_decode($edi->inboundMapping->custom_title,true);
                $customTitle = implode($edi->dataFormat->element_separator, $customTitle).PHP_EOL;
                $parameters['content'] = $customTitle . $parameters['content'];
            };

            if ($client->short_name == 'VCNY' && $edi->transactional_code == 'T940') {
                $parameters['content'] =  str_replace('W01*','~LX*1~W01*', $parameters['content']);

            }
            //move 888 of TLI to TLI-WMS-CHino
            // WILL BE COMMENT AFTER MIGRATE DONE
            // WILL BE COMMENT AFTER MIGRATE DONE
            if(in_array($client->short_name, config('client.move_file_client')) && $edi->transactional_code == 'T888'){
                logger("Client: ".$client->short_name." Transaction Code: " . $edi->transactional_code);
                logger("Start move file..");
                $file_path = explode('/',$file);
                $file_name = end($file_path);
                $to = env('WMS360_FTP_PATCH', 'WMS/toSeldat/'). $file_name;
                $content = $edi->ftp->parseFile($file);
                $edi->ftp->makeFile($to, $content);
                logger("End move file..");
                logger($to, ['file_path_after_move']);
            }

            //app/Parsers/XmlParser/XmlParser.php
            $this->parser = app('Parser', $parameters);

            $result[$file] = $this->parser->inbound()->get();

            $this->data_full = $this->parser->getOriginalParser();

            $this->errors[$file]['error'] = $this->parser->getError();
        }

        return $result;
    }

    /**
     * @param $support
     * @param $edi
     * @param $file
     *
     * @return bool
     */
    protected function fileValidate($support, $edi, $file)
    {
        $ext = '.' . pathinfo($file, PATHINFO_EXTENSION);
        $fileName = pathinfo($file, PATHINFO_FILENAME);

        $validExt = strtolower($support->extension) == strtolower($ext);

        if (!$validExt) {
            return false;
        }

        return $this->validFileNamePrefix($edi, $fileName) ? true : false;
    }

    /**
     * @param $edi
     * @param $fileName
     *
     * @return bool
     */
    protected function validFileNamePrefix($edi, $fileName)
    {
        $pattern = '/([\s\S]*)%s([\s\S]*)/i';

        $arrPrefix = explode(',', $edi->filename_prefix);
        $validate = false;
        if ($edi->filename_prefix) {

            foreach ($arrPrefix as $prefix) {
                $outPattern = sprintf($pattern, $prefix);
                $validate = preg_match($outPattern, $fileName) ? true : false;
                if ($validate) {
                    return $validate;
                }
            }
        }
        return $validate;
    }

    /**
     * @param $responseApi
     * @param $edi
     * @param $client
     * @param $file
     *
     * @return array
     */
    public function insertData($responseApi, $edi, $client, $file)
    {
        $results = [];
        if (isset($responseApi['input']) && $responseApi['input']) {
            $results = $this->ibSer->create(
                $responseApi,
                $edi,
                $client,
                $file,
                $this->inOutMapping,
                $this->logService->getProcessKey()
            );
        }
        return $results;
    }

    /**
     *
     */
    public function processOutbound()
    {
        $clientsInfo = $this->getClientInfo();
        foreach ($clientsInfo as $index => $client) {

            if (!empty($client->apiEndPoint)) {
                $this->getApiEndPointClients($client->apiEndPoint);
            } else {
                logger('API END POINT NOT FOUND');
                break;
            }

            $client->edi['outbound']->each(function ($edi) use ($client) {

                $this->obSer->customerId = $client->clientDefaultWarehouse->customer_id;
                $this->obSer->warehouseID = $client->clientDefaultWarehouse->warehouse_id;
                $this->obSer->clientWarehouses = $client->clientWarehouse;

                $edi->ftp = $this->getStorageMethod($edi->ftpAccount, $client);

                $content = $this->getOutboundData($edi, $client);

                $this->emailHandling->setConfig($edi, $client);

                if (!$content && $this->outBoundSchedule($edi)) {

                    if (!$this->outboundModel->hasOutBoundOnDay($client, $edi)) {

                        $params = [
                            'outboundGroup' => $edi->outboundGroup,
                            'transactionCode' => trim($edi->transactional_code, 'T'),
                            'dataTransaction' => '',
                            'control_number' => 1,
                            'isOutbound' => 1,
                        ];

                        try {
                            DB::beginTransaction();
                            $interId = $this->outboundModel->insertOutboundInterchange($client->rsa);

                            $this->outboundModel->updateInterChange($client->rsa);

                            $this->obSer->insertTransaction($params, $interId);

                            DB::commit();
                        } catch (\Exception $e) {
                            DB::rollback();
                            $messageHandle = [
                                'Message: ' . $e->getMessage(),
                                'File:' . __FILE__,
                                'Class:' . __CLASS__,
                                'Function:' . __FUNCTION__,
                                'Line Number:' . __LINE__,
                            ];

                            logger($messageHandle);
                            $this->emailHandling->sendEmail([$messageHandle]);
                        }

                        $content = $this->getOutboundData($edi, $client);

                    }
                }
                if (! empty($edi->dataFormat)) {
                    $client->data_support = $this->client->getEDIDataFormat($edi);
                }

                $this->processOutboundFlow($content, $edi, $client);
            });
        };
    }

    /**
     * @param $contents
     * @param $edi
     * @param $client
     */
    public function processOutboundFlow($contents, $edi, $client)
    {
        $inboundModel = new InboundModel();
        $inboundMappingCustom = $inboundModel->getInboundMappingConfig($client->id, $edi->name);
        $supportType = $client->data_support->support;
        if (empty($contents)) {
            return;
        }
        $startTime = time();
        $api_end_point = isset($edi->api->url) && isset($this->listApiEndPoint['wms_api']) ?
            $this->listApiEndPoint['wms_api']['api_endpoint'] . $edi->api->url : null;
        $results = $successKeys = $dataOutputs = [];
        foreach ($contents as $key => $content) {
            if (!$content) {
                break;
            }
            $outbound = $this->obSer->getSuccessKeys($content['groups']);
            if ($outbound && ! empty($outbound['success_number'])) {
                if (! empty($outbound['data_output'])) {
                    $dataOutputs[$outbound['success_number']] = $outbound['data_output'];
                }
                $successKeys[] = $outbound['success_number'];
            };
        }

        $customRequired = isset($edi->outboundMapping->custom_required_field) ?
                $edi->outboundMapping->custom_required_field : '';
        if ($edi->message_queue && is_array($dataOutputs)) {

            $dataResults['data'] = $dataOutputs;

        } else {
            $dataResults = $this->obSer->getMultiWMSData(
                $successKeys,
                $edi,
                $client,
                $this->listApiEndPoint,
                $customRequired,
                $inboundMappingCustom
            );
        }
        $logTime = time();
        foreach ($contents as $key => $content) {
            $dataInput = $content;
            $successKey = $this->obSer->getSuccessKeys($content['groups']);
            $successKey = $successKey['success_number'];
            if (!isset($dataResults['data'][$successKey]) || empty($dataResults['data'][$successKey])) {
                continue;
            }
            $suffixName = $this->getSuffixFileName($dataResults['data'][$successKey], $content['groups']);
            $this->logService->setTranId($content['transactionId']);
            $this->logService->setProcessKey(generateKeyX12Log());
            if (!$this->logService->checkExist('outbound')) {
                $this->logService->addLogMessage('Outbound is running...', $startTime);
                $this->logService->addLogMessage('Calling AWMS API: ' . $api_end_point, $startTime);
                $this->logService->addLogMessage('AWMS API response successfully', $logTime);
                $this->logService->addLogMessage('Transaction Id: ' . $content['transactionId']);
                $this->logService->addLogMessage('Success key: ' . $successKey);
                $this->logService->addLogMessage('suffixName: ' . $suffixName);
                $this->logService->writeLog('outbound');
            }
            $wmsStatusCode = isset($dataResults['data'][$successKey]['wms_status_code']) ? $dataResults['data'][$successKey]['wms_status_code'] : null;
            $orderStatusName = isset($dataResults['data'][$successKey]['wms_status_name']) ? $dataResults['data'][$successKey]['wms_status_name'] : null;
            if (!empty($wmsStatusCode) && $content['statusWms'] != $wmsStatusCode) {
                $this->obSer->updateStatusWmsTransaction($content['transactionId'], $wmsStatusCode);
                $this->logService->addLogMessage(
                    'Update WMS Status: ' . $wmsStatusCode . ' - ' . $orderStatusName,
                    $logTime
                );
                $this->logService->updateLog('outbound');
            }
            if (! isset($dataResults['data'][$successKey]['isSuccess']) || ! $dataResults['data'][$successKey]['isSuccess']) {
                continue;
            } else {
                unset($dataResults['data'][$successKey]['wms_status_code']);
                unset($dataResults['data'][$successKey]['wms_status_name']);
            }
            $this->logService->addLogMessage('Mapping data ...');
            $mapData = $this->mapApiOutboundData($dataResults['data'][$successKey], $edi, $client);
            if ($supportType == 'x12') {
                unset($dataInput['transactionId']);
                unset($dataInput['interId']);
                unset($dataInput['interNum']);
                unset($dataInput['groupNum']);
                unset($dataInput['groups'][0]['id']);
                $dataInput['groups'][0]['transactions'] = array_shift($mapData);
                $mapData = $dataInput;
            }

            $client->data_support->support = ($edi->transactional_code == 'T858') ? 'pdf' : $client->data_support->support;
            $client->data_support->extension = ($edi->transactional_code == 'T858') ? '.pdf' : $client->data_support->extension;
            $client->data_support->name = ($edi->transactional_code == 'T858') ? 'Pdf' : $client->data_support->name;

            $client_support = $client->data_support;

            if ($client->short_name == 'GLWORK' && $edi->transactional_code == 'T856') {
                $client_support->support = 'xml';
                $client->data_support->extension = '.xml';
            }
            $this->parser = app('Parser', [
                'content' => $mapData,
                'edi' => $client_support,
                'inbound' => $edi,
                'outbound' => $edi,
                'client' => $client,
                'apiEndPoints' => $this->listApiEndPoint,
            ]);

            //App\Parsers\XmlParser\XmlParser
            $this->parser->outbound();

            $results[$key] = $outputString = $this->parser->getResult();

//             if ($client->short_name == 'UNILEVER') {
//                 $results[$key] = $outputString = json_encode($outputString);
//             }

            if (!$outputString) {
                $this->logService->addLogMessage('Parser data failure');
                $this->logService->updateLog('outbound');
                continue;
            }
            $this->logService->addLogMessage('Parser data successfully');
            $fileName = $this->makeFileName($edi, $client, $content, $suffixName);
            $outboundProcess = $this->outboundProcess($client, $results, $fileName);
            if (isset($outboundProcess['result']) && $outboundProcess['result']) {
                $keyNumber = isset($content['success_number']) ? $content['success_number'] : '';
                $this->makeFileToFTPServer($edi, $client, $outputString, $fileName, $keyNumber);
                $this->processOutboundResults($content, $outputString, $fileName, $edi, $client, $dataResults['data'][$successKey]);
            }
            $this->logService->updateLog('outbound');
            if (isset($outboundProcess['data']['processKey']) && !empty($outboundProcess['data']['processKey'])) {
                $this->logService->updateAs2Log($outboundProcess['data']['processKey']);
            }
        }
        logger($results);
    }

    /**
     * @param $content
     * @param $outputData
     * @param $fileName
     * @param $edi
     * @param $client
     */
    public function processOutboundResults($content, $outputData, $fileName, $edi, $client, $mapData = null)
    {
        if ($fileName) {
            //update status transaction
            $this->obSer->updateStatusTransaction($content, json_encode($mapData));
            $this->obSer->insertTransactionFiles($content, $fileName, $outputData);
            $data = [
                'message' => 'Update EDI ' . $edi->name . ' ' . $client->name . ' successfully!
								Please check your server and mailer API !',
                'data' => [
                ],
                'status_code' => 200,
            ];
            //Log Api response for easy tracking
            logger($content, ['client' => $client->name]);

            $backupFile = $this->makeLocalFilePath($fileName, $client, $edi, 'outbounds');

            //Init attachment file
            $this->parser->attachment = storage_path('app') . DIRECTORY_SEPARATOR . $backupFile;

            //Move file to backup
            // Move content file to local Storage
            Storage::put($backupFile, $outputData);

            $dmsFile = str_replace('/', '_', str_replace('backup/', '', $backupFile));

            if (!file_exists(storage_path('app') . DIRECTORY_SEPARATOR . 'dms-transactions/' . $dmsFile)) {
                Storage::copy($backupFile, 'dms-transactions/' . $dmsFile);
                //put in DMS
                $dmsIntegration = new DMSClientService($this);

                $dataDMS = [
                    'file_name' => $fileName,
                    'file_size' => Storage::size('dms-transactions/' . $dmsFile),
                    'urlDownload' => env('APP_DOMAIN') . '/edi/api/v1/storage/' . $dmsFile . '/' . $fileName,
                    'insertedTranID' => $content['transactionId'],
                    'status' => 'Sent',
                    'tran_number' => getDefault($this->tran_number)
                ];

                $dmsIntegration->define($client, $edi, $dataDMS);

                $dmsIntegration->putAPI();
            }


            //Send Email to customer data response
            $inserts['control_number'] = $content['interId'];
            $this->emailHandling->sendEmail(
                [$data],
                $inserts,
                $fileName,
                'outbound',
                $this->parser->attachment
            );
        }
    }

    /**
     * @param $edi
     * @param $client
     *
     * @return array
     */
    public function getOutboundData($edi, $client)
    {
        return $this->obSer->getDataCreator($edi, $client);
    }

    /**
     * @param $edi
     * @param $client
     * @param $x12String
     * @param $file
     */
    public function makeFileToFTPServer($edi, $client, $x12String, $file, $keyNumber = '')
    {
        $path = $client->edi['outbound'][0]->path;
        $fileName = $path . '/' . $file;
        if ($keyNumber) {
            $this->emailHandling->packingFile = $this->getPackingFile($edi, $client, $path, $keyNumber);
        }

        $edi->ftp->makeFile($fileName, $x12String);
    }

    /**
     * @param $client
     * @param $edi
     */
    public function setInOutMapping($client, $edi)
    {
        $inOutMapping = config('setting.inOutMapping');

        $groupCode = $edi->inboundGroup->group_code;
        $interId = $client->rsa->id;
        if ($groupCode && isset($inOutMapping[$groupCode])) {
            foreach ($inOutMapping[$groupCode] as $outGroupCode) {
                $outMapping = $this->obSer->getOutboundGroupByGroupCode($interId, $outGroupCode);

                if ($outMapping) {
                    $this->inOutMapping[] = $outMapping;
                }
            }
        }

        $this->funcKnowledge =
        $this->obSer->getOutboundGroupByGroupCode($interId, 'FA');
    }

    /**
     * @param $edi
     *
     * @return bool
     */
    private function hasAttachment($edi)
    {
        return $edi->sent_type_id == OUTBOUND_SEND_MAIL_TYPE;
    }

    /**
     * @param $edi
     *
     * @return bool
     */
    private function outBoundSchedule($edi)
    {
        switch ($edi->sent_schedule_id) {
            case OUTBOUND_SCHEDULE_MONTHLY:
                return $this->date->todayIsBeginOfMonth();
            case OUTBOUND_SCHEDULE_WEEKLY:
                return $this->date->todayIsBeginOfWeek();
            case OUTBOUND_SCHEDULE_DAILY:
                return $this->date->nowIsBeginOfDay();
            default:
                return false;
        }
    }

    /**
     * @param $file
     *
     * @return bool
     */
    protected function getParseError($file)
    {
        $result = null;
        if (isset($this->errors[$file]['error']['messages'])) {
            $result = $this->errors[$file];
            $result['message'] = 'Incorrect EDI data. Please check your EDI file.';
            $result['data'] = $this->errors[$file]['error']['messages'];
            $result['status_code'] = '202';
        }

        return $result;
    }

    /**
     * @param $data
     * @param $content
     *
     * @return null|string
     */
    protected function getSuffixFileName($data, $content)
    {
        $suffixName = null;
        $dataGroup = array_shift($content);
        $dataTransactions = array_shift($dataGroup['transactions']);
        $suffixString = $dataTransactions['success_number'];

        foreach ($data as $item) {
            if (isset($item['suffix_name']) && is_array(explode(';', $item['suffix_name']))) {
                $containerSub = explode(';', $item['suffix_name']);
                $lastElement = $containerSub[count($containerSub) - 1];
                if ($lastElement && is_numeric($lastElement)) {
                    array_pop($containerSub);
                }
                $item['suffix_name'] = implode(';', $containerSub);
                $suffixName[$item['suffix_name']] = true;
            }
        }

        $suffixString = is_array($suffixName) ?
        $suffixString . '-' . implode('-', array_keys($suffixName)) :
        $suffixString;

        if (strlen(trim($suffixString)) > 50) {
            $suffixString = $dataTransactions['success_number'] . '-CNTRs-' . count($data);
        }

        return $suffixString;
    }

    /**
     * @param FtpAccounts $ftpAccount
     *
     * @param Clients     $client
     *
     * @return FtpStorage|SftpStorage
     */
    private function getStorageMethod(FtpAccounts $ftpAccount, Clients $client = null)
    {
        $ftpAccountData = $ftpAccount->toArray();

        if ($ftpAccountData['protocol'] == 1) {
            return new SftpStorage($ftpAccount, $client);
        }

        return new FtpStorage($ftpAccountData);
    }

    public function reProcessFTP()
    {
        $this->getClientInfo()->each(function ($client) {
            $client->edi['inbound']->each(function ($edi) use ($client) {
                $path = $client->edi['inbound'][0]->path;
                $reProcessDir = 're-process' . '/' . $client->short_name .
                '/' . $edi->transactional_code;
                $files = Storage::disk('local')->allFiles($reProcessDir);
                if ($files) {
                    $edi->ftp = $this->getStorageMethod($edi->ftpAccount, $client);
                    foreach ($files as $file) {
                        $ext = '.' . pathinfo($file, PATHINFO_EXTENSION);
                        $fileName = $fileName = $path . '/' . pathinfo($file, PATHINFO_FILENAME) . '-re_process' . $ext;
                        $content = Storage::get($file);
                        $edi->ftp->makeFile($fileName, $content);
                        Storage::delete($file);
                    }
                }
            });
        });
    }

    /**
     * @param $from
     * @param $backupFile
     * @param $errorHandleStatus
     */
    private function makeFileReProcess($from, $backupFile, $errorHandleStatus)
    {
        // Read content FTP file
        $content = Storage::get($backupFile);

        if ($errorHandleStatus == ERROR_HANDLING_EDI) {

            $ext = '.' . pathinfo($from, PATHINFO_EXTENSION);
            $fileName = 're-process' . $this->reProcessDir . DIRECTORY_SEPARATOR . pathinfo($from, PATHINFO_FILENAME) . $ext;
            Storage::put($fileName, $content);
        }
    }

    private function outboundProcess($client, $content, $fileName)
    {
        $this->logService->addLogMessage('Outbound process protocol ' . $client->data_support->protocol);
        if ($client->data_support->protocol != SERVICE_PROTOCOL_AS2) {
            return ['result' => true];
        }
        //Call Api
        $api = app(
            'As2Api',
            [
                'content' => [
                    'client_short_name' => $client->short_name,
                    'data' => array_shift($content),
                    'file_name' => $fileName,
                    'process_key' => $this->logService->getProcessKey(),
                ],
            ]
        );

        $responses = $api->callAs2Api();
        if (!isset($responses['result'])) {
            $this->logService->addLogMessage('AS2 API response failure');
            logger('Can not get content from AS2 API');
            return ['result' => false];
        }
        return $responses;
    }

    private function makeFileName($edi, $client, $dataInput, $suffixName)
    {
        $dataType = $client->data_support->extension;
        $interId = $dataInput['interId'];
        $interNum = sprintf('%09d', $dataInput['interNum']);
        $groupNum = $dataInput['groupNum'];
        $fileName = $edi->filename_prefix . '-' . $edi->name . '-' . $suffixName .
            '-' . $interNum . '-' . $groupNum . '-' . $interId . $dataType;

        $this->tran_number = $interNum . '-' . $groupNum . '-' . $interId;

        return $fileName;
    }

    /**
     * re-process:inbound
     */

    public function reProcessInbound()
    {
        $clientsInfo = $this->getClientInfo();
        foreach ($clientsInfo as $index => $client) {

            if (!empty($client->apiEndPoint)) {
                $this->getApiEndPointClients($client->apiEndPoint);
            } else {
                logger('API END POINT NOT FOUND');
                break;
            }
            $this->reProcessInboundClients($client);
        }
    }

    /**
     * @param $client
     */
    public function reProcessInboundClients($client)
    {
        $inboundEdi = $client->edi['inbound'];
        foreach ($inboundEdi as $index => $edi) {
            $this->reProcessInboundClientEdi($client, $edi);
        }
    }

    /**
     * @param $client
     * @param $edi
     */
    public function reProcessInboundClientEdi($client, $edi)
    {
        $reProcessGroups = $this->ibSer->getInboundGroups($client->id, $edi->group_id);
        $this->emailHandling->setConfig($edi, $client);
        foreach ($reProcessGroups as $reProcessGroup) {
            $inboundInterchange = $this->ibSer->getInboundInterchange($reProcessGroup->inbound_interchange_id);
            $inboundTransactions = $this->ibSer->getReProcessInboundTrans(
                $reProcessGroup->id,
                $reProcessGroup->control_number
            );
            $inserts = $inboundInterchange->toArray();
            $inserts['group_id'] = $reProcessGroup->id;
            $inserts['group_control_number'] = $reProcessGroup->control_number;
            $this->reProcessInboundFlow($inboundTransactions, $edi, $client, $inserts);
        }
    }

    /**
     * Total flow of Edi Inbound re processing
     *
     * @param $inboundTransactions
     * @param $edi
     * @param $client
     */
    protected function reProcessInboundFlow($inboundTransactions, $edi, $client, $inserts = [])
    {
        $response = [];
        $this->setInOutMapping($client, $edi);
        $api_end_point = isset($edi->api->url) && isset($this->listApiEndPoint['wms_api']) ? $this->listApiEndPoint['wms_api']['api_endpoint'] . $edi->api->url : null;

        foreach ($inboundTransactions as $inboundTransaction) {
            $this->inboundTransaction = $inboundTransaction;
            $this->logService->addLogMessage('Re-process inbound....');
            $this->logService->setTranId($inboundTransaction->id);
            $status = false;
            if ($this->isStopReProcess($inboundTransaction->created_at)) {
                $inboundTransaction->status = 'Error';
                $inboundTransaction->save();
                $this->logService->addLogMessage('Exceeding the time re-process');
                $this->logService->addLogMessage('Re-process failure');
                $this->logService->updateLog('inbound');
                continue;
            }
            $data = json_decode($inboundTransaction->data, true);
            $this->logService->addLogMessage('Mapping data...');
            $this->logService->addLogMessage('Calling AWMS API: ' . $api_end_point);
            $responses = $this->ApiEndPoint($client, $edi, [$data], false, true);
            if ($responses && !empty($responses['output']) && is_array($responses['output'])) {
                foreach ($responses['output'] as $dataOutput) {
                    $change = false;
                    $messages = isset($dataOutput['message']) ? $dataOutput['message'] : [];
                    $newMessages = is_array($messages) ? array_shift($messages) : $messages;
                    if (hash('sha1', $newMessages) != hash('sha1', $inboundTransaction->message)) {
                        $inboundTransaction->message = $newMessages;
                        $change = true;
                        if (is_array($messages)) {
                            $this->logService->addLogMessages($messages);
                        } else {
                            $this->logService->addLogMessage($messages);
                        }
                    }

                    $dataOutput['success'] = false;
                    if (isset($dataOutput['status_code']) && $dataOutput['status_code'] == 200 && isset($dataOutput['data']['status_type']) &&
                        isset($dataOutput['data']['success_number']) && $dataOutput['data']['success_number']) {
                        $inboundTransaction->status = 'Received';
                        $inboundTransaction->success_number = $dataOutput['data']['success_number'];
                        $dataOutput['success'] = true;
                        $change = true;
                        $this->logService->addLogMessage('Re-process successfully');
                        $this->logService->setStatus(1);
                        $dataInput = json_decode($inboundTransaction->data);
                        $dataOutput['message'] = $inboundTransaction->message;
                        $this->obSer->createOutbound($dataInput, 0, $dataOutput, $client, $this->inOutMapping);
                    } else if (!isset($dataOutput['data']['re_process']) || !$dataOutput['data']['re_process']) {
                        $inboundTransaction->status = 'Error';
                        $inboundTransaction->save();
                        $this->logService->addLogMessage('Re-process failure');
                        $this->logService->addLogMessage('Update status: Error');
                        $this->logService->updateLog('inbound');
                        continue;
                    }
                    if ($change) {
                        $inboundTransaction->save();
                        $this->logService->updateLog('inbound');
                    }
                    $response[] = $dataOutput;
                }

            }
        }
        $this->reProcessInboundApiResult($response, $inserts, $client, $edi);
    }

    public function reProcessInboundApiResult($response, $inserts, $client, $edi)
    {
        $ibFile = $this->ibSer->getInboundTransactionFile($inserts['group_id']);
        $attachment = false;
        $fileName = '';
        if ($ibFile) {
            $fileName = pathinfo($ibFile->file_name, PATHINFO_FILENAME);
            $ext = '.' . pathinfo($ibFile->file_name, PATHINFO_EXTENSION);
            $inserts['control_number'] = sprintf('%09d', $inserts['control_number']);
            $fileName = $fileName . '-' . $inserts['control_number'] . '-' .
                $inserts['group_control_number'] . $inserts['id'] . $ext;
            //Init attachment file
            $reProcessFile = 're-process' . DIRECTORY_SEPARATOR . $fileName;
            $attachment = storage_path('app') . DIRECTORY_SEPARATOR . $reProcessFile;
            if (!file_exists($attachment)) {
                $content = $ibFile->content;
                Storage::put($reProcessFile, $content);
            }

            $dmsFile = str_replace('/', '_', str_replace('backup/', '', $reProcessFile));
            if (!file_exists(storage_path('app') . DIRECTORY_SEPARATOR . 'dms-transactions/' . $dmsFile)) {
                Storage::copy($reProcessFile, 'dms-transactions/' . $dmsFile);
                //put in DMS
                $dmsIntegration = new DMSClientService($this);

                $dataDMS = [
                    'file_name' => $fileName,
                    'file_size' => Storage::size('dms-transactions/' . $dmsFile),
                    'urlDownload' => env('APP_DOMAIN') . '/edi/api/v1/storage/' . $dmsFile . '/' . $fileName,
                    'insertedTranID' => getDefault($this->inboundTransaction->id),
                    'status' => 'Sent',
                    'tran_number' => getDefault($inserts['control_number'] . '-' .
                        $inserts['group_control_number'] . $inserts['id'])
                ];

                $dmsIntegration->define($client, $edi, $dataDMS);

                $dmsIntegration->putAPI();
            }


        }
        $responseError = [];
        $responseSuccess = [];
        foreach ($response as $rs) {
            if ($rs['success']) {
                $responseSuccess[] = $rs;
            } else {
                $responseError[] = $rs;
            }
        }
        if (count($responseError) > 0) {
            $response = array_merge(array_values($responseError), array_values($responseSuccess));
            $inserts['error_number'] = count($responseError);
            $inserts['total_number'] = count($response);
        }
        if (count($responseSuccess)) {
            $inserts['re_process_flag'] = 're-process';
            $this->emailHandling->sendEmail(
                $response,
                $inserts,
                $fileName,
                'inbound',
                $attachment,
                're-process'
            );
        }
        if ($attachment) {
            unlink($attachment);
        }
    }

    public function isStopReProcess($startTime)
    {
        $now = \Carbon\Carbon::now()->timestamp;
        $startTime->addDays(5);
        if ($startTime->timestamp < $now) {
            return true;
        }
        return false;
    }

    private function isEmptyContent($edi, $file)
    {
        $emailMessage = ['message' => 'File is empty', 'status_code' => 422, 'data' => []];
        $this->logService->addLogMessage('Parser data failure, ' . $emailMessage['message']);
        $this->logService->setStatus(0);
        $this->emailHandling->sendEmail(
            $emailMessage,
            $file
        );

        // Delete FTP File
        $edi->ftp->removeFile($file);
        $this->logService->writeLog('inbound');
    }

    private function ApiEndPoint($client, $edi, $mapData, $isX12 = false, $reProcess = false)
    {
        $custom_required = isset($edi->inboundMapping->custom_required_field) ? $edi->inboundMapping->custom_required_field : '';
        //Call Api
        $parameters = [
            'apiEndPoints' => $this->listApiEndPoint,
            'client' => $client->id,
            'client_short_name' => $client->short_name,
            'edi' => $edi->name,
            'data' => $mapData,
            'isX12' => $isX12,
            'inbound' => $edi,
            'custom_required' => $custom_required,
            're_process' => $reProcess,
            'data_full' => $this->data_full
        ];

        //App\Apis\Clients\Edi940Api
        $api = app('Api', $parameters);

        $responses = $api->callApi();
        if ($responses && !empty($responses['output'])) {
            $this->logService->addLogMessage('AWMS API response successfully');
        } else {
            $this->logService->addLogMessage('AWMS API response failure');
            $this->logService->setStatus(0);
        }
        return $responses;
    }

    private function isAcknowledgment($client, $parseResults, $data, $response)
    {
        if ($this->parser->writerOutboundKnowledge() && $this->funcKnowledge
            && $client->rsa->requested
        ) {
            //Prepare data for 997
            $dataOutput = $this->obSer->createData(
                $client,
                $this->parser->get(),
                $parseResults['originalParse'],
                $data,
                $response
            );

            $groupData = $this->funcKnowledge;

            if ($dataOutput) {
                $this->obSer->createX12Knowledge($client, $dataOutput, $groupData);
            }
        }
    }

    private function getApiEndPointClients($apiEndPoints)
    {
        foreach ($apiEndPoints as $apiEndPoint) {
            $this->listApiEndPoint[$apiEndPoint->api_name] = $apiEndPoint->toArray();
        }
    }

    private function getPackingFile($edi, $client, $path, $keyNumber)
    {
        $files = $edi->ftp->readFiles($path);
        if (!is_array($files)) {
            return false;
        }
        //logger($files, ['parseFile' => $client->name]);

        $this->reProcessDir = DIRECTORY_SEPARATOR . $client->short_name .
            DIRECTORY_SEPARATOR . $edi->transactional_code;

        $addDir = 'backup' .
            DIRECTORY_SEPARATOR . 'packingLists' .
            DIRECTORY_SEPARATOR . $client->short_name;

        foreach ($files as $file) {
            $downloadFile  = $edi->ftp->parseFile($file);
            $validate = $this->findPackingList($file, $keyNumber);
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            if ($validate && $ext == 'pdf') {
                $packingFile = $addDir . DIRECTORY_SEPARATOR . $file;
                Storage::put($packingFile, $downloadFile);
                return $packingFile;
            }
        }
        return false;
    }

    private function findPackingList($file, $keyNumber)
    {
        $pattern = '/([\s\S]*)%s([\s\S]*)/i';

        $arrPrefix = explode(',', $keyNumber);
        $validate = false;
        if ($keyNumber) {

            foreach ($arrPrefix as $prefix) {
                $outPattern = sprintf($pattern, $prefix);
                $validate = preg_match($outPattern, $file) ? true : false;
                if ($validate) {
                    return $validate;
                }
            }
        }
        return $validate;
    }

    private function getFilePackingList($inbound_tran_id,$client){
        $inboundModel = new InboundModel();
        $transaction = $inboundModel->getInboundTransaction($inbound_tran_id);

        if(!$transaction) return false;

        if($transaction->transaction_code != '856' || $transaction->status != 'Received') return false;

        try{
            $data = json_decode($transaction->data,1);

            $pdfPath = storage_path('app/backup/packingLists/'.$client);
            if (!file_exists($pdfPath)) {
                mkdir($pdfPath, 0755, true);
            }
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML(view('pdf.make-it-real-856',['data' => $data])->render());

            $pdfFilePath = $pdfPath.'/'.time().date('ymd').'-'.$inbound_tran_id.'-packing-list.pdf';

            $mpdf->Output($pdfFilePath,'F');

            return $pdfFilePath;
        }catch (\Exception $e){
            $messageHandle = [
                'Message: ' . $e->getMessage(),
                'File:' . __FILE__,
                'Class:' . __CLASS__,
                'Function:' . __FUNCTION__,
                'Line Number:' . __LINE__,
            ];

            logger($messageHandle);
            return false;
        }

    }

    /**
     * @param $response
     * @param $file
     * @param $edi
     * @param $parseResults
     * @param $client
     */
    private function processMoveFileToWMS360($response, $file, $edi, $parseResults, $client)
    {
        logger('Start move file...');

        $status_code = null;
        if(is_array($response)) {
            if(in_array(422, array_pluck($response, 'status_code'))){
                $status_code = 422;
            }
        }else {
            $status_code = $response['status_code'] ?? 422;
        }
        logger($status_code, ['client' => $client->name]);

        if ($status_code == 422 && in_array($client->short_name, config('client.move_file_client'))) {
            $file_path = explode('/',$file);
            $file_name = end($file_path);
            $to = env('WMS360_FTP_PATCH', 'WMS/toSeldat/'). $file_name;
            $content = $edi->ftp->parseFile($file);
            $edi->ftp->makeFile($to, $content);
            logger($to, ['file_path_after_move']);
        }

        logger('End process move file..');
    }
}
