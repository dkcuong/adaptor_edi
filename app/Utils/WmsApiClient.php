<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 19-Jan-16
 * Time: 4:08 PM
 */

namespace App\Utils;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class WmsApiClient extends Client
{

    /**
     * WmsApiClient constructor.
     * @param array $config
     */
    public function __construct($config)
    {
        $apiEndPoint = $config['apiEndPoints'];
        $uri = isset($apiEndPoint['wms_api']) && isset($apiEndPoint['wms_api']['api_endpoint']) ? $apiEndPoint['wms_api']['api_endpoint'] : null;
        $token = isset($apiEndPoint['wms_api']) && isset($apiEndPoint['wms_api']['certificate_key'])? $apiEndPoint['wms_api']['certificate_key'] : null;

        return parent::__construct([
            'base_uri' => $uri,
            'headers' => [
                'Content-Type' => 'application/json',
                'token'        => $token
            ],
            'exceptions' => false
        ]);
    }
}
