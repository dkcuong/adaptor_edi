<?php
/**
 * Created by PhpStorm.
 * User: anh.cao
 * Date: 4/4/2019
 * Time: 2:05 PM
 */

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class CommandController extends Controller
{
    public function __construct()
    {
        echo "Call command without run manually - EDI - adaptor - v1";
    }


    public function inbound(Request $request){
       Artisan::call('edi:inbound');
    }

    public function outbound(Request $request){
        Artisan::call('edi:outbound');
    }

    public function reInbound(Request $request){
        Artisan::call('re-process:inbound');
    }
}