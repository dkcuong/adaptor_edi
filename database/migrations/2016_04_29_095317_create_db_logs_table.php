<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('db_logs'))
        {
            Schema::create('db_logs', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('request_time')->nullable();
                $table->string('url')->nullable();
                $table->longText('trace');
                $table->longText('content');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('db_logs');
    }
}
