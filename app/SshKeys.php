<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SshKeys
 *
 * @package App
 * @property string $data
 */
class SshKeys extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data',
        'title',
        'type',
        'status',
    ];

    /**
     * @var string
     */
    protected $table = 'ssh_keys';

    /**
     * @param int $ftpAccountID
     */
    public function loadByFtpAccount($ftpAccountID)
    {
        dr($ftpAccountID, __CLASS__ . ' : ' . __FUNCTION__ . ' : ' . __LINE__);
        $ftp_ssh = FtpSsh::loadByFtpAccount($ftpAccountID);
        dr($ftp_ssh, __CLASS__ . ' : ' . __FUNCTION__ . ' : ' . __LINE__);
    }
}
