<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * @return array
     */
    public function getEmailSmtpAttribute()
    {
        return $this->getData('email_smtp');
    }

    /**
     * @return array
     */
    public function getEmailNotificationAttribute()
    {
        return $this->getData('email_notification');
    }

    /**
     * @param $key
     * @return array
     */
    protected function getData($key)
    {
        $json = Setting::where('key', $key)->first();

        if (! $json) {
            return [];
        }

        return json_decode($json->value);
    }
}
