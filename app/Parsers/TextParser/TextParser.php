<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 21-Jan-16
 * Time: 9:25 PM
 */

namespace App\Parsers\TextParser;


use App\Exceptions\ParserHeaderMappingException;
use App\Parsers\ParserBase;
use App\Parsers\ParserInterface;

class TextParser extends ParserBase implements ParserInterface
{
    /**
     * Default Text Parser Element Separator
     *
     * @var string
     */
    protected $elementSeparator = '|';

    /**
     * Default component separator for text parser
     *
     * @var string
     */
    protected $componentSeparator = "\n";

    /**
     * Default Text Parser values trim characters
     *
     * @var string
     */
    protected $filterString = '\",';

    /**
     * @var string
     */
    protected $spaceForColumn = '50';

    /**
     * @return array
     */
    public function getResultFilter()
    {
        return array_filter($this->result);
    }

    /**
     * Get return data after parsed
     *
     * @return array
     */
    public function get()
    {
        return $this->getResultFilter();
    }

    /**
     * Process Parse Content to array
     *
     * @return array
     */
    public function process()
    {
        $rows = $this->parse($this->getComponentSeparator(), $this->getContent());

        $parsed = array_map(function ($row) {
            return $this->parse($this->getElementSeparator(), $row);
        }, $rows);
        //Map first array to header
        $result = $this->mapHeader($parsed);

        $this->originalParser = $result;
        $this->setResult($result);

        return $this;
    }

    /**
     * Map first array to header
     *
     * @param $data
     * @return mixed
     */
    protected function mapHeader($data)
    {
        $header = array_shift($data);
        $header = $this->filterItem($header);

        $combine = array_map(function ($item) use ($header) {

            $item = $this->filterItem($item);

            if (count($item) < count($header)) {
                for ($i = count($item)+1; $i <= count($header); $i++) {
                    $item[$i] = '';
                }
            } else if (count($item) > count($header)) {
                for ($i = count($header)+1; $i <= count($item); $i++) {
                    $header[$i] = 'USER' . $i;
                }
            }

            if (count($item) == count($header)) {
                return array_combine($header, $item);
            } else {
                logger('Number of values and header does not map', [
                    'data' => $item,
                    'ValuesCount' => count($item),
                    'headerCount' => count($header)
                ]);
            }
        }, $data);

        return $combine;
    }

    /**
     * @param $item
     * @return mixed
     */
    protected function filterItem($item)
    {
        $endItem = end($item);
        $end = $this->filter($endItem);
        $keyEnd = key($item);

        $start = $this->filter(reset($item));

        $item[0] = $start;
        $item[$keyEnd] = $end;

        return $item;
    }

    /**
     * Parse string content from delimiter And Trim unused string
     *
     * @param $delimiter
     * @param $string
     * @return mixed
     */
    protected function parse($delimiter, $string)
    {
        return array_map('trim', explode($delimiter, $string));
    }

    /**
     * processOutbound function
     */
    public function processOutbound()
    {
        $rows = $this->create($this->getContent());

        $this->setResult($rows);
    }

    /**
     * @param $contents
     * @return null|string
     */
    protected function create($contents)
    {
        $maxColumn = $this->setSpaceForColumns($contents);
        $contents = $this->reStructureData($contents, $maxColumn);

        $rows = $this->createHeader($maxColumn);

        foreach ($contents as $key => $content) {
            foreach ($content as $target => $item) {
                $rows .= implode($this->getElementSeparator(), $item);

                $rows .= $this->getElementSeparator();
                $rows .= $this->getData('component_separator', '');
                if ($this->getComponentSeparator() !== '\n') {
                    $rows .= PHP_EOL;
                }
            }
        }
        return $rows;
    }

    /**
     * @param $contents
     * @param $maxColumn
     * @return array
     */
    public function reStructureData($contents, $maxColumn)
    {
        $data = [];
        foreach ($contents as $key => $content) {
            foreach ($content as $target => $item) {
                foreach ($item as $field => $value) {
                    $string = $this->generateSpace($maxColumn[$field], $value);
                    $data[$key][$target][$field] = sprintf('%s', $string);
                }
            }
        }
        return $data;
    }

    /**
     * @param $headers
     * @return null|string
     */
    protected function createHeader($headers)
    {
        $rows = null;
        $maxColumn = $headers;
        $headers = array_keys($headers);
        foreach ($headers as $value) {
            $data[] = sprintf('%s', $this->generateSpace($maxColumn[$value], $value));
        }

        $rows .= implode($this->getElementSeparator(), $data);
        $rows .= $this->getElementSeparator() . PHP_EOL;

        return $rows;
    }

    /**
     * @return array
     */
    public function afterProcess()
    {
        return parent::afterProcess(); // TODO: Change the autogenerated stub
    }

    /**
     * @param $contents
     * @return array
     */
    public function setSpaceForColumns($contents)
    {
        $columns = $maxSize = [];
        foreach ($contents as $content) {
            foreach ($content as $target => $item) {
                foreach ($item as $field => $value) {
                    $columns[$field][] = $field;
                    $columns[$field][] = $item[$field];
                }
            }
        }
        foreach ($columns as $key => $field) {
            $maxSize[$key] = 0;
            foreach ($field as $item) {
                $maxSize[$key] = $maxSize[$key] > strlen($item) ?
                    $maxSize[$key] : strlen($item);
            }
        }
        return $maxSize;

    }

    /**
     * @param $number
     * @param $field
     * @return string
     */
    public function generateSpace($number, $field)
    {
        $num = $number - strlen($field) + 3;
        for ($i=0; $i<=$num; $i++) {
            $field .= ' ';
        }
        return $field;
    }
}
