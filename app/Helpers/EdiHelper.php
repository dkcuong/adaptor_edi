<?php

use App\DbLog;
use Illuminate\Support\Debug\Dumper;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;

/**
 * @param array $array
 * @param array $keys
 *
 * @return array
 */
function array_unset_elements(array $array, array $keys)
{
    foreach ($keys as $key) {
        unset($array[ $key ]);
    }

    return $array;
}

/**
 * @param $string
 *
 * @return bool
 */
function isJson($string)
{
    json_decode($string);

    return (json_last_error() == JSON_ERROR_NONE);
}

/**
 *
 */
function ddd()
{
    array_map(function ($value) {
        (new Dumper)->dump($value);
    }, func_get_args());
}

/**
 *
 */
function dr()
{
    array_map(function ($value) {
        print_r($value);
        print_r("\r\n");
        //var_export ($value);
    }, func_get_args());
    die(1);
}

/**
 *
 */
function drr()
{
    array_map(function ($value) {
        print_r($value);
        print_r("\r\n");
        //var_export ($value);
    }, func_get_args());
}

/**
 * @param mixed $content
 * @param array $option
 *
 * @return bool
 */
function dblog($content, array $option = [])
{
    if (! config('app.db_log')) {
        return false;
    }

    if (is_array($content) || is_object($content)) {
        $content = json_encode((array) $content);
    }

    $requestTime = isset($option['request_time']) ? $option['request_time'] : config('app.request_time');
    $url = isset($option['url']) ? $option['url'] : URL::current();
    $trace = isset($option['trace']) ? $option['trace'] : json_encode(debug_backtrace());

    $log = new DbLog();
    $tableName = $log->getTable();
    if (! Schema::hasTable($tableName)) {
        return false;
    }

    $log->request_time = $requestTime;
    $log->url = $url;
    $log->content = $content;
    $log->trace = $trace;

    return $log->save();
}

function generateKeyX12Log()
{
    return md5(uniqid());
}

function getDefault($name, $default = '')
{
    return ! empty($name) ? $name : $default;
}

function getArr($name, $field, $default = '')
{
    return isset($name[$field]) ? $name[$field] : $default;
}


function generateUCC128($companyPrefix, $cartonID)
{
    if (strlen($cartonID) >= 7) {
        $cartonID = substr($cartonID, 0, 7);
    } else {
        $cartonID = str_pad($cartonID, 7, 0, STR_PAD_LEFT);
    }

    $txEan20 = $companyPrefix . $cartonID;
    $sum = ($txEan20[0] + $txEan20[2]+ $txEan20[4]+ $txEan20[6]+ $txEan20[8]+ $txEan20[10]+ $txEan20[12]+ $txEan20[14]+ $txEan20[16]) * 3 +
        ($txEan20[1] + $txEan20[3]+ $txEan20[5]+ $txEan20[7]+ $txEan20[9] + $txEan20[11]+ $txEan20[13]+ $txEan20[15]) * 1;

    if ($sum%10 == 0) {
        $digit = 0;
    } else {
        $digit = 10 - $sum%10;
    }

    return $txEan20 . $digit;
}