<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 1/25/2019
 * Time: 9:53 AM
 */
return [
    'make_it_real' => env('MAKEITREAL','MAKEITREAL'),
    'make_it_real_logo' => env('MAKEITREAL_LOGO','http://test.awms.staging.seldatdirect.com/wms/index/custom/images/logo_mir.png'),
    'move_file_client' => [
      'TLI'
    ],

    'disabled_wh_default' => [
        'TLI'
    ],
];