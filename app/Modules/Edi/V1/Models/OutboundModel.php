<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 23/02/2016
 * Time: 15:13
 */

namespace App\Modules\Edi\V1\Models;

use App\Clients;
use App\X12Interchanges;
use App\X12OutboundGroupRevisions;
use App\X12OutboundGroups;
use App\X12OutboundInterchanges;
use App\X12OutboundTransactions;
use Illuminate\Support\Facades\DB;

class OutboundModel extends DBModel
{

    /**
     * @param $clientId
     * @param $groupId
     * @return mixed
     */
    public function getOutboundGroups($clientId, $groupId)
    {
        $results = X12Interchanges::select('gr.*')
            ->join('x12_outbound_interchanges AS oi', 'oi.interchange_id', '=', 'x12_interchanges.id')
            ->join('x12_outbound_groups AS g', 'g.interchange_id', '=', 'x12_interchanges.id')
            ->join('x12_outbound_group_revisions AS gr', 'gr.interchange_id', '=', 'oi.id')
            ->join('x12_outbound_transactions AS t', 't.group_id', '=', 'gr.id')
            ->join('outbounds AS ob', 'ob.group_id', '=', 'gr.group_id')
            ->whereRaw('gr.interchange_id = oi.id')
            ->whereRaw('g.id = gr.group_id')
            ->whereRaw('t.group_control_number = gr.control_number')
            ->whereRaw('ob.client_id = x12_interchanges.client_id')
            ->whereRaw('ob.transactional_code = CONCAT("T", t.transaction_code)')
            ->where('x12_interchanges.client_id', $clientId)
            ->where('oi.client_id', $clientId)
            ->where('g.id', $groupId)
            ->where('gr.status', '=', 'Open')
            ->where('t.status', '=', 'Open')
            ->groupBy('gr.id')
            ->get();

        return $results;
    }

    /**
     * @param $groupId
     * @return mixed
     */
    public function getOutboundInterChange($groupId)
    {
        $results = X12OutboundInterchanges::select('x12_outbound_interchanges.*')
            ->join('x12_outbound_group_revisions AS gr', 'gr.interchange_id', '=', 'x12_outbound_interchanges.id')
            ->where('gr.id', $groupId)
            ->where('gr.status', '=', 'open')
            ->first();
        return $results;
    }

    /**
     * @param $inter
     * @param array $parseHeaders
     * @return mixed
     */
    public function insertOutboundInterchange(&$inter, $parseHeaders = [])
    {

        $interchange = $inter->toArray();
        $lockInterchange = [];
        if (! empty($parseHeaders) && ! empty($parseHeaders['interchange'])) {
            $lockInterchange = $parseHeaders['interchange'];
            unset($lockInterchange['control_number']);
            foreach ($lockInterchange as $key => $value) {
                $interchange[$key] = $value;
            }
        }

        $controlNumber = $this->getNextID('x12_outbound_interchanges');

        $data = [
            'auth_qualifier'       => $interchange['auth_qualifier'],
            'auth_info'            => $interchange['auth_info'],
            'security_qualifier'   => $interchange['security_qualifier'],
            'security_info'        => $interchange['security_info'],
            'sender_qualifier'     => $interchange['receiver_qualifier'],
            'sender_code'          => $interchange['receiver_code'],
            'receiver_qualifier'   => $interchange['sender_qualifier'],
            'receiver_code'        => $interchange['sender_code'],
            'date'                 => date('ymd'),
            'time'                 => date('Hi'),
            'repetition_separator' => $interchange['repetition_separator'],
            'control_version'      => $interchange['control_version'],
            'control_number'       => $controlNumber,
            'requested'            => $interchange['requested'],
            'usage'                => $interchange['usage'],
            'separator'            => $interchange['separator'],
            'status'               => '',
            'client_id'            => $interchange['client_id'],
            'interchange_id'       => $interchange['id'],
        ];
        $result = X12OutboundInterchanges::create($data);
        $inter['control_number'] = $controlNumber;
        return $result->id;

    }

    /**
     * @param $groupData
     * @param $interRevId
     * @param $parseHeaders
     * @return array
     */
    public function updateGroup($groupData, $interRevId, $parseHeaders)
    {
        $lockGroupData = [];
        if (! empty($parseHeaders) && ! empty($parseHeaders['groups'][0])) {
            $lockGroupData = $parseHeaders['groups'][0];
        }

        $controlNumber = $this->getNextID('x12_outbound_group_revisions');

        $result = X12OutboundGroupRevisions::create(
            [
                'group_code'       => $groupData['group_code'],
                'sender_code'      => ! empty($lockGroupData['receiver_code']) ? $lockGroupData['receiver_code'] : $groupData['receiver_code'],
                'receiver_code'    => ! empty($lockGroupData['sender_code']) ? $lockGroupData['sender_code'] : $groupData['sender_code'],
                'date'             => date('Ymd'),
                'time'             => date('Hi'),
                'control_number'   => $controlNumber,
                'control_standard' => $groupData['control_standard'],
                'control_version'  => $groupData['control_version'],
                'status'           => 'open',
                'interchange_id'   => $interRevId,
                'group_id'         => $groupData['id'],
            ]
        );

        $this->updateGroupControlNumber($groupData['id'], $controlNumber);

        return [
            'group_rev_id'         => $result->id,
            'group_control_number' => $controlNumber,
        ];
    }

    /**
     * @param $groupID
     * @param $nextControlNumber
     * @return mixed
     */
    public function updateGroupControlNumber($groupID, $nextControlNumber)
    {
        return X12OutboundGroups::where('id', '=', $groupID)
            ->update(['control_number' => $nextControlNumber]);
    }

    /**
     * @param $groupData
     * @param $controlNumber
     * @param $dataInput
     * @param $dataOutput
     * @param $outboundGroup
     * @return mixed
     */
    public function insertOutboundTransactions(
        $groupData,
        $controlNumber,
        $dataInput,
        $dataOutput,
        $outboundGroup
    ) {
        $successNumber = isset($dataOutput['data']['success_number']) ?
            $dataOutput['data']['success_number'] : null;

        if (isset($dataOutput['data']['disableOutbound']) && ! $dataOutput['data']['disableOutbound']) {
            $dataOutput['status'] = 'disabled';
        }
        $tranCode = $outboundGroup->outboundTransaction->transactional_code;
        $tranCode = substr($tranCode, 1, strlen($tranCode));

        //if success_number is array
        if (is_array($successNumber)) {
            foreach ($successNumber as $key => $sn) {
                if ($key == $tranCode) {
                    $data = $this->makeDataBeforeInsert($tranCode, $controlNumber, $groupData, $sn, $dataInput, $dataOutput);
                    $tran = $this->createOBTransaction($data);
                }
            }
        } else {
            $data = $this->makeDataBeforeInsert($tranCode, $controlNumber, $groupData, $successNumber, $dataInput, $dataOutput);
            $tran = $this->createOBTransaction($data);
        }

        return $tran;
    }

    /**
     * @param $inter
     */
    public function updateInterChange($inter)
    {
        X12Interchanges::where('id', $inter->id)
            ->update(['control_number' => $inter->control_number]);
    }

    /**
     * @param $client
     * @param $edi
     * @return bool
     */
    public function hasOutBoundOnDay($client, $edi)
    {
        $transactionCode = trim($edi->transactional_code, 'T');
        $currentDate = date('Y-m-d');

        $result = X12OutboundTransactions::join(
            'x12_outbound_group_revisions AS ogr',
            'x12_outbound_transactions.group_id',
            '=',
            'ogr.id'
        )
        ->join('x12_outbound_interchanges AS oi', 'ogr.interchange_id', '=', 'oi.id')
        ->where('x12_outbound_transactions.transaction_code', $transactionCode)
        ->where('oi.client_id', $client->id)
        ->whereRaw('date(x12_outbound_transactions.created_at) = "' . $currentDate . '"')
        ->count();

        return $result ? true : false;
    }

    /**
     * @param $successNumber
     * @param $data
     * @param bool $checkMessageQueue
     * @param bool $transactional
     * @return bool
     */
    public function updateDataOutputBySuccessNumber($successNumber, $data, $checkMessageQueue = false, $transactional = false)
    {
        logger("Transaction code: ". json_encode($transactional),['type' => ' debug']);
        $transactionCodes = $this->getTransactionCode($transactional);
        logger("Transaction code after: ". json_encode($transactionCodes),['type' => ' debug']);
        $data = is_array($data) ? json_encode($data) : $data;
        logger("Check IF: " . ($checkMessageQueue && !$this->checkMessageQueue($successNumber)), ['type'=>'debug']);
        logger("Success Number: ".$successNumber, ['type'=>'debug']);
        if ($checkMessageQueue && !$this->checkMessageQueue($successNumber)) {
            return false;
        }
        logger("update X12OutboundTransactions",['type' => ' debug']);
        $queryUpdate = X12OutboundTransactions::where('success_number', $successNumber);

        $queryUpdate->where('status', 'Open');
        if(!empty($transactionCodes)) {
            $queryUpdate->whereIn('transaction_code', $transactionCodes);
        }

        return $queryUpdate->update(['data_output' => $data]);
    }

    /**
     * @param $successNumber
     * @param $data
     * @return mixed
     */
    public function updateTMSDataOutputBySuccessNumber($successNumber, $data, $checkMessageQueue = false)
    {
        $data = is_array($data) ? json_encode($data) : $data;
        if ($checkMessageQueue && !$this->checkMessageQueue($successNumber)) {
            logger('not message queue');
            return;
        }
        if (!$data) {
            logger('empty data');
            return;
        }
        $res = X12OutboundTransactions::where('success_number', $successNumber)
            ->update(['data_output' => $data, 'status' => 'Open']);

        return $res;
    }

    public function checkMessageQueue($successNumber)
    {
        $result = X12OutboundTransactions::select('ob.message_queue')
            ->join('x12_outbound_group_revisions AS ogr', 'x12_outbound_transactions.group_id', '=', 'ogr.id')
            ->join('x12_outbound_interchanges AS oi', 'ogr.interchange_id', '=', 'oi.id')
            ->join('outbounds AS ob','ob.group_id', '=', 'ogr.group_id')
            ->where('success_number', '=', $successNumber)
            ->first();
        if ($result) {
            return $result->message_queue;
        }
        return false;
    }

    public function makeDataBeforeInsert($tranCode, $controlNumber, $groupData, $successNumber, $dataInput, $dataOutput)
    {
        $data = [];
        if (is_array($successNumber)) {
            foreach ($successNumber as $snumber) {
                $data[] = [
                    'transaction_code'     => $tranCode,
                    'control_number'       => $controlNumber,
                    'group_id'             => $groupData['group_rev_id'],
                    'group_control_number' => $groupData['group_control_number'],
                    'success_number'       => $snumber,
                    'data_input'           => json_encode($dataInput),
                    'status'               => $dataOutput['status'],
                    'message'              => $dataOutput['message'],
                ];
            }
        } else {
            $data[] = [
                'transaction_code'     => $tranCode,
                'control_number'       => $controlNumber,
                'group_id'             => $groupData['group_rev_id'],
                'group_control_number' => $groupData['group_control_number'],
                'success_number'       => $successNumber,
                'data_input'           => json_encode($dataInput),
                'status'               => $dataOutput['status'],
                'message'              => $dataOutput['message'],
            ];
        }

        return $data;
    }


    public function createOBTransaction($data)
    {
        $trans = X12OutboundTransactions::insert(
            $data
        );
        return $trans;
    }

    /**
     * @param $transactional
     * @return array|string
     */
    private function getTransactionCode($transactional)
    {
        $tranCode = '';
        if(!$transactional)
            return $tranCode;

        switch ($transactional) {
            case 'REC':
                $tranCode = ['861', '944'];
                break;
            case 'ORD':
                $tranCode = ['945', '856'];
                break;
            case 'BOL':
                $tranCode = ['858'];
                break;
            case 'INV':
                $tranCode = ['810'];
                break;
            case 'ORD_UPDATED':
                $tranCode = ['855'];
                break;
            default:
                break;
        }
        return $tranCode;
    }

}
