<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 22-Jan-16
 * Time: 9:24 AM
 */

namespace App\Mappers;


interface MapperInterface
{
    /**
     * Get return data after mapping
     *
     * @return mixed
     */
    public function get();
}
