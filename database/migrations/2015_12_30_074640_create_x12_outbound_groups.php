<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateX12OutboundGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( '
			CREATE TABLE IF NOT EXISTS `x12_outbound_groups` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `group_code` char(2) COLLATE utf8_unicode_ci NOT NULL,
			  `sender_code` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `receiver_code` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `date` int(8) DEFAULT NULL,
			  `time` int(8) DEFAULT NULL,
			  `control_number` int(9) DEFAULT NULL,
			  `control_standard` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT \'X\',
			  `control_version` smallint(5) unsigned zerofill NOT NULL DEFAULT \'04010\',
			  `status` bit(1) NOT NULL DEFAULT b\'0\',
			  `interchange_id` bigint(20) NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `UNI_INTERCHANGE_GROUP_NUMBER` (`interchange_id`,`group_code`,`control_number`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
