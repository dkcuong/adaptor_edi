<?php

/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/02/2016
 * Time: 02:47
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class X12InboundGroups extends Model
{
    /**
     * @var string
     */
    protected $table = 'x12_inbound_groups';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @var array
     */
    public $outboundFields = [
        "group_code",
        "sender_code",
        "receiver_code",
        "date",
        "time",
        "control_number",
        "control_standard",
        "control_version",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groupRevisions()
    {
        return $this->hasMany(X12InboundGroupRevisions::class, 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(X12InboundTransactions::class, 'group_id');
    }
}
