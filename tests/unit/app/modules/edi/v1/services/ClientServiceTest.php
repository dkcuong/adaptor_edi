<?php

/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 05-Apr-16
 * Time: 1:58 PM
 */
class ClientServiceTest extends TestCase
{
    protected $object;

    public function __construct()
    {
        $this->object = Mockery::mock('App\Modules\Edi\V1\Services\ClientService');
    }

    public function testMakeLocalFilePath()
    {
        $file = 'test.txt';
        $client = new stdClass();
        $client->short_name = 'COCI';

        $result = $this->object->makeLocalFilePath($file, $client, 'inbound');

        $resultString = 'backup'.DIRECTORY_SEPARATOR.'COCI'.DIRECTORY_SEPARATOR.'inbound'.DIRECTORY_SEPARATOR.'test.txt';
        $this->assertEquals($resultString, $result);
    }

    public function testFileValidate()
    {
        $file = 'testing_test.txt';
        $support = $edi = new stdClass();
        $support->extension = '.txt';
        $edi->filename_prefix = 'testing_';

        $result = $this->object->fileValidate($support, $edi, $file);
        $this->assertEquals(true, $result);
    }

    public function testFileFullValidate()
    {
        $file = 'testing_test.txt';
        $support = $edi = new stdClass();
        $support->extension = '.txt';
        $edi->filename_prefix = 'testing_test';

        $result = $this->object->fileValidate($support, $edi, $file);
        $this->assertEquals(true, $result);
    }

    public function testFileFailValidate()
    {
        $file = 'testing_test.txt';
        $support = $edi = new stdClass();
        $support->extension = '.txtx';
        $edi->filename_prefix = 'testing_test';

        $result = $this->object->fileValidate($support, $edi, $file);
        $this->assertEquals(false, $result);
    }

    public function testFileFail2Validate()
    {
        $file = 'testing_test.txt';
        $support = $edi = new stdClass();
        $support->extension = '.txt';
        $edi->filename_prefix = 'jonh';

        $result = $this->object->fileValidate($support, $edi, $file);
        $this->assertEquals(false, $result);
    }

    public function testValidFileNamePrefix()
    {
        $file = 'testing_test.txt';
        $edi = new stdClass();
        $edi->filename_prefix = 'testing_test';

        $result = $this->object->validFileNamePrefix($edi, $file);
        $this->assertEquals(true, $result);
    }
}
