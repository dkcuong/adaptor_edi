<?php
/**
 * Created by PhpStorm.
 * User: viet.le
 * Date: 8/25/2017
 * Time: 9:14 AM
 */

namespace App\Console\Commands;

use App\Modules\Edi\V1\Services\NotiFtpInboundService;
use App\Modules\Edi\V1\Services\EmailHandlingService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RunReNotiFtpInbound extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edi:renotiftpinbound';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Ftp client upload';


    /**
     * @var ClientService
     */
    protected $service;

    /**
     * @var EmailHandlingService
     */
    protected $errorHandle;

    /**
     * RunEdi constructor.
     * @param NotiFtpInboundService $service
     * @param EmailHandlingService $emailHandlingService
     */
    public function __construct(
        NotiFtpInboundService $service,
        EmailHandlingService $emailHandlingService
    )
    {
        parent::__construct();

        $this->service = $service;
        $this->errorHandle = $emailHandlingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        logger('Notification for ftp inbound Is Running');
        try {
            $this->service->init();
            $this->service->reCheckFtpInbound();
        } catch (\Exception $e) {

            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            Log::error($e->getMessage(), ['info' => $errorInfo]);

            $messageHandle = [
                'Message: ' . $e->getMessage(),
                'File:' . __FILE__,
                'Class:' . __CLASS__,
                'Function:' . __FUNCTION__,
                'Line Number:' . __LINE__
            ];

            logger($messageHandle);
            $this->errorHandle->sendEmail([$messageHandle]);

            //Show error when run CLI
            $this->error($e->getMessage());
            $this->error($errorInfo);
        }
    }
}