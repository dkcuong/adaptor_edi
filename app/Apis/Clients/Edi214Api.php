<?php

namespace App\Apis\Clients;


use App\Apis\ApiBase;
use App\Apis\ApiInterface;

class Edi214Api extends ApiBase implements ApiInterface
{
    /**
     * @var string
     */
    protected $api = 'items/v1/214';

    /**
     * Call Api items/v1/214
     * With data from client
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function callApi()
    {
        $data = $this->getConfig('data');

        $response = $this->post($data);

        return $response;
    }
}
