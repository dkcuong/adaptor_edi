<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 24-Jan-16
 * Time: 9:37 PM
 */

namespace App\Apis\Clients;

use App\Apis\ApiBase;
use App\Apis\ApiInterface;

class Edi856Api extends ApiBase implements ApiInterface
{
    /**
     * @var string
     */
    protected $api = 'items/v1/receiving';

    /**
     * @return array
     */
    public function callApi()
    {

        $result = [];
        $data = $dataInputs = $this->getConfig('data');

        $reference = $vendor_id = $warehouse_code = $vendor_name = '';
        foreach ($data as $key => $val) {
            if (! isset($val['reference']) || ! $val['reference']) {
                unset($data[$key]);
            }

            if (isset($val['reference'])) {
                if ($reference && $reference != $val['reference']) {
                    logger('Too many reference number in an imported file. The row: ' . $val['reference']);
                    dd('Too many vendor ID in an imported file.' . $val['reference'], __FILE__ . ' ' . __CLASS__ . ' '. __LINE__);
                } else {
                    $reference = $val['reference'];
                }
            }

            if (isset($val['vendor_id'])) {
                if ($vendor_id && $vendor_id != $val['vendor_id']) {
                    logger('Too many vendorID in an imported file. The row: ' . $val['vendor_id']);
                    dd('Too many vendorID in an imported file.' . $val['vendor_id'], __FILE__ . ' ' . __CLASS__ . ' '. __LINE__);
                } else {
                    $vendor_id = $val['vendor_id'];
                }
            }

            if (isset($val['warehouse_code'])) {
                if ($warehouse_code && $warehouse_code != $val['warehouse_code']) {
                    logger('Too many WarehouseShortName in an imported file. The row: ' . $val['vendor_id']);
                    dd('Too many WarehouseShortName in an imported file.' . $val['warehouse_code'], __FILE__ . ' ' . __CLASS__ . ' '. __LINE__);
                } else {
                    $warehouse_code = $val['warehouse_code'];
                }
            }

            if (isset($val['vendor_name'])) {
                if ($vendor_name && $vendor_name != $val['vendor_name']) {
                    logger('Too many VendorName in an imported file. The row: ' . $val['vendor_id']);
                    dd('Too many VendorName in an imported file.' . $val['vendor_name'], __FILE__ . ' ' . __CLASS__ . ' '. __LINE__);
                } else {
                    $vendor_name = $val['vendor_name'];
                }
            }
        }

        $reProcess = $dataInputs = $this->getConfig('re_process');
        $isX12 = $this->getConfig('isX12');

        $fieldInLoop = [
            'container',
            'exp_date',
            'purchase_order',
            'po',
            'size',
            'color',
            'batch',
            'sku',
            'description',
            'pack_size',
            'pack',
            'cartons',
            'quantity',
            'lot',
            'height',
            'width',
            'length',
            'weight',
            'volume',
            'customer_upc',
            'cus_upc',
            'cbm',
            'gross_weight',
            'pcs_expected'
        ];


        if (! $isX12 && ! $reProcess) {
            $data = $dataInputs = $this->groupBy($data, 'reference', 'containers', $fieldInLoop);
        }
        foreach ($data as $index => $ref) {

            if (! $isX12) {
                $ref['containers'] = $this->groupBy($ref['containers'], 'container', 'items', $fieldInLoop);
            }

            $data = $this->post($ref);
            $result[$index] = json_decode($data->getBody(), true);
            $result[$index]['reference'] = isset($ref['reference'])?$ref['reference']:'';
            $result[$index]['container'] = isset($ref['container'])?$ref['container']:'';
            $result[$index]['lot'] = isset($ref['purchase_order'])?$ref['purchase_order']:'';
        }
        $results = [
            'data' => $dataInputs,
            'output' => $result,
        ];
        return $results;
    }
}
