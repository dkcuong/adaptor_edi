<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataOnAddressesTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('address_types')->insert(
            array(
                'name' => 'Shipping address',
                'short_name' => 'SA',
                'description' => ''
            )
        );
        
        DB::table('address_types')->insert(
            array(
                'name' => 'Billing address',
                'short_name' => 'BA',
                'description' => ''
            )
        );
        
        DB::table('address_types')->insert(
            array(
                'name' => 'Primary address',
                'short_name' => 'PA',
                'description' => ''
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses_types', function (Blueprint $table) {
            //
        });
    }
}
