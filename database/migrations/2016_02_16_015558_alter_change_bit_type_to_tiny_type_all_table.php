<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChangeBitTypeToTinyTypeAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `x12_inbound_groups` 
            CHANGE `status` `status` TINYINT(1) NOT NULL');
        
        DB::statement('ALTER TABLE `x12_inbound_interchanges` 
            CHANGE `requested` `requested` TINYINT(1) NOT NULL');
        
        DB::statement('ALTER TABLE `x12_inbound_interchanges` 
            CHANGE `status` `status` TINYINT(1) NOT NULL');
        
        DB::statement('ALTER TABLE `x12_inbound_transactions` 
            CHANGE `status` `status` TINYINT(1) NOT NULL');
        
        DB::statement('ALTER TABLE `x12_outbound_group_revisions` 
            CHANGE `status` `status` TINYINT(1) NOT NULL');
        
        DB::statement('ALTER TABLE `x12_outbound_groups` 
            CHANGE `status` `status` TINYINT(1) NOT NULL');
        
        DB::statement('ALTER TABLE `x12_outbound_interchanges` 
            CHANGE `requested` `requested` TINYINT(1) NOT NULL');
        
        DB::statement('ALTER TABLE `x12_outbound_interchanges` 
            CHANGE `status` `status` TINYINT(1) NOT NULL');
        
        DB::statement('ALTER TABLE `x12_outbound_interchanges` 
            CHANGE `status` `status` TINYINT(1) NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('x12_inbound_groups', function (Blueprint $table) {
            //
        });
    }
}
