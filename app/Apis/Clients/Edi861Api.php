<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 24/02/2016
 * Time: 10:20
 */

namespace App\Apis\Clients;


use App\Apis\ApiBase;
use App\Apis\ApiInterface;

class Edi861Api extends ApiBase implements ApiInterface
{
    /**
     * @var string
     */
    protected $api = 'items/v1/receiving-report';

    /**
     * Call Api items/v1/861
     * With data from client
     *
     * @return array
     */
    public function callApi()
    {
        $input['references'] = $this->getConfig('data');
        $response = $this->post($input);

        return $response;
    }
}
