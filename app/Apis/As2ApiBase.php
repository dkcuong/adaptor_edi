<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 17/06/2016
 * Time: 10:53
 */

namespace App\Apis;


use App\Utils\As2ApiClient;

class As2ApiBase
{
    public $client;
    public $config;
    public $api;

    /**
     * As2ApiBase constructor.
     * @param As2ApiClient $client
     * @param $config
     */
    public function __construct(As2ApiClient $client, $config)
    {
        $this->setConfig($config);

        $this->client = $client;
    }

    /**
     * @param $key
     * @param string $default
     * @return array
     */
    public function getConfig($key, $default = '')
    {
        return isset($this->config[$key]) ? $this->config[$key] : $default;
    }

    /**
     * @param array $config
     * @return array
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function post($data)
    {
        logger($data, ['type' => 'Call Api Data']);

        $api = $this->getApiEndPoint();
        try {
            return $this->client->post($api, ['json' => $data]);
        } catch (\Exception $e) {
            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            logger($e->getMessage(), ['info' => $errorInfo]);
        }
    }

    private function getApiEndPoint()
    {
        $domain = config('api.as2_outbound_api');

        return $domain . $this->api;
    }

    public function callAs2Api()
    {
        $data = $this->getConfig('content');

        $response = $this->post($data);

        $result = json_decode($response->getBody(), true);

        return $result;
    }

}