<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Mappers\MapperBase;

class MapperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Mapper', function ($app, $config) {
            $class = MapperBase::class;
            // If client has its Mapper, system will call Client Mapper
            // And extends MapperBase class
            if (isset($config['client']) && isset($config['type'])) {
                $extendClass = 'App\Mappers\\' . $config['type'] . '\\' . $config['client'] . 'Mapper';

                $class = class_exists($extendClass) ? $extendClass : $class;
            }

            return new $class($config);
        });
    }
}
