<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class X12OutboundTransactionFiles extends Model
{
    /**
     * @var string
     */
    protected $table = 'x12_outbound_transaction_files';

    /**
     * @var array
     */
    protected $fillable = [
        'outbound_group_id',
        'file_name',
        'content'
    ];
}
