<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class X12InboundTransactionFiles extends Model
{
    /**
     * @var string
     */
    protected $table = 'x12_inbound_transaction_files';

    /**
     * @var array
     */
    protected $fillable = [
        'inbound_group_id',
        'file_name',
        'content'
    ];
}
