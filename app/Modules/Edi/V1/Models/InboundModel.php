<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/02/2016
 * Time: 16:41
 */

namespace App\Modules\Edi\V1\Models;

use App\X12InboundGroupRevisions;
use App\X12InboundGroups;
use App\X12InboundInterchange;
use App\X12InboundTransactionFiles;
use App\X12InboundTransactions;
use App\X12Interchanges;
use App\Inbounds;

class InboundModel
{
    /**
     * @param $clientId
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getRsa($clientId)
    {
        return X12InboundInterchange::with('inboundGroups')
            ->where('client_id', $clientId)
            ->first();
    }

    /**
     * @param $groupId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getInboundGroupRevisions($groupId)
    {
        return X12InboundGroups::with('groupRevisions')
            ->where('id', $groupId)
            ->get();
    }

    /**
     * @param $interchangeData
     */
    public function updateInboundInterchange($interchangeData)
    {
        X12Interchanges::where('id', $interchangeData['id'])
            ->update(
                [
                    'control_number' => $interchangeData['control_number'] + 1,
                ]
            );
    }

    /**
     * @param $inter
     * @param $inboundGroup
     */
    public function updateInboundGroup($inter, $inboundGroup)
    {
        X12InboundGroups::where('interchange_id', $inter['id'])
            ->where('group_code', $inboundGroup['group_code'])
            ->update(
                [
                    'control_number' => $inboundGroup['control_number'] + 1,
                ]
            );

    }

    /**
     * @param $interId
     * @param $groupData
     * @param $interchange
     * @return array
     */
    public function insertInboundGroupRevisions($interId, $groupData, $interchange)
    {
        $result = X12InboundGroupRevisions::create(
            [
                'sender_code'            => $groupData['sender_code'],
                'receiver_code'          => $groupData['receiver_code'],
                'date'                   => date('Ymd'),
                'time'                   => date('His'),
                'control_number'         => $groupData['control_number'],
                'control_standard'       => $groupData['control_standard'],
                'control_version'        => $groupData['control_version'],
                'status'                 => $interchange['status'],
                'interchange_id'         => $interchange['id'],
                'inbound_interchange_id' => $interId,
                'group_id'               => $groupData['id'],
            ]
        );

        return [
            'group_rev_id'         => $result->id,
            'group_control_number' => $groupData['control_number'],
        ];
    }

    /**
     * @param $edi
     * @param $groupData
     * @param $dataInput
     * @param $parseData
     * @param $conNum
     */
    public function insertInboundTransactions($edi, $groupData, $dataInput, $parseData, $conNum)
    {
        $successNumber = isset($parseData['data']['success_number']) ?
            $parseData['data']['success_number'] : null;
        if (is_array($successNumber)) {
            $successNumber = $successNumber[$edi->name];
        }
        $result = X12InboundTransactions::create(
            [
                'transaction_code'     => $edi->name,
                'control_number'       => $conNum,
                'group_id'             => $groupData['group_rev_id'],
                'group_control_number' => $groupData['group_control_number'],
                'data'                 => json_encode($dataInput),
                'status'               => $parseData['status'],
                'status_wms'           => X12InboundTransactions::NEW_STATUS_WMS,
                'message'              => $parseData['message'],
                'success_number'       => $successNumber,
            ]
        );
        return $result->id;
    }

    /**
     * @param $interchange
     * @return mixed
     */
    public function insertInboundInterchange($interchange)
    {
        $interchange = X12InboundInterchange::create(
            [
                'auth_qualifier'       => $interchange['auth_qualifier'],
                'auth_info'            => $interchange['auth_info'],
                'security_qualifier'   => $interchange['security_qualifier'],
                'security_info'        => $interchange['security_info'],
                'sender_qualifier'     => $interchange['sender_qualifier'],
                'sender_code'          => $interchange['sender_code'],
                'receiver_qualifier'   => $interchange['receiver_qualifier'],
                'receiver_code'        => $interchange['receiver_code'],
                'date'                 => date('ymd'),
                'time'                 => date('His'),
                'repetition_separator' => $interchange['repetition_separator'],
                'control_version'      => $interchange['control_version'],
                'control_number'       => $interchange['control_number'],
                'requested'            => $interchange['requested'],
                'usage'                => $interchange['usage'],
                'separator'            => $interchange['separator'],
                'status'               => '1',
                'client_id'            => $interchange['client_id'],
                'interchange_id'       => $interchange['id'],
            ]
        );

        return $interchange->id;
    }

    /**
     * @param $clientId
     * @return mixed
     */
    public function getInterchange($clientId)
    {
        return X12Interchanges::where('client_id', $clientId)->first();
    }

    /**
     * @param $client
     * @param $interchange
     * @return bool
     */
    public function isExistInboundInterchange($client, $interchange)
    {
        $interId = $client->rsa->id;
        $result = X12InboundInterchange::where('interchange_id', $interId)
            ->where('control_number', '>=', intval($interchange['control_number']))
            ->first();

        return $result ? true : false;
    }

    /**
     * @param $interchange
     * @return int
     */
    public function getNextIsaConNum($interchange)
    {
        $conNum=  X12InboundInterchange::where('interchange_id', $interchange['id'])
            ->max('control_number');
        return $conNum ? $conNum + 1 : 1;
    }

    /**
     * @param $groupData
     * @param $fileName
     */
    public function insertTransactionFiles($groupData, $fileName, $inputContent = '')
    {
        X12InboundTransactionFiles::create([
            'inbound_group_id' => $groupData['group_rev_id'],
            'file_name' => $fileName,
            'content' => $inputContent
        ]);
    }

    /**
     * @param $clientId
     * @param $outboundTransactionCode
     * @return string
     */
    public function getInboundMappingConfig($clientId,$outboundTransactionCode)
    {
        $transactionCode = '';
        switch ($outboundTransactionCode) {
            case '861':
                $transactionCode = '856';
                break;
        }
        $inbound = Inbounds::where('client_id', $clientId)->where('name', $transactionCode)->get()->first();
        if (empty($inbound)) {
            return '';
        }
        $inboundMapping = $inbound->inboundMapping;
        if (empty($inboundMapping)) {
            return '';
        }
        $customRequiredField = $inboundMapping->custom_required_field;
        return empty($customRequiredField) ? '' : $customRequiredField;
    }

     /* @param $groupId
     * @return mixed
     */
    public function getInboundGroups($clientId, $groupId, $status = null)
    {
        $status = empty($status) ? 'Re-process' : $status;
        $results = X12Interchanges::select('gr.*')
            ->join('x12_inbound_interchanges AS ii', 'ii.interchange_id', '=', 'x12_interchanges.id')
            ->join('x12_inbound_groups AS g', 'g.interchange_id', '=', 'x12_interchanges.id')
            ->join('x12_inbound_group_revisions AS gr', 'gr.inbound_interchange_id', '=', 'ii.id')
            ->join('x12_inbound_transactions AS t', 't.group_id', '=', 'gr.id')
            ->join('inbounds AS ib', 'ib.group_id', '=', 'gr.group_id')
            ->whereRaw('gr.inbound_interchange_id = ii.id')
            ->whereRaw('g.id = gr.group_id')
            ->whereRaw('t.group_control_number = gr.control_number')
            ->whereRaw('ib.client_id = x12_interchanges.client_id')
            ->whereRaw('ib.transactional_code = CONCAT("T", t.transaction_code)')
            ->where('x12_interchanges.client_id', $clientId)
            ->where('ii.client_id', $clientId)
            ->where('g.id', $groupId)
            ->where('t.status', '=', $status)
            ->groupBy('gr.id')
            ->get();
        return $results;
    }

    public function getInboundTransaction($id){
        return X12InboundTransactions::find($id);
    }
}
