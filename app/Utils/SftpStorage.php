<?php
namespace App\Utils;

use App\Clients;
use App\FtpAccounts;
use App\Modules\Edi\V1\Services\SshKeysService;
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;

/**
 * Class SftpStorage
 *
 * @package App\Utils
 */
class SftpStorage implements StorageInterface
{

    /**
     * @var string
     */
    public $name = 'SftpStorage';

    /**
     * @var int
     */
    protected $timeout = 5;

    /**
     * @var array
     */
    public $sshKeys = [];

    /**
     * Store Ftp configuration info
     *
     * @var array
     */
    protected $clientInfo;

    /**
     * @var SFTP
     */
    protected $disk = null;

    /**
     * @return mixed
     */
    public function getClientInfo()
    {
        return $this->clientInfo;
    }

    /**
     * @param mixed $clientInfo
     */
    public function setClientInfo($clientInfo)
    {
        $this->clientInfo = $clientInfo;
    }

    /**
     * FtpStorage constructor.
     *
     * @param FtpAccounts $ftpAccount
     * @param Clients     $client
     */
    public function __construct(FtpAccounts $ftpAccount, Clients $client)
    {
        $this->loadSshKeys($client);
        $this->setClientInfo($ftpAccount->toArray());

        try {
            $this->disk = $this->connect();
        } catch (\Exception $e) {
            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            logger($e->getMessage(), ['info' => $errorInfo]);
        }
    }

    /**
     * @param Clients $client
     */
    public function loadSshKeys(Clients $client)
    {
        $this->sshKeys = $client->sshKeys;
    }

    /**
     *  Connect To Ftp Server by Customer information
     *
     * @return SFTP
     */
    public function connect()
    {
        $username = $this->clientInfo['username'];
        $password = $this->clientInfo['password'];

        $sftp = new SFTP($this->clientInfo['host'], $this->clientInfo['port'], $this->timeout);

        if ($password) {
            $status = $sftp->login($username, $password);
            if ($status) {
                return $sftp;
            }
        }

        $status = $this->login($sftp, $username, $password);
        if ($status) {
            return $sftp;
        }

        //foreach ($this->sshKeys as $sshKey) {
        //
        //    $data = isset($sshKey->data) ? $sshKey->data : '';
        //
        //    if (! $data) {
        //        continue;
        //    }
        //
        //    $status = $this->login($sftp, $username, $password, $sshKey->data);
        //
        //    if ($status) {
        //        return $sftp;
        //    }
        //}

        return $sftp;
    }

    /**
     * @param SFTP   $sftp
     * @param string $username
     * @param string $password
     * @param string $sshKeyData
     *
     * @return mixed
     */
    public function login(&$sftp, $username, $password, $sshKeyData = null)
    {
        if ($sshKeyData === null) {
            $service = new SshKeysService();
            $privateKey = $service->currentPrivateKey();
            $sshKeyData = $privateKey->data;
        }

        $key = new RSA();

        $key->loadKey($sshKeyData);

        if ($password) {
            $key->setPassword($password);
        }

        return $sftp->login($username, $key);
    }

    /**
     * Remove Ftp file
     *
     * @param $file
     */
    public function removeFile($file)
    {
        $this->disk->delete($file);
    }

    /**
     * Move Ftp file to new Path
     *
     * @param $old
     * @param $new
     *
     * @internal param $file
     */
    public function moveFile($old, $new)
    {
        //$path = pathinfo($new, PATHINFO_DIRNAME);
        //dr($path, __CLASS__ . ' :: ' . __FUNCTION__ . ' Line:' . __LINE__);
        //$this->disk->makeDirectory($path);
        //$this->disk->move($old, $new);
    }

    /**
     * List all files in directory
     *
     * @param string $path
     *
     * @return array
     */
    public function readFiles($path)
    {
        if(!$this->disk){
            return [];
        }

        $files = (array) $this->disk->nlist($path);
        foreach ($files as $index => $file) {
            if ($file == '.' || $file == '..') {
                unset($files[ $index ]);
            } else {
                $files[ $index ] = sprintf('%s/%s', $path, $file);
            }
        }

        return $files;
    }

    /**
     * Get content of the file
     *
     * @param $file
     *
     * @return mixed
     */
    public function parseFile($file)
    {
        $content = $this->disk->get($file);

        return $content;
    }

    /**
     * @param $file
     * @param $content
     */
    public function makeFile($file, $content)
    {
        $this->disk->put($file, $content);
    }

    /**
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    public function disk()
    {
        return $this->disk;
    }
}
