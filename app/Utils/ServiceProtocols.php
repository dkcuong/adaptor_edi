<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 06/07/2016
 * Time: 15:48
 */

namespace App\Utils;


use Illuminate\Database\Eloquent\Model;

class ServiceProtocols extends Model
{
    public $table = 'service_protocols';

    public $timestamps = 'false';

}