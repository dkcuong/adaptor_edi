<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataColumnOnTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `x12_inbound_transactions` 
            ADD `data` LONGTEXT AFTER `message`');
        
        DB::statement('ALTER TABLE `x12_outbound_transactions` 
            ADD `data` LONGTEXT AFTER `message`');
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('x12_inbound_transactions', function (Blueprint $table) {
            //
        });
    }
}
