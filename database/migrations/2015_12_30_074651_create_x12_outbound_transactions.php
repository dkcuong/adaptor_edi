<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateX12OutboundTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( '
			CREATE TABLE IF NOT EXISTS `x12_outbound_transactions` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `transaction_code` smallint(3) NOT NULL,
			  `control_number` int(9) NOT NULL,
			  `group_id` bigint(20) NOT NULL,
			  `group_control_number` int(9) NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `UNI_TRANSACTION_NUMBER_IN_GROUP` (`group_id`,`group_control_number`,`control_number`),
			  CONSTRAINT `FK_x12_outbound_transactions_0` FOREIGN KEY (`group_id`) REFERENCES `x12_outbound_groups` (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
