<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataSentTypesAndSentSchedulesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            UPDATE outbound_sent_types 
            SET     `name` = "Create File",
                    `short_name` = "CF",
                    `description` = "Only create file"
            WHERE   `id` = 1'
        );
        
        DB::statement('
            UPDATE outbound_sent_types 
            SET     `name` = "Sent Mail" ,
                    `short_name` = "SM",
                    `description` = "Only sent mail"
            WHERE   `id` = 2'
        );
        
        DB::statement('
            UPDATE outbound_sent_types
            SET     `name` = "Both"
            WHERE   `id` = 3'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outbound_sent_types', function (Blueprint $table) {
            //
        });
    }
}
