<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class X12Interchanges extends Model
{
    /**
     * Table name map with this model
     *
     * @var string
     */
    protected $table = 'x12_interchanges';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client()
    {
        return $this->hasOne(Clients::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inboundGroup()
    {
        return $this->hasMany(X12InboundGroups::class, 'interchange_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ibInter()
    {
        return $this->hasMany(X12InboundInterchange::class, 'interchange_id');
    }
}
