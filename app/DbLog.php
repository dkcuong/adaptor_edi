<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DbLog
 *
 * @package App
 *
 * @property string $request_time
 * @property string $url
 * @property string $content
 * @property string $trace
 * @property array  $fillable
 */
class DbLog extends Model
{

    /**
     * @var array
     */
    public $options_raw = [];

    /**
     * @var array
     */
    protected $fillable = ['request_time', 'url', 'trace', 'content'];

    /**
     * @var string
     */
    protected $table = 'db_logs';

    /**
     * DbLog constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $request_time = config('app.request_time');
        $this->options_raw['request_time'] = $request_time ? $request_time : time();

        parent::__construct($attributes);
    }

    /**
     * @param array $options
     *
     * @return bool
     */
    public function save(array $options = [])
    {
        $options = array_merge($this->options_raw, $options);

        return parent::save($options);
    }
}
