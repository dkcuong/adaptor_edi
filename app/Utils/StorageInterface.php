<?php
/**
 * Created by PhpStorm.
 * User: Simon Sai (Duc)
 * Date: 4/29/2016
 * Time: 11:00 AM
 */

namespace App\Utils;

use App\Clients;
use App\FtpAccounts;

/**
 * Interface StorageInterface
 *
 * @package App\Utils
 */
interface StorageInterface
{

    /**
     * FtpStorage constructor.
     *
     * @param FtpAccounts $ftpAccount
     * @param Clients $client
     */
    public function __construct(FtpAccounts $ftpAccount, Clients $client);

    /**
     * @param mixed $clientInfo
     */
    public function setClientInfo($clientInfo);

    /**
     *  Connect To Ftp Server by Customer information
     *
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    public function connect();

    /**
     * Remove Ftp file
     *
     * @param $file
     */
    public function removeFile($file);

    /**
     * Move Ftp file to new Path
     *
     * @param $old
     * @param $new
     *
     * @internal param $file
     */
    public function moveFile($old, $new);

    /**
     * List all files in directory
     *
     * @param string $path
     *
     * @return array
     */
    public function readFiles($path);

    /**
     * Get content of the file
     *
     * @param $file
     *
     * @return mixed
     */
    public function parseFile($file);

    /**
     * @param $file
     * @param $content
     */
    public function makeFile($file, $content);

    /**
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    public function disk();
}
