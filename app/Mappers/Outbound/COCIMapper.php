<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 22-Jan-16
 * Time: 9:33 AM
 */

namespace App\Mappers\Outbound;

use App\Mappers\MapperBase;

class COCIMapper extends MapperBase
{
    /**
     * Mapper data for Concept One customer
     * Edi945
     *
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map945($items, $conditions)
    {
        return $this->assign($items, $conditions);
    }

    /**
     * @param $orderData
     * @param $conditions
     * @return array
     */
    public function assign($orderData, $conditions)
    {
        $results = [];

        foreach ($orderData['items'] as $index => $item) {
            foreach ($conditions as $target => $from) {
                $results[$index][$from] = isset($orderData[$target]) ? $orderData[$target] : null;
            }
            foreach ($conditions as $target => $from) {
                $results[$index][$from] = isset($item['product_order'][$target])
                    ? $item['product_order'][$target] : $results[$index][$from];
            }
        }
        return $results;
    }
}
