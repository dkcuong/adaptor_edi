<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 28/06/2016
 * Time: 16:09
 */

namespace App\Utils;

use App\Modules\Edi\V1\Models\ClientModel;
use Illuminate\Support\Facades\Storage;

class ReProcessFTP
{
    public $clientInfo;

    /**
     * ReProcessFTP constructor.
     * @param ClientModel $client
     */
    public function __construct(
        ClientModel $client
    ) {
        $this->client = $client;
    }

    /**
     * @return array
     */
    public function getClientInfo()
    {
        return $this->clientInfo;
    }

    /**
     * @param array $clientInfo
     */
    public function setClientInfo($clientInfo)
    {
        $this->clientInfo = $clientInfo;
    }


    /**
     * Process Inbound Action
     */
    public function process()
    {
        $clientsInfo = $this->getClientInfo();

        foreach ($clientsInfo as $index => $client) {
            $this->ReProcessClients($client);
        }
    }
}