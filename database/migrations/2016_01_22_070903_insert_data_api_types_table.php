<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataApiTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::table('api_types')->insert([
            [
                'name' => 'Inbound',
                'short_name' => 'IN',
                'description' => 'Inbound API Type',
            ],
            [
                'name' => 'Outbound',
                'short_name' => 'OUT',
                'description' => 'Outbound API Type',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
