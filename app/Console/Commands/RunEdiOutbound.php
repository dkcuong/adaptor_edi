<?php

namespace App\Console\Commands;

use App\Modules\Edi\V1\Services\EmailHandlingService;
use App\Outbounds;
use Illuminate\Console\Command;
use App\Modules\Edi\V1\Services\ClientService;
use Illuminate\Support\Facades\Log;

class RunEdiOutbound extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edi:outbound';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var ClientService
     */
    protected $service;

    /**
     * @var EmailHandlingService
     */
    protected $errorHandle;

    /**
     * RunEdi constructor.
     * @param ClientService $service
     * @param EmailHandlingService $emailHandlingService
     */
    public function __construct(
        ClientService $service,
        EmailHandlingService $emailHandlingService
    )
    {
        parent::__construct();

        $this->service = $service;
        $this->errorHandle = $emailHandlingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        logger('Outbound Is Running');
        try {
            $this->service->init();
            $this->service->processOutbound();
        } catch (\Exception $e) {

            // Get log information
            $errorInfo = $e->getFile() . ' Line ' . $e->getLine();

            // Log errors for tracking
            Log::error($e->getMessage(), ['info' => $errorInfo]);

            $messageHandle = [
                'Message: ' . $e->getMessage(),
                'File:' . __FILE__,
                'Class:' . __CLASS__,
                'Function:' . __FUNCTION__,
                'Line Number:' . __LINE__
            ];

            logger($messageHandle);
            $this->errorHandle->sendEmail([$messageHandle]);

            //Show error when run CLI
            $this->error($e->getMessage());
            $this->error($errorInfo);
        }
    }
}
