<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorNameAndStatusColumnOnClientWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `client_warehouse` 
            ADD `vendorName` VARCHAR(50) AFTER `warehouse_id`');
        
        DB::statement('ALTER TABLE `client_warehouse` 
            ADD `status` TINYINT(1) AFTER `default`');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_warehouse', function (Blueprint $table) {
            //
        });
    }
}
