<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 24/10/2016
 * Time: 10:56
 */

namespace App\Http\Requests;


use Illuminate\Support\Facades\Facade;

class Responder extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Responder';
    }
}