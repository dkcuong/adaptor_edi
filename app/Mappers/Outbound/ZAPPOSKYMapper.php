<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/4/2019
 * Time: 10:43 AM
 */

namespace App\Mappers\Outbound;

use App\Mappers\MapperBase;

use App\Mappers\SimpleXMLElementExtend;
use DOMDocument;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class ZAPPOSKYMapper extends MapperBase
{
    const AI_CODE = '00';
    const CUSTOMER_CODE = '95011010';

    /**
     * Mapper data for Concept One customer
     * Edi945
     *
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map855($items, $conditions)
    {
        $results[] = $this->parse855($items);

        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parse855($tranData)
    {
        $tran1 = ['transaction_code' => '855'];
        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];

        //BAK*00*AD*ZUTSRQ6543217*20110920
        $tran1[] = [
            'segment' => 'BAK',
            'BAK01' => '00',
            'BAK02' => 'AC',
            'BAK03' => $tranData['cus_po'],
            'BAK04' => !empty($tranData['odr_req_dt'])?date('Ymd', $tranData['odr_req_dt']):'',
        ];

        if (getArr($order_meta, 'product_type', '')) {
            $tran1[] = [
                'segment' => 'REF',
                'REF01' => 'PRT',
                'REF02' => getArr($order_meta, 'product_type'),
            ];
        }

        if (getArr($order_meta, 'vendor_id_ref')) {
            $tran1[] = [
                'segment' => 'REF',
                'REF01' => 'VR',
                'REF02' => $order_meta['vendor_id_ref'],
            ];
        }

        if (!empty($tranData['sac_data'])) {
            $sac_data = $tranData['sac_data'];
            foreach ($sac_data as $sac) {
                /*$tran1[] = [
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC02' => '',
                    'SAC03' => '',
                    'SAC04' => '',
                    'SAC05' => '',
                    'SAC06' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                    'SAC01' => '',
                ];*/
            }
        }
        //        ITD*14*3*****60*****NET~
        $tran1[] = [
            'segment' => 'ITD',
            'ITD01' => getArr($order_meta, 'term_type_code'),
            'ITD02' => getArr($order_meta, 'date_code'),
            'ITD03' => getArr($order_meta, 'discount_percent'),
            'ITD04' => '',
            'ITD05' => '',
            'ITD06' => getArr($order_meta, 'net_due_date'),
            'ITD07' => getArr($order_meta, 'net_days'),
            'ITD08' => '',
            'ITD09' => '',
            'ITD10' => '',
            'ITD11' => '',
            'ITD12' => getArr($order_meta, 'term_sale_desc'),
        ];
        //        DTM*010*20110916~
        $tran1[] = [
            'segment' => 'DTM',
            'REF01' => '010',
            'REF02' => date('Ymd', $tranData['cancel_by_dt']),
        ];
//        ACK*IA~

//        N1*ST*ZAPPOS MERCHANDISING INC.*92*0001~
        $tran1[] = [
            'segment' => 'N1',
            'entity_code' => 'ST',
            'name' => getArr($tranData, 'ship_to_name'),
            'id_qualifier' => getArr($order_meta, 'shipping_code', '92'),
            'id' => getArr($order_meta, 'ship_to_id', "0001"),
        ];

//        N3*376 ZAPPOS.COM BLVD~
        $tran1[] = [
            'segment' => 'N3',
            'address' => $tranData['ship_to_add_1']
        ];

//        N3*FTZ#029, SITE 6~

//        if (!getArr($additional_order, 'shipping_address_2')) {
//            $tran1[] = [
//                'segment' => 'N3',
//                'address' => $tranData['shipping_address_street']
//            ];
//        }

//        N4*SHEPHERDSVILLE*KY*40165*US~
        $tran1[] = [
            'segment' => 'N4',
            'name' => $tranData['ship_to_city'],
            'province_code' => $tranData['ship_to_state'],
            'postal_code' => $tranData['ship_to_zip'],
            'country_code' => $tranData['ship_to_country'],
        ];

//        PO1*1*6*EA*18.90*WE*UP*801234567891*PI*ZZZ-49876543*VA*7607~

        $num = 0;
        foreach ($tranData['items'] as $item_id => $item) {
            $num++;
            $product_order  = $item['product_order'];
            $product_meta   = isset($product_order['product_meta']) ? json_decode($product_order['product_meta'], true) : [];

            //        PO1*1*6*EA*18.90*WE*UP*801234567891*PI*ZZZ-49876543*VA*7607~
            $tran1[] = [
                'segment'          => 'PO1',
                'PO101'     => $num,
                'PO102'     => $product_order['qty'],
                'PO103'     => 'EA',
                'PO104'     => getArr($product_meta, 'price', ''),
                'PO105'     => getArr($product_meta, 'price_unit', 'WE'),
                'PO106'     => getArr($product_meta, 'upc_qualifier', 'UP'),
                'PO107'     => getArr($product_meta, 'upc', ''),
                'PO108'     => getArr($product_meta, 'item_qualifier', 'VA'),
                'PO109'     => $product_order['sku'],
                'PO110'     => getArr($product_meta, 'style_qualifier',''),
                'PO111'     => getArr($product_meta, 'product_load_id', ''),
            ];

            //PID*~
            if(!empty($product_meta['product_item_description'])){
                $tran1[] = [
                    'segment'       => 'PID',
                    'entity_code'   => !empty($product_meta['product_item_description']['entity_code'])?$product_meta['product_item_description']['entity_code']:'F',
                    'process_code'  => !empty($product_meta['product_item_description']['process_code'])?$product_meta['product_item_description']['process_code']:'91',
                    "agency_code"      => !empty($product_meta['product_item_description']['agency_code'])?$product_meta['product_item_description']['agency_code']:'',
                    "product_des_code" => !empty($product_meta['product_item_description']['product_des_code'])?$product_meta['product_item_description']['product_des_code']:'',
                    'description'   => !empty($product_meta['product_item_description']['description'])?$product_meta['product_item_description']['description']:'',
                ];
            }
            $tran1[]=[
                'segment' => 'ACK',
                'ACK01' => 'IA'
            ];
        }

        return $tran1;
    }


    /**
     * @param $items
     * @param $conditions
     * @return array
     */
    public function map856($items, $conditions){
        $results[] = $this->parse856($items);
        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    private function parse856($tranData)
    {
        $tran1 = ['transaction_code' => '856'];
        //BSN*00**20171212**~

        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];
        $ship_from = isset($order_meta['ship_from']) ? $order_meta['ship_from'] : '';

        $tran1[] = [
            'segment'       => 'BSN',
            'entity_code'   => '00',
            'BSN02' => $tranData['cus_po'],
            'BSN03' => date('Ymd', strtotime($tranData['ship_by_dt'])),
            'BSN04' => date('His', strtotime($tranData['ship_by_dt'])),
        ];
        //HL*1**S~
        $tran1[] = [
            'segment'       => 'HL',
            'entity_code'   => '1',
            'parent_number' => '',
            'level_code'    => 'S'
        ];
        //TD1*CTN*40*G*52*LB~
        $tran1[] = [
            'segment'     => 'TD1',
            'entity_code' => 'CTN',
            'landing_quantity' => $tranData['total_piece'],
            'weight_code' => 'G',
            'weight'      => $tranData['total_weight'],
            'measurement_code' => 'LB',

        ];
        //TD5*O*2***~
        $tran1[] = [
            'segment'     => 'TD5',
            'entity_code' => 'O',
            'qualifier_code' => '2',
            'routing'     => $tranData['carrier_code']??'',

        ];
        //REF*2I*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => '2I',
            'REF02'           => $tranData['track_num']??'',
        ];

        //REF*BX*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'BX',
            'REF02'           => $tranData['odr_num'],
        ];

        //DTM
        $tran1[] = [
            'segment'       => 'DTM',
            'entity_code'   => '011',
            'date'          => date('Ymd', strtotime($tranData['ship_by_dt'])),
            'time'          => '',
            'time_code'     => 'UT',
        ];

        //FOB
        $tran1[] = [
            'segment'     => 'FOB',
            'entity_code' => 'CC',
        ];

        //N1*SF*TEXTILES FROM EUROPE~
        if(!empty($ship_from)){
            $tran1[] = [
                'segment'      => 'N1',
                'entity_code'  => 'SF',
                'name'         => $ship_from['name'],
            ];
        }

        //N1*ST*BED BATH & BEYOND #0655*92*0655~
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'ST',
            'name'         => $tranData['ship_to_name'],
            'id_qualifier' => '92',
            'id'           => $tranData['ship_to_zip'],
        ];
        //N2*C/O AMAZON.COM KYDC LLC
        $tran1[] = [
            'segment'     => 'N2',
            'name' => $tranData['ship_to_name'],
        ];
        //N3
        $tran1[] = [
            'segment'      => 'N3',
            'name '        => $tranData['ship_to_add_1'],
        ];
        //N4*SHEPHERDSVILLE*KY*40165*US
        $tran1[] = [
            'segment'           => 'N4',
            'name'              => substr($tranData['ship_to_city'], 0, 30),
            'province_code'     => substr($tranData['ship_to_state'], 0, 2),
            'postal_code'       => substr($tranData['ship_to_zip'], 0, 15),
            'country_code'      => substr($tranData['ship_to_country'], 0, 3),
        ];


        //HL*2*1*T
        $tran1[] = [
            'segment'       => 'HL',
            'entity_code'   => '2',
            'parent_number' => '1',
            'level_code'    => 'O'
        ];

        $tran1[] = [
            'segment'       => 'PRF',
            'order_number'  => $tranData['cus_po'],
            'date'          => date('Ymd',strtotime($tranData['odr_req_dt'])),
        ];

        //REF*VR*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'VR',
            'REF02'           => getArr($order_meta, 'vendor_id_ref'),
        ];

        //REF*PRT*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'PRT',
            'REF02'           => getArr($order_meta, 'product_type'),
        ];

        $HL = 3;
        $LIN = 1;
        foreach ($tranData['items'] as $item_id => $item) {
            $product_order  = $item['product_order'];
            $product_meta   = isset($product_order['product_meta']) ? json_decode($product_order['product_meta'], true) : [];
            foreach ($product_order['cartons'] as $cart_id => $carton) {
                //HL*3*2*P~
                $tran1[] = [
                    'segment'       => 'HL',
                    'entity_code'   => $HL,
                    'parent_number' => 2,
                    'level_code'    => 'P'
                ];
                //MAN*GM*00007357328004124800**CP*832150070055699~
                $tran1[] = [
                    'segment' => 'MAN',
                    'entity_code' => 'GM',
                    'ref11' => $carton['tracking_number']??'',
                    'ref12' => '',
                    'qualifier2' => !isset($order_meta['order_type']) || $order_meta['order_type'] != 1 ? 'CP' : '',
                    'ref21' => $carton['pack_hdr_num']
                ];

                $tran1[] = [
                    'segment'       => 'HL',
                    'entity_code'   => $HL+= 1,
                    'parent_number' => $HL - 1,
                    'level_code'    => 'I'
                ];
                //LIN*~
                $tran1[] = [
                    'segment'       => 'LIN',
                    'entity_code'   => str_pad($LIN,4,0,STR_PAD_LEFT),
                    'product_code'  => $product_meta['upc_qualifier'],
                    'upc'           => $product_meta['upc'],
                ];

                //SN1*~
                $tran1[] = [
                    'segment'           => 'SN1',
                    'entity_code'       => '',
                    'units'             => $carton['piece_qty'],
                    'measure_code'      => 'EA',
                    'units_ship_date'   => '',
                    'quantity_ordered'  => $product_order['picked_qty'],
                ];

                //PID*~
                if(!empty($product_meta['product_item_description'])){
                    $tran1[] = [
                        'segment'       => 'PID',
                        'entity_code'   => !empty($product_meta['product_item_description']['entity_code'])?$product_meta['product_item_description']['entity_code']:'F',
                        'process_code'  => !empty($product_meta['product_item_description']['process_code'])?$product_meta['product_item_description']['process_code']:'91',
                        "agency_code"      => !empty($product_meta['product_item_description']['agency_code'])?$product_meta['product_item_description']['agency_code']:'',
                        "product_des_code" => !empty($product_meta['product_item_description']['product_des_code'])?$product_meta['product_item_description']['product_des_code']:'',
                        'description'   => !empty($product_meta['product_item_description']['description'])?$product_meta['product_item_description']['description']:'',
                    ];
                }

                $HL ++ ;
            }

        }

        $countHL = 0;
        foreach ($tran1 as $index => $value){
            if(isset($value['segment']) && $value['segment'] == 'HL'){
                $countHL +=1;
            }
        }
        $tran1[] = [
            'segment'       => 'CTT',
            'number_item'   => $countHL,
        ] ;

        return $tran1;
    }

    private function parse856_bk($tranData)
    {

        $tran1 = ['transaction_code' => '856'];
        //BSN*00**20171212**~

        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];
        $additional_order = isset($order_meta['additional']) ? $order_meta['additional'] : [];
        $ship_to_order = isset($order_meta['ship_to']) ? $order_meta['ship_to'] : [];

        $tran1[] = [
            'segment'       => 'BSN',
            'entity_code'   => '00',
            'BSN02' => $tranData['customerordernumber'],
            'BSN03' => $tranData['start_ship_date'],
            'BSN04' => '',
        ];
        //HL*1**S~
        $tran1[] = [
            'segment'       => 'HL',
            'entity_code'   => '1',
            'parent_number' => '',
            'level_code'    => 'S'
        ];
        //TD1*CTN*40*G*52*LB~
        $tran1[] = [
            'segment'     => 'TD1',
            'entity_code' => 'CTN',
            'landing_quantity' => $tranData['total_pieces'],
            'weight_code' => 'G',
            'weight'      => $tranData['total_weight'],
            'measurement_code' => 'LB',

        ];
        //TD5*O*2***~
        $tran1[] = [
            'segment'     => 'TD5',
            'entity_code' => 'O',
            'qualifier_code' => '2',
            'routing'     => $tranData['shipType'],

        ];
        //REF*2I*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => '2I',
            'REF02'           => $tranData['trackingNumber'],
        ];

        //REF*BX*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'BX',
            'REF02'           => $tranData['odr_num'],
        ];

        //DTM
        $tran1[] = [
            'segment'       => 'DTM',
            'entity_code'   => '011',
            'date'          => $tranData['start_ship_date'],
            'time'          => '',
            'time_code'     => 'UT',
        ];

        //FOB
        $tran1[] = [
            'segment'     => 'FOB',
            'entity_code' => 'CC',
        ];

        //N1*SF*TEXTILES FROM EUROPE~
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'SF',
            'name'         => $tranData['shipfromname'],
        ];

        //N1*ST*BED BATH & BEYOND #0655*92*0655~
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'ST',
            'name'         => $tranData['shiptoname'],
            'id_qualifier' => '92',
            'id'           => $tranData['shiptozip'],
        ];
        //N2*C/O AMAZON.COM KYDC LLC
        $tran1[] = [
            'segment'     => 'N2',
            'name' => $tranData['shiptoname'],
        ];
        //N3
        $tran1[] = [
            'segment'      => 'N3',
            'name '        => $tranData['shiptoaddress'],
        ];
        //N4*SHEPHERDSVILLE*KY*40165*US
        $tran1[] = [
            'segment'           => 'N4',
            'name'              => substr($tranData['shiptocity'], 0, 30),
            'province_code'     => substr($tranData['shiptostate'], 0, 2),
            'postal_code'       => substr($tranData['shiptozip'], 0, 15),
            'country_code'      => substr($tranData['shiptocountry'], 0, 3),
        ];


        //HL*2*1*T
        $tran1[] = [
            'segment'       => 'HL',
            'entity_code'   => '2',
            'parent_number' => '1',
            'level_code'    => 'O'
        ];

        $tran1[] = [
            'segment'       => 'PRF',
            'order_number'  => $tranData['customerordernumber'],
            'date'          => $tranData['order_date'],
        ];

        //REF*VR*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'VR',
            'REF02'           => getArr($order_meta, 'vendor_id_ref'),
        ];

        //REF*PRT*~
        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'PRT',
            'REF02'           => getArr($order_meta, 'product_type'),
        ];

        $HL = 3;
        $LIN = 1;
        foreach ($tranData['items'] as $item_id => $item) {
            $cartonData = $item['cartons'];
            $firstCarton = array_shift($cartonData);

            foreach ($item['cartons'] as $carton) {
                $product_meta = isset($carton['product_meta'])? json_decode($carton['product_meta'],true):[];
                //HL*3*2*P~
                $tran1[] = [
                    'segment'       => 'HL',
                    'entity_code'   => $HL,
                    'parent_number' => $HL - 1,
                    'level_code'    => 'P'
                ];
                //MAN*GM*00007357328004124800**CP*832150070055699~
                $tran1[] = [
                    'segment' => 'MAN',
                    'entity_code' => 'GM',
                    'ref11' => self::AI_CODE . generateUCC128('00' . self::CUSTOMER_CODE, $carton['cartonID']),
                    'ref12' => '',
                    'qualifier2' => $tranData['order_type'] != 1 ? 'CP' : '',
                    'ref21' => $tranData['order_type'] != 1 ? $firstCarton['tracking_number'] : '',
                ];

                $tran1[] = [
                    'segment'       => 'HL',
                    'entity_code'   => $HL + 1,
                    'parent_number' => $HL,
                    'level_code'    => 'I'
                ];
                //LIN*~
                $tran1[] = [
                    'segment'       => 'LIN',
                    'entity_code'   => str_pad($LIN,4,0,STR_PAD_LEFT),
                    'product_code'  => $product_meta['upc_qualifier'],
                    'upc'           => $carton['upc'],
                ];

                //SN1*~
                $tran1[] = [
                    'segment'           => 'SN1',
                    'entity_code'       => '',
                    'units'             => $item['piecesCount'],
                    'measure_code'      => $product_meta['uom_code'],
                    'units_ship_date'   => '',
                    'quantity_ordered'  => $item['shippedQuantity'],
                ];
                //PID*~
                $tran1[] = [
                    'segment'     => 'PID',
                    'entity_code' => 'F',
                    'code_name'   => '91',
                    'description' => '',
                ];
                $HL ++ ;
            }

        }

        $countHL = 0;
        foreach ($tran1 as $index => $value){
            if(isset($value['segment']) && $value['segment'] == 'HL'){
                $countHL +=1;
            }
        }
        $tran1[] = [
            'segment'       => 'CTT',
            'number_item'   => $countHL,
        ] ;

        return $tran1;
    }


    protected function map810($items, $conditions){
        $results[] = $this->parse810($items);
        //dd($results);
        return $results;
    }

    private function parse810($items)
    {
        $data = $items;
        $tran1 = ['transaction_code' => '810'];
        if(empty($data)){
            return $tran1;
        }
        $order = array_shift($data['orders']);
        
        //BIG*20110706*0714449999**ABCDEF5432107

        $tran1[] = [
            'segment'           => 'BIG',
            'inv_date'          => date('Ymd', strtotime($data['created_at'])),
            'inv_num'           => $data['inv_num'],
            'inv_date_assigned' => date('Ymd', strtotime($data['created_at'])),
            'po_num'            => $order['po'],
        ];

        //REF*VR*4321

        $tran1[] = [
            'segment'         => 'REF',
            'entity_code'     => 'VR',
            'REF02'           => $order['order_meta']['vendor_id_ref'],
        ];
        //ITD*******60

        $tran1[] = [
            'segment' => 'ITD',
            'ITD01' => getArr($order['order_meta'], 'term_type_code'),
            'ITD02' => getArr($order['order_meta'], 'date_code'),
            'ITD03' => getArr($order['order_meta'], 'discount_percent'),
            'ITD04' => '',
            'ITD05' => '',
            'ITD06' => getArr($order['order_meta'], 'net_due_date'),
            'ITD07' => getArr($order['order_meta'], 'net_days'),
            'ITD08' => '',
            'ITD09' => '',
            'ITD10' => '',
            'ITD11' => '',
            'ITD12' => getArr($order['order_meta'], 'term_sale_desc'),
        ];


        //IT1*000010*6*EA*34.2**UP*765432123456
        //IT1*000010*7*EA*57.7**EN*7766554433217

        foreach ($order['items'] as $item){
            $tran1[] = [
                'segment'                   => 'IT1',
                'purchase_order_line_item'  => getArr($item['product_meta'], 'item_qualifier'),
                'number_of_unit'            => getArr($item, 'qty'),
                'unit_code'                 => 'EA',
                'unit_price'                => $item['product_meta']['price'],
                'undefined'                 => '',
                'product_qualify_code'      => $item['product_meta']['upc_qualifier'],
                'product_id'                => $item['product_meta']['upc'],
            ];
        }

        //TDS*44425

        $tran1[] = [
            'segment'           => 'TDS',
            'monetary_amount'   => $data['total'],
        ];

        //PLEASE NOTE THAT AT LEAST 2 DIGITS ARE REQUIRED
        //Example a value of $0.09 should be TDS*09

        //SAC*A*C000***2.78**********Defective Allowance

        //foreach ($items['sac'] as $sac){
        foreach ($data['sacs'] as $sac){ //test
            $tran1[] = [
                'segment'                   => 'SAC',
                'entity_code'               => 'A',
                'service'                   => 'C000',
                'qualifier_code'            => '',
                'agency_service'            => '',
                'amount'                    => $sac['sac_amount'],
                'charge_percent_qualifier'  => '',
                'format'                    => '',
                'rate'                      => '',
                'measurement_code'          => '',
                'quantity'                  => '',
                'quantity2'                 => '',
                'handling_code'             => '',
                'ref_identification'        => '',
                'option_number'             => '',
                'description'               => $sac['sac_description'],
                'language_code'             => '',
            ];
        }

        //CTT*37
        $tran1[] = [
            'segment'                   => 'CTT',
            'total_item_number'         => $data['item_ttl'],
        ];

        return $tran1;
    }


    /**
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map997($items, $conditions)
    {
        $results[] = $this->parse997($items);

        return $results;
    }

    private function parse997($items)
    {
        if(isset($items['isSuccess']))
            unset($items['isSuccess']);
        $tran1 = $items;
        $tran1['transaction_code'] = '997';
        return $tran1;
    }

}
