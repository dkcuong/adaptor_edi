<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InboundMapping extends Model
{
    /**
 * @var string
 */
    protected $table = 'inbound_mapping';
}
