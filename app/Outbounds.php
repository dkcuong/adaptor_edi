<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 17/02/2016
 * Time: 17:20
 */

namespace App;

use App\Modules\Edi\V1\Models\OutboundModel;
use App\Modules\Edi\V1\Services\InboundService;
use App\Modules\Edi\V1\Services\OutboundService;
use App\Parsers\ParserBase;
use App\Parsers\X12Parser\X12Parser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Outbounds extends Model
{
    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ftpAccount()
    {
        return $this->belongsTo(FtpAccounts::class, 'ftp_account_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function outboundMapping()
    {
        return $this->hasOne(OutboundMapping::class, 'outbound_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function outboundGroup()
    {
        return $this->belongsTo(X12OutboundGroups::class, 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function api()
    {
        return $this->belongsTo(Api::class, 'api_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function dataFormat()
    {
        return $this->hasOne(DataFormat::class, 'id', 'data_format_id');
    }

}
