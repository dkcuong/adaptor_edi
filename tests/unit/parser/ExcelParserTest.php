<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExcelParserTest extends TestCase
{
    protected $data = [
        [
            'name' => 'Vuong',
            'email' => 'vuong.nguyen@seldatinc.com',
        ],
        [
            'name' => 'seldat',
            'email' => 'seldat@seldatinc.com',
        ],
    ];

    public function test_parser_xlsx_file_content()
    {
        \Maatwebsite\Excel\Facades\Excel::create('sample', function($excel) {
            $excel->sheet('sample', function($sheet) {
                $sheet->fromArray($this->data);
            });
        })->store('xlsx',storage_path('app'));

        $content = \Illuminate\Support\Facades\Storage::get('sample.xlsx');

        $testObject = new \App\Parsers\ExcelParser\ExcelParser([
            'content' => $content,
        ]);

        $result = $testObject->inbound()->get();

        $this->assertCount(2, $result);
        $this->assertEquals($result[0]['name'], 'Vuong');
        $this->assertEquals($result[1]['email'], 'seldat@seldatinc.com');
    }

    public function test_parser_csv_file_content()
    {
        \Maatwebsite\Excel\Facades\Excel::create('sample', function($excel) {
            $excel->sheet('sample', function($sheet) {
                $sheet->fromArray($this->data);
            });
        })->store('csv',storage_path('app'));

        $content = \Illuminate\Support\Facades\Storage::get('sample.csv');

        $testObject = new \App\Parsers\ExcelParser\ExcelParser([
            'content' => $content,
        ]);

        $result = $testObject->inbound()->get();

        $this->assertCount(2, $result);
        $this->assertEquals($result[0]['name'], 'Vuong');
        $this->assertEquals($result[1]['email'], 'seldat@seldatinc.com');
    }

    public function test_parser_xls_file_content()
    {
        \Maatwebsite\Excel\Facades\Excel::create('sample', function($excel) {
            $excel->sheet('sample', function($sheet) {
                $sheet->fromArray($this->data);
            });
        })->store('xls',storage_path('app'));

        $content = \Illuminate\Support\Facades\Storage::get('sample.xls');

        $testObject = new \App\Parsers\ExcelParser\ExcelParser([
            'content' => $content,
        ]);

        $result = $testObject->inbound()->get();

        $this->assertCount(2, $result);
        $this->assertEquals($result[0]['name'], 'Vuong');
        $this->assertEquals($result[1]['email'], 'seldat@seldatinc.com');
    }
}
