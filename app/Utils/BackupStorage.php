<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 17-Mar-16
 * Time: 10:46 AM
 */

namespace App\Utils;


use Illuminate\Support\Facades\Storage;

class BackupStorage
{
    /**
     * @var
     */
    public $ftpInter;

    /**
     * @var
     */
    public $ftpSecond;

    /**
     * @var
     */
    public $ftps;

    /**
     *
     */
    public function process()
    {
        //remove backup over 7 days
        $this->removeOllDirectory();
    }

    /**
     *
     */
    public function removeOllDirectory()
    {
        $currentDate = date('Y-m-d');
        $dirs = Storage::disk('local')->allDirectories('backup');
        foreach ($dirs as $dir) {
            $dirOb = explode('/', $dir);
            if (strtotime($currentDate) > strtotime($dirOb[1]) + 864000) {
                Storage::disk('local')->deleteDirectory($dir);
            }
        }
    }

    /**
     * @param $otherStorage
     * @return bool
     */
    public function backupAction($otherStorage)
    {
        $files = Storage::disk('local')->allFiles('backup');
        if (! $files) {
            return false;
        }

        foreach ($otherStorage as $ftpAccount) {
            $this->ftpInter = new FtpStorage($ftpAccount);
            $files = $this->ftpInter->readFiles('/edi');
            foreach ($files as $file) {
                $contents = Storage::get($file);
                $this->ftpInter->makeFile(date('Y-m-d-H-i-s'). '/' . $file, $contents);
            }
        }
    }
}
