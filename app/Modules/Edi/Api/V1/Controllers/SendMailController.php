<?php
/**
 * Created by PhpStorm.
 * User: TuLe
 * Date: 20/07/2016
 * Time: 11:22
 */
namespace App\Modules\Edi\Api\V1\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Modules\Edi\V1\Services\EmailHandlingService;
use App\Modules\Edi\V1\Services\SettingService;

class SendMailController extends Controller
{
    public function sendMail(Request $request)
    {
        $response = new Response();
        try {
            $listMail = $request->get('listMail');
            str_replace(' ', '', $listMail);
            $listMail = explode(',', $listMail);
            $setting = new SettingService();
            $setting->emailSetting();
            $emailService = new EmailHandlingService();
            $emailService->sendMailAction($listMail,
                [
                    'message' => 'Testing Email SMTP Notification successfully',
                    'status_code' => 200,
                    'data' => ''
                ],
                'Testing Email SMTP Notification',
                '',
                null,
                null,
                'inbound', 
                'reminder');
            $response->setStatusCode(200);
        } catch (\Exception $e) {
            logger($e->getMessage() . '-----' . $e->getLine() . '-----' . $e->getFile(),
                [__CLASS__, __FUNCTION__, __LINE__]);
            $response->setStatusCode(500);
        }
        return $response;
    }
}