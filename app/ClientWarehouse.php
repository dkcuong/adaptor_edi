<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientWarehouse extends Model
{

    /**
     * @var string
     */
    protected $table = 'client_warehouse';

    /**
     * @param $query
     * @return mixed
     */
    public function scopeDefault($query)
    {
        return $query->where('default', 1);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Clients::class, 'client_id');
    }

    public function warehouseTypes()
    {
        return $this->hasOne(WarehouseTypes::class, 'whs_type_id', 'whs_type_id');
    }
}
