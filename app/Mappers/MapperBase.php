<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 22-Jan-16
 * Time: 9:25 AM
 */

namespace App\Mappers;

class MapperBase implements MapperInterface
{
    /**
     * Array to be mapped
     *
     * @var array
     */
    protected $data = [];

    /**
     * Array condition for mapping
     *
     * @var array
     */
    protected $mapCondition = [];

    /**
     * Array data after mapping
     *
     * @var array
     */
    protected $result = [];

    /**
     * Array input config
     *
     * @var array
     */
    protected $config = [];

    /**
     * MapperBase constructor.
     *
     * @param $config
     */

    const SHIPPED_ORDER = 'SHCO';

    const CANCEL_ORDER = 'CNCL';

    /**
     * MapperBase constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->setConfig($config);

        $this->process();
    }

    /**
     * @param $key
     * @param string $default
     * @return array
     */
    public function getConfig($key, $default = '')
    {
        return isset($this->config[$key]) ? $this->config[$key] : $default;
    }

    /**
     * @param array $config
     * @return array
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param array $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * Get array to be mapped
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->getConfig('data');
    }

    /**
     * Get map array - Key to Key
     *
     * @return array
     */
    public function getMapCondition()
    {
        $mapping = $this->getConfig('mapping');

        $condition = json_decode($mapping, true);

        return $condition ? array_filter($condition,function ($var) {
            return trim($var) !== '';
        }) : [];
    }

    /**
     * @return array
     */
    public function getEdi()
    {
        return $this->getConfig('edi');
    }

    /**
     * Get return data after mapping
     *
     * @return array
     */
    public function get()
    {
        return $this->getResult();
    }

    /**
     * @return $this
     */
    protected function process()
    {
        $result = array_map([$this, 'map'], $this->getData());

        $this->setResult($result);

        return $this;
    }

    /**
     * @param $items
     * @return array
     */
    protected function map($items)
    {
        $conditions = $this->getMapCondition();
        return $this->callEdiMethod($items, $conditions);
    }

    /**
     * @param $item
     * @param $conditions
     * @return array
     */
    protected function callEdiMethod($item, $conditions)
    {
        $method = 'map' . $this->getEdi()->name;

        if (method_exists($this, $method)) {
            return $this->$method($item, $conditions);
        }

        return $this->assign($item, $conditions);
    }

    /**
     * @param $items
     * @return array
     */
    protected function mapMany($items)
    {
        $conditions = $this->getMapCondition();

        return array_map(function ($item) use ($conditions) {
            return $this->callEdiMethod($item, $conditions);
        }, $items);
    }

    /**
     * @param $item
     * @param $conditions
     * @return array
     */
    protected function assign($item, $conditions)
    {
        $result = [];

        foreach ($conditions as $target => $from) {
            $result[$target] = isset($item[$from]) ? $item[$from] : '';
        }

        return $result;
    }

    /**
     * @param $item
     * @param $conditions
     * @return array
     */
    protected function deAssign($item, $conditions)
    {
        $result = [];

        foreach ($conditions as $target => $from) {
            $result[$from] = isset($item[$target]) ? (string) $item[$target] : '';
        }

        return $result;
    }

    /**
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function reAssign($items, $conditions)
    {
        $results = [];

        foreach ($items['items'] as $index => $item) {
            foreach ($conditions as $target => $from) {
                $results[$index][$from] = isset($orderData[$target]) ? $orderData[$target] : '';
            }
            foreach ($conditions as $target => $from) {
                $results[$index][$from] = isset($item['product_order'][$target])
                    ? $item['product_order'][$target] : $results[$index][$from];
            }
        }
        return $results;
    }

    /**
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map997($items, $conditions)
    {
        $results[] = $items;
        return $results;
    }

    /**
     * @param $item
     * @param $conditions
     * @return array
     */
    protected function map945($item, $conditions)
    {
        $mappedData = $this->assignOutbound($item, $conditions);
        if($this->countdim($mappedData) < 2){
            $results[] = $mappedData;
        }else{
            $results = $mappedData;
        }
        return $results;
    }

    /**
     * @param $item
     * @param $conditions
     * @return array
     */
    protected function map858($item, $conditions)
    {
        return $item;
    }

    /**
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map861($items, $conditions)
    {
        return $this->deAssignOutbound($items, $conditions);
    }

    /**
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map944($items, $conditions)
    {
        return $this->deAssignOutbound($items, $conditions);
    }

    /**
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map846($items, $conditions)
    {
        return $this->deAssignOutbound($items, $conditions);
    }

    /**
     * @param $orderData
     * @param $conditions
     * @return array
     */

    public function assignOutbound($orderData, $conditions)
    {
        $results = [];
        foreach ($orderData['items'] as $index => $item) {
            foreach ($conditions as $target => $from) {
                $results[$index][$from] = isset($orderData[$target]) ? (string) $orderData[$target] : null;
            }
            foreach ($conditions as $target => $from) {
                $results[$index][$from] = isset($item['product_order'][$target])
                    ? (string) $item['product_order'][$target] : (string) $results[$index][$from];
            }
            if (isset($item['product_order']['product_meta'])) {
                $product_meta = json_decode($item['product_order']['product_meta'], true);
                if(!empty($product_meta)) {
                    foreach ($conditions as $target => $from) {
                        $results[$index][$from] = (isset($product_meta[$target]) && empty($results[$index][$from])) ? (string)$product_meta[$target] : (string)$results[$index][$from];
                    }
                }
            }
        }
        return $results;
    }

    public function deAssignOutbound($item, $conditions)
    {
        $result = [];
        if (isset($item['container_name']) && is_array(explode(';', $item['container_name']))) {
            $containerSub = explode(';', $item['container_name']);
            $lastElement = $containerSub[count($containerSub)-1];
            if ($lastElement && is_numeric($lastElement)) {
                array_pop($containerSub);
            }
            $item['container_name'] = implode(';', $containerSub);
        }
        foreach ($conditions as $target => $from) {
            $result[$from] = isset($item[$target]) ? (string) $item[$target] : null;
        }

        return $result;
    }

    /**
     * Group by $Key - Group in field name by $groupInField
     * Group with all fields in $fieldInLoop
     *
     * @param $data
     * @param $key
     * @param $groupInField
     * @param array $fieldInLoop
     * @return array
     */
    protected function groupBy($data, $key, $groupInField, $fieldInLoop = [])
    {
        $result = $storage = [];
        $autoIndex = 0;

        foreach ($data as $index => $item) {
            $storage[$item[$key]][] = $index;
        }

        foreach ($storage as $index => $items) {
            $autoIndex++;
            $result[$autoIndex] = $data[$items[0]];

            $result[$autoIndex][$groupInField] = array_map(function ($item) use ($data, $groupInField, $fieldInLoop) {
                foreach ($fieldInLoop as $fieldKey => $field) {
                    if (isset($data[$item][$field])) {

                        $child[$field] = $data[$item][$field];
                    }
                }
                return $child;
            }, $items);
        }

        return $result;
    }

    public function countdim($array)
    {
        if (is_array(reset($array))) {
            $return = $this->countdim(reset($array)) + 1;
        } else {
            $return = 1;
        }

        return $return;
    }
}
