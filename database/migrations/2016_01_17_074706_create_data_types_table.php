<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDataTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description');
        });

        DB::table('data_types')->insert([
            ['name' => 'x12'],
            ['name' => 'txt'],
            ['name' => 'excel'],
            ['name' => 'xml'],
            ['name' => 'json']
        ]);

        Schema::table('data_formats', function (Blueprint $table) {
            $table->bigInteger('data_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
