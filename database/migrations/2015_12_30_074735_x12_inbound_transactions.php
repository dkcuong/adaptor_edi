<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class X12InboundTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( '
			CREATE TABLE IF NOT EXISTS `x12_inbound_transactions` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `transaction_code` smallint(3) DEFAULT NULL,
			  `control_number` int(9) DEFAULT NULL,
			  `group_control_number` int(9) NOT NULL,
			  `status` bit(1) DEFAULT b\'0\',
			  `group_id` bigint(20) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `FK_x12_inbound_transactions_0` (`group_id`),
			  CONSTRAINT `FK_x12_inbound_transactions_0` FOREIGN KEY (`group_id`) REFERENCES `x12_inbound_groups` (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
