<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 06-May-16
 * Time: 3:21 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        $this->attributes['created_at'] = date('Y-m-d H:i:s');

        return parent::save($options);
    }
}
