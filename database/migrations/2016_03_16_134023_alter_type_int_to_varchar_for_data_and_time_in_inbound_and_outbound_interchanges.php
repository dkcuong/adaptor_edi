<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTypeIntToVarcharForDataAndTimeInInboundAndOutboundInterchanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `x12_inbound_interchanges`
            CHANGE `date` `date` VARCHAR(6) NOT NULL, CHANGE `time` `time` VARCHAR(4) NOT NULL;');
        DB::statement('ALTER TABLE `x12_outbound_interchanges`
            CHANGE `date` `date` VARCHAR(6) NOT NULL, CHANGE `time` `time` VARCHAR(4) NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
