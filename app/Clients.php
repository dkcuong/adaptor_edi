<?php

namespace App;

use App\Utils\ServiceProtocols;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Clients
 *
 * @package App
 *
 * @property HasMany $sshKeys
 */
class Clients extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['id', 'name', 'short_name', 'description', 'status', 'type_id'];

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @return HasMany
     */
    public function contact()
    {
        return $this->hasMany(Contacts::class, 'client_id')->active();
    }

    /**
     * @return HasMany
     */
    public function clientWarehouse()
    {
        return $this->hasMany(ClientWarehouse::class, 'client_id');
    }

    /**
     * @return HasMany
     */
    public function clientDefaultWarehouse()
    {
        return $this->hasOne(ClientWarehouse::class, 'client_id');
    }

    /**
     * @return HasOne
     */
    public function x12InterChange()
    {
        return $this->hasOne(X12Interchanges::class, 'client_id');
    }

    /**
     * @return HasMany|array
     */
    public function sshKeys()
    {
        return $this->hasMany(SshKeys::class, 'client_id', 'id');
    }

    /**
     * @return HasMany|array
     */
    public function apiEndPoint()
    {
        return $this->hasMany(ApiEndPoint::class, 'client_id', 'id');
    }

}
