<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 17/06/2016
 * Time: 10:54
 */

namespace App\Utils;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class As2ApiClient extends Client
{
    /**
     * WMS API Client Constructor
     */
    public function __construct()
    {
        $uri = Config::get('api.as2_outbound_api');
        $token = Config::get('api.as2_outbound_token');

        return parent::__construct([
            'base_uri' => $uri,
            'headers' => [
                'Content-Type' => 'application/json',
                /*'token'        => $token*/
            ],
            'exceptions' => false
        ]);
    }
}