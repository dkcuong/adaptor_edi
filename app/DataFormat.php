<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 3/24/2017
 * Time: 12:17
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class DataFormat extends Model
{
    public $table = 'data_formats';
}