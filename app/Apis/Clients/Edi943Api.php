<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 24-Jan-16
 * Time: 9:37 PM
 */

namespace App\Apis\Clients;

use App\Apis\ApiBase;
use App\Apis\ApiInterface;

class Edi943Api extends ApiBase implements ApiInterface
{
    /**
     * @var string
     */
    protected $api = 'items/v1/943';

    /**
     * Call Api items/v1/943
     * With data from client
     *
     * @return array
     */
    public function callApi()
    {
        $result = [];
        $data = $this->getConfig('data');
        $isX12 = $this->getConfig('isX12');
        $reProcess = $this->getConfig('re_process');
        if (! $isX12 && ! $reProcess) {
            $data = $this->groupBy($data, 'container', 'items', [
                'purchase_order', 'size', 'color', 'batch', 'sku', 'description', 'quantity', 'pack', 'lot',
            ]);

            $data = $dataInputs = $this->groupBy($data, 'reference', 'containers', [
                'container', 'items'
            ]);
        }
        $dataInputs = $data;
        foreach ($data as $index => $ref) {
            $this->formatData($ref);
            $data = $this->post($ref);
            $result[$index] = json_decode($data->getBody(), true);
        }

        $results = [
            'data' => $dataInputs,
            'output' => $result,
        ];

        logger(json_encode($results), ['info' => '943 Response']);

        return $results;
    }

    private function formatData(&$ref)
    {
        $arrs = ['container','purchase_order', 'size', 'color', 'batch', 'sku', 'description', 'quantity','pack_size', 'cartons', 'cus_upc','lot', 'pack', 'items'];
        foreach ($arrs as $arr) {
            if (isset($ref[$arr])) {
                unset($ref[$arr]);
            }
        }
    }
}
