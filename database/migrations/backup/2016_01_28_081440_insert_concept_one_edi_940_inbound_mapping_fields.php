<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertConceptOneEdi940InboundMappingFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $mapping = [
            'client_order_id' => 'Order Number',
            'customer_order_id' => 'PO Number',
            'user_id' => '',
            'vendor_id' => '',
            'shipping_address_street' => 'Ship To Address 1',
            'shipping_city' => 'Ship To City',
            'shipping_state' => 'Ship To State',
            'shipping_postal_code' => 'Ship To Zip',
            'shipping_country' => 'Ship To Country',
            'additional_shipper_information' => '',
            'special_instruction' => '',
            'order_date' => 'Order Date',
            'carrier' => 'Carrier',
            'shipping_first_name' => 'Ship To Name',
            'shipping_last_name' => 'Ship To Name',
            'sku' => 'Warehouse Item Code',
            'color' => 'Color',
            'size' => 'Size',
            'quantity' => 'Qty Ordered',
            'description' => 'Order Line User 3',
        ];

        DB::table('inbound_mapping')->insert([
            'fields' => json_encode($mapping),
            'inbound_id' => 3,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
