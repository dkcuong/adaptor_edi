<?php

namespace App\Apis\Clients;


use App\Apis\ApiBase;
use App\Apis\ApiInterface;

class Edi210Api extends ApiBase implements ApiInterface
{
    /**
     * @var string
     */
    protected $api = 'items/v1/210';

    /**
     * Call Api items/v1/210
     * With data from client
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function callApi()
    {
        $data = $this->getConfig('data');

        $response = $this->get($data);

        return $response;
    }
}
