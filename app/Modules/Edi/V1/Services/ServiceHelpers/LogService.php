<?php
namespace App\Modules\Edi\V1\Services\ServiceHelpers;

use App\X12InboundTransactionLogs;
use App\X12OutboundTransactionLogs;

class LogService
{

    private $messages = [];

    public function getProcessKey()
    {
        return isset($this->process_key) ? $this->process_key : null;
    }

    /**
     * @param $processKey
     */
    public function setProcessKey($processKey)
    {
        $this->process_key = $processKey;
    }

    /**
     * @param $processKey
     */
    public function detectProcessKey($fileName)
    {
        $re = "/#pKey#([A-Za-z0-9]*)#\\./";
        preg_match($re, $fileName, $matches);
        if (!empty($matches[1])) {
            $this->process_key = $matches[1];
            return $this->process_key;
        }
        return null;
    }

    /**
     * @param $rawData
     */
    public function setRawData($rawData)
    {
        $this->raw_data = $rawData;
    }

    /**
     * @param $content
     * @param $time
     */
    public function addLogMessage($content, $time = null)
    {
        $time = empty($time) ? time() : $time;
        $this->messages[] = array(
            'content' => $content,
            'time' => date('Y-m-d H:i:s', $time),
        );
    }

    /**
     * @param $array content
     * @param $time
     */
    public function addLogMessages($contents, $time = null)
    {
        foreach ($contents as $content) {
            $time = empty($time) ? time() : $time;
            $this->messages[] = array(
                'content' => $content,
                'time' => date('Y-m-d H:i:s', $time),
            );
        }
    }

    public function reverseLogMessage()
    {
        $this->messages = array_reverse($this->messages);
    }

    /**
     * @param $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * @param $payload
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param $rawData
     */
    public function setTranId($tranId)
    {
        $this->tran_id = $tranId;
    }

    private function parseMessage()
    {
        $log_message = '';
        foreach ($this->messages as $message) {
            $log_message = $log_message . '[' . $message['time'] . ']: ' .
                $message['content'] . "\r\n";
        }
        unset($this->messages);
        return $log_message;
    }

    public function messageLogProcess($type, $log_message = '')
    {
        $this->log_message = $this->parseMessage();
        if ($type == 'inbound') {
            $this->log_message = $log_message . $this->log_message;
        } else {
            $this->log_message = $this->log_message . $log_message;
        }
    }

    private function clearOldLog($type)
    {
        if (!empty($this->tran_id)) {
            if ($type == 'inbound') {
                X12InboundTransactionLogs::where('tran_id', '=', $this->tran_id)
                    ->where('process_key', '!=', $this->process_key)
                    ->delete();
            } else {
                X12OutboundTransactionLogs::where('tran_id', '=', $this->tran_id)
                    ->where('process_key', '!=', $this->process_key)
                    ->delete();
            }
        }
    }

    private function getLogger($type, $new = false, $mdnFlag = false)
    {
        if ($new) {
            if ($type == 'inbound') {
                return new X12InboundTransactionLogs();
            } else {
                return new X12OutboundTransactionLogs();
            }
        }
        if ($mdnFlag) {
            if (empty($this->process_key)) {
                return false;
            }
            if ($type == 'inbound') {
                return X12InboundTransactionLogs::where('process_key', '=', $this->process_key)
                    ->where('type', '=', 'mdn_message')
                    ->first();
            } else {
                return X12OutboundTransactionLogs::where('process_key', '=', $this->process_key)
                    ->where('type', '=', 'mdn_message')
                    ->first();
            }
        }
        if ($this->process_key) {
            if ($type == 'inbound') {
                $logger = X12InboundTransactionLogs::where('process_key', '=', $this->process_key)
                    ->where('type', '!=', 'mdn_message')
                    ->first();
                $logger = $logger ? $logger : new X12InboundTransactionLogs();
                return $logger;
            } else {
                $logger = X12OutboundTransactionLogs::where('process_key', '=', $this->process_key)
                    ->where('type', '!=', 'mdn_message')
                    ->first();
                $logger = $logger ? $logger : new X12OutboundTransactionLogs();
                return $logger;
            }
        } else {
            if ($type == 'inbound') {
                return new X12InboundTransactionLogs();
            } else {
                return new X12OutboundTransactionLogs();
            }
        }
    }

    private function updateTranIDMDN($type)
    {
        $logger = $this->getLogger($type, false, true);
        if ($logger) {
            $logger->tran_id = isset($this->tran_id) ? $this->tran_id : null;
            $logger->save();
        }
    }

    /**
     * @param $type
     */
    public function writeLog($type, $new = false)
    {
        $this->clearOldLog($type);
        $logger = $this->getLogger($type, $new);
        $this->messageLogProcess($type, $logger->log_message);
        $data = get_object_vars($this);
        foreach ($data as $name => $value) {
            $logger->$name = $value;
        }
        $logger->save();
        if (!$new) {
            $this->updateTranIDMDN($type);
        }
    }

    public function updateLog($type)
    {
        if (isset($this->tran_id)) {
            $logger = null;
            if ($type == 'inbound') {
                $logger = X12InboundTransactionLogs::where('tran_id', '=', $this->tran_id)
                    ->where('type', '!=', 'mdn_message')
                    ->first();
            } else {
                $logger = X12OutboundTransactionLogs::where('tran_id', '=', $this->tran_id)
                    ->where('type', '!=', 'mdn_message')
                    ->first();
            }
            if ($logger) {
                $log_message = $this->parseMessage();
                $data = get_object_vars($this);
                foreach ($data as $name => $value) {
                    $logger->$name = $value;
                }
                $logger->log_message .= $log_message;
                $logger->save();
            }
        }
    }

    public function checkExist($type)
    {
        if (isset($this->tran_id)) {
            $logger = null;
            if ($type == 'inbound') {
                $logger = X12InboundTransactionLogs::where('tran_id', '=', $this->tran_id)
                    ->where('type', '!=', 'mdn_message')
                    ->first();
            } else {
                $logger = X12OutboundTransactionLogs::where('tran_id', '=', $this->tran_id)
                    ->where('type', '!=', 'mdn_message')
                    ->first();
            }
            return $logger;
        }
    }

    public function updateAs2Log($processKey, $type = 'outbound')
    {
        if ($type == 'outbound') {
            $logger = X12OutboundTransactionLogs::where('process_key', '=', $processKey)
                ->whereNull('tran_id')
                ->update(['process_key' => $this->process_key]);
        } else {
            $logger = X12InboundTransactionLogs::where('process_key', '=', $processKey)
                ->whereNull('tran_id')
                ->update(['process_key' => $this->process_key]);
        }
    }
}
