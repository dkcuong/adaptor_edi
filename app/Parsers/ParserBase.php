<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 25-Jan-16
 * Time: 4:40 PM
 */

namespace App\Parsers;


class ParserBase
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * The return result after parse
     *
     * @var array
     */
    protected $result = [];

    /**
     * The return result after parse
     *
     * @var array
     */
    protected $error = [];

    /**
     * @var
     */
    public $data;

    /**
     * @var
     */
    public $originalParser;

    /**
     * @var
     */
    protected $filterString;

    /**
     * @var
     */
    public $attachment;

    /**
     * ParserBase constructor.
     * @param $config
     */
    final public function __construct($config)
    {
        $this->setConfig($config);
    }

    /**
     * @return $this
     */
    public function inbound()
    {
        $this->process();

        return $this;
    }

    /**
     * processOutbound function
     */
    public function outbound()
    {
        $this->processOutbound();
    }

    /**
     * @param $key
     * @param string $default
     * @return array
     */
    public function getConfig($key, $default = '')
    {
        return isset($this->config[$key]) ? $this->config[$key] : $default;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @param $config
     */
    public function addConfig($config)
    {
        $this->config += $config;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->getConfig('content');
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return array
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param array $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @param $error
     */
    public function setResultError($error)
    {
        $this->error = $error;
    }

    /**
     * @param $key
     * @param string $default
     * @return string
     */
    public function getData($key, $default = '')
    {
        $edi = $this->getConfig('edi');

        return isset($edi->$key) && ! empty($edi->$key) ? $edi->$key : $default;
    }

    /**
     * @return array
     */
    public function getClient()
    {
        return $this->getConfig('client');
    }

    /**
     * Get return data after parsed
     *
     * @return array
     */
    public function getOriginalParser()
    {
        return $this->originalParser;
    }

    /**
     * @return string
     */
    public function getElementSeparator()
    {
        return $this->getData('element_separator', $this->elementSeparator);
    }

    /**
     * @return string
     */
    public function getComponentSeparator()
    {
        return $this->getData('component_separator', $this->componentSeparator);
    }

    /**
     * @return string
     */
    public function getSegmentTerminator()
    {
        return $this->getData('segment_terminator', '');
    }
    /**
     * beforeProcess function
     */
    public function beforeProcess()
    {
        // TODO: Implement beforeProcess() method.
    }

    /**
     * @return array
     */
    public function afterProcess()
    {
        $results = $this->getResult();

        $data = [
            'input' => $results['data'],
            'output' => $results['output'],
            'originalParse' => $this->originalParser
        ];
        return $data;
    }

    /**
     * processOutbound function
     */
    public function processOutbound()
    {
        logger('Outbound');
    }

    /**
     * @return bool
     */
    public function writerOutboundKnowledge()
    {
        return false;
    }

    /**
     * Custom trim filter for parser
     *
     * @param $string
     * @return string
     */
    protected function filter($string)
    {
        $filter = " \t\n\r\0\x0B" . $this->filterString;

        return trim($string, $filter);
    }

    /**
     * @return string
     */
    protected function getApiEndPoint()
    {
        $listApi = $this->getConfig('apiEndPoints');
        if (! isset($listApi['parser_api'])) {
            logger('API END POINT NOT FOUND');
            return;
        }
        $domainApi = $listApi['parser_api'];

        return $domainApi;
    }
}
