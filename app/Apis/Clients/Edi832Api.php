<?php

namespace App\Apis\Clients;

use App\Apis\ApiBase;
use App\Apis\ApiInterface;
/**
 * Class Edi832Api
 *
 * @package \App\Apis\Clients
 */
class Edi832Api extends ApiBase implements ApiInterface
{
    /**
     * @var string
     */
    protected $api = 'items/v1/888';

    /**
     * Call Api items/v1/888
     * With data from client
     *
     * @return array
     */
    public function callApi()
    {
        $data = $this->getConfig('data');
        $data = $this->remakeData($data);
        $largeFileCase = $this->processLargeFile($data);

        if ($largeFileCase['output']) {
            return $largeFileCase;
        }

        $result = $this->callCondition($data);

        if ($this->countChild($data) < 2) {
            $data = [$data];
        }

        $results = [
            'data' => $data,
            'output' => $result,
        ];

        return $results;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function callCondition($data)
    {
        $result = [];

        if ($this->countChild($data) > 1) {
            foreach ($data as $item) {
                $res = $this->post(['data' => $item]);
                $result[] = json_decode($res->getBody(), true);
            }
        } else {
            $res = $this->post(['data' => $data]);
            $result[] = json_decode($res->getBody(), true);
        }

        return $result;
    }

    /**
     * @param $data
     * @param int $index
     * @return array|int|mixed
     */
    protected function countChild($data, $index = 0)
    {
        foreach ($data as $item) {
            if (is_array($item)) {
                $index = $this->countChild($item, $index + 1);
                break;
            }
        }

        return $index;
    }

    /**
     * @param $input
     * @return array
     */
    protected function processLargeFile($input)
    {
        $result = [];

        if (count($input) > MAX_ITEMS_CALL_API_888) {
            $collect = collect($input)->chunk(MAX_ITEMS_CALL_API_888);

            $chunk = $collect->toArray();

            foreach ($chunk as $data) {
                $res = $this->post(['data' => $data]);
                $result[] = json_decode($res->getBody(), true);
            }
        }

        return [
            'data' => $input,
            'output' => $result,
        ];
    }

    private function remakeData($input)
    {
        $result = [];
        foreach ($input as $index => $value){
            foreach ($value as $data) {
                $result[] = $data;
            }
        }
        return $result;
    }
}
