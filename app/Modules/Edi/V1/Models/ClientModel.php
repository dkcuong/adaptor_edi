<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 20-Jan-16
 * Time: 10:58 AM
 */

namespace App\Modules\Edi\V1\Models;

use App\Clients;
use App\ClientWarehouse;
use App\Inbounds;
use App\Outbound;
use App\Outbounds;
use App\X12InboundInterchange;
use App\X12Interchanges;
use App\X12OutboundInterchanges;
use Illuminate\Support\Facades\DB;

class ClientModel
{

    /**
     * @return mixed
     */
    public function allClientInformation()
    {
        $clients = Clients::with('contact')->with('clientDefaultWarehouse')->with('clientWarehouse')->with('apiEndPoint')->active()->get();

        $result = $clients->each(
            function ($client) {
                $client->data_support = $this->getClientDataFormat($client->id);
                $client->edi = $this->getClientEdiType($client->id);
                $client->rsa = $this->getRsaInformation($client->id);
            }
        );

        return $result;
    }

    /**
     * @param $clientId
     *
     * @return \Illuminate\Support\Collection
     */
    public function getClientDataFormat($clientId)
    {
        $result = DB::table('data_formats AS df')
            ->join('client_data AS cd', 'cd.data_format_id', '=', 'df.id')
            ->join('data_types AS dt', 'dt.id', '=', 'df.data_type_id')
            ->join('service_protocols AS sp', 'sp.id', '=', 'df.protocol_id')
            ->select('df.*', 'dt.name AS support', 'sp.short_name AS protocol')
            ->where('cd.client_id', $clientId)
            ->where('df.status', 1)
            ->first();

        return $result;
    }

    /**
     * @param $clientId
     *
     * @return mixed
     */
    public function getClientEdiType($clientId)
    {
        $inbound = Inbounds::with('ftpAccount')
            ->with('inboundMapping')
            ->with('inboundGroup')
            ->with('dataFormat')
            ->with('api')
            ->active()
            ->where('client_id', $clientId);

        $outbound = Outbounds::with('ftpAccount')
            ->with('outboundMapping')
            ->with('outboundGroup')
            ->with('dataFormat')
            ->with('api')
            ->active()
            ->where('client_id', $clientId);

        if (! config('setting.sequentially')) {
            $inbound->whereRaw('updated_at <= DATE_SUB(NOW(),INTERVAL 5 MINUTE)');
        }

        return [
            'inbound'  => $inbound->get(),
            'outbound' => $outbound->get(),
        ];
    }

    /**
     * @param $clientId
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRsaInformation($clientId)
    {
        $result = X12Interchanges::with('inboundGroup')
            ->where('client_id', $clientId)
            ->orderBy('x12_interchanges.id', 'asc')
            ->first();
        $maxControlNum = X12InboundInterchange::where('client_id', $clientId)
            ->max('control_number');
        if ($maxControlNum && isset($result->control_number)) {
            $result->control_number = $maxControlNum;
        }

        return $result;
    }

    /**
     * @param $id
     */
    public function touchInbound($id)
    {
        Inbounds::find($id)->touch();
    }

    /**
     * @param $whs_id
     * @param $cus_id
     * @return mixed
     */
    public function getClient($whs_id, $cus_id)
    {
        $results = DB::table('client_warehouse')
            ->where('warehouse_id', $whs_id)
            ->where('customer_id', $cus_id)
            ->first();

        if (! $results) {
            return false;
        }
        $client_id = $results->client_id;

        $clients = Clients::with('contact')
            ->with('clientDefaultWarehouse')
            ->with('apiEndPoint')
            ->where('id', $client_id)
            ->active()
            ->first();

        $clients->edi = $this->getClientEdiType($client_id);

        $clients->rsa = $this->getRsaInformation($client_id);

        return $clients;
    }

    /**
     * @param $edi
     * @return mixed
     */
    public function getEDIDataFormat($edi)
    {
        $result = DB::table('data_formats AS df')
            ->join('data_types AS dt', 'dt.id', '=', 'df.data_type_id')
            ->join('service_protocols AS sp', 'sp.id', '=', 'df.protocol_id')
            ->select('df.*', 'dt.name AS support', 'sp.short_name AS protocol')
            ->where('df.id', $edi->data_format_id)
            ->where('df.status', 1)
            ->first();
        return $result;
    }
}
