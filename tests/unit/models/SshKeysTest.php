<?php

namespace tests\unit\models;

use App\SshKeys;
use TestCase;

/**
 * Class SshKeysTest
 *
 * @package tests\unit\models
 */
class SshKeysTest extends TestCase
{

    /**
     * @var array
     */
    protected $raw_data = [];

    /**
     * SshKeysTest constructor.
     */
    public function __construct()
    {
        $this->raw_data = [
            'data'        => sprintf('data_%s', str_random(200)),
            'title'       => sprintf('title_test_%s', str_random(10)),
            'fingerprint' => sprintf('pub_test_%s', str_random(20)),
            'client_id'   => rand(1, 10),
            'type_id'     => rand(0, 1),
            'status'      => rand(0, 1),
        ];
    }

    /**
     *
     */
    public function testInsert()
    {
        $key = new SshKeys();

        $key->setRawAttributes($this->raw_data);
        $status = $key->save();
        $this->assertTrue($status);

        $deleteCount = [];
        $keys = SshKeys::where('title', 'LIKE', '%title_test_%')->get();
        foreach ($keys as $index => $key) {
            $deleteCount[] = $key->delete();
        }
        $this->assertGreaterThanOrEqual(1, count($deleteCount));
    }
}
