<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiEndPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_types', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('short_name', 10);
            $table->text('description');
        });
        
        Schema::create('apis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->string('method', 10);
            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')->on('api_types');
            $table->tinyInteger('status');
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
