<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/03/2016
 * Time: 09:22
 */

namespace App\Mappers\Outbound;


use App\Mappers\MapperBase;

use App\Mappers\SimpleXMLElementExtend;
use DOMDocument;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class IDEANOUVAMapper extends MapperBase
{
    const AI_CODE = '00';
    const CUSTOMER_CODE = '95011010';

    /**
     * Mapper data for Concept One customer
     * Edi945
     *
     * @param $items
     * @param $conditions
     * @return array
     */
    protected function map945($items, $conditions)
    {
        $results[] = $this->parseTree($items);

        return $results;
    }

    /**
     * @param $tranData
     * @return array
     */
    public function parseTree($tranData)
    {
        $tran1 = ['transaction_code' => '945'];
        $order_meta = isset($tranData['order_meta']) ? json_decode($tranData['order_meta'], true) : [];
        $additional_order = isset($order_meta['additional']) ? $order_meta['additional'] : [];
        $ship_to_order = isset($order_meta['ship_to']) ? $order_meta['ship_to'] : [];

        //W06*F*DN17022700999*20170308*07357320000059025**BA4K8KR*SN17022700511*21061~
        $tran1[] = [
            'segment'        => 'W06',
            'report_code'    => 'N',
            'client_order'   => $tranData['client_pick_ticket'],
            'date'           => (isset($tranData['start_ship_date']) && !empty($tranData['start_ship_date'])) ? date('Ymd', strtotime($tranData['start_ship_date'])) : '',
            'shipment_id'    => getArr($additional_order, 'cus_bol_number') ? getArr($additional_order, 'cus_bol_number') : getArr($tranData, 'trackingNumber'),
            'agent_id'       => getArr($additional_order, 'cus_pro_number'),
            'purchase_order' => $tranData['customerordernumber'],
            'bol_number' => $tranData['clientordernumber'],
            'scac' => getArr($additional_order,'carrier_code'),
        ];

        //N1*ST*BED BATH & BEYOND #0655*92*0655~
        $tran1[] = [
            'segment'      => 'N1',
            'entity_code'  => 'ST',
            'name'         => getArr($ship_to_order, 'shipping_to'),
            'id_qualifier' => getArr($ship_to_order, 'shipping_to_qualifier', '92'),
            'id'           => getArr($ship_to_order, 'shipping_to_id', "00001"),
        ];

        //N3*73 STATION ROAD~

        if (! getArr($additional_order, 'shipping_address_2')) {
            $tran1[] = [
                'segment'      => 'N3',
                'address'      => $tranData['shipping_address_street']
            ];
        } else {
            $tran1[] = [
                'segment'      => 'N3',
                'address'      => $tranData['shipping_address_street'],
                'address_2'      => getArr($additional_order, 'shipping_address_2'),
            ];
        }

        $tran1[] = [
            'segment'           => 'N4',
            'name'              => $tranData['shiptocity'],
            'province_code'     => $tranData['shiptostate'],
            'postal_code'       => $tranData['shiptozip'],
        ];

        if (! empty($tranData['client_department'])) {
            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'DP',
                'ref_qualifier'      => $tranData['client_department'],
            ];
        }

        if (! empty($additional_order['cus_pro_number'])) {
            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'CN',
                'ref_qualifier'      => $additional_order['cus_pro_number'],
            ];
        }
        if (! empty($additional_order['load_id'])) {
            $tran1[] = [
                'segment'           => 'N9',
                'entity_code'       => 'LO',
                'ref_qualifier'      => $additional_order['load_id'],
            ];
        }

        if (! empty($tranData['bol_ship_date'])) {
            $tran1[] = [
                'segment'        => 'G62',
                'date_qualifier' => '10',
                'date'           => date('Ymd', strtotime($tranData['bol_ship_date'])),
            ];
        } else {
            $tran1[] = [
                'segment'        => 'G62',
                'date_qualifier' => '10',
                'date'           => date('Ymd', strtotime($tranData['start_ship_date'])),
            ];
        }
        //G62* Estimate Arrival Date ??
        if (! empty($tranData['estimate_arrival_date'])) {
            $tran1[] = [
                'segment'        => 'G62',
                'date_qualifier' => '17',
                'date'           => date('Ymd', strtotime($tranData['estimate_arrival_date'])),
            ];
        }

        if (! empty($additional_order['edi_transaction_service_type'])) {
            $tran1[] = [
                'segment'        => 'NTE',
                'entity_code'    => 'TRA',
                'trans_LSC'      => $additional_order['edi_transaction_service_type'],        //EDI Trans Service Level Code
            ];
        }

        if (! empty($additional_order['special_instruction'])) {
            foreach ($additional_order['special_instruction'] as $ins) {
                $tran1[] = [
                    'segment'        => 'NTE',
                    'entity_code'    => 'OC',
                    'trans_LSC'      => $ins,        //EDI Trans Service Level Code
                ];
            }
        }

        $tran1[] = [
            'segment'               => 'W27',
            'transportation_method' => $additional_order['transportation_method'],
            'carrier_code'          => $additional_order['carrier_code'],
            'routing'               => $additional_order['routing'],
            'payment_method'        => $additional_order['payment_method'],
            //'equipment_code'        => 'EQ',
            //'trailernumber'         => $tranData['trailernumber'],
        ];

        $tran1[] = [
            'segment'        => 'G72',
            'entity_code' => '504',
            'entity_2' => '06',
            'entity_3' => '',
            'entity_4' => '',
            'entity_5' => '',
            'entity_6' => '',
            'entity_7' => '',
            'charge_amount' => (int) $tranData['freightchargeterminfo'],
        ];

        $total_carton = 1;
        foreach ($tranData['items'] as $item_id => $item) {
            $cartonData = $item['cartons'];
            $firstCarton = array_shift($cartonData);

            foreach ($item['cartons'] as $cart_id => $carton) {
                $product_meta = isset($carton['product_meta']) ? json_decode($carton['product_meta'], true) : [];
                //LX*7~
                $tran1[] = [
                    'segment' => 'LX',
                ];
                //MAN*GM*00007357328004124800**CP*832150070055699~
                $tran1[] = [
                    'segment'     => 'MAN',
                    'qualifier1'   => 'GM',
                    'ref11'         => self::AI_CODE . generateUCC128('00'. self::CUSTOMER_CODE, $carton['cartonID']),
                    'ref12' => '',
                    'qualifier2'   => 'CP',
                    'ref21'         => $firstCarton['tracking_number'],
                ];

                $tran1[] = [
                    'segment' => 'PAL',
                    'pallet_type'  => '',
                    'pallet_tier'  => '',
                    'pallet_block'  => '',
                    'pack'  => '',
                    'unit_weight'  => '',
                    'unit_code'  => '',
                    'lenght'  => $carton['length'],
                    'width'  => $carton['width'],
                    'height'  => $carton['height'],
                    'unit_code2'  => '',
                    'weightcarton'  => (int) $carton['weight'],
                    'entity_code'  => 'LB',
                    //'cartoncube'  => $carton['volume'],
                ];

                $tran1[] = [
                    'segment'  => 'N9',
                    'entity_code'  => 'TYP',
                    'boxtype'  => 'CTN25',
                ];

                //W12*SH**1**EA**UP*856456124525*************VN*5KL25~
                $tran1[] = [
                    'segment'          => 'W12',
                    'ordered_quantity' => 'SH',
                    'quantity'         => '',
                    'unit_number'      => $carton['uom'],
                    'quantity_def'     => '',
                    'uom'              => getArr($product_meta, 'unit_code', 'EA'),
                    'upc_1'            => '',
                    'qualifier_id1'    => getArr($product_meta, 'upc_code', 'UP'),
                    'product_id1'      => $carton['upc'],
                    'warehouse_num'    => $tranData['client_pick_ticket'],
                    'uoc_10' => '',
                    'weight'           => getArr($product_meta, 'item_code', 'IT'),
                    'weight_type'      => $carton['product_line_num'],
                    'weight_unit'      => 'VN',
                    'weight_2'         => $carton['sku']
                ];

                //N9*SZ**

                if(! empty($carton['size'])){
                    $tran1[] = [
                        'segment'           => 'N9',
                        'entity_code'       => 'SZ',
                        'description'       => $carton['size'],
                    ];
                }
                //N9*VC**
                if(! empty($carton['color'])){
                    $tran1[] = [
                        'segment'           => 'N9',
                        'entity_code'       => 'VC',
                        'description'       => $carton['color'],
                    ];
                }
                if (! empty($carton['product_load_id'])) {
                    $tran1[] = [
                        'segment'           => 'N9',
                        'entity_code'       => 'LI',
                        'ref_qualifier'     => $carton['product_load_id'],
                    ];
                }
                //MEA*PD*WT*
                $tran1[] = [
                    'segment' => 'MEA',
                    'entity_code' => 'PD',
                    'entity_code2' => 'WT',
                    'carton_weight' => $carton['weight'],
                ];
                //MEA*PD*VOL*
                $tran1[] = [
                    'segment' => 'MEA',
                    'entity_code' => 'PD',
                    'entity_code2' => 'VOL',
                    'carton_volume' => $carton['volume'],
                ];
            }
            $total_carton += $item['cartonCount'];

        }

        //W03*1*1*LB***1*CT~
        $tran1[] = [
            'segment'     => 'W03',
            'unit_number' => $tranData['total_pieces'],
            'weight'      => $tranData['total_weight'],
            'unit_code'   => 'LB',
            'unit_code1'   => '',
            'unit_code2'   => '',
            'carton'      => $tranData['total_carton'],
            'label_type' => 'CT'

        ];
        return $tran1;
    }



}
