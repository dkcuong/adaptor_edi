<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 17/06/2016
 * Time: 11:37
 */

namespace App\Providers;


use App\Apis\As2ApiBase;
use App\As2Api;
use App\Utils\As2ApiClient;
use Illuminate\Support\ServiceProvider;

class As2ApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('As2Api', function ($app, $config) {
            $class = As2ApiBase::class;

            return new $class(new As2ApiClient, $config);
        });
    }
}