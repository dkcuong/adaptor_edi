<?php
/**
 * Created by PhpStorm.
 * User: Vuong
 * Date: 24-Jan-16
 * Time: 9:38 PM
 */

namespace App\Apis;


interface ApiInterface
{
    /**
     * @return mixed
     */
    public function callApi();
}
