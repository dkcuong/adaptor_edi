@include('emails.template')
<table border="1" cellspacing="0" style="border-collapse: collapse">
    <thead>
    <tr>
        <th>PO</th>
        <th>Client PO</th>
        <th>WMS Order Number</th>
        <th>Status</th>
        <th>Message</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($data as $index => $item)
        <tr>
            <?php
            $item['data']['status_type'] = isset($item['data']['status_type']) ? $item['data']['status_type'] : '';
            if ($item['data']['status_type']) {
                logger('Missing Status Response');
            }
            $result = format_data_940($item['data'], $item['message'], $data);
            $color = format_color_940($item['data']['status_type']);
            ?>
            <th style="color: {{$color['po']}}; font-weight:normal; padding:0 15px">{{$result['po']}}</th>
            <th style="color: {{$color['client_po']}}; font-weight:normal; padding:0 15px">{{$result['client_po']}}</th>
            <th style="color: {{$color['order_number']}}; font-weight:normal; padding:0 15px">
                {{$result['order_number']}}</th>
            <th style="color: {{$color['status_type'] }}; font-weight:normal; padding:0 15px">{{$result['status']}}</th>
            <th style="color: {{$color['message']}}; font-weight:normal; padding:0 15px">{{$result['message']}}</th>
        </tr>
    @endforeach
    </tbody>
</table>