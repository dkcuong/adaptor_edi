<?php
$arr_freightChargeTermBy = [
        'collect' => $freight_charge_terms == 'CL' ? 'checked' : '',
        'prepaid' => $freight_charge_terms == 'PR' ? 'checked' : '',
        '3rdparty' => $freight_charge_terms == '3P' ? 'checked' : '',
];
$arr_feeTermBy = [
        'collect' => $fee_terms == 'CL' ? 'checked' : '',
        'prepaid' => $fee_terms == 'PR' ? 'checked' : '',
];
$arr_trailerLoadBy = [
        'shipper' => $trailer_loaded_by == 'BS' ? 'checked' : '',
        'driver' => $trailer_loaded_by == 'BD' ? 'checked' : '',
];
$arr_freightCountedBy = [
        'shipper' => $freight_counted_by == 'BS' ? 'checked' : '',
        'driverpieces' => $freight_counted_by == 'BDP' ? 'checked' : '',
        'driverpallets' => $freight_counted_by == 'BDL' ? 'checked' : '',
];
$ship_from_address = trim($ship_from_addr_1 . ', ' . $ship_from_addr_2, ',');
$ship_to_address = trim($ship_to_addr_1 . ', ' . $ship_to_addr_2, ',');
$bill_to_address = trim($bill_to_addr_1 . ', ' . $bill_to_addr_2, ',');
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>BOL pdf</title>
    <style type="text/css">
        .content {
            font-size: 14px;
            font-family: Arial
        }
        .border {
            border: 1px solid #000;
        }
        .full-width{
            width:100%;
        }
        .page-break {
            page-break-after: always;
        }
        .txt-right {
            text-align: right;
        }
        .nopadding {
            padding: 0;
        }
        .checkbox-inline {
            position: relative;
            display: inline-block;
            padding-left: 20px;
            vertical-align: middle;
        }
        .checkbox-inline input[type=checkbox] {
            position: absolute;
            margin: 0;
            margin-top: -3px;
            margin-left: -20px;
        }
        .footer {
            font-size: 13px;
        }
    </style>
</head>
<body>
<div class="content" style="margin: 0 auto;">
    <table class="full-width border nopadding" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td style="text-align: left;border-bottom: 1px solid #000;padding: 5px" width="20%">Date: {{ date('m/d/Y', time()) }}</td>
            <td colspan="2"
                style="text-align: center;text-transform: uppercase; font-weight: bold; font-size: 16px;border-bottom: 1px solid #000;padding: 5px"
                width="60%">
                bill of lading
            </td>
            <td style="text-align: right;border-bottom: 1px solid #000;padding: 5px" width="20%">Page 1</td>
        </tr>
        <tr>
            <td width="50%" colspan="2" style="padding: 0; margin: 0;vertical-align: top">
                <table width="100%" style="padding: 0; margin: 0; border-right: 1px solid #000" cellspacing="0"
                       cellpadding="0">
                    <thead>
                    <tr>
                        <th colspan="2"
                            style="text-align: center; text-transform: uppercase; background-color: #000; color: #fff; font-size: 16px;font-weight: 500; padding: 2.5px">
                            ship from
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="padding: 5px">Name:</td>
                        <td style="font-style: italic">{{$ship_from_name}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Address:</td>
                        <td style="font-style: italic">{{$ship_from_address}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">City/Sate/Zip:</td>
                        <td style="font-style: italic">{{$ship_from_city}}, {{$ship_from_state}}, {{$ship_from_zip}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">SID#: {{$ship_from_sid}}</td>
                        <td style="padding: 5px; font-style: italic">
                            <label style="margin-left: 70%;">FOB: <input type="checkbox" {{$ship_from_fob ? 'checked' : ''}}></label>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table width="100%" style="padding: 0; margin: 0;border-right: 1px solid #000" cellpadding="0"
                       cellspacing="0">
                    <thead>
                    <tr>
                        <th colspan="2"
                            style="text-align: center; text-transform: uppercase; background-color: #000; color: #fff; font-size: 16px;font-weight: 500; padding: 2.5px">
                            ship to
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="padding: 5px">Name:</td>
                        <td>{{$ship_to_name}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Address:</td>
                        <td>{{$ship_to_address}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">City/Sate/Zip:</td>
                        <td>{{$ship_to_city}}, {{$ship_to_state}}, {{$ship_to_zip}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">TEL#: {{$ship_to_tel}}</td>
                        <td style="padding: 5px">
                            <label style="margin-left: 70%;">FOB: <input type="checkbox" {{$ship_to_fob ? 'checked' : ''}}></label>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table width="100%" style="padding: 0; margin: 0;border-right: 1px solid #000; border-bottom: 1px solid #000" cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th colspan="2"
                            style="text-align: center; text-transform: uppercase; background-color: #000; color: #fff; font-size: 16px;font-weight: 500; padding: 2.5px">
                            third party freight charges bill to
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="padding: 5px">Name:</td>
                        <td>{{$bill_to_name}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Address:</td>
                        <td>{{$bill_to_address}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">City/Sate/Zip:</td>
                        <td>{{$bill_to_city}}, {{$bill_to_state}}, {{$bill_to_zip}}</td>
                    </tr>
                    </tbody>
                </table>
                <table width="100%" style="padding: 0; margin: 0;border-right:1px solid #000 " cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td style="text-transform: uppercase; padding: 10px">special instructions:</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 30px; text-transform: uppercase">{{$special_inst}}</td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td width="50%" colspan="2" style="margin: 0; vertical-align: top">
                <table width="100%" style="padding: 0; margin: 0;border-bottom: 1px solid #000">
                    <tbody>
                    <tr>
                        <td style="padding: 5px" width="50%">Bill of Lading Number:</td>
                        <td style="font-weight: bold">{{$bo_num}}</td>
                    </tr>
                    <tr><td  style="height: 65px"></td></tr>
                    </tbody>
                </table>
                <table width="100%" style="padding: 0; margin: 0;border-bottom: 1px solid #333">
                    <tbody>
                    <tr>
                        <td style="text-transform: uppercase; padding: 5px;" width="40%">carrier name:</td>
                        <td style="font-style: italic">{{$carrier}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Trailer Number:</td>
                        <td style="font-style: italic">{{$trailer_num}}</td>
                    </tr>
                    <tr>
                        <td style="padding: 5px">Seal Number(s):</td>
                        <td>{{$seal_num}}</td>
                    </tr>
                    <tr></tr>
                    </tbody>
                </table>
                <table width="100%" style="padding: 0; margin: 0;border-bottom: 1px solid #333">
                    <tbody>
                    <tr>
                        <td style="text-transform: uppercase;padding: 5px" width="40%">scac:</td>
                        <td style="text-transform: uppercase; padding: 5px; font-style: italic">{{$scac}}</td>
                    </tr>
                    <tr>
                        <td style="padding:5px">Pro Number:</td>
                        <td style="font-style: italic">{{$pro_num}}</td>
                    </tr>
                    <tr></tr>
                    </tbody>
                </table>
                <table width="100%" style="padding: 0; margin: 0;border-bottom: 1px solid #333">
                    <tbody>
                    <tr>
                        <td colspan="3" style="font-size:13px; padding-bottom: 60px">Freight Charge Terms: (freight
                            charges are prepaid unless marked otherwise)
                        </td>
                    </tr>
                    <tr>
                        <td style="font-style: italic"><label class="checkbox-inline"><input type="checkbox" {{$arr_freightChargeTermBy['prepaid']}} >Prepaid</label></td>
                        <td style="font-style: italic"><label class="checkbox-inline"><input type="checkbox" {{$arr_freightChargeTermBy['collect']}} >Collect</label></td>
                        <td style="font-style: italic"><label class="checkbox-inline"><input type="checkbox" {{$arr_freightChargeTermBy['3rdparty']}} >3rdParty</label></td>
                    </tr>
                    </tbody>
                </table>
                <table width="100%">
                    <tbody>
                    <tr>
                        <td style="text-align: center; border-right: 1px solid #000" width="30%">
                            <p style="text-align: center"><input type="checkbox" {{ $is_attach ? 'checked' : ''}}></p>
                            <p>(checkbox)</p>
                        </td>
                        <td style="vertical-align: top; padding: 5px">Master Bill of Lading with attached underlying
                            Bills of Lading
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <h3 style="text-align: center; text-transform: uppercase; background-color: #000; color: #fff;font-weight: 500;margin: 0; padding: 2.5px">
                    customer order information</h3>
                <table width="100%" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                PO#
                            </th>
                            <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; white-space: nowrap">
                                # PKGS
                            </th>
                            <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                UNITS
                            </th>
                            <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                WEIGHT
                            </th>
                            <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; white-space: nowrap">
                                DEPT #
                            </th>
                            <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                PLTS
                            </th>
                            <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                PICK TICKET
                            </th>
                            <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; white-space: nowrap">
                                CUS REF #
                            </th>
                            <th style="text-align: center; font-weight: 300;border-bottom: 1px solid #000">ADDITIONAL
                                SHIPPER INFO
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($bol_odr_dtl as $order)
                        <tr>
                            <td style="text-align: center ;padding: 2.5px; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{wordwrap($order['cus_po'], 11, "\n", true)}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$order['ctn_qty']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$order['piece_qty']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$order['weight']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{wordwrap($order['cus_dept'], 11, "\n", true)}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$order['plt_qty']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{$order['cus_ticket']}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000">
                                {{wordwrap($order['cus_odr'], 11, "\n", true)}}
                            </td>
                            <td style="text-align: center; font-weight: 300; border-bottom: 1px solid #000">
                                {{wordwrap($order['add_shipper_info'], 20, "\n", true)}}
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td style="padding: 2.5px; font-weight: 300; border-right: 1px solid #000;">GRAD TOTAL</td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000">{{$ctn_qty_ttl}}</td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000">{{$piece_qty_ttl}}</td>
                            <td style="text-align: center; font-weight: 300; border-right: 1px solid #000">{{$weight_ttl}}</td>
                            <td colspan="5" style="background-color: #7d7d7d"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <h3 style="text-align: center; text-transform: uppercase; background-color: #000; color: #fff;font-weight: 500;margin: 0; padding: 2.5px">
                    carrier information</h3>
                <table style="border: 1px solid #000" cellpadding="0" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th colspan="2" style="font-size: 12px; padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;text-transform: uppercase; white-space: nowrap">handling unit</th>
                        <th colspan="2" style="font-size: 12px; padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;text-transform: uppercase">package</th>
                        <th rowspan="2" style="font-size: 12px; padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;text-transform: uppercase">weight</th>
                        <th rowspan="2" style="font-size: 12px; padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;text-transform: uppercase">H.M.<br>(X)</th>
                        <th style="padding:2.5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;text-transform: uppercase">commodity description</th>
                        <th colspan="2" style="padding:2.5px;text-align: center; font-weight: 300; border-bottom: 1px solid #000;text-transform: uppercase">ltl only</th>
                    </tr>
                    <tr>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase">qty</th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase">type</th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase">qty</th>
                        <th style="text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase">type</th>
                        <th style="padding: 5px; font-size: 10px; font-weight: 500; text-align: left;border-right: 1px solid #000; border-bottom: 1px solid #000;">Commodities requiring special or additional care orattention in handling or stowing must be
                            so marked andpackages as ensure sage transportation with ordinarycare.See Section 2(e) of
                            nmfc Item 360
                        </th>
                        <th style="padding:5px; text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000; text-transform: uppercase;white-space: nowrap">nmfc #</th>
                        <th style="padding:5px; text-align: center; font-weight: 300; border-bottom: 1px solid #000; text-transform: uppercase">class</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $carrier_ttl_hdl_qty = 0;
                    $carrier_ttl_pkg_qty = 0;
                    $carrier_ttl_weight = 0;
                    ?>
                    @foreach($bol_carrier_dtl as $carrier)
                        <?php
                        $carrier_ttl_hdl_qty += $carrier['hdl_qty'];
                        $carrier_ttl_pkg_qty += $carrier['pkg_qty'];
                        $carrier_ttl_weight += $carrier['weight'];
                        ?>
                    <tr>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier['hdl_qty']}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier['hdl_type']}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier['pkg_qty']}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier['pkg_type']}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier['weight']}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier['hm']}}</td>
                        <td  style="text-transform:uppercase;padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier['cmd_des']}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier['nmfc_num']}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-bottom: 1px solid #000;font-size: 16px">{{$carrier['cls_num']}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier_ttl_hdl_qty}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px; background-color: #7d7d7d"></td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier_ttl_pkg_qty}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px;background-color: #7d7d7d"></td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">{{$carrier_ttl_weight}}</td>
                        <td  style="padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px;background-color: #7d7d7d"></td>
                        <td  style="text-transform:uppercase;padding:5px;text-align: center; font-weight: 300; border-right: 1px solid #000; border-bottom: 1px solid #000;font-size: 16px">GRAND TOTAL </td>
                        <td colspan="2"  style="padding:5px;text-align: center; font-weight: 300;border-bottom: 1px solid #000;font-size: 16px; background-color: #7d7d7d"></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2"
                style="font-size: 12px; border-bottom: 1px solid #000; border-right: 1px solid #000; padding: 5px; text-align: center">
                <p style="margin: 0;"> Where the rate is dependent on value, shippers are required to state specially in writing the agreed or declared value of the property as follows:</p>
                <p style="margin: 0; padding-bottom: 5px;">"The agreed or declared value of the property is specially stated
                    by the shipper to be not <span style="white-space: nowrap">exceeding __________ per __________</span>"</p>
            </td>
            <td colspan="2" style="border-bottom: 1px solid #000; padding: 5px; vertical-align: top">
                <table>
                    <tr style="font-size: 18px; margin: 5px"><td colspan="3">COD Amount $__________________</td></tr>
                    <tr style="font-size: 18px; margin: 5px">
                        <td>Fee terms: </td>
                        <td><label class="checkbox-inline"><input style="margin-top: 0;" type="checkbox" {{$arr_feeTermBy['collect']}}>Collect</label></td>
                        <td><label class="checkbox-inline"><input style="margin-top: 0;" type="checkbox" {{$arr_feeTermBy['prepaid']}}>Prepaid</label></td>
                    </tr>
                    <tr style="font-size: 18px;padding-left: 30px; margin: 5px">
                        <td colspan="3"><label class="checkbox-inline">Customer check acceptable $ <input style="margin-left: 0;margin-top: 0;" type="checkbox" {{$cus_accept ? 'checked' : ''}}></label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="border-bottom: 1px solid #000; font-size: 17px;padding: 5px">NOTE: Liability
                Limitation for loss or damage in this shipment may be applicable. See 49 U.S.C.
                - 14706(c)(1)(A) and (B)
            </td>
        </tr>
        <tr class="footer page-break">
            <td colspan="2" style="border-bottom: 1px solid #000; border-right: 1px solid #000; padding: 5px; text-align: center">
                <p style="margin: 0; font-size: 12px"> RECEIVED, subject to individually determined rates or contracts
                    that have been
                    agreed upon in writing between the carrier and shipper, otherwise to the
                    rates,classifications and rules that have been established by the carrier and are
                    available to the shipper, on request, and to all applicable states and federal regulations</p>
            </td>
            <td colspan="2" style="border-bottom: 1px solid #000; padding-left: 5px; vertical-align: top ">
                The carrier shall not make delivery of this shipment without payment
                of freight and all other lawful charges<br>
                <p style="margin-top: 30px; margin-bottom: 10px">_____________________________ Shipper Signature</p>
            </td>
        </tr>
        <tr class="footer">
            <td colspan="4">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td width="33%" style="border-left: 1px solid #000; vertical-align: top; padding-left: 2.5px">
                            SHIPPER SIGNATURE / DATE
                            <p style="margin: 0">This is to certify that the above named materials
                                are properly classified, packaged, marked and
                                labeled, and are proper condition for transportation
                                according to the applicable regulations of the DOT.</p>
                            <p style="margin-bottom: 10px">__________________ / __________</p>
                        </td>
                        <td width="34%" style="border-left: 1px solid #000; vertical-align: top">
                            <table width="100%">
                                <thead>
                                <tr>
                                    <th style="font-weight: 500;">Trailer Loaded</th>
                                    <th style="font-weight: 500;">Freight Counted</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td width="50%">
                                    <p><label class="checkbox-inline"><input type="checkbox" {{$arr_trailerLoadBy['shipper']}}>By Shipper</label></p>
                                    <p><label class="checkbox-inline"><input type="checkbox" {{$arr_trailerLoadBy['driver']}}>By Driver</label></p>
                                </td>
                                <td width="50%">
                                    <p><label class="checkbox-inline"><input type="checkbox" {{$arr_freightCountedBy['shipper']}}>By Shipper</label></p>
                                    <p><label class="checkbox-inline"><input type="checkbox" {{$arr_freightCountedBy['driverpallets']}}>By Driver/pallets said to contain</label></p>
                                    <p><label class="checkbox-inline"><input type="checkbox" {{$arr_freightCountedBy['driverpieces']}}>By Driver/Pieces</label></p>
                                </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td width="33%" style="border-left: 1px solid #000; vertical-align: top; padding-left: 2.5px">
                            CARRIER SIGNATURE / PICKUP DATE
                            <p style="margin: 0">Carrier acknowledges receipt of packaged and
                                required placards. Carrier certifies emergency response
                                information was made available and/or carrier has the
                                U.S. DOT emergency response guidebook or equivalent
                                documentation in the vehicle.
                            <p style="margin-bottom: 10px">__________________ / __________</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>