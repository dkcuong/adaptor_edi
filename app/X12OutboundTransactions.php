<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 17/02/2016
 * Time: 17:13
 */

namespace App;

class X12OutboundTransactions extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'x12_outbound_transactions';

    /**
     * @var array
     */
    protected $fillable = [
        'transaction_code',
        'control_number',
        'group_id',
        'group_control_number',
        'success_number',
        'data_input',
        'data_output',
        'status_wms',
        'status',
        'message'
    ];

    /**
     * @var array
     */
    public $obFields = [
        'id',
        'transaction_code',
        'control_number',
        'success_number',
        'data_input',
        'data_output'
    ];

}
