<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 17/02/2016
 * Time: 17:11
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class X12OutboundGroups extends Model
{
    /**
     * @var string
     */
    protected $table = 'x12_outbound_groups';

    /**
     * @var array
     */
    public $obFields = [
        'id',
        "group_code",
        "sender_code",
        "receiver_code",
        "date",
        "time",
        "control_number",
        "control_standard",
        "control_version",
    ];


    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param $groupID
     * @param $nextControlNumber
     * @return mixed
     */
    public function updateControlNumber($groupID, $nextControlNumber)
    {
        return X12OutboundGroups::where('id', '=', $groupID)
            ->update(['control_number' => $nextControlNumber]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groupRevisions()
    {
        return $this->hasMany(X12OutboundGroupRevisions::class, 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function outboundTransaction()
    {
        return $this->hasOne(Outbounds::class, 'group_id');
    }
}
