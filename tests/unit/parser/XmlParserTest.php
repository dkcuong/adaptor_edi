<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 3/13/2017
 * Time: 13:34
 */


class XmlParserTest extends TestCase
{
    public function test_text_parser_xml_content()
    {
        $content = '<?xml version="1.0" ?>
<File>
    <Document>
        <Header>
            <CompanyCode>10120</CompanyCode>
            <CustomerNumber>325</CustomerNumber>
            <Direction>Outbound</Direction>
            <DocumentType>856</DocumentType>
            <Version>3.5</Version>
            <Footprint>ASN</Footprint>
            <ShipmentID>BLC1012797</ShipmentID>
            <InternalDocumentNumber>164622</InternalDocumentNumber>
        </Header>
        <ShipmentLevel>
            <TransactionSetPurposeCode>00</TransactionSetPurposeCode>
            <PRONumber>BLC1012797</PRONumber>
            <DateLoop>
                <DateQualifier Desc="ShipDate">011</DateQualifier>
                <Date>2017-03-09</Date>
                <DateQualifier Desc="DeliveryDate">002</DateQualifier>
                <Date>2017-03-14</Date>
            </DateLoop>
            <ManifestCreateTime>12:03:00</ManifestCreateTime>
            <HierarchyStructureCode>0001</HierarchyStructureCode>
            <ShipmentTotals>
                <ShipmentTotalCube>33.14</ShipmentTotalCube>
                <ShipmentTotalCases>618</ShipmentTotalCases>
                <ShipmentTotalWeight>2693</ShipmentTotalWeight>
            </ShipmentTotals>
            <Carrier>
                <CarrierCode>RDSS</CarrierCode>
            </Carrier>
            <BillOfLadingNumber>
                <![CDATA[12350010004283836]]>
</BillOfLadingNumber>
            <MethodOfPayment>PP</MethodOfPayment>
            <Name>
                <BillAndShipToCode>SF</BillAndShipToCode>
                <DUNSOrLocationNumber>828766738</DUNSOrLocationNumber>
                <NameComponentQualifier>Ship From</NameComponentQualifier>
                <NameComponent>
                    <![CDATA[LIFEWORKS]]>
</NameComponent>
                <CompanyName>
                    <![CDATA[LIFEWORKS]]>
</CompanyName>
                <CompanyName>
                    <![CDATA[LIFEWORKS]]>
</CompanyName>
                <Address>
                    <![CDATA[809 E. 236TH STREET]]>
</Address>
                <City>
                    <![CDATA[CARSON]]>
</City>
                <State>CA</State>
                <Zip>90745</Zip>
                <Country>USA</Country>
            </Name>
            <Name>
                <BillAndShipToCode>ST</BillAndShipToCode>
                <DUNSOrLocationNumber>0870</DUNSOrLocationNumber>
                <NameComponentQualifier>Ship To</NameComponentQualifier>
                <NameComponent>
                    <![CDATA[MONTGOMERY DC - #0870]]>
</NameComponent>
                <CompanyName>
                    <![CDATA[MONTGOMERY DC - #0870]]>
</CompanyName>
                <Address>
                    <![CDATA[2855 SELMA HWY]]>
</Address>
                <City>
                    <![CDATA[MONTGOMERY]]>
</City>
                <State>AL</State>
                <Zip>36108</Zip>
                <Country>United States</Country>
            </Name>
            <VendorCode>0004000277</VendorCode>
            <ContactType>
                <FunctionCode>
                    <![CDATA[CUST]]>
</FunctionCode>
                <ContactName>
                    <![CDATA[CARLOS RODRIGUEZ]]>
</ContactName>
                <ContactQualifier>FX</ContactQualifier>
                <PhoneEmail>
                    <![CDATA[3102417654]]>
</PhoneEmail>
                <ContactQualifier>PH</ContactQualifier>
                <PhoneEmail>
                    <![CDATA[3102417655]]>
</PhoneEmail>
            </ContactType>
        </ShipmentLevel>
        <OrderLevel>
            <IDs>
                <PurchaseOrderNumber>
                    <![CDATA[0086767952]]>
</PurchaseOrderNumber>
                <PurchaseOrderSourceID>27447369</PurchaseOrderSourceID>
                <PurchaseOrderDate>2017-02-28</PurchaseOrderDate>
                <StoreNumber>0870</StoreNumber>
                <DepartmentNumber></DepartmentNumber>
                <DivisionNumber></DivisionNumber>
            </IDs>
            <OrderTotals>
                <OrderTotalCases>618</OrderTotalCases>
                <OrderTotalWeight>2693</OrderTotalWeight>
                <OrderTotalCube>33.14</OrderTotalCube>
            </OrderTotals>
            <DateLoop>
                <DateQualifier Desc="PurchaseOrderDate">004</DateQualifier>
                <Date>2017-02-28</Date>
            </DateLoop>
            <Name>
                <BillAndShipToCode>BT</BillAndShipToCode>
                <DUNSOrLocationNumber>0870</DUNSOrLocationNumber>
                <NameComponentQualifier>Bill To</NameComponentQualifier>
                <NameComponent>
                    <![CDATA[BIG LOTS C/O UNYSON LOGISTICS]]>
</NameComponent>
                <CompanyName>
                    <![CDATA[BIG LOTS C/O UNYSON LOGISTICS]]>
</CompanyName>
                <Address>
                    <![CDATA[PO BOX 7047]]>
</Address>
                <City>
                    <![CDATA[DOWNERS GROVE]]>
</City>
                <State>IL</State>
                <Zip>60515</Zip>
                <Country>USA</Country>
            </Name>
            <Name>
                <BillAndShipToCode>RE</BillAndShipToCode>
                <DUNSQualifier>1</DUNSQualifier>
                <DUNSOrLocationNumber>828766738</DUNSOrLocationNumber>
                <NameComponentQualifier>Remit To</NameComponentQualifier>
                <NameComponent>
                    <![CDATA[LIFEWORKS]]>
</NameComponent>
                <CompanyName>
                    <![CDATA[LIFEWORKS]]>
</CompanyName>
                <Address>
                    <![CDATA[1412 BROADWAY, 7TH FLOOR]]>
</Address>
                <City>
                    <![CDATA[NEW YORK]]>
</City>
                <State>NY</State>
                <Zip>10018</Zip>
                <Country>USA</Country>
            </Name>
            <PickPackStructure>
                <Carton>
                    <Marks>
                        <UCC128>00812350011038568425</UCC128>
                    </Marks>
                    <Quantities>
                        <QtyQualifier>ZZ</QtyQualifier>
                        <QtyUOM>ZZ</QtyUOM>
                        <Qty>48</Qty>
                    </Quantities>
                </Carton>
                <Item>
                    <ItemIDs>
                        <IdQualifier>UP</IdQualifier>
                        <Id>
                            <![CDATA[5297]]>
</Id>
                    </ItemIDs>
                    <ItemIDs>
                        <IdQualifier>VN</IdQualifier>
                        <Id>
                            <![CDATA[IH-BL-T2300]]>
</Id>
                    </ItemIDs>
                    <Quantities>
                        <QtyQualifier>39</QtyQualifier>
                        <QtyUOM>EA</QtyUOM>
                        <Qty>48</Qty>
                    </Quantities>
                    <PackSize>6</PackSize>
                    <Inners>8</Inners>
                    <EachesPerInner></EachesPerInner>
                    <InnersPerPacks></InnersPerPacks>
                    <Size>
                        <![CDATA[]]>
</Size>
                    <Measurement>
                        <MeasureQual>WT</MeasureQual>
                        <MeasureValue>1</MeasureValue>
                    </Measurement>
                </Item>
            </PickPackStructure>
            <PickPackStructure>
                <Carton>
                    <Marks>
                        <UCC128>00812350011038568418</UCC128>
                    </Marks>
                    <Quantities>
                        <QtyQualifier>ZZ</QtyQualifier>
                        <QtyUOM>ZZ</QtyUOM>
                        <Qty>48</Qty>
                    </Quantities>
                </Carton>
                <Item>
                    <ItemIDs>
                        <IdQualifier>UP</IdQualifier>
                        <Id>
                            <![CDATA[5297]]>
</Id>
                    </ItemIDs>
                    <ItemIDs>
                        <IdQualifier>VN</IdQualifier>
                        <Id>
                            <![CDATA[IH-BL-T2300]]>
</Id>
                    </ItemIDs>
                    <Quantities>
                        <QtyQualifier>39</QtyQualifier>
                        <QtyUOM>EA</QtyUOM>
                        <Qty>48</Qty>
                    </Quantities>
                    <PackSize>6</PackSize>
                    <Inners>8</Inners>
                    <EachesPerInner></EachesPerInner>
                    <InnersPerPacks></InnersPerPacks>
                    <Size>
                        <![CDATA[]]>
</Size>
                    <Measurement>
                        <MeasureQual>WT</MeasureQual>
                        <MeasureValue>1</MeasureValue>
                    </Measurement>
                </Item>
            </PickPackStructure>
        </OrderLevel>
    </Document>
</File>
';

        $edi = new stdClass();
        $edi->support = 'xml';

        $testObject = new \App\Parsers\XmlParser\XmlParser([
            'content' => $content,
            'edi' => $edi,
        ]);

        $result = $testObject->inbound()->get();
    }

    public function parseArraytoXML()
    {

    }
}