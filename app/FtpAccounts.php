<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

/**
 * Class FtpAccounts
 *
 * @package App
 *
 * @property int     $protocol
 * @property HasMany $ftpSsh
 */
class FtpAccounts extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ftpType()
    {
        return $this->belongsTo(FtpTypes::class, 'type_id');
    }

    /**
     * @return HasMany|array
     */
    public function ftpSsh()
    {
        return $this->hasMany(FtpSsh::class, 'ftp_id', 'id');
    }
}
