@include('emails.template')

    <?php if (isset($data[0]['data']['item']) && is_array($data[0]['data']['item'])) {
        ?>
        <table border="1" cellspacing="0" style="border-collapse: collapse">
            <thead>
            <tr>
                <th>Sku</th>
                <th>Size</th>
                <th>Color</th>
                <th>Status</th>
                <th>Message</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
        <?php
        $messageError = ! empty($data[0]['message'][0]) ? $data[0]['message'][0] : '';
        foreach ($data[0]['data']['item'] as $index => $item) { ?>
            <tr>

                <?php
                    $status = isset($item['statusResponse']) ? $item['statusResponse'] : '';
                    $color = '#000000';

                if ($status == 'created') {
                    $status = 'Created';
                    $statusType = 'C';
                    $message = 'Created';
                    $color = '#4cae4c';
                } else if ($status == 'existed') {
                    $status = 'Existed';
                    $statusType = 'E';
                    $message = 'Existed';
                    $color = '#FF9900';
                } else if ($status == 'duplicateUPC') {
                    $status = 'Error';
                    $statusType = 'D';
                    $message = 'duplicateUPC';
                    $color = '#FF9900';
                    $messageError = 'Duplicate UPC';
                } else {
                    logger('Missing Status Response');
                    $status = '';
                    $statusType = 'ER';
                    $message = '';
                    $color = '#FF9900';
                }
                ?>
                <th style="padding:0 15px">{{isset($item['sku']) ? $item['sku'] : ''}}</th>
                <th style="padding:0 15px">{{isset($item['size']) ? $item['size'] : ''}}</th>
                <th style="padding:0 15px">{{isset($item['color']) ? $item['color'] : ''}}</th>
                <th style="color:{{$color}}; padding:0 15px">{{$status}}</th>
                <th style="color:{{$color}}; padding:0 15px">{{$messageError}}</th>
                <th></th>
            </tr>
    <?php }
    ?>
            </tbody>
        </table>
        <?php

    } else {
        $messageError = ! empty($data[0]['message'][0]) ? $data[0]['message'][0] : '';
        ?>
        <span style="color: red">{{$messageError}}</span>
    <?php
    }
    ?>

    @if(isset($data[0]['data']['duplicateUPC']))
    <br/>
    <h3>We found some upcs were duplicated!</h3>
    <table border="1" cellspacing="0" style="border-collapse: collapse">
        <thead>
        <tr>
            <th>UPC</th>
            <th>Status</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($data[0]['data']['duplicateUPC'] as $upc => $positions){
        $statusType = 'DD';
        $message = 'UPCs has been duplicated '.count($positions).' rows';
        $color = '#FF9900';
        ?>
        <tr>
            <th style="padding:0 15px">{{$upc}}</th>
            <th style="color:{{$color}}; padding:0 15px">{{$statusType}}</th>
            <th style="color:{{$color}}; padding:0 15px">{{$message}}</th>
        </tr>
    <?php
    }
    ?>
        </tbody>
    </table>
        @endif
