<?php

namespace App\Providers;

use App\Apis\ApiBase;
use Illuminate\Support\ServiceProvider;
use App\Utils\WmsApiClient;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Api', function ($app, $config) {

            $class = ApiBase::class;

            // If client has its Api Caller, system will call Client Caller
            // And extends ApiBase class
            if (isset($config['edi'])) {
                $extendClass = 'App\Apis\Clients\Edi' . $config['edi'] . 'Api';

                if (isset($config['outbound_process']) && $config['outbound_process']) {
                    $tmp_class = 'App\Apis\Clients\Outbound\Edi' . $config['edi'] . 'Api';
                    if(class_exists($tmp_class)){
                        $extendClass = $tmp_class;
                    }
                }

                $class = class_exists($extendClass) ? $extendClass : $class;
            }

            return new $class(new WmsApiClient($config), $config);
        });
    }
}
