<?php
/**
 * Created by PhpStorm.
 * User: DucTran
 * Date: 5/6/2016
 * Time: 4:45 PM
 */

namespace App\Modules\Edi\V1\Services;

use App\SshKeys;
use phpseclib\Crypt\RSA;

/**
 * Class SshKeysService
 *
 * @package App\Admin\V1\Services
 */
class SshKeysService
{

    const PUBLIC_KEY_TYPE = 1;
    const PRIVATE_KEY_TYPE = 2;

    /**
     * @var string
     */
    protected $private_key_title = 'seldat_private_key';


    /**
     * @return SshKeys
     */
    public function currentPrivateKey()
    {
        $where = ['title' => $this->private_key_title];

        $key = SshKeys::where($where)->first();

        $rsa = new RSA();
        $loadStatus = $rsa->loadKey($key->data);

        if (! $loadStatus) {
            return new SshKeys();
        }

        $key->publicKey = $rsa->getPublicKey(RSA::PUBLIC_FORMAT_OPENSSH);

        return $key;
    }
}