<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/02/2016
 * Time: 02:54
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class X12InboundGroupRevisions extends Model
{
    /**
     * @var string
     */
    protected $table = 'x12_inbound_group_revisions';

    /**
     * @var array
     */
    protected $fillable = [
        'sender_code',
        'receiver_code',
        'date',
        'time',
        'control_number',
        'control_standard',
        'control_version',
        'status',
        'interchange_id',
        'inbound_interchange_id',
        'group_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function outboundTransaction()
    {
        return $this->hasMany(X12InboundTransactions::class, 'group_id');
    }
}
