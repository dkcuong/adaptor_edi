<?php
/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 22-Jan-16
 * Time: 9:33 AM
 */

namespace App\Mappers\Inbound;

use App\Mappers\MapperBase;

class JOIMapper extends MapperBase
{
    /**
     * Mapper data for Just One customer
     *
     * @param $item
     * @param $conditions
     * @return array
     */
    protected function map888($item, $conditions)
    {
        $result = [];

        foreach ($conditions as $target => $from) {

            if (isset($item['Dimensions'])) {
                $dimension = explode('x', $item['Dimensions']);
                $result['length'] = $dimension[0];
                $result['width'] = $dimension[1];
                $result['height'] = $dimension[2];
            }

            if (isset($item[$from])) {
                $result[$target] = $item[$from];
            }
        }
        return $result;
    }
}
