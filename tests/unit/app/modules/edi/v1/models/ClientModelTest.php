<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientModelTest extends TestCase
{
    public function testGetClientFtp()
    {
        $model = new \App\Modules\Edi\V1\Models\ClientModel();

        $result = $model->allClientInformation();

        $this->assertGreaterThanOrEqual(2, $result->count());
    }
}
