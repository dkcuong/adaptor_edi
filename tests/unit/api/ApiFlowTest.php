<?php

/**
 * Created by PhpStorm.
 * User: vuong
 * Date: 08-Apr-16
 * Time: 2:28 PM
 */
class ApiFlowTest extends TestCase
{
    public function testGroupByBeforeCallApi()
    {
        $obj = Mockery::mock('App\Apis\ApiBase');
        $data = [
            [
                'reference' => 'a1',
                'container' => 'c1',
                'sku' => 'sku1',
                'size' => 'size1',
            ],
            [
                'reference' => 'a1',
                'container' => 'c2',
                'sku' => 'sku2',
                'size' => 'size2',
            ],
            [
                'reference' => 'a1',
                'container' => 'c2',
                'sku' => 'sku3',
                'size' => 'size3',
            ]
        ];

        $result =  $obj->groupBy($data, 'reference', 'containers', [
            'container', 'sku','size'
        ]);

        $this->assertArrayHasKey('containers', $result[1]);
        $this->assertCount(1, $result);
    }
}
