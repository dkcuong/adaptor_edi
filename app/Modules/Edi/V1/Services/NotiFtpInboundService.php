<?php
/**
 * Created by PhpStorm.
 * User: viet.le
 * Date: 8/25/2017
 * Time: 9:29 AM
 */
namespace App\Modules\Edi\V1\Services;
use App\Modules\Edi\V1\Models\ClientModel;
use App\Clients;
use App\FtpAccounts;
use App\NotiFtpUpload;
use App\Modules\Edi\V1\Models\InboundModel;
use App\Modules\Edi\V1\Services\ServiceHelpers\LogService;
use Illuminate\Support\Facades\DB;
use App\Setting;
use App\Utils\Date;
use App\Utils\FtpStorage;
use App\Utils\SftpStorage;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class NotiFtpInboundService
{
    const POS_NAME_OF_FILE = 1;

    const MAIL_TEMPLATE = 'notification';

    protected $client;
    /**
     * @var
     */
    protected $inbound;
    /**
     * @var InboundModel
     */
    protected $ibModel;
    /**
     * @var
     */
    protected $inboundData;
    /**
     * @var
     */
    protected $inter;
    /**
     * @var
     */
    protected $ibGroup;

    /**
     * @var
     */
    protected $settingService;

    /**
     * @var EmailHandlingService
     */
    protected $emailHandling;

    /**
     * @var array
     */
    protected $errors = [];

    public $notiFtp;

    public $obModel;

    /**
     * NotiFtpInboundService constructor.
     * @param ClientModel $client
     * @param InboundModel $inboundModel
     * @param NotiFtpUpload $notiFtp
     * @param EmailHandlingService $emailHandlingService
     * @param SettingService $settingService
     */
    public function __construct(
        ClientModel $client,
        InboundModel $inboundModel,
        NotiFtpUpload $notiFtp,
        EmailHandlingService $emailHandlingService,
        SettingService $settingService
    ) {
        $this->client = $client;
        $this->ibModel = $inboundModel;
        $this->emailHandling = $emailHandlingService;
        $this->settingService = $settingService;
        $this->notiFtp = $notiFtp;
    }
    /**
     * Init Master data
     */
    public function init()
    {
        $this->settingService->emailSetting();
    }

    /**
     * Check new file on FTP every minute
     */
     public function checkFtpInbound(){
        $clientsInfo = $this->client->allClientInformation();
        $mailListNew = array();

        foreach ($clientsInfo as $index => $client) {
            if (!empty($client)) {
                $inboundEdi = $client->edi['inbound'];
                if(isset($inboundEdi[0])){
                    $edi = $inboundEdi[0];
                    $listFileClient = $this->getUploadInboundClientEdi($client, $edi);
                    if(isset($listFileClient['new'])){
                        $mailListNew [] = [
                            'message' =>  'Client - '. $client->name .' has uploaded files successfully!',
                            'client' => $client->name,
                            'status_code' => 200,
                            'data' => $listFileClient['new'],
                            'ftp_noti' => ERROR_HANDLING_EDI_FTP
                        ];
                    }
                }
            } else {
                logger('API END POINT NOT FOUND');
                break;
            }
        }
        /*if(isset($edi)){
            $this->emailHandling->setConfig($edi, $client);
            if(count($mailListNew) > 0){
                $this->emailHandling->sendEmail($mailListNew,[],'','inbound',false,self::MAIL_TEMPLATE);
            }
        }*/
    }

    /**
     * Re-check old files every 30 minutes
     */
    public function reCheckFtpInbound(){
        $clientsInfo = $this->client->allClientInformation();

        $mailListOld = array();
        foreach ($clientsInfo as $index => $client) {
            if (!empty($client)) {
                $inboundEdi = $client->edi['inbound'];
                if(isset($inboundEdi[0])){
                    $edi = $inboundEdi[0];
                    $listFileClient = $this->getUploadInboundClientEdiReNoti($client, $edi);

                    if(isset($listFileClient['old'])){
                        $mailListOld    = [];
                        $mailListOld [] = [
                            'message' =>  'Files still have been not resolved. Client - '. $client->name . '!',
                            'client' => $client->name,
                            'status_code' => 200,
                            'data' => $listFileClient['old'],
                            'ftp_noti' => ERROR_HANDLING_EDI_FTP_RESEND
                        ];

                        $this->emailHandling->setConfig($edi, $client);
                        $this->emailHandling->sendEmail($mailListOld,[],'','inbound',false,self::MAIL_TEMPLATE);
                    }
                }
            } else {
                logger('API END POINT NOT FOUND');
                break;
            }
        }
//        if(isset($edi)){
//            $this->emailHandling->setConfig($edi, $client);
//            if(count($mailListOld) > 0){
//                $this->emailHandling->sendEmail($mailListOld,[],'','inbound',false,self::MAIL_TEMPLATE);
//            }
//        }
    }
    /**
     * @param $edi
     *
     * @return array
     */
    public function readFromFtpClient($edi)
    {
        return $this->parseFileList($edi);
    }

    /**
     * @param $edi
     *
     * @return array
     */
    public function parseFileList($edi)
    {
        $files = $edi->ftp->readFiles($edi->path);
        if (!is_array($files)) {
            return false;
        }else{
            return $files;
        }
    }

    /**
     * @param $client
     * @param $edi
     * @return array
     */
    public function getUploadInboundClientEdi($client, $edi)
    {
        $edi->ftp = $this->getStorageMethod($edi->ftpAccount, $client);
        $fileLists = $this->readFromFtpClient($edi);
        $listNotifications = $this->notiFtp->getNotificationIB(['client_id' =>  $client->id]);
        $uploadedList = array();
        $uploadedListWhen = array();
        foreach ($listNotifications as $noti){
            $uploadedList [] = $noti['file_name'];
            $uploadedListWhen [$noti['file_name']] =  $noti['created_at'];
        }
        $data = array();
        if(! empty($fileLists)){
            $fileNewNoti = [];
            $fileFtpArray = array();
            foreach($fileLists as $file){

                $fileFtpArray[] = $file;
                if(in_array($file, $uploadedList)){
                    $this->notiFtp->updateNotificationIB(['file_name' => $file],['status'=>1, 'created_at' => $uploadedListWhen [$file]]);
                }else{
                    $this->notiFtp->createNewNotificationIB(['client_id' => $client->id,'client_name' => $client->name, 'file_name' => $file, 'status'=> 0]);
                    $fileNewNoti[] = $file;
                }
            }
            //Check data send mail
            if($fileNewNoti!= null){
                $data['new'] = $fileNewNoti;
            }
        }else{
            $fileFtpArray = array();
        }
        //Check file resolved

        foreach($uploadedList as $file){
            if(!in_array($file, $fileFtpArray)){
                $this->notiFtp->deleteNotificationIB(['file_name' => $file,'client_id' => $client->id]);
            }
        }
        return $data;
    }

    /**
     * @param $client
     * @param $edi
     * @return array
     */
    public function getUploadInboundClientEdiReNoti($client, $edi)
    {
        $edi->ftp = $this->getStorageMethod($edi->ftpAccount, $client);
        $fileLists = $this->readFromFtpClient($edi);
        $listNotifications = $this->notiFtp->getNotificationIB(['client_id' =>  $client->id, 'status' => 1]);
        $uploadedList = array();
        $uploadedListWhen = array();
        foreach ($listNotifications as $noti){
            $uploadedList [] = $noti['file_name'];
            $uploadedListWhen [$noti['file_name']] =  $noti['created_at'];
        }
        $data = array();
        if(! empty($fileLists)){
            $fileOldNoti = [];
            $fileFtpArray = array();
            foreach($fileLists as $file){

                $fileFtpArray[] = $file;
                if(in_array($file, $uploadedList)){
                    $fileOldNoti[] = $file;
                }
            }
            //Check data send mail

            if($fileOldNoti!= null){
                $data['old'] = $fileOldNoti;
            }
        }
        //Check file resolved

      /*  foreach($uploadedList as $file){
            if(!in_array($file, $fileFtpArray)){
                $this->notiFtp->deleteNotificationIB(['file_name' => $file,'client_id' => $client->id]);
            }
        }*/
        return $data;
    }

    /**
     * @param FtpAccounts $ftpAccount
     *
     * @param Clients     $client
     *
     * @return FtpStorage|SftpStorage
     */
    private function getStorageMethod(FtpAccounts $ftpAccount, Clients $client = null)
    {
        $ftpAccountData = $ftpAccount->toArray();

        if ($ftpAccountData['protocol'] == 1) {
            return new SftpStorage($ftpAccount, $client);
        }

        return new FtpStorage($ftpAccountData);
    }
}
