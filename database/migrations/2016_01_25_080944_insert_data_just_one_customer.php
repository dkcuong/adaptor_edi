<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataJustOneCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $client = DB::table('clients')->insertGetId([
            'name' => 'Just One',
            'short_name' => 'JOI',
            'description' => 'Just One Customer',
            'type_id' => 2,
        ]);

        $address = DB::table('addresses')->insertGetId([
            'street' => '2585 Skymark Ave.',
            'department' => 'Unit # 306',
        ]);

        $location = DB::table('locations')->insertGetId([
            'city_name' => 'Mississauga',
            'state_province_code' => 'ON',
            'postal_code' => 'L4W 4L5',
            'country_code' => 'CA',
            'location_qualifier' => '',
            'location_identifier' => '',
            'state_province_name' => 'Ontario',
        ]);

        DB::table('client_addresses')->insert([
            'address_id' => $address,
            'location_id' => $location,
            'client_id' => $client,
            'type_id' => 3,
        ]);

        DB::table('contacts')->insert([
            [
                'name' => 'Peter Rodrigues',
                'title' => 'Amt Software Division',
                'phone' => '917-763-0643',
                'extension' => '',
                'email' => 'Peter@orioninc.com',
                'note' => 'Software Partner of Concept One. Email: Pscir@aol.com',
                'type_id' => 2,
                'client_id' => $client,
            ],
            [
                'name' => 'Mark Finkelstein',
                'title' => 'Vice President Sales',
                'phone' => '416-258-9415',
                'extension' => '',
                'email' => 'markf@concept1canada.com',
                'note' => '',
                'type_id' =>1,
                'client_id' => $client,
            ],
        ]);

        $inboundFtpAcc = DB::table('ftp_accounts')->insertGetId([
            'host' => '210.245.33.77',
            'username' => 'just_one',
            'password' => 'justone2016',
            'port' => '21',
            'transfer_mode' => 'passive',
            'encryption' => '',
            'type_id' => 1,
            'client_id' => $client,
        ]);

        DB::table('ftp_accounts')->insert([
            'host' => '210.245.33.77',
            'username' => 'just_one',
            'password' => 'justone2016',
            'port' => '21',
            'transfer_mode' => 'passive',
            'encryption' => '',
            'type_id' => 2,
            'client_id' => $client,
        ]);

        $dataFormat = DB::table('data_formats')->insertGetId([
            'name' => 'TXT',
            'extension' => '.txt',
            'description' => 'Text file',
            'status' => 1,
            'component_separator' => '',
            'element_separator' => ',',
            'segment_terminator' => '',
            'data_type_id' => 2
        ]);

        DB::table('client_data')->insert([
            'client_id' => $client,
            'data_format_id' => $dataFormat,
        ]);

        $interChanges = DB::table('x12_interchanges')->insertGetId([
            'auth_qualifier' => '00',
            'auth_info' => '',
            'security_qualifier' => '00',
            'security_info' => '',
            'sender_qualifier' => 'ZZ',
            'sender_code' => 'JustOne',
            'receiver_qualifier' => 'ZZ',
            'receiver_code' => 'Seldat',
            'date' => date('Ymd'),
            'time' => date('Hi'),
            'control_standard' => 'U',
            'control_version' => '00401',
            'control_number' => '1',
            'requested' => '0',
            'usage' => 'T',
            'separator' => '|',
            'status' => 1,
            'client_id' => $client,
        ]);

        $inboundGroup = DB::table('x12_inbound_groups')->insertGetId([
            'sender_code' => 'JustOne',
            'group_code' => 'QG',
            'receiver_code' => 'Seldat',
            'date' => date('Ymd'),
            'time' => date('Hi'),
            'control_number' => '1',
            'control_standard' => 'X',
            'control_version' => '04010',
            'status' => 1,
            'inbound_interchange_id' => $interChanges,
        ]);

        $inbound = DB::table('inbounds')->insertGetId([
            'name' => '888',
            'description' => 'Insert Product Master Data',
            'client_id' => $client,
            'ftp_account_id' => $inboundFtpAcc,
            'path' => 'inbounds/888',
            'filename_prefix' => 'PM',
            'group_id' => $inboundGroup,
            'status' => 1,
            'transactional_code' => 'T888',
        ]);

        $mapping = [
            'sku' => 'Style',
            'size' => 'Sizes',
            'color' => 'Colors',
            'pack_size' => 'Pack',
            'weight' => 'Weight',
            'height' => 'Dimensions',
            'width' => 'Dimensions',
            'length' => 'Dimensions',
            'cube' => 'Cubic Ft',
            'description' => 'Description',
            'product_type' => 'Category',
            'upc' => '',
        ];

        DB::table('inbound_mapping')->insert([
            'fields' => json_encode($mapping),
            'inbound_id' => $inbound,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
