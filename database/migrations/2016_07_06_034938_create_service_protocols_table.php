<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceProtocolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('service_protocols'))
        {
            Schema::create('service_protocols', function (Blueprint $table) {
                $table->increments('id');
                $table->string('short_name', 10);
                $table->string('name', 50)->nullable();
                $table->longText('description')->nullable();
            });


            $ftp = DB::table('service_protocols')->insertGetId([
                'short_name' => 'VPN/FTP',
                'name' => 'VPN/FTP',
                'description' => 'VPN/FTP',
            ]);

            DB::table('service_protocols')->insert([
                'short_name' => 'FTPS',
                'name' => 'FTPS',
                'description' => 'FTPS',
            ]);

            DB::table('service_protocols')->insert([
                'short_name' => 'SFTP',
                'name' => 'SFTP',
                'description' => 'SFTP',
            ]);

            DB::table('service_protocols')->insert([
                'short_name' => 'AS2',
                'name' => 'AS2',
                'description' => 'AS2',
            ]);

            DB::table('service_protocols')->insert([
                'short_name' => 'AS3',
                'name' => 'AS3',
                'description' => 'AS3',
            ]);

            DB::table('service_protocols')->insert([
                'short_name' => 'HTTPS',
                'name' => 'HTTPS',
                'description' => 'HTTPS',
            ]);

            Schema::table('data_formats', function($table) use ($ftp)
            {
                $table->integer('protocol_id')->after('data_type_id')->default($ftp);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
