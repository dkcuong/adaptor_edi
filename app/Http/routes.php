<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/ftp', function (Illuminate\Http\Request $request) {
    $input = $request->all();
    $type = $request->input('type', true);

    $ftpService = \Illuminate\Support\Facades\Storage::createFtpDriver([
        'host' => $input['host'],
        'username' => $input['username'],
        'password' => $input['password'],
        'passive' => (bool)$type,
    ]);

    $dirs = $ftpService->allDirectories();
    foreach ($dirs as $dir) {
        $files[] = $ftpService->allFiles($dir);
    }

    return $files;
});

Route::group(
    [
        'namespace' => 'App\Modules\Edi\Api\V1\Controllers',
        'prefix' => '/edi/api/v1',
    ],
    function () {
        Route::post('/authenticate', '\App\Http\Controllers\Auth\JWTController@authenticate');
        Route::get('', 'EdiController@index');
        Route::get('parse', 'EdiController@parseHelp');
        Route::post('parse', 'EdiController@parse');
        Route::post('send_mail', 'SendMailController@sendMail');
        Route::post('outbound/update_output', 'OutboundController@updateOutput')
            ->middleware(['jwt.auth', 'jwt.refresh']);
        Route::post('dms-integration', 'JWTController@dmsIntegration');
        Route::get('storage/{filePath}/{fileName}', 'OutboundController@getFileStorage');
        Route::post('/outbound-tms', 'TMSOutboundController@updateOutput');
    }
);


//For testing only
if(env('APP_ENV') == 'develop') {
    Route::group( [
        'namespace' => 'App\Http\Controllers',
        'prefix' => '/apps',
    ],
        function () {
            Route::get('inbound', 'CommandController@inbound');
            Route::get('outbound', 'CommandController@outbound');
            Route::get('re-inbound', 'CommandController@reInbound');
        });

}
