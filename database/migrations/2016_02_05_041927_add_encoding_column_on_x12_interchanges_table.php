<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEncodingColumnOnX12InterchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('x12_interchanges', function (Blueprint $table) {
            $table->string('encoding', 20);
        });
        
        DB::statement('ALTER TABLE x12_interchanges MODIFY COLUMN `separator` VARCHAR(10)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('x12_interchanges', function (Blueprint $table) {
            //
        });
    }
}
