<?php
/**
 * Created by PhpStorm.
 * User: viet.le
 * Date: 8/25/2017
 * Time: 9:29 AM
 */
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NotiFtpUpload
 * @package App
 */
class NotiFtpUpload extends Model
{
    /**
     * @var string
     */
    protected $table = 'noti_ftp_uploads';
    /**
     * @var array
     */
    protected $fillable =
        [
            'client_id',
            'file_name',
            'client_name',
            'status'
        ];

    /**
     * @param $data
     * @return NotiFtpUpload|null
     */
    public function createNewNotificationIB($data = array())
    {
        $noti = null;
        // Start transaction!
        if(is_array($data) && count($data) > 0){
            DB::beginTransaction();
            $noti = $this->insertNotificationIB($data);
            DB::commit();
        }
        return $noti;
    }


    /**
     * @param $data
     * @return NotiFtpUpload
     */
    public function insertNotificationIB($data)
    {
        $newNotification = new NotiFtpUpload();
        $newNotification::updateOrCreate($data);
        return $newNotification;
    }

    /**
     * @param $data
     * @param $dataUpdate
     * @return NotiFtpUpload
     */
    public function updateNotificationIB($data, $dataUpdate){

        $updateNotification = new NotiFtpUpload();
        $updateNotification::where($data)->update($dataUpdate);
        return $updateNotification;
    }

    /**
     * @param $data
     * @return NotiFtpUpload
     */
    public function deleteNotificationIB($data){

        $deleteNotification = new NotiFtpUpload();
        $deleteNotification::where($data)->update(['status' => 1,'delete' => 1]);
        return $deleteNotification->id;
    }

    /**
     * @param array $where
     * @return mixed
     */
    public function getNotificationIB($where = [] ){
        $getNotification = new NotiFtpUpload();
        if(!is_array($where)){
            $where = ['status' => 0];
        }
        $listNotification = $getNotification::where($where)
            ->where(['delete' => 0])
            ->get();
        $result = $listNotification->toArray();
        return $result;
    }

    /**
     * @return mixed
     */
    public function getAllNotificationIB(){
        $getNotification = new NotiFtpUpload();
        $listNotification = $getNotification::get();
        $result = $listNotification->toArray();
        return $result;
    }
}
