<?php

namespace App\Mappers\Inbound;

use App\Mappers\MapperBase;

class MAKEITREALMapper extends MapperBase
{
    private $meta_fields = [
        'vendor' => 'Vendor #',
        'ship_to_address_2'    => 'Ship To Address 2',
        'inventory_site'    => 'Inventory Site',
        'routing_guide'     => 'Routing Guide',
        'ship_service'      => 'Ship Service',
        'shipping_account'  => 'Shipping Account #',
        'billing_type'      => 'Billing Type',
        'carrier_account_zip_code' => 'Carrier Account Zip Code'

    ];
    /**
     * @return $this
     */
    protected function process()
    {
        $data = $this->getData();
        if($this->getEdi()->name == '888' && isset($data[0])){
            array_shift($data);
        }

        $result = array_map([$this, 'map'], $data);

        $this->setResult($result);

        return $this;
    }

    protected function map940($item, $conditions){

        $result = [];

        foreach ($conditions as $target => $from) {
            $result[$target] = isset($item[$from]) ? $item[$from] : '';
        }

        $meta_fields = $this->meta_fields;
        foreach ($meta_fields as $target => $from) {
            $result['order_meta'][$target] = isset($item[$from]) ? $item[$from] : '';
        }

        //for AWMS only
        $result['quantity'] = $result['quantity'] * $result['case_pack'];

        return $result;
    }
}
