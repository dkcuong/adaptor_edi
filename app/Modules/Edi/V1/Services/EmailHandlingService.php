<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 21/04/2016
 * Time: 13:59
 */

namespace App\Modules\Edi\V1\Services;


use App\Setting;
use Illuminate\Support\Facades\Mail;

class EmailHandlingService
{
    /**
     * @var
     */
    public $edi;

    /**
     * @var
     */
    public $client;

    public $errorHandling = 'EDI';

    static $TLI_CA_Email_Flag = false;

    static $TLI_CA_EMAIL = 'tradelines@seldatinc.com';

    static $TLI_CA_VENDOR_ID = '11281';
    public $packingFile = '';

    /**
     * @param $edi
     * @param $client
     */
    public function setConfig($edi, $client)
    {
        $this->edi = $edi;
        $this->client = $client;
    }
    /**
     */
    public function setHandling($errorHandling = 'EDI')
    {
        $this->$errorHandling = $errorHandling;
    }

    /**
     * @param $status
     * @return null
     */
    public function getStatus($status)
    {
        $handlingException = config('setting.outbound_schedule.monthly');
        $errorHandle = array_flip($handlingException);
        return isset($errorHandle[$status]) ? $errorHandle[$status] : null;
    }

    /**
     * @param $response
     * @return null
     */
    public function getStatusErrorHandling($response)
    {
        if (! isset($response['status_code'])) {
            $response = array_shift($response);
        }
        $isSuccess = isset($response['status_code']) ? $response['status_code'] : null;
        if(! $isSuccess) {
            $errorHandling = ERROR_HANDLING_EDI;
        } else {

            $errorHandling = $isSuccess == '200' ?
                ERROR_HANDLING_CUSTOMER : ERROR_HANDLING_DATA;
        }

        return $errorHandling;
    }

    /**
     * @param $data
     * @param $inserts
     * @param $type
     * @return array
     */
    public function getEmailNotification($data, $inserts, $type)
    {
        $exceptionHandle = true;

        $conNum = isset($inserts['control_number']) ? sprintf('%09d', $inserts['control_number']) : '';
        if ($conNum) {
            $conNum = ' - #'. $conNum;
        }
        $countError = '';
        if (isset($inserts['error_number']) && isset($inserts['total_number']) && $inserts['error_number'] > 0) {
            $countError = sprintf(' (Error Rate: %d/%d)', $inserts['error_number'], $inserts['total_number']);
        }
        $clientEmails = $dataEmails = [];
        $title = env('TITLE_EMAIL_NOTIFICATION', '[SEG ');

        if ($this->errorHandling != ERROR_HANDLING_EDI && $this->errorHandling != ERROR_HANDLING_EDI_FTP) {
            //$clientEmails = $this->getClientEmails($this->client->contact);

            $dataEmails = !(empty($this->edi->emails)) ? explode(',', $this->edi->emails) : [];
            if (self::$TLI_CA_Email_Flag && env('APP_ENV') == 'production') {
                $dataEmails[] = self::$TLI_CA_EMAIL;
            }
        }

        $emails = $this->getNotificationEmail($type) ?
            $this->getNotificationEmail($type) :
            config('setting.emailHandleException');

        switch ($this->errorHandling) {
            case ERROR_HANDLING_CUSTOMER:
                $title .= ' '. $this->edi->name . '] Notification For ' . $this->client->name . $conNum . ' ' . $countError;
                $emails = array_merge($dataEmails, $emails);
                break;
            case ERROR_HANDLING_DATA:
                $title .= ' '. $this->edi->name . '] Notification For ' . $this->client->name . $conNum . ' ' . $countError;
                $emails = array_merge($dataEmails, $emails);
                break;
            case ERROR_HANDLING_EDI:
                $exceptionHandle = false;
                $title .= '] Error Handling Exceptions';
                $data  = [
                    [
                        'message' => array_shift($data),
                        'data' => '',
                        'status_code' => 422
                    ]
                ];
                break;
            case ERROR_HANDLING_EDI_FTP:
                $exceptionHandle = false;
                $title .= 'NOTIFICATION' . '] New update files inbound ';
                $emails = array_merge($dataEmails, $emails);
                break;
            case ERROR_HANDLING_EDI_FTP_RESEND:
                $exceptionHandle = false;
                $title .= 'NOTIFICATION' . '] File(s) not resolved inbound ';
                $clientEmails = $this->getClientEmails($this->client->contact);
                $emails = array_merge($clientEmails, $emails);
                break;
            default:
                $emails = [];
                break;
        }
        return [
            'emails' => $emails,
            'title' => $title,
            'data' => $data,
            'exceptionHandle' => $exceptionHandle
        ];
    }

    /**
     * @param $data
     * @param $inserts
     * @param string $file
     * @param string $type
     * @param bool $attachment
     * @return bool|mixed
     */
    public function sendEmail($data, $inserts = [], $file = '', $type = 'inbound', $attachment = false, $name='')
    {
        //Get list email in inbound table first, otherwise get in contact
        if(!isset($data[0]['ftp_noti'])){
            $this->errorHandling = $this->getStatusErrorHandling($data);
        }else{
            $this->errorHandling = $data[0]['ftp_noti'];
        }
        $response = $this->getEmailNotification($data, $inserts, $type);

        $hasAttachment = ($type == 'inbound' && $file) ? true : (
        $response['exceptionHandle'] ? $this->hasAttachment() : null
        );

        $emails = $response['emails'];

        if (empty($emails)) {
            return false;
        }
        $type = empty($inserts['re_process_flag']) ? $type : $inserts['re_process_flag'];
        $this->sendMailAction(
            $emails,
            $response['data'],
            $response['title'],
            $file,
            $attachment,
            $hasAttachment,
            $type,
            $name
        );

        return $this->errorHandling;
    }

    /**
     * @return bool
     */
    protected function hasAttachment()
    {
        return $this->edi->sent_type_id == OUTBOUND_SEND_MAIL_TYPE;
    }

    /**
     * @param $emails
     * @param $data
     * @param $message
     * @param $file
     * @param $attachment
     * @param $hasAttachment
     * @param string $type
     * @return mixed
     */
    public function sendMailAction($emails, $data, $message, $file, $attachment, $hasAttachment, $type = 'inbound',
       $name)
    {
        $template = ($name && file_exists(resource_path('views/emails/' . $name . '.blade.php'))) ?
            'emails.' . $name :
            'emails.reminder';

        if(!empty($data['error'])){
            $template = 'emails.reminder';
        }
        Mail::send(
            $template,
            [
                'data' => $data,
                'file' => $file,
                're_process_title' => ($type == 're-process') ? 'Re-Process ' : ''
            ],
            function ($mailer) use ($type, $emails, $message, $attachment, $hasAttachment) {

                $this->sendTo($mailer, $emails, $message, $attachment, $hasAttachment);
            }
        );
    }

    /**
     * @param $mailer
     * @param $mails
     * @param $message
     * @param $attachment
     * @param bool $hasAttachment
     */
    protected function sendTo($mailer, $mails, $message, $attachment = '', $hasAttachment = false)
    {
        if ($attachment && $hasAttachment) {
            if(is_array($attachment)){
                foreach($attachment as $item){
                    $mailer->attach($item);
                }
            }else{
                $mailer->attach($attachment);
            }
        }
        if ($this->packingFile && $hasAttachment) {
            $mailer->attach(storage_path('app') . DIRECTORY_SEPARATOR . $this->packingFile);
        }
        foreach ($mails as $mail) {
            $toEmail = is_array($mail) ? $mail['email'] : $mail;
            $mailer->to(trim($toEmail))->subject($message);
        }
    }

    /**
     * @param $type
     * @return array
     */
    protected function getNotificationEmail($type)
    {
        $emails= (new Setting())->email_notification;

        if (empty($emails->{$type.'_emails'})) {
            return [];
        }

        $result = explode(',', $emails->{$type.'_emails'});

        return $result;
    }

    /**
     * @param $message
     * @return bool
     */
    public function sendMailError($message)
    {
        $messageHandle = [
            'Message: ' . $message,
            'File:' . __FILE__,
            'Class:' . __CLASS__,
            'Function:' . __FUNCTION__,
            'Line Number:' . __LINE__
        ];

        logger($messageHandle);
        return $this->sendEmail([$messageHandle]);
    }

    private function getClientEmails($contacts)
    {
        $clientEmails = [];
        if(empty($contacts)) {
            return $clientEmails;
        }

        foreach ($contacts as $contact) {
            $contactEmails = explode(',',$contact->email);
            foreach ($contactEmails as $email){
                $clientEmails[] = trim($email);
            }
        }
        return $clientEmails;
    }
}
