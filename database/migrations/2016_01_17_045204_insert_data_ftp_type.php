<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertDataFtpType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('ftp_types')->insert([
            [
                'id' => 1,
                'name' => 'Inbound',
                'short_name' => 'IN',
                'description' => 'Inbound FTP Type',
            ],
            [
                'id' => 2,
                'name' => 'Outbound',
                'short_name' => 'OUT',
                'description' => 'Outbound FTP Type',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
