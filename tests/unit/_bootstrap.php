<?php
// Here you can initialize variables that will be available to your tests
use \Illuminate\Support\Facades\Artisan;
use \Illuminate\Support\Facades\App;

if (App::environment('testing')) {
    Artisan::call('migrate');
}

