<?php

/**
 * Created by PhpStorm.
 * User: rober
 * Date: 22/02/2016
 * Time: 02:52
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class X12InboundTransactions extends Model
{
    const NEW_STATUS_WMS = 'New';
    /**
     * @var string
     */
    protected $table = 'x12_inbound_transactions';

    /**
     * @var array
     */
    protected $fillable = [
        'transaction_code',
        'control_number',
        'group_id',
        'group_control_number',
        'data',
        'status',
        'message',
        'success_number'
    ];
}
