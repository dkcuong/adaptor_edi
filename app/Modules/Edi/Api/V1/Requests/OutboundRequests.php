<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 24/10/2016
 * Time: 10:03
 */

namespace app\Modules\Edi\Api\V1\Requests;


use App\Http\Requests\Request;

class OutboundRequests extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cus_id' => 'required',
            'whs_id' => 'required',
            'api_app' => 'required',
            'transactional_code' => 'required',
            'data' => 'required|array',
        ];
    }
}