<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class X12InboundGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( '
			CREATE TABLE IF NOT EXISTS `x12_inbound_groups` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `group_code` char(2) COLLATE utf8_unicode_ci NOT NULL,
			  `sender_code` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `receiver_code` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `date` bigint(8) DEFAULT NULL,
			  `time` bigint(8) DEFAULT NULL,
			  `control_number` bigint(9) DEFAULT NULL,
			  `control_standard` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT \'X\',
			  `control_version` smallint(5) NOT NULL DEFAULT \'4010\',
			  `status` bit(1) NOT NULL DEFAULT b\'0\',
			  `inbound_interchange_id` bigint(20) NOT NULL,
			  PRIMARY KEY (`id`),
			  KEY `FK_x12_inbound_groups_0` (`inbound_interchange_id`),
			  CONSTRAINT `FK_x12_inbound_groups_0` FOREIGN KEY (`inbound_interchange_id`) REFERENCES `x12_interchanges` (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
