<?php

/**
 * Created by PhpStorm.
 * User: rober
 * Date: 18/02/2016
 * Time: 17:38
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class X12OutboundGroupRevisions extends Model
{
    /**
     * @var string
     */
    protected $table = 'x12_outbound_group_revisions';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'group_code',
        'sender_code',
        'receiver_code',
        'date',
        'time',
        'control_number',
        'control_standard',
        'control_version',
        'status',
        'interchange_id',
        'group_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function outboundTransaction()
    {
        return $this->hasMany(X12OutboundTransactions::class, 'group_id');
    }
}
