<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiEndPoint extends Model
{
    public $table = 'client_api_endpoints';
}
