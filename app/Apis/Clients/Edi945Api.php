<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 24/02/2016
 * Time: 10:20
 */

namespace App\Apis\Clients;


use App\Apis\ApiBase;
use App\Apis\ApiInterface;

class Edi945Api extends ApiBase implements ApiInterface
{
    /**
     * @var string
     */
    protected $api = 'items/v1/945';

    /**
     * Call Api items/v1/945
     * With data from client
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function callApi()
    {
        $data = $this->get(['data' => $this->getConfig('data')]);

        return $data;

        // TODO: Implement callApi() method.
    }
}
