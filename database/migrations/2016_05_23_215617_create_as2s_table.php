<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAs2sTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('as2s')) {
            Schema::create('as2s', function (Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('client_id', false, true);
                $table->string('outbound_url');
                $table->text('outbound_email_success');
                $table->text('outbound_email_fault');
                $table->string('inbound_url');
                $table->string('inbound_path');
                $table->text('inbound_email_success');
                $table->text('inbound_email_fault');
                $table->boolean('status');
                $table->boolean('sign_status');
                $table->boolean('encrypt_status');
                $table->boolean('receipt_status');
                $table->boolean('compress_status');
                $table->string('encrypt_algorithm');
                $table->text('cert_key');
                $table->text('public_key');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('as2s')) {
            Schema::drop('as2s');
        }
    }
}
