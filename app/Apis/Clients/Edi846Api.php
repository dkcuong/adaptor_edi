<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 24/02/2016
 * Time: 10:20
 */

namespace App\Apis\Clients;


use App\Apis\ApiBase;
use App\Apis\ApiInterface;

class Edi846Api extends ApiBase implements ApiInterface
{
    /**
     * @var string
     */
    protected $api = 'items/v1/inventory-report';

    /**
     * Call Api items/v1/846
     * With data from client
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function callApi()
    {
        $data = $this->getConfig('data');
        $data['report_date'] = date('Ym');

        $response = $this->post($data);

        return $response;
    }
}
