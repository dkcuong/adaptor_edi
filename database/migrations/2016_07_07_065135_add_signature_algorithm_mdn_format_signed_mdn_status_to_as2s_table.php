<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSignatureAlgorithmMdnFormatSignedMdnStatusToAs2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('as2s', function (Blueprint $table) {
            $table->string('signature_algorithm',255)->after('encrypt_algorithm');
            $table->text('mdn_format')->after('signature_algorithm');
            $table->tinyInteger('signed_mdn_status')->after('compress_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('as2s', function (Blueprint $table) {
            $table->dropColumn('signature_algorithm');
            $table->dropColumn('mdn_format');
            $table->dropColumn('signed_mdn_status');
        });
    }
}
