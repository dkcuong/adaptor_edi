<?php

namespace App\Utils;

use App\X12InboundInterchange;
use App\X12Interchanges;
use GuzzleHttp\Client as Api;

class X12ParserAdapter
{
    /**
     * @var string
     */
    protected $api;

    /**
     * @var array
     */
    protected $result;

    /**
     * @var
     */
    private $apiEndPoint;

    /**
     * @param array $config
     * @return $this
     */
    public function parse(array $config)
    {
        $client = $config['client'];
        $this->apiEndPoint = $config['apiEndPoint'];

        $interchanges = [];

        $results = X12Interchanges::with('inboundGroup')->where('client_id', $client->id)->get();
        if (!count($results) < 1) {
            foreach ($results as &$result) {
                $maxControlNum = X12InboundInterchange::where('client_id', $client->id)->max('control_number');
                if ($maxControlNum && isset($result->control_number)) {
                    $result->control_number = $maxControlNum;
                }

                $interchanges[] = $result->getAttributes();
            }
        }

        $group = $config['inbound']->inboundGroup->toArray();
        $interchange = collect($interchanges)
            ->where('id', $group['interchange_id'])
            ->first();

        $input = [
            'content' => $config['content'],
            'interchange' => $interchange,
            'interchanges' => $interchanges,
            'groups' => [$group],      // because of Parser service is running the array of groups
            'client' => [
                'name' => $client->name,
                'short_name' => $client->short_name,
                'element_separator' => $config['element_separator'],
                'component_separator' => $config['component_separator'],
                'isValidate' => ($client->short_name == 'ZAPPOSKY') ? 1 : 0
            ],
            'app' => [
                'token' => $this->apiEndPoint['certificate_key'],
            ]
        ];


        unset($input['interchange']['location']);

        logger(json_encode($input), ['X12 Parser']);

        // Send API Parser
        $response = $this->getApi()->post('parse', [
            'json' => $input
        ]);

        $this->setResult($response);

        return $this;


    }

    /**
     * @return array
     */
    public function get()
    {
        return json_decode($this->result->getBody(), true);
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->get()['data'];
    }

    /**
     * @return Api|string
     */
    protected function getApi()
    {
        if (! $this->api) {
            $this->api = new Api([
                'base_uri' => $this->apiEndPoint['api_endpoint'],
            ]);
        }

        return $this->api;
    }

    /**
     * @param $data
     * @return $this
     */
    protected function setResult($data)
    {
        $this->result = $data;

        return $this;
    }

    /**
     * @return string
     */
    protected function getEndpoint()
    {
        return config('api.edi_parser_api');
    }

    /**
     * @param array $config
     * @return $this
     */
    public function create(array $config)
    {
        $client = $config['client'];

        $api = $this->apiEndPoint = $config['apiEndPoint'];
        $input = [
            'client' => [
                'name' => $client->name,
                'short_name' => $client->short_name,
                'element_separator' => $config['element_separator'],
                'component_separator' => $config['component_separator'],
            ],

            'data' => $config['data'],
            'app' => [
                'token' => $api['certificate_key'],
            ]
        ];

        logger(json_encode($input), ['X12 Parser']);

        $response = $this->getApi()->post('create', ['json' => $input]);

        $this->setResult($response);

        return $this;
    }
}
