<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 3/21/2017
 * Time: 9:57
 */

namespace App\Modules\Edi\V1\Services\ServiceHelpers;


use Firebase\JWT\JWT;
use GuzzleHttp\Client;

class DMSClientService
{
    private static $dataOutput = '';

    public $client;

    public function define($client, $edi, $data)
    {
        $attr = [
            'CLIENT NAME' => $client->name,
            'ISA NUMBER' => $client->rsa->control_number,
            'ISA SENDER CODE' => $client->rsa->sender_code,
            'ISA RECEIVER CODE' => $client->rsa->receiver_code,
            'GROUP CODE' => $edi->name,
            'EDI TYPE' => isset($edi->inboundMapping) ? 'INBOUND' : 'OUTBOUND',
            'SUCCESS KEY' => $data['insertedTranID'],
            'STATUS' => $data['status'],
            'DATE CREATED' => date('Ymd')
        ];

        $lst_files[] = [
            'doc_type' => [
                'id' => $edi->api->id,
                'name' => $edi->api->name,
                'attrs' => $attr,
            ],
            'file_detail' => [
                'name' => getDefault($data['file_name']),
                'file_size' => $data['file_size'],
                'doc_num' => $client->short_name. '_' . $edi->transactional_code . '_' . $data['insertedTranID'],
                'trans' => getDefault($data['tran_number']),
                'doc_date' => date('Y-m-d'),
                'url_download' => getDefault($data['urlDownload']),
            ],
        ];

        $dataOutput = [
            'status' => true,
            'results' => [
                'lst_files' => $lst_files,
            ],
        ];

        $jwt = self::generateJWT($dataOutput);

        self::$dataOutput = [
            'sys' => 'edi',
            'jwt' => $jwt['jwt']
        ];
    }


    public static function generateJWT($data = []) {

        $jwt = '';

        if (!empty($data)) {
            $tokenId = base64_encode(mcrypt_create_iv(32));
            $issuedAt = time();
            $notBefore = $issuedAt + 10;             //Adding 10 seconds
            $expire = $notBefore + 60;            // Adding 60 seconds
            $serverName = env('APP_DOMAIN'); // Retrieve the server name from config file

            /*
             * Create the token as an array
             */
            $dt = [
                'iat' => $issuedAt, // Issued at: time when the token was generated
                'jti' => $tokenId, // Json Token Id: an unique identifier for the token
                'iss' => $serverName, // Issuer
                'nbf' => $notBefore, // Not before
                'exp' => $expire, // Expire
                'data' => $data,
            ];


            /*
             * Encode the array to a JWT string.
             * Second parameter is the key to encode the token.
             *
             * The output string can be validated at http://jwt.io/
             */
            $jwt = JWT::encode(
                $dt, //Data to be encoded in the JWT
                env('DMS_KEY', 'edi-dms'), // The signing key
                'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
            );

            $unencodedArray = ['jwt' => $jwt];

            $jwt = $unencodedArray;
        }

        return $jwt;
    }

    public static function validJWT($jwt = '') {

        $rs = [];
        JWT::$leeway = time();
        /*
         * decode the jwt using the key from config
         */
        try {
            $token = JWT::decode($jwt, env('DMS_KEY', 'edi-dms'), array('HS512'));

            $rs = $token;
        } catch (\UnexpectedValueException $ex) {
            $rs = [];
        }

        return $rs;
    }

    public function makeFileDownload($fileName, $content)
    {
        $fileName = pathinfo($fileName)['basename'];
        if (\Storage::disk()->put($fileName, $content)) {
            return storage_path('app/' . $fileName);
        }
        return false;
    }

    public function putAPI()
    {
        logger(json_encode(self::$dataOutput), ['POST DMS API']);

        try {
            $client  = new Client([
                'base_uri' => env('DMS_URL', 'https://gateway.dms.staging.seldatdirect.com/api/v1/document/process/integration-system'),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'token'        => 'EKOMdsOzLBXBHgmi5Lf0Kuew6NqLJIBT'
                ],
                'exceptions' => false
            ]);

            $response = $client->post(
                env('DMS_URL', 'https://gateway.dms.staging.seldatdirect.com/api/v1/document/process/integration-system'),
                ['json' => self::$dataOutput]
            );
            logger(json_encode($response));
            return $response->getStatusCode();

        } catch (\Exception $e) {
            $result['status_code'] = $e->getCode();
            $result['message'] = $e->getMessage();
        }
    }

}