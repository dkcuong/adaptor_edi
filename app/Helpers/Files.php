<?php
if (!function_exists('make_temp_name')) {
    /**
     * @param $path
     *
     * @return mixed
     */
    function make_temp_name($path)
    {
        $fileName = pathinfo($path, PATHINFO_BASENAME);
        $date = date('Y_m_d_h_i_s');

        return preg_replace('/(\.[^.]+)$/', sprintf('%s$1', $date), $fileName);
    }
}

if (!function_exists('make_temp_dir')) {
    /**
     * @param        $path
     * @param string $addOn
     *
     * @return string
     */
    function make_temp_dir($path, $addOn = '')
    {
        $addOn = $addOn ? $addOn . '/' : '';
        $dir = pathinfo($path, PATHINFO_DIRNAME);

        return $addOn . $dir . '/' . make_temp_name($path);
    }
}

if (!function_exists('show_json')) {
    /**
     * Show beauty json output
     *
     * @param $data
     */
    function show_json($data)
    {
        echo '<pre>';
        echo json_encode($data, JSON_PRETTY_PRINT);
        echo '</pre>';
    }
}

if (!function_exists('show_array_message')) {

    /**
     * @param array $data
     */
    function show_array_message(array $data, $isPrint = true)
    {
        if($isPrint) {
            echo implode('<br />',$data);
        } else{
            return implode(PHP_EOL,$data);
        }
    }
}


if (!function_exists('status_message')) {
    function status_message($code, $messages)
    {
        if (!is_array($messages)) {
            $messages = [$messages];
        }
        $status = '';
        $message = '';
        switch ($code) {
            case 'CP':
                $status = 'Back order';
                $message = 'Lack of quantity';
                break;
            case 'CC':
                $message = 'Online Order has been Updated';
                if (! empty($messages) && is_array($messages)) {
                    $message = $messages[0];
                }
                $status = 'Success';
                break;
            case 'MC':
                $status = 'Error';
                if (is_array($messages)) {
                    $message = show_array_message($messages,false);
                } else {
                    $message = $messages;
                }
                break;
            case 'BO':
                $status = 'Back order';
                if (is_array($messages)) {
                    $message = show_array_message($messages,false);
                } else {
                    $message = $messages;
                }
                break;
            case 'ERR':
                $status = 'Error';
                if (is_array($messages)) {
                    $message = show_array_message($messages,false);
                } else {
                    $message = $messages;
                }
                break;
        }
        return [$status, $message];
    }
}

if (!function_exists('format_data_940')) {
    function format_data_940(array $data, $messages, $originalData)
    {
        $result = [
            'po' => '',
            'client_po' => '',
            'order_number' => '',
            'status_type' => '',
            'description' => ''
        ];
        $data['status_type'] = isset($data['status_type']) ? $data['status_type'] : '';
        try {
            if (is_array($messages)) {
                $message = array_shift($messages);
            } else {
                $message = $messages;
            }
            $status_message = status_message($data['status_type'], $messages);
            $result = [
                'po' => $data['customer_order_id'],
                'client_po' => $data['client_order_id'],
                'order_number' => ! empty($data['success_number']) ? $data['success_number'] : '',
                'status_type' => $data['status_type'],
                'status' => array_shift($status_message),
                'description' => $message,
                'message' => $message
            ];
            /*if (isset($data['customer_order_id'])) {
                $messageStatus = status_message($data['status_type'], $messages);
                $result['po'] = ! empty($data['customer_order_id']) ? $data['customer_order_id'] : '';
                $result['client_po'] = ! empty($data['client_order_id']) ? $data['client_order_id'] : '';
                $result['status_type'] = ! empty($data['status_type']) ? $data['status_type'] : 'MC';
                $result['status'] = ! empty($messageStatus[0]) ? $messageStatus[0] : '';
                $result['message'] = ! empty($messageStatus[1]) ? $messageStatus[1] : '';
            } else if () {
                    foreach ($messages as $message) {
                        $posPO = strpos($message,'PO#: ');
                        if ($posPO !== false) {
                            $result['po'] = substr($message, $posPO + 5, strpos($message, ',') - $posPO - 5);
                        }
                        $posPO = strpos($message,'PO NBR#: ');
                        if($posPO !== false ) {
                            $result['po'] = substr($message, $posPO + 9, strpos($message, ',') - $posPO - 9);
                        }
                        $postClient = strpos($message, 'client PO#: ');
                        if ($postClient !== false) {
                            $result['client_po'] = substr($message, $postClient + 12, strlen($message) - $postClient - 12);
                        }
                        $posOrder = strpos($message, 'WMS Order Number');
                        if ($posOrder !== false) {
                            $arr = explode(': ', $message);
                            $result['order_number'] = $arr[1];
                        }
                    }
                    $result['status_type'] = $data['status_type'];
                    $messageStatus = status_message($data['status_type'], $messages);
                    $result['status'] = ! empty($messageStatus[0]) ? $messageStatus[0] : '';
                    $result['message'] = ! empty($messageStatus[1]) ? $messageStatus[1] : '';
            } else {
                $result['message'] = $messages;
                $result['status'] = 'MC';
                $result['status_type'] = 'MC';
            }
            $result['description'] = (! empty($data['description']) &&
                is_array($data['description'])) ?
                $data['description'] : '';*/
            return $result;
        } catch (\Exception $ex) {
            logger($ex->getFile().'-'.$ex->getLine().'-'.$ex->getMessage(), ['format_data_940']);
        }
    }
}

if(! function_exists('color_format_940')) {
    function format_color_940($statusType)
    {
        $result = [
            'po'=>'black',
            'client_po'=>'black',
            'order_number' => 'black',
            'status'=>'black',
            'status_type'=> 'black',
            'message' => 'black',
            'description' => 'black'
        ];
        switch ($statusType){
            case 'CC':
                $result =  [
                    'po'=>'black',
                    'client_po'=>'black',
                    'order_number' => '#4cae4c',
                    'status'=>'black',
                    'status_type'=> 'black',
                    'message' => '#4cae4c',
                    'description' => 'black'
                ];
                break;
            case 'CP':
                $result =  [
                    'po'=>'#FF9900',
                    'client_po'=>'#FF9900',
                    'order_number' => '#FF9900',
                    'status'=>'#FF9900',
                    'status_type'=> '#FF9900',
                    'message' => 'red',
                    'description' => '#FF9900'
                ];
                break;
            case 'MC':
                $result = [
                    'po' => 'red',
                    'client_po'=>'red',
                    'order_number' => 'red',
                    'status'=>'red',
                    'status_type'=> 'red',
                    'message' => 'red',
                    'description' => 'red'
                ];
                break;
            case 'BO':
                $result = [
                    'po'=>'#FF9900',
                    'client_po'=>'#FF9900',
                    'order_number' => '#FF9900',
                    'status'=>'#FF9900',
                    'status_type'=> '#FF9900',
                    'message' => 'red',
                    'description' => '#FF9900'
                ];
                break;
        }
        return $result;
    }
}

if (!function_exists('format_data_204')) {
    function format_data_204(array $data, $messages, $originalData)
    {
        $result = [
            'job_ord_hdr_num' => '',
            'status_type' => '',
            'description' => ''
        ];
        $data['status_type'] = isset($data['status_type']) ? $data['status_type'] : '';
        try {
            if (is_array($messages)) {
                $message = array_shift($messages);
            } else {
                $message = $messages;
            }
            $status_message = status_message($data['status_type'], $messages);
            $result = [
                'job_ord_hdr_num' => ! empty($data['success_number']['204']) ? $data['success_number']['204'] : '',
                'status_type' => $data['status_type'],
                'status' => array_shift($status_message),
                'description' => $message,
                'message' => $message
            ];
            return $result;
        } catch (\Exception $ex) {
            logger($ex->getFile().'-'.$ex->getLine().'-'.$ex->getMessage(), ['format_data_204']);
        }
    }
}

if(! function_exists('color_format_204')) {
    function format_color_204($statusType)
    {
        $result = [
            'job_ord_hdr_num' => 'black',
            'status'=>'black',
            'status_type'=> 'black',
            'message' => 'black'
        ];
        switch ($statusType){
            case 'CC':
                $result =  [
                    'job_ord_hdr_num' => '#4cae4c',
                    'status'=>'black',
                    'status_type'=> 'black',
                    'message' => '#4cae4c'
                ];
                break;
            case 'CP':
                $result =  [
                    'job_ord_hdr_num' => '#FF9900',
                    'status'=>'#FF9900',
                    'status_type'=> '#FF9900',
                    'message' => 'red'
                ];
                break;
            case 'MC':
                $result = [
                    'job_ord_hdr_num' => 'red',
                    'status'=>'red',
                    'status_type'=> 'red',
                    'message' => 'red'
                ];
                break;
            case 'BO':
                $result = [
                    'job_ord_hdr_num' => '#FF9900',
                    'status'=>'#FF9900',
                    'status_type'=> '#FF9900',
                    'message' => 'red'
                ];
                break;
            case 'ERR':
                $result = [
                    'job_ord_hdr_num' => '#FF9900',
                    'status'=>'#FF9900',
                    'status_type'=> '#FF9900',
                    'message' => 'red'
                ];
                break;
        }
        return $result;
    }
}
