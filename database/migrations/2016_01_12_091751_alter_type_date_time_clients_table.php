<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTypeDateTimeClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_types', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('clients', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('type_id')->change();
            $table->boolean('status')->default(1)->change();
            $table->boolean('deleted')->default(0);
            $table->softDeletes();
        });
        
        Schema::table('clients', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('client_types');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            //
        });
    }
}
