<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeIdAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('address_types', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('addresses', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('client_addresses', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('client_data', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('contact_types', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('contacts', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('data_formats', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('ftp_accounts', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('ftp_types', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('inbound_mapping', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('inbounds', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('locations', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('outbound_mapping', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('outbounds', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('roles', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
        Schema::table('x12_inbound_group_revisions', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('address_types', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('addresses', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('client_addresses', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('client_data', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('contact_types', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('contacts', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('data_formats', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('ftp_accounts', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('ftp_types', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('inbound_mapping', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('inbounds', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('locations', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('outbound_mapping', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('outbounds', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('roles', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
        Schema::table('x12_inbound_group_revisions', function (Blueprint $table) {
            $table->increments('id')->change();
        });
        
    }
}
