<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateX12InboundTransactionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('x12_inbound_transaction_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('process_key', 50);
            $table->string('tran_id', 20)->nullable();
            $table->text('log_message')->nullable();
            $table->text('raw_data')->nullable();
            $table->text('header')->nullable();
            $table->text('payload')->nullable();
            $table->string('type', 20);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('x12_inbound_transaction_logs');
    }
}
