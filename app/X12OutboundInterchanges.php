<?php
/**
 * Created by PhpStorm.
 * User: rober
 * Date: 17/02/2016
 * Time: 17:13
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class X12OutboundInterchanges extends Model
{
    /**
     * @var string
     */
    protected $table = 'x12_outbound_interchanges';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'auth_qualifier',
        'auth_info',
        'security_qualifier',
        'security_info',
        'sender_qualifier',
        'sender_code',
        'receiver_qualifier',
        'receiver_code',
        'date',
        'time',
        'control_standard',
        'control_version',
        'control_number',
        'requested',
        'usage',
        'separator',
        'status',
        'client_id',
        'interchange_id',
        'repetition_separator'
    ];
    public $obFields = [
        'auth_qualifier',
        'auth_info',
        'security_qualifier',
        'security_info',
        'sender_qualifier',
        'sender_code',
        'receiver_qualifier',
        'receiver_code',
        'date',
        'time',
        'repetition_separator',
        'control_version',
        'control_number',
        'requested',
        'usage',
        'separator',
    ];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function outboundGroup()
    {
        return $this->hasMany(X12OutboundGroups::class, 'interchange_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function outboundGroupRevisions()
    {
        return $this->hasMany(X12OutboundGroupRevisions::class, 'interchange_id');
    }
}
