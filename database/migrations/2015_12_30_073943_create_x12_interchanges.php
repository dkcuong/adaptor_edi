<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateX12Interchanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement( '
			CREATE TABLE IF NOT EXISTS `x12_interchanges` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `auth_qualifier` set(\'00\',\'01\') COLLATE utf8_unicode_ci NOT NULL DEFAULT \'00\',
			  `auth_info` char(10) COLLATE utf8_unicode_ci NOT NULL,
			  `security_qualifier` set(\'00\',\'01\') COLLATE utf8_unicode_ci NOT NULL DEFAULT \'00\',
			  `security_info` char(10) COLLATE utf8_unicode_ci NOT NULL,
			  `sender_qualifier` char(2) COLLATE utf8_unicode_ci NOT NULL,
			  `sender_code` char(15) COLLATE utf8_unicode_ci NOT NULL,
			  `receiver_qualifier` char(2) COLLATE utf8_unicode_ci NOT NULL,
			  `receiver_code` char(15) COLLATE utf8_unicode_ci NOT NULL,
			  `date` mediumint(6) NOT NULL,
			  `time` smallint(4) NOT NULL,
			  `control_standard` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT \'X\',
			  `control_version` smallint(5) unsigned zerofill NOT NULL DEFAULT \'04010\',
			  `control_number` int(9) NOT NULL,
			  `requested` bit(1) NOT NULL DEFAULT b\'0\',
			  `usage` set(\'T\',\'P\') COLLATE utf8_unicode_ci NOT NULL DEFAULT \'T\',
			  `separator` char(1) COLLATE utf8_unicode_ci DEFAULT \'>\',
			  `status` bit(1) NOT NULL DEFAULT b\'0\',
			  `client_id` int(11) NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `UNI_CLIENT_INTERCHANGE_NUMBER` (`client_id`,`control_number`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
