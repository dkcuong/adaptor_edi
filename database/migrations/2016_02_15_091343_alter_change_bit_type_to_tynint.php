<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChangeBitTypeToTynint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `x12_interchanges` 
            CHANGE `requested` `requested` TINYINT(1) NOT NULL');
        DB::statement('ALTER TABLE `x12_interchanges` 
            CHANGE `status` `status` TINYINT(1) NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('x12_interchanges', function (Blueprint $table) {
            //
        });
    }
}
