<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterConfigDataformatEachTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inbounds', function (Blueprint $table) {
            $table->bigInteger('data_format_id')->after('group_id')->nullable();
        });
        Schema::table('outbounds', function (Blueprint $table) {
            $table->bigInteger('data_format_id')->after('group_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inbounds', function (Blueprint $table) {
            $table->dropColumn('data_format_id');
        });
        Schema::table('outbounds', function (Blueprint $table) {
            $table->dropColumn('data_format_id');
        });
    }
}
