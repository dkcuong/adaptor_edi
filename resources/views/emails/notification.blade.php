@include('emails.template')
<h3>Files still have been not resolved!</h3>

<table border="1" cellspacing="0" style="border-collapse: collapse">
    <thead>
    <tr>
        <th>Client</th>
        <th>Files</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($data as $key => $client)
        @foreach ($client['data'] as $index => $item)
        <tr>
            <th style="font-weight:bold; padding:0 15px">{{$client['client']}}</th>
            <th style="color: brown; font-weight:bold; padding:0 15px">{{$item}}</th>
        </tr>
        @endforeach
    @endforeach
    </tbody>
</table>