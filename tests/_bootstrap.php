<?php
$app = require 'bootstrap/app.php';

if (! empty($_SERVER['argv']) && in_array('--env=local', $_SERVER['argv'])) {
    $app->loadEnvironmentFrom('.env');
} else {
    $app->loadEnvironmentFrom('.env.test');
}

$app->instance('request', new \Illuminate\Http\Request);
$app->make('Illuminate\Contracts\Http\Kernel')->bootstrap();
