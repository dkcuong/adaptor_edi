<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataOnClientTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        DB::table('client_types')->insert(
            array(
                'name' => 'Consignee',
                'short_name' => 'CN',
                'description' => 'The consignee is the entity who is '
                . 'financially responsible (the buyer) for the receipt '
                . 'of a shipment'
            )
        );
        
        DB::table('client_types')->insert(
            array(
                'name' => 'Customer',
                'short_name' => 'CT',
                'description' => ''
            )
        );
        
        DB::table('client_types')->insert(
            array(
                'name' => 'Supplier',
                'short_name' => 'SP',
                'description' => ''
            )
        );
        
        DB::table('client_types')->insert(
            array(
                'name' => 'Manufacturer',
                'short_name' => 'MN',
                'description' => ''
            )
        );
        
        DB::table('client_types')->insert(
            array(
                'name' => 'Partner',
                'short_name' => 'PN',
                'description' => ''
            )
        );
        
        DB::table('client_types')->insert(
            array(
                'name' => 'Vendor',
                'short_name' => 'VN',
                'description' => ''
            )
        );
        
        DB::table('client_types')->insert(
            array(
                'name' => 'Warehouse',
                'short_name' => 'WH',
                'description' => ''
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
