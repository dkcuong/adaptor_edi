<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Packing List</title>
</head>
<body>
<div style="position: absolute; top: 50px">
    <img style="width: 140px; height: auto;" src="<?=config('client.make_it_real_logo')?>">
</div>
<h1 style="text-align: center;"><strong>Make It Real LLC</strong></h1>
<p style="text-align: center;"><?=$data['ship_from_address']??''?></p>
<h2 style="text-align: center;">Packing List</h2>
<table style="width: 100%;">
    <tbody>
        <tr>
            <td style="width: 50%;"><strong>Shipper:</strong>
                <p><?=$data['ship_from']??''?></p>
                <p><?=$data['ship_from_address']??''?></p>
            </td>
            <td style="width: 50%;">
                <p><strong>Consignee:</strong></p>
                <p><?=$data['ship_to']??''?></p>
                <p><?=$data['ship_to_address']??''?></p>
                <p>TEL: <?=$data['ship_to_phone']??''?></p>
            </td>
        </tr>
        <tr>
            <td style="width: 50%;">
                <p>Port of Loading: <?=$data['port_loading']??''?></p>
                <p>Port of Discharge: <?=$data['port_discharge']??''?></p>
                <p>Loading Date: <?=$data['loading_date']??''?></p>
                <p><strong><?=$data['freight_ship']??''?></strong></p>
            </td>
            <td style="width: 50%;">
                <p>Vessel # & Voyage: <?=$data['vessel_voyage']??''?></p>
                <p>Container #: <?=$data['container']??''?></p>
                <p>Seal #: <?=$data['seal']??''?></p>
                <p>ETD: <?=$data['etd']??''?></p>
                <p>ETA: <?=$data['eta']??''?></p>
            </td>
        </tr>
    </tbody>
</table>
<table style="width: 100%; text-align: center; margin-top: 15px"  border="1" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <th style="width:13%">Item#</th>
            <th style="width:15%">Batch#</th>
            <th style="width:15%">PCS#</th>
            <th style="width:10%">PACK SIZE#</th>
            <th style="width:12%">CTNS#</th>
            <th style="width:15%">CBM#</th>
            <th style="width:20%">G.W. - KGS#</th>
        </tr>
        <?php $total_cartons = $total_cbm = $total_gross_weight = 0; ?>
        @foreach($data['containers'] as $item)
            <?php
            $total_cartons += $item['cartons']??0;
            $total_cbm += $item['cbm']??0;
            $total_gross_weight += $item['gross_weight']??0;
            $pack_size  = $item['pack_size']??0;
            $cartons    = $item['cartons']??0;
            $quantity   = $pack_size * $cartons;
            ?>
        <tr>
            <td><?=$item['sku']??''?></td>
            <td><?=$item['batch']??''?></td>
            <td><?=$quantity?></td>
            <td><?=$item['pack_size']??''?></td>
            <td><?=$item['cartons']??''?></td>
            <td><?=$item['cbm']??''?></td>
            <td><?=$item['gross_weight']??''?></td>
        </tr>
        @endforeach
        <tr>
            <td colspan="4">Total</td>
            <td><?=$total_cartons?></td>
            <td><?=$total_cbm?></td>
            <td><?=$total_gross_weight?></td>
        </tr>
    </tbody>
</table>
</body>
</html>